<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/testdate', function(){

	// $time = date("Y-m-d", strtotime("+1 week"));
	// print_r($time); die;
	// print_r(date('Y-m-d', strtotime($time .' -1 day')));
	


});
Route::get('restricted_page',function(){

	return view('errors.503');
});

Route::resource('job_monitoring','JobOrderMonitoringController');

Route::group(['middleware' => 'guest'], function () {
	Route::get('/', 'Auth\AuthController@getLogin');
	Route::get('login', 'Auth\AuthController@getLogin');
	Route::post('login', 'Auth\AuthController@postLogin');		
	Route::get('reset_password.php',['as'=>'reset_password', 'uses'=>'ResetPasswordController@index']);
	Route::post('reset_password/store.php',['as'=>'reset_password.store', 'uses'=>'ResetPasswordController@store']);
});


Route::get('auth/logout', ['as' => 'logout', 'uses' => 'Auth\AuthController@getLogout']);

Route::group(['middleware' => 'auth'], function () {
	Route::resource('dashboard','DashboardController');	
	Route::resource('sticky_notes','NotesController');
	Route::post('sticky_notes/delete',['as'=>'sticky_notes.delt','uses'=>'NotesController@delt']);
	Route::post('dashboard/calendar/filter',['as'=>'dashboard.calendar','uses'=>'DashboardController@calendarFilter']);

	//positions
	Route::group(['middleware' => 'maintenancepage'], function () {
		Route::resource('positions','PositionController');
		Route::post('getPositionDetails','PositionController@positionDetails');
		Route::post('position/updates',['as'=>'positions.updates','uses'=>'PositionController@positionUpdates']);
	});


	//departments
	Route::group(['middleware' => 'maintenancepage'], function () {
		Route::resource('departments','DepartmentController');
	});
	Route::post('getDepartmentDetails','DepartmentController@departmentDetails');
	Route::post('getSubDepartmentDetails','DepartmentController@subDepartmentDetails');
	Route::post('department/updates',['as'=>'departments.updates','uses'=>'DepartmentController@departmentUpdates']);

	//workgroups
	Route::resource('workgroups','WorkgroupController');
	Route::post('getWorkgroupDetails','WorkgroupController@WorkgroupDetails');
	Route::post('workgroups/updates',['as'=>'workgroups.updates','uses'=>'WorkgroupController@workgroupUpdates']);

	//users
	Route::group(['middleware' => 'accesspage'], function () {
		Route::resource('users','UserController');
		Route::get('users/{id}/activate',['as'=>'users.activate','uses'=>'UserController@activate']);
	});

	Route::post('users/workgroups/update',['as'=>'users.update_workgroup','uses'=>'UserController@updateWorkgroup']);
	Route::post('getUserDetails','UserController@getUserDetails');
	Route::get('user/{reset}/password.php',['as'=>'users.reset_password_user','uses'=>'UserController@user_reset_pass']);
	Route::post('user/reset_password/update.php',['as'=>'users.reset_password_update_user','uses'=>'UserController@reset_password_update_user']);

	//contact type
	Route::group(['middleware' => 'maintenancepage'], function () {
		Route::resource('contact_types','ContactTypeController');
		Route::post('getContactTypeDetails','ContactTypeController@typeDetails');
		Route::post('contact_types/updates',['as'=>'contact_types.updates','uses'=>'ContactTypeController@contactTypeUpdates']);
	});

	//user request
	Route::resource('user_request','UserRequestController');
	Route::post('user_request/filterUserDept','UserRequestController@filterUserDept');
	Route::post('user_request/UserCC','UserRequestController@userCC');

	//myRequest
	Route::resource('my_requests','MyRequestController');
	Route::post('my_requests/add_remarks/save',['as'=>'my_requests.add_remarks','uses'=>'MyRequestController@add_remarks']);
	Route::post('my_requests/remarks/download',['as'=>'my_requests.remarks_download_files','uses'=>'MyRequestController@downloadFile']);
	Route::post('my_requests/file/download',['as'=>'my_requests.download','uses'=>'MyRequestController@download']);
	Route::post('my_requests/getProjects','MyRequestController@getProjectDetailsApi');
	Route::post('my_requests/linking',['as'=>'my_requests.linking','uses'=>'MyRequestController@linking']);
	Route::post('my_requests/add_rating',['as'=>'my_requests.add_rating','uses'=>'MyRequestController@add_rating']);
	Route::post('my_requests/add_participant',['as'=>'my_request.add_member','uses'=>'MyRequestController@add_participant']);
	Route::post('my_requests/add_participant_cc',['as'=>'my_request.add_member_cc','uses'=>'MyRequestController@addCC']);
	Route::post('my_requests/remove_participant_cc',['as'=>'my_request.remove_member_cc','uses'=>'MyRequestController@removeCC']);

	//contact
	Route::resource('contacts','ContactController');
	Route::post('contacts/filter',['as'=>'contacts.filter','uses'=>'ContactController@filter']);
	Route::post('contacts/transfer',['as'=>'contacts.transfer','uses'=>'ContactController@transfer']);
	Route::get('contacts/{encrypt}/overview',['as'=>'contacts.overview','uses'=>'ContactController@overview']);
	Route::post('contacts/add_person',['as'=>'contacts.add_person','uses'=>'ContactController@add_person']);

	//contact approval
	Route::group(['middleware' => 'contactapprovalpage'], function () {
		Route::resource('contact_approvals','ContactApprovalController');
		Route::post('contact_approvals/update_status',['as'=>'contact_approvals.update_status','uses'=>'ContactApprovalController@update_status']);
		Route::put('contact_approvals/{encrypt}/head_overide',['as'=>'contact_approvals.head_overide','uses'=>'ContactController@head_overide']);
		Route::post('contact_approvals/filter',['as'=>'contact_approvals.filter','uses'=>'ContactApprovalController@filter']);
	});

	//project
	Route::resource('projects','ProjectController');
	Route::post('projects/transfer',['as'=>'projects.transfer','uses'=>'ProjectController@transfer']);
	Route::get('projects/{encrypt}/overview',['as'=>'projects.overview','uses'=>'ProjectController@overview']);
	Route::post('projects/getusers','ProjectController@getUsersApi');	
	Route::get('projects/{encrypt}/timetable',['as'=>'projects.timetable','uses'=>'ProjectController@timetable']);
	Route::post('projects/getleadusers','ProjectController@getLeadUsersApi');		
	Route::post('projects/download_files',['as'=>'projects.download_files','uses'=>'ProjectController@downloadFile']);

	//project approval
	Route::group(['middleware' => 'projectapprovalpage'], function () {
		Route::resource('project_approvals','ProjectApprovalController');
		Route::post('project_approvals/update_status',['as'=>'project_approvals.update_status','uses'=>'ProjectApprovalController@update_status']);
		Route::put('project_approvals/head_overide',['as'=>'project_approvals.head_overide','uses'=>'ProjectApprovalController@head_overide']);
		Route::post('project_approvals/filter',['as'=>'project_approvals.filter','uses'=>'ProjectApprovalController@filter']);
	});
	
	//project workgroups
	Route::resource('project_workgroups','ProjectWorkgroupController');
	Route::post('project_workgroups/remove_member',['as'=>'project_workgroups.remove_member','uses'=>'ProjectWorkgroupController@remove_member']);

	//project threads
	Route::resource('project_threads','ProjectThreadController');
	Route::post('project_threads/comments',['as'=>'project_threads.comment','uses'=>'ProjectThreadController@comment']);
	Route::post('project_thread/files',['as'=>'project_thread_files.download','uses'=>'ProjectThreadController@download']);
	Route::post('project_threads/comment/files',['as'=>'project_thread_comment_files.download','uses'=>'ProjectThreadController@comment_download']);

	//project timetable
	Route::resource('project_timetables','ProjectTimetableController');
	Route::get('project_timetables/{encrypt}/details',['as'=>'project_timetables.details','uses'=>'ProjectTimetableController@overview']);	
	Route::post('project_timetables/topic_leads',['as'=>'project_timetables.add_lead','uses'=>'ProjectTimetableController@add_leads']);

	//project tasks
	Route::resource('project_tasks','ProjectTaskController');
	Route::get('project_tasks/{encrypt}/overview',['as'=>'project_tasks.overview','uses'=>'ProjectTaskController@overview']);

	//Job Orders
	Route::resource('job_orders','JobOrderController');
	Route::get('job_orders/{encrypt}/overview',['as'=>'job_orders.overview','uses'=>'JobOrderController@overview']);
	Route::post('job_orders/files/download',['as'=>'job_orders.download_files','uses'=>'JobOrderController@downloadFile']);
	Route::post('job_orders/update_status',['as'=>'job_orders.update_status','uses'=>'JobOrderController@update_status']);
	Route::post('job_orders/rating',['as'=>'job_orders.rating','uses'=>'JobOrderController@rating']);
	Route::post('getJobOrderDetails','JobOrderController@jobOrderDetails');
	Route::post('job_orders/new_store',['as'=>'job_orders.new_store','uses'=>'JobOrderController@new_store']);
	Route::post('job_orders/add_participant',['as'=>'job_orders.add_participant','uses'=>'JobOrderController@add_participant']);
	Route::post('job_orders/add_participant_cc',['as'=>'job_orders.add_member_cc','uses'=>'JobOrderController@addCC']);
	Route::post('job_orders/remove_participant_cc',['as'=>'job_orders.remove_member_cc','uses'=>'JobOrderController@removeCC']);
	Route::post('job_orders/export_in_projects',['as'=>'job_orders.exportInProjects','uses'=>'JobOrderController@exportInProjects']);
	

	//job order workgroups
	Route::resource('job_order_workgroups','JobOrderWorkgroupController');
	Route::post('job_order_workgroups/remove_member',['as'=>'job_order_workgroups.remove_member','uses'=>'JobOrderWorkgroupController@remove_member']);

	//job order threads
	Route::resource('job_order_threads','JobOrderThreadController');
	Route::post('job_order_threads/comments',['as'=>'job_order_threads.comment','uses'=>'JobOrderThreadController@comment']);
	Route::post('job_order_thread/files',['as'=>'job_order_thread_files.download','uses'=>'JobOrderThreadController@download']);
	Route::post('job_order_threads/comment/files',['as'=>'job_order_thread_comment_files.download','uses'=>'JobOrderThreadController@comment_download']);

	//Timetables
	Route::group(['middleware' => 'maintenancepage'], function () {
		Route::resource('timetables','TimetableController');
		Route::post('getTimetableDetails','TimetableController@timetableDetails');
		Route::post('timetables/updates',['as'=>'timetables.updates','uses'=>'TimetableController@timetableUpdates']);
	});


	//Timetable Task	
	Route::resource('timetable_tasks','TimetableTaskController');
	Route::post('getTaskDetails','TimetableTaskController@taskDetails');
	Route::post('timetable_tasks/updates',['as'=>'timetable_tasks.updates','uses'=>'TimetableTaskController@taskUpdates']);
	Route::post('timetableTask/getList','TimetableTaskController@getList');

	//Expenses
	Route::resource('expenses','ExpenseController');
	Route::group(['middleware'=>'expensespage'], function () {
		Route::get('expenses/{encrypt}/overview',['as'=>'expenses.overview','uses'=>'ExpenseController@overview']);
		Route::post('expenses/approval',['as'=>'expenses.approval','uses'=>'ExpenseController@approval']);
		Route::post('expenses/download',['as'=>'expenses.download','uses'=>'ExpenseController@download']);
	});

	//particular type
	Route::group(['middleware' => 'maintenancepage'], function () {
		Route::resource('particular_types','ParticularTypeController');
		Route::post('getPartTypeDetails','ParticularTypeController@typeDetails');
		Route::post('particular_types/updates',['as'=>'particular_types.updates','uses'=>'ParticularTypeController@typeUpdates']);
	});

	//Bill of Materials
	Route::resource('bill_of_materials','BillOfMaterialsController');
	Route::post('getBOMDetails','BillOfMaterialsController@bomDetails');	
	Route::post('bill_of_materials/updates',['as'=>'bill_of_materials.updates','uses'=>'BillOfMaterialsController@bomUpdates']);

	//Warehouse Inventory
	Route::resource('warehouse_inventory','WarehouseInventoryController');
	Route::post('getWarehouseItemDetails','WarehouseInventoryController@itemDetails');	
	Route::post('warehouse_inventory/updates',['as'=>'warehouse_inventory.updates','uses'=>'WarehouseInventoryController@itemUpdates']);
	Route::post('warehouse_inventory/uploads',['as'=>'warehouse_inventory.upload','uses'=>'WarehouseInventoryController@uploads']);
	Route::post('warehouse_inventory/release',['as'=>'warehouse_inventory.release','uses'=>'WarehouseInventoryController@release']);
	Route::get('warehouse_inventory/{barcode}/history',['as'=>'warehouse_inventory.histories','uses'=>'WarehouseInventoryController@history']);

	//modules
	Route::group(['middleware' => 'modulepage'], function () {
		Route::resource('modules','ModulesController');
	});

	//asset tracking
	Route::resource('asset_trackings','AssetTrackingController');
	Route::get('asset_trackings/{id}/route_history',['as'=>'asset_trackings.route_history','uses'=>'AssetTrackingController@route_history']);
	Route::post('asset_trackings/route_history_update',['as'=>'asset_trackings.route_history_update','uses'=>'AssetTrackingController@route_history_update']);

	//Access Control
	Route::group(['middleware' => 'accesspage'], function () {
		Route::resource('access_controls','AccessControlController');
		Route::get('access_controls/{id}/manage_access',['as'=>'access_controls.manage_access','uses'=>'AccessControlController@manage']);
	});
	//Announcement
	Route::group(['middleware' => 'announcementpage'], function () {
		Route::resource('announcements','AnnouncementController');
	});
	Route::post('getMemoDetails','AnnouncementController@getDetails');
	Route::post('memo/download',['as'=>'memo.download','uses'=>'AnnouncementController@download']);
	Route::get('memo/{id}/details','AnnouncementController@memo_details');
	Route::post('memo/file/download',['as'=>'memo.download_files','uses'=>'AnnouncementController@download_file']);

	//request Type
	Route::group(['middleware' => 'maintenancepage'], function () {
		Route::resource('request_types','RequestTypeController');
		Route::post('getTypeDetails','RequestTypeController@typeDetails');
		Route::post('request_types/updates',['as'=>'request_types.updates','uses'=>'RequestTypeController@typeUpdates']);
	});

	//request report
	Route::resource('req_reports','RequestReportController');

	Route::get('reports/all_jo',['as'=>'reports.all_jo','uses'=>'JobOrderController@jobOrderReport']);
	Route::get('reports/employees/job',['as'=>'reports.emp_jo','uses'=>'JobOrderController@JoPerEmployee']);
	Route::get('reports/{id}/emp_jo_detailed',['as'=>'job_orders.emp_detailed','uses'=>'JobOrderController@job_emp_detailed']);

	Route::post('job_reports/filterUserDept','JobOrderController@filterUserDept');
	Route::post('reports/filter_emp/job',['as'=>'job_orders.filter_report','uses'=>'JobOrderController@JoPerEmployeeFilter']);
	Route::post('reports/filter_emp/alljob',['as'=>'job_orders.filter_report2','uses'=>'JobOrderController@JoPerEmployeeFilter2']);
	Route::post('job_reports/filterOption',['uses'=>'JobOrderController@filterOption']);
	Route::post('job_reports/extraction_process',['as'=>'job_orders.extraction','uses'=>'JobOrderController@extraction']);

	//meeting scheduler
	Route::resource('planner','PlannerController');
	Route::get('planner/{id}/reminder','PlannerController@details');
	Route::post('planner/stop',['as'=>'planners.stop','uses'=>'PlannerController@stop']);
	Route::post('planner/download',['as'=>'planners.download','uses'=>'PlannerController@download']);
	Route::post('planner/comment',['as'=>'planners.comment','uses'=>'PlannerController@comment']);
	Route::post('planner/comment_download',['as'=>'planners.comment_download','uses'=>'PlannerController@comment_download']);
	
	// outgoing emails
	Route::resource('email_outs','EmailOutController');

	//project report
	Route::get('reports/projects',['as'=>'reports.all_projects','uses'=>'ProjectController@report_project']);

	Route::post('reports/projects/export',['as'=>'reports.projects_exports','uses'=>'ProjectController@report_export']);

	//incident report
	Route::resource('incident_reports','IncidentReportController');
	Route::post('incident_reports/download',['as'=>'incident_reports.download_files','uses'=>'IncidentReportController@downloadFile']);
	Route::post('incident_reports/comment',['as'=>'incident_reports.comment_store','uses'=>'IncidentReportController@comment_store']);
	Route::post('incident_reports/solution',['as'=>'incident_reports.solution_store','uses'=>'IncidentReportController@solution_store']);

	Route::post('incident_reports/closing',['as'=>'incident_reports.closing_store','uses'=>'IncidentReportController@closing_store']);

	Route::post('incident_reports/download_thread_files',['as'=>'incident_reports.download_thread_files','uses'=>'IncidentReportController@downloadThreadFile']);
	Route::post('incident_reports/download_solution_files',['as'=>'incident_reports.download_solution_files','uses'=>'IncidentReportController@downloadSolutionFile']);

	Route::post('incident_reports/export',['as'=>'incident_reports.export','uses'=>'IncidentReportController@export']);

	// overtime
	Route::resource('overtimes','OverTimeController');
	Route::group(['middleware' => 'overtimepage'], function () {
		Route::get('overtimes/{encrypt}/overview',['as'=>'overtimes.overview','uses'=>'OverTimeController@overview']);
		Route::post('overtimes/approval',['as'=>'overtimes.approval','uses'=>'OverTimeController@approval']);
	});
});

Route::get('/getHash', function () {
	
	$str = \Hash::make('modules');
            $str2 = str_replace('/', "1", $str);            
            $str3 = substr($str2, 0, 25);
	echo "<pre>";
	print_r($str3); die;
});