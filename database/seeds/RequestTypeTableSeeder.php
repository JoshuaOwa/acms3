<?php

use Illuminate\Database\Seeder;

class RequestTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('request_types')->truncate();

		DB::statement("INSERT INTO request_types (id, type) VALUES
			(1, 'CLOSE SALES'),
            (2, 'ESA MONTHLY'),
            (3, 'STOCK REQUEST'),
            (4, 'IMPLEMENTATION'),
            (5, 'SERVICE'),
            (6, 'DELIVERY'),
            (7, 'POTENTIAL'),
			(8, 'ASSET'),
			(9, 'COLLECTION')");			
    }
}
