<?php

use Illuminate\Database\Seeder;

class ModuleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('modules')->truncate();

		DB::statement("INSERT INTO modules (id, module,description,routeUri,default_url,icon,encryptname) VALUES
			(1, 'Positions','MAINTENANCE FOR POSITIONS','positions','positions.index','fa fa-handshake-o',''),
			(2, 'Departments','MAINTENANCE FOR DEPARTMENTS','departments','departments.index','fa fa-delicious',''),
			(3, 'Users','MAINTENANCE FOR USERS','users','users.index','fa fa-users',''),
			(4, 'Contact Types','MAINTENANCE FOR CONTACT TYPES','contact_types','contact_types.index','fa fa-snowflakes-o',''),
			(5, 'Timetables','MAINTENANCE FOR TIMETABLES AND TASKS','timetables','timetables.index','fa fa-table',''),
			(6, 'Particular Types','MAINTENANCE FOR PARTICULAR TYPES THAT IS USING FOR EXPENSE','particular_types','particular_types.index','fa fa-circle-o',''),
			(7, 'User Request','MODULE USE TO REQUEST TO PARTICULAR PERSON ','user_request','user_request.index','fa fa-grav fa-2x',''),
			(8, 'My Request','LISTS OF ALL YOUR REQUESTS AND REQUESTS TO YOU ','my_requests','my_requests.index','fa fa-superpowers fa-2x',''),
			(9, 'Contacts','LISTS OF ALL CREATED CONTACTS','contacts','contacts.index','fa fa-address-card fa-2x',''),
			(10, 'Contacts For Approval','LISTS OF CONTACTS FOR APPROVAL','contact_approvals','contact_approvals.index','fa fa-reddit fa-2x',''),
			(11, 'My Projects','LISTS OF PROJECTS THAT YOU CREATED','projects','projects.index','fa fa-briefcase fa-2x',''),
			(12, 'Projects For Approval','LISTS OF PROJECTS FOR APPROVAL','project_approvals','project_approvals.index','fa fa-folder-open fa-2x',''),
			(13, 'Warehouse Inventory','MAINTENANCE OF ITEMS IN WAREHOUSE','warehouse_inventory','warehouse_inventory.index','fa fa-cubes fa-2x',''),
			(14, 'Modules','MAINTENANCE OF SYSTEM MODULEs','modules','modules.index','fa fa-files',''),
			(15, 'My Job Order','LIST OF JOB ORDERS THAT I BELONG','job_orders','job_orders.index','fa fa-beer fa-2x',''),
			(16, 'Project Workgroup','LIST OF PROJECTS THAT I BELONG','project_workgroups','project_workgroups.index','fa fa-life-saver fa-2x',''),
			(17, 'Announcement','Announcement Form','announcements','announcements.index','fa fa-bullhorn fa-2x',''),
			(18, 'Request Type','Maintenance for Request Type','request_types','request_types.index','fa fa-bandcamp',''),
			(19, 'Job Monitoring','Job Order Monitoring For Each Day','job_monitoring','job_monitoring.index','fa fa-search fa-2x',''),
			(20, 'Create Job Order','Creation of Job Order Outside Project','job_orders/create','job_orders.create','fa fa-plus fa-2x',''),
			(21, 'All JO','Job Order Report','reports/all_jo','reports.all_jo','fa fa-file',''),
			(22, 'Employee JO','Job Order Report Per Employee','reports/employees/job','reports.emp_jo','fa fa-file',''),
			(23, 'Projects','Project Reports','reports/projects','reports.all_projects','fa fa-file',''),
			(24, 'Planner','Planner for every employee','planner','planner.index','fa fa-calendar fa-2x','')
			-- (25, 'Expenses','List of Expenses for Approval','expenses','expenses.index','fa fa-money fa-2x',''),
			-- (26, 'Overtimes','List of Overtime for Approval','overtimes','overtimes.index','fa fa-clock-o fa-2x','')
			");
    }
}
