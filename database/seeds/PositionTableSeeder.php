<?php

use Illuminate\Database\Seeder;

class PositionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('positions')->truncate();

		DB::statement("INSERT INTO positions (id, position) VALUES
			(1, 'ACCOUNT EXECUTIVE'),
            (2, 'ASSISTANT ACCOUNT EXECUTIVE'),
            (3, 'TEAM LEAD'),
            (4, 'TECHNICAL SUPPORT'),
            (5, 'BUSINESS MANAGER'),
            (6, 'GOVERNANCE')");
    }
}
