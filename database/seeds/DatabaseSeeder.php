<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	// $this->call(DepartmentTableSeeder::class);
    	// $this->call(PositionTableSeeder::class);
     //    $this->call(UserTableSeeder::class);
     //    $this->call(ContactTypeTableSeeder::class);
     //    $this->call(TimetableTableSeeder::class);
     //    $this->call(TimetableTaskSeeder::class);
     //    $this->call(ParticularTypeTableSeeder::class);
        $this->call(ModuleTableSeeder::class);  
        // $this->call(RequestTypeTableSeeder::class);
    }
}
