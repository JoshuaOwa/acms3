<?php

use Illuminate\Database\Seeder;
use App\Department;
class DepartmentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('departments')->truncate();

		DB::statement("INSERT INTO departments (id, head, department) VALUES
			(1, 1,'AMS/PAYROLL/GL/PCOUNT'),
			(2, 1,'ASSEMBLY/IN-HOUSE SERVICE'),			
			(3, 1,'ASSET TRACKING/WMS/HOTEL'),
			(4, 1,'BARCODE SCANNER/PRINTER'),			
			(5, 1,'CIVIL WORKS/NETWORK'),
            (6, 1,'CONSUMABLES '),          
            (7, 1,'DEALER/PRODUCT'),          
            (8, 1,'EVENTS/EXHIBIT'),
            (9, 1,'FINANCE/ACCOUNTING/PURCHASING DEPARTMENT'),          
            (10, 1,'HR/ASSET/BIR DEPARTMENT'),
            (11, 1,'OTHERS'),
            (12, 1,'PARKING'),
            (13, 1,'WAREHOUSE/PRODUCTION/DISPATCH'),
            (14, 1,'WEB'),
            (15, 1,'WINPOS/FOODPOS');");
    
    }
}





 









 





