<?php

use Illuminate\Database\Seeder;
use App\User;
class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        DB::table('users')->truncate();    	
    	
		User::insert(array(
			'first_name'     => 'CHASE',			
			'last_name' => 'ADMIN',
			'username' => 'chaseadmin',
			'position_id' => '1',
			'dept_id' => '1',
			'empid' => 'CHASE2018',
			'active' => '1',			
			'email'    => 'admin@chasetech.com',
			'image' => '',
			'cellno' => '09123456789',
			'localno' => '4447',
			'username' => 'admin',
			'password' => Hash::make('password'),
			'active' => '1',
			'online' => '0'
			
		));
        
		DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}                                                                                                                       
                        