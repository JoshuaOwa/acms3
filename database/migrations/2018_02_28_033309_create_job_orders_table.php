<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('contact_id')->default(0);
            $table->string('contact_person')->nullable();
            $table->integer('project_id')->default(0);
            $table->string('project_name')->nullable(); 
            $table->integer('timetable_id');           
            $table->integer('task_id');
            $table->integer('jo_workgroup')->default(0);                        
            $table->integer('created_by');
            $table->integer('assigned_to')->default(0);
            $table->string('assigned_name')->nullable();            
            $table->date('start_date');
            $table->date('end_date');
            $table->text('details');
            $table->string('encryptname');
            $table->integer('status')->default(1);
            $table->integer('rating')->default(0);
            $table->text('rating_remarks')->nullable();       
            $table->boolean('onetime')->default(0);
            $table->timestamps();
        });       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_orders');
    }
}
