<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCoorOnIncidentReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('incident_reports',function( Blueprint $table){

            $table->string('client_coor')->nullable()->after('created_by');
            $table->text('solution')->nullable()->after('status');
            $table->integer('resolved_by')->nullable()->after('solution');
            $table->string('resolved_date')->nullable()->after('resolved_by');
            $table->integer('closed_by')->nullable()->after('resolved_date');
            $table->text('final_remarks')->nullable()->after('closed_by');            
            $table->integer('ratings')->nullable()->after('final_remarks');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    { 
        Schema::table('incident_reports', function (Blueprint $table) {            
            $table->dropColumn('client_coor');                               
            $table->dropColumn('solution');  
            $table->dropColumn('resolved_by');  
            $table->dropColumn('resolved_date');  
            $table->dropColumn('closed_by');
            $table->dropColumn('final_remarks');
            $table->dropColumn('ratings');
        });//
    }
}
