<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expenses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('contact_id')->default(0);
            $table->integer('project_id')->default(0);
            $table->integer('timetable_id')->default(0);
            $table->integer('task_id')->default(0);
            $table->integer('job_order_id')->default(0);
            $table->integer('user_id')->default(0);
            $table->string('particular');
            $table->integer('particular_type_id');
            $table->date('date');
            $table->string('amount');
            $table->integer('qty');
            $table->string('total_amount');
            $table->integer('status')->default(1);
            $table->integer('approved_by')->default(0);
            $table->string('encryptname');
            $table->timestamps();
        });
    }
        
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expenses');
    }
}
