<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReminderUpdatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reminder_updates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('info');
            $table->string('link');
            $table->date('last_update');
            $table->integer('cnt_need');
            $table->integer('cnt_donw');
            $table->integer('type');    
            $table->integer('user_id');
            $table->integer('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reminder_updates');
    }
}
