<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('created_by');
            $table->integer('approver_id');
            $table->integer('contact_type_id')->nullable();                                                        
            $table->integer('status')->default(0);
            $table->string('industry')->nullable();            
            $table->string('company')->nullable();
            $table->text('comp_address')->nullable();
            $table->string('encryptname')->nullable();
            $table->timestamps();        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
}
