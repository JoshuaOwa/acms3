<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserRequestRemarkFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_request_remark_files', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('request_id');
            $table->integer('remark_id');
            $table->string('filename');
            $table->string('encryptname');
            $table->integer('download')->default(0);
            $table->integer('uploaded_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_request_remark_files');
    }
}
