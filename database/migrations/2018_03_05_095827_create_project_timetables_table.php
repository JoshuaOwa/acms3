<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectTimetablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_timetables', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('contact_id')->default(0);
            $table->integer('project_id')->default(0);
            $table->integer('timetable_id');
            $table->integer('handle_by')->default(0);
            $table->string('encryptname');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_timetables');
    }
}
