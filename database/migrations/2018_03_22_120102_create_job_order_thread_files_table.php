<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobOrderThreadFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_order_thread_files', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('contact_id');
            $table->integer('project_id');
            $table->integer('timetable_id');
            $table->integer('task_id');
            $table->integer('job_order_id');
            $table->integer('thread_id');
            $table->text('filename');
            $table->string('encrpytname');
            $table->integer('download')->default(0);
            $table->integer('uploaded_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_order_thread_files');
    }
}