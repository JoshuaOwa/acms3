<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOverTimesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('over_times', function (Blueprint $table) {
            $table->increments('id');
            $table->string('encrypt_name');
            $table->integer('user_id');
            $table->integer('job_order_id');
            $table->integer('project_id');
            $table->integer('company');
            $table->integer('branch_id');
            $table->date('start_date');
            $table->date('end_date');
            $table->string('start_time');
            $table->string('end_time');
            $table->integer('category_id');
            $table->integer('type_id');
            $table->integer('status')->default(1);
            $table->integer('level');
            $table->string('remarks');
            $table->integer('approved_by')->nullable();
            $table->integer('confirmed_by')->default(0);
            $table->string('update_remarks')->nullable();
            $table->string('confirmed_remarks')->nullable();
            $table->date('downloaded')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('over_times');
    }
}
