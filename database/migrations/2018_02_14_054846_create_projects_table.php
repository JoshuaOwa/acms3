<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('created_by');
            $table->integer('contact_id');
            $table->string('project_name');
            $table->text('project_details')->nullalbe();
            $table->text('project_address')->nullalbe();
            $table->string('branch')->nullalbe();
            $table->string('tag');
            $table->date('date_reported');
            $table->date('start_date');
            $table->date('end_date');
            $table->integer('stage')->default(0);
            $table->integer('status')->default(0);
            $table->integer('approver_id')->default(0);
            $table->string('project_cost')->nullable();
            $table->string('allowed_ot')->nullable();
            $table->string('encryptname');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}