<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->string('request_code');
            $table->string('request_no');
            $table->integer('requestor');
            $table->integer('contact_id')->default(0);
            $table->integer('project_id')->default(0);
            $table->text('details');
            $table->boolean('status')->default(1);
            $table->date('start_date');
            $table->date('end_date');
            $table->integer('recuring');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_requests');
    }
}
