<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScheduleJobOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedule_job_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('contact_id')->default(0);
            $table->string('contact_person')->nullable();
            $table->integer('project_id')->default(0);
            $table->string('project_name')->nullable();
            $table->integer('timetable_id')->default(0);
            $table->integer('task_id')->default(0);
            $table->integer('created_by');
            $table->integer('assigned_to')->default(0);
            $table->string('assigned_name')->nullable();
            $table->integer('jo_workgroup')->default(0);
            $table->date('date');            
            $table->text('details');
            $table->integer('status')->default(0);
            $table->integer('main_jo_id')->default(0);           
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
            Schema::dropIfExists('schedule_job_orders');
    }
}
