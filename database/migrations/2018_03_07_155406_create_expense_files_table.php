<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpenseFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expense_files', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('contact_id')->default(0);
            $table->integer('project_id')->default(0);
            $table->integer('timetable_id')->default(0);
            $table->integer('task_id')->default(0);
            $table->integer('job_order_id')->default(0);
            $table->integer('expense_id');
            $table->string('filename');
            $table->string('encrpytname');
            $table->integer('download');
            $table->integer('uploaded_by');        
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expense_files');
    }
}
