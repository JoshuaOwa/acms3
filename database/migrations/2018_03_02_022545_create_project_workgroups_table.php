<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectWorkgroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_workgroups', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('contact_id');
            $table->integer('project_id');
            $table->integer('user_id');
            $table->string('position');
            $table->string('workgroup_name');
            $table->integer('added_by');
            $table->timestamps();            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_workgroups');
    }
}
