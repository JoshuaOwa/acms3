<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobOrderThreadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_order_threads', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('contact_id');
            $table->integer('project_id');
            $table->integer('timetable_id');
            $table->integer('task_id');
            $table->integer('job_order_id');            
            $table->text('message');
            $table->integer('user_id');
            $table->integer('main')->default(1);
            $table->integer('comment_to')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_order_threads');
    }
}
