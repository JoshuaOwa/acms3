@extends('layouts.master')
@section('main-body')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2><i class="fa fa-building-o"></i> <i class="fa fa-eye"></i> PROJECT DETAILS</h2>        
    </div>
</div>        
<div class="row  border-bottom white-bg dashboard-header">
    <div class="ibox-content ibox-heading">
        <center>
            <h2><strong>{!! strtoupper($project->project_name) !!}</strong></h2>
            <small>Project Name</small>
        </center>
    </div>   
    <div class="col-md-6">                
        <ul class="list-group clear-list m-t">
            <li class="list-group-item fist-item">
                <span class="pull-right">
                    {!! strtoupper($project->approver->first_name.' '.$project->approver->last_name) !!}
                </span>
                <span class="label label-info">*</span> Project Approver:
            </li>
            <li class="list-group-item">
                <span class="pull-right">
                    {!! $project->contact->contact_person !!}
                </span>
                <span class="label label-info">*</span> Contact Person:
            </li>
            <li class="list-group-item">
                <span class="pull-right">
                    {!! $project->contact->mobile_number.' / '.$project->contact->tel_no !!}
                </span>
                <span class="label label-info">*</span> Contact Number:
            </li>
            <li class="list-group-item">
                <span class="pull-right">
                    {!! $project->contact->email !!}
                </span>
                <span class="label label-info">*</span> Email:
            </li>         
            <li class="list-group-item">
                <span class="pull-right">
                    @if($project->status === 0)
                        <span class="label label-info"><i class="fa fa-clock-o"></i> For Approval</span>
                    @elseif($project->status === 1)
                        <span class="label label-success"><i class="fa fa-check"></i> Approved</span>
                    @elseif($project->status === 2)
                        <span class="label label-primary"> <i class="fa fa-user"></i>Assigned</span>                                       
                    @elseif($project->status === 3)
                        <span class="label label-danger"> <i class="fa fa-angellist"></i>Denied</span>                                       
                    @elseif($project->status === 4)
                        <span class="label label-warning"> <i class="fa fa-refresh"></i> For Re-Approve</span>                                       
                    @endif
                </span>
                <span class="label label-info">*</span> Project Status:
            </li>  
            <li class="list-group-item">
                <span class="pull-right">
                    {!! $project->allowed_ot.' HOURS' !!}
                </span>
                <span class="label label-info">*</span> Allowed OT:
            </li>                
            <li class="list-group-item">
                <span class="pull-right">
                    {!! $project->project_cost !!}
                </span>
                <span class="label label-info">*</span> Project Cost:
            </li> 
        </ul>
    </div>
    <div class="col-md-6">                
        <ul class="list-group clear-list m-t">
            <li class="list-group-item fist-item">
                <span class="pull-right">
                    {!! strtoupper($project->user->first_name.' '.$project->user->last_name) !!}
                </span>
                <span class="label label-info">*</span> Created By:
            </li>                        
            <li class="list-group-item">
                <span class="pull-right">
                    {!! $project->tag !!}
                </span>
                <span class="label label-info">*</span> Tag:
            </li>      
            <li class="list-group-item">
                <span class="pull-right">
                    {!! $project->project_address !!}
                </span>
                <span class="label label-info">*</span> Project Address:
            </li>      
            <li class="list-group-item">
                <span class="pull-right">
                    {!! $project->branch !!}
                </span>
                <span class="label label-info">*</span> Branch:
            </li>                          
            <li class="list-group-item">
                <span class="pull-right">
                    {!! $project->start_date !!}
                </span>
                <span class="label label-info">*</span> Start Date:
            </li>                
            <li class="list-group-item">
                <span class="pull-right">
                    {!! $project->end_date !!}
                </span>
                <span class="label label-info">*</span> End Date:
            </li>                                        
            
        </ul>
    </div>
</div>&nbsp;
<div class="row  border-bottom white-bg dashboard-header">
    <div class="ibox-content ibox-heading">
        <center>
            <h2><strong>Project Details</strong></h2>                        
        </center>        
    </div>   
    &nbsp;
    <div class="col-md-12">  
        <p>{!! $project->project_details !!}</p>
    </div>
    <div class="col-md-12">  
        
    </div>
</div>
<div class="row">
    <div class="col-lg-12 animated flash">
        <?php if (session('is_lead')): ?>
            <div class="alert alert-success alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
            </button>
                <center><h4>New Lead has been added!<i class="fa fa-check"></i></h4></center>                
            </div>
        <?php endif;?>            
    </div>        
</div>    
<div class="wrapper wrapper-content animated fadeInRight">  
    <div class="row">     
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title" style="background-color:#009688">                        
                        <div class="pull-right">                                                                                                        
                            {!! Html::decode(link_to_Route('projects.timetable','<i class="fa fa-plus"></i>  Add Timetable', $project->encryptname, array('class' => 'btn btn-white btn-xs')))!!}                                              
                        </div>                          
                    </div>                    
                    <div class="ibox-content ibox-heading">
                        <center><h3><i class="fa fa-tasks"> Timetable / Job Order!</i></h3></center>
                    </div>
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example">
                                <thead>
                                <tr>
                                    <th>Topic</th>
                                    <th style="text-align: center">Sub topic count</th>
                                    <th style="text-align: center">Job order count</th>   
                                    <th style="text-align: center">Total OT</th>
                                    <th style="text-align: center">Total Expense</th>
                                    <th style="text-align: center">Handle By</th>                                
                                    <th style="text-align: center;">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($timetables as $timetable)
                                        <tr>
                                            <td>{!! $timetable->timetable->timetable !!}</td>
                                            <td style="text-align: center;">{!! $timetable->subcount !!}</td>
                                            <td style="text-align: center;">{!! $timetable->jobcount !!}</td>
                                            <td style="text-align: center;"></td>
                                            <td style="text-align: center;"></td>
                                            <td style="text-align: center;">
                                                @if($timetable->handle_by < 1)                                                                                                                            
                                                    <button type="button" class="btn btn-warning btn-xs" data-toggle="modal" value = "{{$timetable->timetable_id}}" id = "{{$timetable->timetable_id}}" onclick="leadTopicFunction(this)" >
                                                        <i class="fa fa-plus"></i> Assign Lead
                                                    </button>                                                                                                                      
                                                @else
                                                    {!! strtoupper($timetable->handle->first_name.' '.$timetable->handle->last_name) !!}
                                                @endif
                                            </td>          
                                            <td style="text-align: center;">{!! Html::decode(link_to_Route('project_timetables.details','<i class="fa fa-eye"></i> View', $timetable->encryptname, array('class' => 'btn btn-info btn-xs','id'=>$timetable->encryptname)))!!}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>              
        </div>
        <div class="row">
            <div class="col-lg-12 animated flash">
                <?php if (session('is_added')): ?>
                    <div class="alert alert-success alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                        <center><h4>New Member has been added!<i class="fa fa-check"></i></h4></center>                
                    </div>
                <?php endif;?>            
            </div>
            <div class="col-lg-12 animated flash">
                <?php if (session('is_added')): ?>
                    <div class="alert alert-success alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                        <center><h4>Collection has been added!<i class="fa fa-check"></i></h4></center>                
                    </div>
                <?php endif;?>            
            </div>        
        </div>    
        <div class="row">
            <div class="col-lg-6">
                <div class="ibox float-e-margins">
                    <div class="ibox-title" style="background-color:#009688">
                        <div class="pull-right">                                                                            
                            <button type="button" class="btn btn-white btn-xs" data-toggle="modal" data-target="#workgroupAddModal">
                                Add Member
                            </button>                                                                      
                        </div>                          
                    </div>
                    <div class="ibox-content ibox-heading">
                        <center><h3><i class="fa fa-users"> Workgroup Members!</i></h3></center>
                    </div>
                    <div class="ibox-content">
                        <table class="table table-hover no-margins">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Position</th>                            
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($members as $member)
                                    <tr>
                                        <td>{!! strtoupper($member->user->first_name.' '.$member->user->last_name) !!}</td>
                                        <td>{!! strtoupper($member->position) !!}</td>                                    
                                    </tr>                   
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>            
            <div class="col-lg-6">
                <div class="ibox float-e-margins">
                    <div class="ibox-title" style="background-color:#009688">
                        <h5 style="color: white"></h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>                        
                        </div>
                    </div>
                    <div class="ibox-content ibox-heading">
                        <center><h3><i class="fa fa-money"> Collection Logs!</i></h3></center>
                        <h4>Remaining Balance: </i></h4>
                    </div>
                    <div class="ibox-content">
                        <table class="table table-hover no-margins">
                            <thead>
                            <tr>
                                <th>Collection ID</th>                                
                                <th>Amount</th>                                                        
                                <th>Done by</th>                            
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td><small>Pending...</small></td>
                                <td><i class="fa fa-clock-o"></i> 11:20pm</td>
                                <td>Samantha</td>                            
                            </tr>                   
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8">
                <div class="ibox float-e-margins">
                    <div class="ibox-title" style="background-color:#009688">
                                                                
                        <div class="pull-right">
                            <button type="button" class="btn btn-white btn-xs" data-toggle="modal" data-target="#threadModal">
                                Create New Thread
                            </button>                                        
                        </div>                                
                    </div>
                    <div class="ibox-content ibox-heading">
                        <center><h3><i class="fa fa-comment"> Message Threads!</i></h3></center>
                    </div>
                    <div class="ibox-content">
                        <div class="feed-activity-list">                                
                            <div class="feed-element box" id="flowTimeProj">
                                @forelse($threads as $thread)
                                @if(!empty($thread->user->image))                                           
                                    {!! Html::image('/uploads/users/'.$thread->user->employee_number.'/'.$thread->user->image,'',['class'=>'img-circle pull-left','style'=>'width:50px; height:50px;']) !!}
                                @else                                           
                                    <img src="/uploads/users/default.jpg" class="img-circle pull-left" style="width:50px; height:50px;"></img>
                                @endif                                       
                                <div class="media-body" style="padding-right:10px;">                                        
                                    <div style="padding-top:10px;"></div>
                                    <small class="text-muted"></small>
                                    <div class="well">                  
                                        <small class="pull-right">
                                            {!! $thread->created_at->format('M-d-Y H:i:s A') !!}
                                        </small>                                                                                                                                                        
                                        <strong>From: </strong>
                                            {!! strtoupper($thread->user->first_name.' '.$thread->user->last_name) !!} <br/>                                            
                                        <br/>
                                        <br />
                                        <strong>Message: </strong><br/>
                                        {!! $thread->message!!}                                                                                                                                                         
                                            @if(count($thread->files) > 0)
                                                <div style="padding-top:30px;">
                                                    <strong>Attachment: </strong><br/>                                                    
                                                    @forelse($thread->files as $thread_file)                                                                                                                                                                 
                                                        {!! Form::open(array('route'=>'project_thread_files.download','method'=>'POST')) !!}
                                                        {!! Form::hidden('encname',$thread_file->encrpytname) !!}
                                                        {!! Form::submit($thread_file->filename, array('type' => 'submit', 'class' => 'btn btn-primary btn-xs')) !!}                                                        
                                                        {!! Form::close() !!}                                                           
                                                    @empty
                                                    @endforelse                                                 
                                                </div>      
                                            @else                                                                                       
                                        @endif                                                                                                                                                                                              
                                        <br /><br />                                                                            
                                        <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#addCommentModal" id="{{$thread->id}}" value="{{$thread->id}}" onclick="ThreadCommentFunction(this)">
                                            <i class="fa fa-comment"></i>
                                        </button>                                           
                                        @if(!empty($thread->comments))
                                            @foreach($thread->comments as $comment)                                                    
                                                <div style="padding-top: 10px;">                                                                           
                                                    @if(!empty($comment->user->image))                                           
                                                        {!! Html::image('/uploads/users/'.$comment->user->employee_number.'/'.$comment->user->image,'',['class'=>'img-circle pull-left','style'=>'width:50px; height:50px;']) !!}
                                                    @else                                           
                                                        <img src="/uploads/users/default.jpg" class="img-circle pull-left" style="width:50px; height:50px;"></img>
                                                    @endif    
                                                    <div class="media-body" style="padding-right:10px;">                                        
                                                        <div style="padding-top:10px;"></div>
                                                        <small class="text-muted"></small>
                                                        <div class="well" style="border-color: #009688;">
                                                            <small class="pull-right">
                                                                {!! $comment->created_at !!}
                                                            </small>                                                                                                                                                        
                                                            <strong>From: </strong>                                                             
                                                            {!! strtoupper($comment->user->first_name.' '.$comment->user->last_name) !!}                                                              
                                                            <br /><br />
                                                            <strong>Message: </strong><br/>
                                                                {!! $comment->message !!}                                                                                                                                                                                       
                                                            <br />
                                                            @if(count($comment->files) > 0)
                                                                <div style="padding-top:20px;">
                                                                    <strong>Attachment: </strong><br/>                                                                      
                                                                    @forelse($comment->files as $comment_file)                                                                                                                                                                   
                                                                        {!! Form::open(array('route'=>'project_thread_comment_files.download','method'=>'POST')) !!}
                                                                        {!! Form::hidden('encname',$comment_file->encrpytname) !!}
                                                                        {!! Form::submit($comment_file->filename, array('type' => 'submit', 'class' => 'btn btn-primary btn-xs')) !!}                                                       
                                                                        {!! Form::close() !!}                                                           
                                                                    @empty
                                                                    @endforelse                                                 
                                                                </div>      
                                                            @else                                                                                       
                                                            @endif                                                          
                                                        </div>                                                            
                                                    </div>                                                          
                                                </div>
                                            @endforeach
                                            @else
                                        @endif                                                         
                                    </div>                                                                      
                                  <br />                                                            
                                </div>
                                @empty
                                @endforelse
                            </div>                                     
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="ibox float-e-margins">
                    <div class="ibox-title"  style="background-color:#009688">                        
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>                                
                        </div>
                    </div>
                    <div class="ibox-content ibox-heading">
                        <center><h3><i class="fa fa-book"> Project logs!</i></h3></center>
                    </div>
                    <div class="ibox-content inspinia-timeline" id="flowTimeProj">
                        @foreach($logs as $log)                           
                            <div class="timeline-item">
                                <div class="row">
                                    <div class="col-xs-3 date">
                                        <i class="fa fa-book"></i>
                                        {!! $log->created_at->diffForHumans() !!}
                                        <br/>
                                        <small class="text-navy">21 hour ago</small>
                                    </div>
                                    <div class="col-xs-7 content">
                                        <p class="m-b-xs"><strong>Details</strong></p>
                                        <p>
                                            {!! $log->log !!}
                                        </p>
                                    </div>
                                </div>
                            </div>  
                        @endforeach                            
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 animated flash">
                <?php if (session('is_bom')): ?>
                    <div class="alert alert-success alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                        <center><h4>Bill Of Materials has been added! <i class="fa fa-check"></i></h4></center>                
                    </div>
                <?php endif;?>       
                <?php if (session('bom_update')): ?>
                    <div class="alert alert-success alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                        <center><h4>Material has been updated! <i class="fa fa-check"></i></h4></center>                
                    </div>
                <?php endif;?>       
                <?php if (session('bom_qty')): ?>
                    <div class="alert alert-danger alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                        <center><h4>Actual Qty Must Not Be Lower Than The Remaining Qty When Subtracted! <i class="fa fa-close"></i></h4></center>                
                    </div>
                <?php endif;?>            
            </div>        
        </div>    
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title" style="background-color:#009688">                        
                        <div class="pull-right">                                                                                                                                    
                            <button type="button" class="btn btn-white btn-xs" data-toggle="modal" data-target="#bomAddModal">
                                <i class="fa fa-upload"></i> Upload BOM
                            </button>
                        </div>                          
                    </div>                    
                    <div class="ibox-content ibox-heading">
                        <center><h3><i class="fa fa-cubes"> Bills of Materials!</i></h3></center>
                    </div>
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example">
                                <thead>
                                <tr>
                                    <th>Category</th>
                                    <th style="text-align: center">Item</th>
                                    <th style="text-align: center">Description</th>   
                                    <th style="text-align: center">Total Qty</th>
                                    <th style="text-align: center">Deducted Qty</th>
                                    <th style="text-align: center">Remaining Qty</th>                                
                                    <th style="text-align: center;">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($boms as $bom)
                                        <tr>
                                            <td>{!! $bom->category !!}</td>
                                            <td style="text-align: center;">{!! $bom->item !!}</td>
                                            <td style="text-align: center;">{!! $bom->description !!}</td>
                                            <td style="text-align: center;">{!! $bom->qty !!}</td>
                                            <td style="text-align: center;">{!! $bom->qty - $bom->rem_qty !!}</td>
                                            <td style="text-align: center;">
                                                {!! $bom->rem_qty !!}
                                            </td>          
                                            <td>
                                                <button name="rbtn" class="rolebuttn btn btn-info btn-xs" id="{{$bom->id}}" value="{{$bom->id}}" onclick="bomEditFunction(this)">
                                                    <i class="fa fa-pencil"></i> Edit
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>              
        </div>
    </div>
</div>
<div class="modal inmodal" id="workgroupAddModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-users modal-icon"></i>
                <h4 class="modal-title">ADD WORKGROUP MEMBER</h4>                    
            </div>              
            {!! Form::open(array('route'=>'project_workgroups.store','method'=>'POST','files'=>true)) !!}                                                                               
            <div class="modal-body">                                                 
                <div class="row" style="padding-top:10px">
                    <div class="col-lg-6">
                        {!! Form::label('department','Department')!!}
                        {!! Form::select('department[]',$departments,'',['class'=>'form-control','id'=>'deptid','multiple'=>'multiple']) !!}                                  
                    </div>      
                    <div class="col-lg-6">
                        {!! Form::label('user_id','Name')!!}
                        {!! Form::select('user_id[]',$users,'',['class'=>'form-control','id'=>'user_id','multiple'=>'multiple']) !!}                     
                    </div>  
                </div>                                    
            </div>
            <div class="modal-footer">   
                {!! Form::hidden('contact_id',$project->contact_id) !!}
                {!! Form::hidden('project_id',$project->id,['id'=>'proj_id']) !!}
                {!! Form::button('Close', array('type' => 'submit', 'class' => 'btn btn-white','data-dismiss'=>'modal')) !!}            
                {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'button', 'class' => 'btn btn-primary hidden sbalert','id'=>'urbtn')) !!}
                {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'submit', 'class' => 'btn btn-primary hidden', 'id'=>'urbtn2')) !!}                    
            </div>   
            {!! Form::close() !!}                           
        </div>
    </div>
</div>
<div class="modal inmodal" id="workgroupRemoveModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-users modal-icon"></i>
                <h4 class="modal-title">REMOVE WORKGROUP MEMBER</h4>                    
            </div>              
            {!! Form::open(array('route'=>'project_workgroups.remove_member','method'=>'POST','files'=>true)) !!}                                                                               
            <div class="modal-body">                                                 
                <div class="row" style="padding-top:10px">                    
                    <div class="col-lg-6">
                        {!! Form::label('user_id','Name')!!}                        
                    </div>  
                </div>                                    
            </div>
            <div class="modal-footer">   
                {!! Form::hidden('contact_id',$project->contact_id) !!}
                {!! Form::hidden('project_id',$project->id,['id'=>'proj_id']) !!}
                {!! Form::button('Close', array('type' => 'submit', 'class' => 'btn btn-white','data-dismiss'=>'modal')) !!}            
                {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'button', 'class' => 'btn btn-primary hidden sbalert','id'=>'urbtn')) !!}
                {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'submit', 'class' => 'btn btn-primary hidden', 'id'=>'urbtn2')) !!}                    
            </div>   
            {!! Form::close() !!}                           
        </div>
    </div>
</div>
<div class="modal inmodal" id="topicLeadAddModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-users modal-icon"></i>
                <h4 class="modal-title">ADD LEAD TO TOPIC</h4>                    
            </div>              
            {!! Form::open(array('route'=>'project_timetables.add_lead','method'=>'POST','files'=>true)) !!}                                                                               
            <div class="modal-body">                                                 
                <div class="row" style="padding-top:10px">
                    <div class="col-lg-6">
                        {!! Form::label('department','Department')!!}
                        {!! Form::select('department[]',$departments,'',['class'=>'form-control','id'=>'leaddeptid','multiple'=>'multiple']) !!}                                  
                    </div>      
                    <div class="col-lg-6">
                        {!! Form::label('handle_by','Name')!!}
                        {!! Form::select('handle_by',$for_leads,'',['class'=>'form-control','id'=>'handle_by','multiple'=>'multiple']) !!}                     
                    </div>  
                </div>                                    
            </div>
            <div class="modal-footer">                   
                {!! Form::hidden('timetable_id','',['id'=>'leadtimetableid']) !!}
                {!! Form::hidden('project_id',$project->id) !!}
                {!! Form::button('Close', array('type' => 'submit', 'class' => 'btn btn-white','data-dismiss'=>'modal')) !!}            
                {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'button', 'class' => 'btn btn-primary hidden','id'=>'leadbtn')) !!}
                {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'submit', 'class' => 'btn btn-primary hidden', 'id'=>'leadbtn2')) !!}                    
            </div>   
            {!! Form::close() !!}                           
        </div>
    </div>
</div>
<div class="modal inmodal" id="bomAddModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-cubes modal-icon"></i>
                <h4 class="modal-title">UPLOAD BILL OF MATERIALS</h4>                    
            </div>              
            {!! Form::open(array('route'=>'bill_of_materials.store','method'=>'POST','files'=>true)) !!}                                                                               
            <div class="modal-body">                                                 
                <div class="row" style="padding-top:10px;">
                    <div class="col-lg-12">                            
                        {!!Form::label('attached','Attached File')!!}                             
                        {!! Form::file('attached', array('id' => 'bomfileid', 'class' => 'photo_files', 'accept'=>'.csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel','required'=>'required')) !!}                   
                    </div>
                </div>                             
            </div>
            <div class="modal-footer">   
                {!! Form::hidden('contact_id',$project->contact_id) !!}
                {!! Form::hidden('project_id',$project->id,['id'=>'proj_id']) !!}
                {!! Form::button('Close', array('type' => 'submit', 'class' => 'btn btn-white','data-dismiss'=>'modal')) !!}            
                {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'button', 'class' => 'btn btn-primary','id'=>'bombtn')) !!}
                {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'submit', 'class' => 'btn btn-primary hidden', 'id'=>'bombtn2')) !!}                    
            </div>   
            {!! Form::close() !!}                           
        </div>
    </div>
</div>
<div id="editBOMModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        {!! Form::open(array('route'=>'bill_of_materials.updates','method'=>'POST')) !!}
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                {!!Form::label('name','Edit MAterial')!!}
            </div>
            <div class="modal-body">
                <div class="form-group">
                    {!! Form::label('category','Category') !!}
                    {!!Form::text('category','',['class'=>'form-control','placeholder'=>'Enter Category','id'=>'BOMCategory','required'=>'required'])!!}                                              
                </div>  
                <div class="form-group">
                    {!! Form::label('item','Item') !!}
                    {!!Form::text('item','',['class'=>'form-control','placeholder'=>'Enter Item','id'=>'BOMItem','required'=>'required'])!!}                                              
                </div>                       
                <div class="form-group">
                    {!! Form::label('description','Description') !!}
                    {!!Form::textarea('description','',['class'=>'form-control','placeholder'=>'Enter Item','id'=>'BOMDesc','required'=>'required'])!!}
                </div>      
                <div class="form-group">
                    {!! Form::label('specs','Specs') !!}
                    {!!Form::textarea('specs','',['class'=>'form-control','placeholder'=>'Enter Specs','id'=>'BOMSpec','required'=>'required'])!!}
                </div>                       
                <div class="form-group">
                    {!! Form::label('qty','Qty') !!}
                    {!!Form::number('qty','',['class'=>'form-control','placeholder'=>'Enter Qty','id'=>'BOMQty','min'=>0,'required'=>'required'])!!}                                              
                </div>                       
            </div>
            <div class="modal-footer">
                {!! Form::hidden('bomid','',['id'=>'BOMID']) !!}
                {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'button', 'class' => 'btn btn-primary','id'=>'bomupdatebtn')) !!}
                {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'submit', 'class' => 'btn btn-primary hidden', 'id'=>'bomupdatebtn2')) !!}                    
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-arrow-left"></i> Close</button>
            </div>
        </div>
        {!!Form::close()!!}
    </div>
</div>
<div class="modal inmodal" id="threadModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-comment modal-icon"></i>
                <h4 class="modal-title">New Thread</h4>                    
            </div>              
            {!! Form::open(array('route'=>'project_threads.store','method'=>'POST','files'=>true,'enctype'=>'multipart/form-data')) !!}
                <div class="modal-body">                            
                    <div class="row">                  
                        {!! Form::label('message','Message') !!}                    
                        {!! Form::textarea('message','',['class'=>'form-control','placeholder'=>'Enter it here....','required'=>'required','id'=>'threadmessageid']) !!}
                    </div>                             
                    <div class="row" style="padding-top:5px;">
                        <div class="col-lg-12">                  
                            {!! Form::label('attached','Attached File')!!}
                            {!! Form::file('attached[]', array('id' => 'thread_inputs', 'class' => 'photo_files', 'accept' => 'pdf|docx')) !!}                   
                        </div>
                    </div>                                    
                </div>
                <div class="modal-footer">                 
                    {!! Form::hidden('proj_id',$project->id) !!}                                                 
                    {!! Form::button('Close', array('type' => 'submit', 'class' => 'btn btn-white','data-dismiss'=>'modal')) !!}                    
                    {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'button', 'class' => 'btn btn-primary sbalert','id'=>'threadbtn')) !!}                    
                    <button class="btn btn-primary hidden" id="threadbtn2" value="Submit" onclick="this.disabled=true; this.form.submit();">Submit</button>                    
                </div>                  
            {!! Form::close() !!}
        </div>
    </div>
</div>
<div class="modal inmodal" id="addCommentModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content animated fadeIn">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <i class="fa fa-comment modal-icon"></i>
                    <h4 class="modal-title">Comment</h4>                    
                </div>              
                {!! Form::open(array('route'=>'project_threads.comment','method'=>'POST','files'=>true,'enctype'=>'multipart/form-data')) !!}
                    <div class="modal-body">                                 
                        <div class="row">                  
                            {!! Form::label('message','Comment') !!}                    
                            {!! Form::textarea('message','',['class'=>'form-control','placeholder'=>'Enter it here....','required'=>'required','id'=>'commentmessageid']) !!}
                        </div>                                                                              
                        <div class="row" style="padding-top:5px;">
                            <div class="col-lg-12">                  
                                {!! Form::label('attached','Attached File')!!}
                                {!! Form::file('attached[]', array('id' => 'comment_inputs', 'class' => 'photo_files', 'accept' => 'pdf|docx')) !!}                   
                            </div>
                        </div>       
                    </div>
                    <div class="modal-footer">                                              
                        {!! Form::hidden('thread_id','',['id'=>'mainthread_id']) !!}
                        {!! Form::button('Close', array('type' => 'submit', 'class' => 'btn btn-white','data-dismiss'=>'modal')) !!}              
                        {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'button', 'class' => 'btn btn-primary sbalert','id'=>'commentbtn')) !!}                              
                        <button class="btn btn-primary hidden" id="commentbtn2" value="Submit" onclick="this.disabled=true; this.form.submit();">Submit</button>                    
                    </div>                  
                {!! Form::close() !!}
        </div>

    </div>
</div>
@stop
@section('page-script')

$('#deptid,#user_id,#ex_user_id,#handle_by,#leaddeptid').multiselect({
    maxHeight: 200,    
    includeSelectAllOption: true,
    enableCaseInsensitiveFiltering: true,
    enableFiltering: true,
    buttonWidth: '100%',
    buttonClass: 'form-control'        
});
$('#comment_inputs,#thread_inputs').filer({

    showThumbs:true,
    addMore:true
});


$('#deptid').on("change", function(){

    var dept_id = $('#deptid').val();                           
    var proj_id = $('#proj_id').val();
    $.ajax({
        type:"POST",
        dataType: 'json',
        data: {dept_id: dept_id, proj_id: proj_id},
        url: "../../projects/getusers",
        success: function(data){            

            $('select#user_id').empty();
            
            $.each(data, function(i, text) {
                $('<option />',{value: i, text: text}).appendTo($('select#user_id'));
            });          
            var user = $('#user_id').val();       
            
            if(user != ""){                
                $('#urbtn').removeClass('hidden');
            }else{
                $('#urbtn').addClass('hidden');
            }        

            $('select#user_id').multiselect('rebuild');
        }
    });      
});    
$('#user_id').on("change", function(){

   var user2 = $('#user_id').val();       
            
    if(user2 != ""){                
        $('#urbtn').removeClass('hidden');
    }else{
        $('#urbtn').addClass('hidden');
    }        
});    
$('.sbalert').click(function () {

    if($('#user_id').val() != ""){
        
        swal({
            title: "Allright!",
            text: "I'm now going to add this to workgroup!",
            type: "success"
            });            
        $('#urbtn2').click();            
    }else{

        swal({
            title: "Warning",
            text: "Oh Come on! Select at least one member before you submit!",
            type: "warning"
        });      
    }                            
});
$('#leaddeptid').on("change", function(){

    var dept_id = $('#leaddeptid').val();                               
    $.ajax({
        type:"POST",
        dataType: 'json',
        data: {dept_id: dept_id},
        url: "../../projects/getleadusers",
        success: function(data){            

            $('select#handle_by').empty();
            
            $.each(data, function(i, text) {
                $('<option />',{value: i, text: text}).appendTo($('select#handle_by'));
            });                          
        
            var user = $('#handle_by').val();                 
            if(user != ""){                
                $('#leadbtn').removeClass('hidden');
            }else{
                $('#leadbtn').addClass('hidden');
            }

            $('select#handle_by').multiselect('rebuild');
        }
    });      
}); 

$('#handle_by').on("change", function(){

   var user2 = $('#handle_by').val();       
            
    if(user2 != ""){                
        $('#leadbtn').removeClass('hidden');
    }else{
        $('#leadbtn').addClass('hidden');
    }        
});  
$('#leadbtn').click(function () {

    if($('#handle_by').val() != ""){
        
        swal({
            title: "Got it!",
            text: "I'm now going to assign this topic!",
            type: "success"
            });            
        $('#leadbtn2').click();            
    }else{

        swal({
            title: "Warning",
            text: "Hey! Select a lead for this topic before you submit!",
            type: "warning"
        });      
    }                            
});

$('#bombtn').click(function () {
    
    
    if($('#bomfileid').val() != ""){
        
        swal({
            title: "Got it!",
            text: "Your BOM are now Uploading!",
            type: "success"
            });            
        $('#bombtn2').click();            
    }else{

        swal({
            title: "Warning",
            text: "Yow! Attach Your BOM File!",
            type: "warning"
        });      
    }                            
});

$('#bomupdatebtn').click(function () {
       
    swal({
        title: "Got it!",
        text: "Your BOM are now Uploading!",
        type: "success"
        });            
    $('#bomupdatebtn2').click();    
});

$('#threadbtn').click(function () {

    if($('#threadmessageid').val() != ""){        
        
        swal({
            title: "That's Great!",
            text: "Please wait....!",
            type: "success"
        });            
        $('#threadbtn2').click();                         
    }else{
        swal({
            title: "Warning",
            text: "Oooopss! You forgot to put a message!",
            type: "warning"
        });      
    }                            
});
$('#commentbtn').click(function () {

    if($('#commentmessageid').val() != ""){        
        
        swal({
            title: "That's Great!",
            text: "Please wait....!",
            type: "success"
        });            
        $('#commentbtn2').click();                         
    }else{
        swal({
            title: "Warning",
            text: "Oooopss! You forgot to put a message!",
            type: "warning"
        });      
    }                            
});
@endsection
<script type="text/javascript">
    
    function leadTopicFunction(elem){

        $('#leadtimetableid').val(elem.value);
        $('#topicLeadAddModal').modal('show');
    }

    function bomEditFunction(elem){

        var x = elem.id;

        $.ajax({

            type:"POST",
            dataType: 'json',
            data: {bomid: x},
            url: "../../getBOMDetails",
            success: function(data){            
                            
                $('#BOMCategory').val(data['category']);
                $('#BOMItem').val(data['item']);
                $('#BOMDesc').val(data['description']);
                $('#BOMQty').val(data['qty']);
                $('#BOMID').val(data['id']);
                $('#BOMSpec').val(data['specs']);
                                                
                $('#editBOMModal').modal('show');                
            }    
        });
    }    
    
    function ThreadCommentFunction(val){

    var x = val.value;
        if(x != ""){
            $('#mainthread_id').val(x);
        }    
        else{
            alert("Please Reload The Page And Try Again.")
        }
    }
</script>

