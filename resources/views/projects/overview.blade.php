@extends('layouts.master')
@section('main-body')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2><i class="fa fa-building-o"></i> <i class="fa fa-eye"></i> PROJECT DETAILS</h2>        
    </div>
</div>   
&nbsp;
<div class="row">
    <div class="col-lg-12">
        <div class="tabs-container">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#tab-projecdetails"> PROJECT DETAILS</a></li>
                <li class=""><a data-toggle="tab" href="#tab-timetable">PROJECT TIMETABLE</a></li>
                <li class=""><a data-toggle="tab" href="#tab-workgroup">PROJECT WORKGROUP</a></li>
                <li class=""><a data-toggle="tab" href="#tab-outgoing">OUTGOING EMAIL</a></li>                
                <li class=""><a data-toggle="tab" href="#tab-alljo">JOB ORDERS</a></li>
                <li class=""><a data-toggle="tab" href="#tab-request">REQUEST</a></li>
                <li class=""><a data-toggle="tab" href="#tab-incident">INCIDENT REPORT</a></li>
            </ul>
            <div class="tab-content">
                <div id="tab-projecdetails" class="tab-pane active">
                    <div class="panel-body">
                        <div class="tabs-container">
                            <div class="row  border-bottom white-bg dashboard-header">
                                <div class="ibox-content ibox-heading">
                                    <center>
                                        <h2><strong>{!! strtoupper($project->project_name) !!}</strong></h2>
                                        <small>Project Name</small>
                                    </center>
                                </div>   
                                <div class="col-md-6">                
                                    <ul class="list-group clear-list m-t">
                                        <li class="list-group-item fist-item">
                                            <span class="pull-right">
                                                @if($project->status === 0)
                                                    <span class="label label-info"><i class="fa fa-clock-o"></i> For Approval</span>
                                                @elseif($project->status === 1)
                                                    <span class="label label-success"><i class="fa fa-check"></i> Approved</span>
                                                @elseif($project->status === 2)
                                                    <span class="label label-primary"> <i class="fa fa-user"></i>Assigned</span>                                       
                                                @elseif($project->status === 3)
                                                    <span class="label label-danger"> <i class="fa fa-angellist"></i>Denied</span>                                       
                                                @elseif($project->status === 4)
                                                    <span class="label label-warning"> <i class="fa fa-refresh"></i> For Re-Approve</span>                                       
                                                @endif
                                            </span>
                                            <span class="label label-info">*</span> <strong>Project Status:</strong>
                                        </li>  
                                        <li class="list-group-item ">
                                            <span class="pull-right">
                                                {!! $project->tag !!}
                                            </span>
                                            <span class="label label-info">*</span> <strong>Tag:</strong>
                                        </li>      
                                        <li class="list-group-item">
                                            <span class="pull-right">
                                                {!! strtoupper($project->approver->first_name.' '.$project->approver->last_name) !!}
                                            </span>
                                            <span class="label label-info">*</span> <strong>Project Approver:</strong>
                                        </li>
                                        <li class="list-group-item">
                                            <span class="pull-right">
                                                {!! $project->project_address !!}
                                            </span>
                                            <span class="label label-info">*</span> <strong>Project Address:</strong>
                                        </li>      
                                        <li class="list-group-item">
                                            <span class="pull-right">
                                                {!! $project->branch !!}
                                            </span>
                                            <span class="label label-info">*</span> <strong>Branch:</strong>
                                        </li>                                                                                                  
                                    </ul>                                    
                                </div>
                                <div class="col-md-6">                
                                    <ul class="list-group clear-list m-t">
                                        <li class="list-group-item fist-item">
                                            <span class="pull-right">
                                                {!! strtoupper($project->user->first_name.' '.$project->user->last_name) !!}
                                            </span>
                                            <span class="label label-info">*</span> <strong>Created By:</strong>
                                        </li>                        
                                        
                                        <li class="list-group-item fist-item">
                                            <span class="pull-right">
                                                {!! $project->start_date !!}
                                            </span>
                                            <span class="label label-info">*</span> <strong>Start Date:</strong>
                                        </li>                
                                        <li class="list-group-item">
                                            <span class="pull-right">
                                                {!! $project->end_date !!}
                                            </span>
                                            <span class="label label-info">*</span> <strong>End Date:</strong>
                                        </li>                              
                                        
                                        
                                        <li class="list-group-item">
                                            <span class="pull-right">
                                                {!! $project->allowed_ot.' HOURS' !!}
                                            </span>
                                            <span class="label label-info">*</span> <strong>Allowed OT:</strong>
                                        </li>                
                                        <li class="list-group-item">
                                            <span class="pull-right">
                                                {!! $project->project_cost !!}
                                            </span>
                                            <span class="label label-info">*</span> <strong>Project Cost:</strong>
                                        </li>                                                  
                                    </ul>
                                </div>
                                <div class="col-lg-12">
                                    <div class="col-lg-6">
                                        <ul class="list-group clear-list m-t">
                                            <li class="list-group-item">
                                                <span class="pull-right">
                                                    <strong>{!! number_format($project->expense,2) !!}</strong>
                                                </span>
                                                <span class="label label-info">*</span><strong> TOTAL PROJECT EXPENSE: </strong>
                                            </li>                
                                        </ul>           
                                    </div>
                                    <div class="col-lg-6">
                                        <ul class="list-group clear-list m-t">
                                            <li class="list-group-item">
                                                <span class="pull-right">
                                                    <strong>{!! number_format($ot_approved,2) !!} HOURS</strong>
                                                </span>
                                                <span class="label label-info">*</span><strong> TOTAL OT IN THIS PROJECT: </strong>
                                            </li>                
                                        </ul>           
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <ul class="list-group clear-list m-t">
                                        <li class="list-group-item">                                            
                                            <span class="label label-info">*</span><strong> CONTACT PERSON/S: </strong>
                                            <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#contactpersonModal">
                                                <i class="fa fa-plus"> New</i>
                                            </button>  
                                            <br /><br />
                                            <table class="table">
                                                <thead>
                                                    <th>NAME</th>
                                                    <th>FUNCTION</th>
                                                    <th>EMAIL</th>
                                                    <th>MOBILE #</th>
                                                    <th>TEL #</th>                                                        
                                                </thead>
                                                <tbody>
                                                     @foreach($project->person as $person)
                                                    <tr>
                                                        <td>{!! $person->contact_person !!}</td>
                                                        <td>{!! $person->function !!}</td>
                                                        <td>{!! $person->email !!}</td>
                                                        <td>{!! $person->mobile_number !!}</td>
                                                        <td>{!! $person->tel_no!!}</td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>                                            
                                        </li>                
                                    </ul>           
                                </div>
                            </div>&nbsp;
                            <div class="row  border-bottom white-bg dashboard-header">
                                <div class="ibox-content ibox-heading">
                                    <center>
                                        <h2><strong>Project Details</strong></h2>                        
                                    </center>        
                                </div>   
                                &nbsp;
                                <div class="col-md-12">  
                                    <p>{!! $project->project_details !!}</p>
                                </div>
                                <div class="col-md-12">  
                                    
                                </div>
                            </div>                                                        
                        </div>
                    </div>
                </div>
                <div id="tab-timetable" class="tab-pane">
                    <div class="panel-body">
                        <div class="tabs-container">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-title" style="background-color:#009688">                                                            
                                            <div class="pull-right">
                                                <button type="button" class="btn btn-white btn-xs" data-toggle="modal" data-target="#threadModal">
                                                    New Minutes                                                    
                                                </button>           
                                                
                                                <button type="button" class="btn btn-white btn-xs" data-toggle="modal" data-target="#jobOrderModal">
                                                    Create Job Order
                                                </button>                                           
                                                
                                            </div>     
                                        </div>
                                        <div class="ibox-content ibox-heading">
                                            <center><h3><i class="fa fa-users"> TIMETABLE!</i></h3></center>                                            
                                        </div>
                                        <div class="ibox-content">                                                
                                            <div class="tabs-container">
                                                <ul class="nav nav-tabs">
                                                    <?php $cnt = 1;?>
                                                    @foreach($timetables as $timetable)
                                                        @if($cnt < 2)
                                                            <li class="active"><a data-toggle="tab" href="#tab-{!! $timetable->tab !!}"> {!! $timetable->timetable !!}</a></li>
                                                        @else
                                                            <li class=""><a data-toggle="tab" href="#tab-{!! strtolower($timetable->tab) !!}"> {!! $timetable->timetable !!}</a></li>
                                                        @endif
                                                        <?php $cnt++; ?>
                                                    @endforeach                                                 
                                                </ul>
                                                <div class="tab-content">
                                                    <?php 
                                                        $cnt = 1;
                                                        $cn2 = 1;
                                                    ?>
                                                    @foreach($timetables as $timetable)                                                       
                                                            @if($cnt < 2)    
                                                            <div id="tab-{!! $timetable->tab !!}" class="tab-pane active">
                                                                <div class="panel-body">                                                            
                                                                    <div class="col-lg-12">
                                                                        <div class="ibox float-e-margins">
                                                                            <div class="ibox-title" style="background-color:#009688">                                                                                                                                                                                                                   
                                                                            </div>
                                                                            <div class="ibox-content ibox-heading">
                                                                                <center><h3><i class="fa fa-comment">DISCUSSION WITH CLIENTS!</i></h3></center>
                                                                            </div>
                                                                            <div class="ibox-content">
                                                                                <div class="feed-activity-list">                                
                                                                                    <div class="feed-element box" id="flowTimeProj">
                                                                                        @forelse($threads as $thread)
                                                                                        @if(!empty($thread->user->image))                                           
                                                                                            {!! Html::image('/uploads/users/'.$thread->user->employee_number.'/'.$thread->user->image,'',['class'=>'img-circle pull-left','style'=>'width:50px; height:50px;']) !!}
                                                                                        @else                                           
                                                                                            <img src="/uploads/users/default.jpg" class="img-circle pull-left" style="width:50px; height:50px;"></img>
                                                                                        @endif                                       
                                                                                        <div class="media-body" style="padding-right:10px;">                                        
                                                                                            <div style="padding-top:10px;"></div>
                                                                                            <small class="text-muted"></small>
                                                                                            <div class="well">                  
                                                                                                <small class="pull-right">
                                                                                                    {!! $thread->created_at->format('M-d-Y H:i:s A') !!}
                                                                                                </small>                                                                                                                                                        
                                                                                                <strong>From: </strong>
                                                                                                    {!! strtoupper($thread->user->first_name.' '.$thread->user->last_name) !!} <br/>                                            
                                                                                                <br/>
                                                                                                <br />
                                                                                                <strong>Message: </strong><br/>
                                                                                                {!! $thread->message!!}                                                                                                                                                         
                                                                                                    @if(count($thread->files) > 0)
                                                                                                        <div style="padding-top:30px;">
                                                                                                            <strong>Attachment: </strong><br/>                                                    
                                                                                                            @forelse($thread->files as $thread_file)                                                                                                                                                                 
                                                                                                                {!! Form::open(array('route'=>'project_thread_files.download','method'=>'POST')) !!}
                                                                                                                {!! Form::hidden('encname',$thread_file->encrpytname) !!}
                                                                                                                {!! Form::submit($thread_file->filename, array('type' => 'submit', 'class' => 'btn btn-primary btn-xs')) !!}                                                        
                                                                                                                {!! Form::close() !!}                                                           
                                                                                                            @empty
                                                                                                            @endforelse                                                 
                                                                                                        </div>      
                                                                                                    @else                                                                                       
                                                                                                @endif                                                                                                                                                                                              
                                                                                                <br /><br />                                                                            
                                                                                                <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#addCommentModal" id="{{$thread->id}}" value="{{$thread->id}}" onclick="ThreadCommentFunction(this)">
                                                                                                    <i class="fa fa-comment"></i>
                                                                                                </button>                                           
                                                                                                @if(!empty($thread->comments))
                                                                                                    @foreach($thread->comments as $comment)                                                    
                                                                                                        <div style="padding-top: 10px;">                                                                           
                                                                                                            @if(!empty($comment->user->image))                                           
                                                                                                                {!! Html::image('/uploads/users/'.$comment->user->employee_number.'/'.$comment->user->image,'',['class'=>'img-circle pull-left','style'=>'width:50px; height:50px;']) !!}
                                                                                                            @else                                           
                                                                                                                <img src="/uploads/users/default.jpg" class="img-circle pull-left" style="width:50px; height:50px;"></img>
                                                                                                            @endif    
                                                                                                            <div class="media-body" style="padding-right:10px;">                                        
                                                                                                                <div style="padding-top:10px;"></div>
                                                                                                                <small class="text-muted"></small>
                                                                                                                <div class="well" style="border-color: #009688;">
                                                                                                                    <small class="pull-right">
                                                                                                                        {!! $comment->created_at !!}
                                                                                                                    </small>                                                                                                                                                        
                                                                                                                    <strong>From: </strong>                                                             
                                                                                                                    {!! strtoupper($comment->user->first_name.' '.$comment->user->last_name) !!}                                                              
                                                                                                                    <br /><br />
                                                                                                                    <strong>Message: </strong><br/>
                                                                                                                        {!! $comment->message !!}                                                                                                                                                                                       
                                                                                                                    <br />
                                                                                                                    @if(count($comment->files) > 0)
                                                                                                                        <div style="padding-top:20px;">
                                                                                                                            <strong>Attachment: </strong><br/>                                                                      
                                                                                                                            @forelse($comment->files as $comment_file)                                                                                                                                                                   
                                                                                                                                {!! Form::open(array('route'=>'job_order_thread_comment_files.download','method'=>'POST')) !!}
                                                                                                                                {!! Form::hidden('encname',$comment_file->encrpytname) !!}
                                                                                                                                {!! Form::submit($comment_file->filename, array('type' => 'submit', 'class' => 'btn btn-primary btn-xs')) !!}                                                       
                                                                                                                                {!! Form::close() !!}                                                           
                                                                                                                            @empty
                                                                                                                            @endforelse                                                 
                                                                                                                        </div>      
                                                                                                                    @else                                                                                       
                                                                                                                    @endif                                                          
                                                                                                                </div>                                                            
                                                                                                            </div>                                                          
                                                                                                        </div>
                                                                                                    @endforeach
                                                                                                    @else
                                                                                                @endif                                                         
                                                                                            </div>                                                                      
                                                                                          <br />                                                            
                                                                                        </div>
                                                                                        @empty
                                                                                        @endforelse
                                                                                    </div>                                     
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>                                                                        
                                                                </div>
                                                            </div>                                                                
                                                            @else
                                                            <div id="tab-{!! $timetable->tab !!}" class="tab-pane">
                                                                <div class="panel-body">
                                                                    <div class="tabs-container">
                                                                        <div class="tabs-left">
                                                                            <ul class="nav nav-tabs">
                                                                                @foreach($timetable->tasks as $task)
                                                                                @if($cn2 < 2)
                                                                                <li class="active"><a data-toggle="tab" href="#tab-{!! $task->tab !!}"> * {!! $task->task !!}</a></li>
                                                                                @else
                                                                                <li class=""><a data-toggle="tab" href="#tab-{!! $task->tab !!}"> * {!! $task->task !!}</a></li>
                                                                                @endif
                                                                                <?php $cn2++;?>
                                                                                @endforeach
                                                                            </ul>
                                                                            <div class="tab-content ">
                                                                                <?php 
                                                                                    $cn2 = 1;
                                                                                ?>
                                                                                @foreach($timetable->tasks as $task)
                                                                                    @if($cn2 < 2)
                                                                                        <div id="tab-{!! $task->tab !!}" class="tab-pane active">
                                                                                            <div class="panel-body">                                                            
                                                                                                <div class="col-lg-12">
                                                                                                    <div class="ibox float-e-margins">
                                                                                                        <div class="ibox-title" style="background-color:#009688">
                                                                                                                                                                                                                                                                                
                                                                                                        </div>
                                                                                                        <div class="ibox-content ibox-heading">
                                                                                                            <center><h3><i class="fa fa-comment">{!! $task->task !!}</i></h3></center>
                                                                                                        </div>
                                                                                                        <div class="ibox-content">
                                                                                                            <div class="feed-activity-list">                                
                                                                                                                <div class="feed-element box" id="flowTimeProj">
                                                                                                                    @forelse($task->threads as $thread)
                                                                                                                    @if(!empty($thread->user->image))                                           
                                                                                                                        {!! Html::image('/uploads/users/'.$thread->user->employee_number.'/'.$thread->user->image,'',['class'=>'img-circle pull-left','style'=>'width:50px; height:50px;']) !!}
                                                                                                                    @else                                           
                                                                                                                        <img src="/uploads/users/default.jpg" class="img-circle pull-left" style="width:50px; height:50px;"></img>
                                                                                                                    @endif                                       
                                                                                                                    <div class="media-body" style="padding-right:10px;">                                        
                                                                                                                        <div style="padding-top:10px;"></div>
                                                                                                                        <small class="text-muted"></small>
                                                                                                                        @if($date_now > $thread->end_date AND $thread->status == 1)
                                                                                                                            <div class="well" style="background-color: #fab1a0">                  
                                                                                                                        @elseif($thread->status > 1 AND $thread->return_date > $thread->end_date)
                                                                                                                            <div class="well" style="background-color: #fab1a0">                  
                                                                                                                        @else
                                                                                                                            <div class="well">                  
                                                                                                                        @endif                                                                                                                                                                                                                                                
                                                                                                                            <small class="pull-right">
                                                                                                                                {!! $thread->created_at->format('M-d-Y H:i:s A') !!}
                                                                                                                            </small>                                                                                                                                                        
                                                                                                                            <strong>Status:  </strong> 
                                                                                                                                @if($thread->status === 1)
                                                                                                                                    <span class="label label-info"><i class="fa fa-clock-o"></i> Ongoing</span>
                                                                                                                                @elseif($thread->status === 2)
                                                                                                                                    <span class="label label-success"><i class="fa fa-check"></i> Returned</span>
                                                                                                                                @elseif($thread->status === 3)
                                                                                                                                    <span class="label label-primary"> <i class="fa fa-user"></i>Closed</span>                                                                               
                                                                                                                                @endif                             
                                                                                                                            <br />
                                                                                                                            @if($thread->status > 1)                                                                                              
                                                                                                                                <strong>Return Date: </strong>{!! $thread->return_date !!}
                                                                                                                            @endif
                                                                                                                            <br/><br />                                                                                                                                                                                                                                                                    
                                                                                                                            <strong>From: </strong>
                                                                                                                                {!! strtoupper($thread->user->first_name.' '.$thread->user->last_name) !!} <br/>                                            
                                                                                                                            <strong>Rating: </strong>
                                                                                                                                @if($thread->status == 2)
                                                                                                                                    @if($uid == $thread->created_by)
                                                                                                                                        <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#ratingtModal" id="{{$thread->id}}}" value="{{$thread->id}}}" onclick="RatingFunction(this)">
                                                                                                                                            <i class="fa fa-star"></i>
                                                                                                                                        </button>    
                                                                                                                                    @else
                                                                                                                                        N/A
                                                                                                                                    @endif
                                                                                                                                @elseif($thread->status == 3)
                                                                                                                                    @for($i=0; $i < $thread->rating; $i++)
                                                                                                                                        <i class="fa fa-star"></i>
                                                                                                                                    @endfor
                                                                                                                                    @for($j=$i; $j < 10; $j++)
                                                                                                                                        <i class="fa fa-star-o"></i>
                                                                                                                                    @endfor
                                                                                                                                @else
                                                                                                                                    N/A
                                                                                                                                @endif 
                                                                                                                            <br/>
                                                                                                                            @if($thread->status == 3)
                                                                                                                            <strong>Rating Remarks: </strong><br />
                                                                                                                                {!! $thread->rating_remarks !!}
                                                                                                                            <br />
                                                                                                                            @endif
                                                                                                                            <br />                                                                                                                                                        
                                                                                                                            <strong>Job Order #: </strong> {!! $thread->id !!} <br/>
                                                                                                                            <strong>Scope: </strong>
                                                                                                                            @if($thread->type < 1)
                                                                                                                                Internal
                                                                                                                            @else
                                                                                                                                External
                                                                                                                            @endif
                                                                                                                            <strong>Assigned To: </strong> {!! strtoupper($thread->assigned.'-'.$thread->assigned_name) !!} <br/>                                 
                                                                                                                            <strong>Date: </strong> {!! strtoupper($thread->start_date.' - '.$thread->end_date) !!} <br/>                                                                                                                            
                                                                                                                            <br />
                                                                                                                            <strong>Details: </strong><br/>
                                                                                                                            {!! $thread->details!!}                                                                                                                                                         
                                                                                                                            @if(count($thread->files) > 0)
                                                                                                                                <div style="padding-top:30px;">
                                                                                                                                    <strong>Attachment: </strong><br/>                                                    
                                                                                                                                    @forelse($thread->files as $thread_file)                                                                                                                                                                 
                                                                                                                                        {!! Form::open(array('route'=>'job_orders.download_files','method'=>'POST')) !!}
                                                                                                                                        {!! Form::hidden('encname',$thread_file->encrpytname) !!}
                                                                                                                                        {!! Form::submit($thread_file->filename, array('type' => 'submit', 'class' => 'btn btn-primary btn-xs')) !!}                                                        
                                                                                                                                        {!! Form::close() !!}                                                           
                                                                                                                                    @empty
                                                                                                                                    @endforelse                                                 
                                                                                                                                </div>      
                                                                                                                            @else                                                                                       
                                                                                                                            @endif               
                                                                                                                            @if(!empty($thread->emails))
                                                                                                                                <div style="padding-top: 20px;"></div>
                                                                                                                                <strong>*Sent to: </strong>
                                                                                                                                <p>{!! $thread->emails !!}</p>
                                                                                                                            @endif
                                                                                                                            <br /><br />                                                        
                                                                                                                            <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#addJobOrderCommentModal" id="{{$thread->mainthreadid}}" value="{{$thread->mainthreadid}}" onclick="ThreadJobCommentFunction(this)">
                                                                                                                                <i class="fa fa-comment"></i>
                                                                                                                            </button>                                           
                                                                                                                            @if($thread->status < 2)
                                                                                                                            <button type="button" class="btn btn-success btn-xs" id="returnjo"><i class="fa fa-reply"></i></button>                                                                                                           
                                                                                                                            @endif
                                                                                                                            @if($thread->status < 2)
                                                                                                                                {!! Form::open(array('route'=>array('job_orders.update_status'),'method'=>'POST'))!!}
                                                                                                                                    {!! Form::hidden('job_id',$thread->id) !!}                                                                                                                                    
                                                                                                                                    {!! Form::button('<i class="fa fa-save"></i> Return', array('value'=>'2','type' => 'submit', 'class' => 'btn btn-primary hidden', 'id'=>'returnJObtn','name'=>'status')) !!}                            
                                                                                                                                {!! Form::close() !!}
                                                                                                                            @endif         
                                                                                                                            @if(!empty($thread->comments))
                                                                                                                                @foreach($thread->comments as $comment)                                                    
                                                                                                                                    <div style="padding-top: 10px;">                                    `                                       
                                                                                                                                        @if(!empty($comment->user->image))                                           
                                                                                                                                            {!! Html::image('/uploads/users/'.$comment->user->employee_number.'/'.$comment->user->image,'',['class'=>'img-circle pull-left','style'=>'width:50px; height:50px;']) !!}
                                                                                                                                        @else                                           
                                                                                                                                            <img src="/uploads/users/default.jpg" class="img-circle pull-left" style="width:50px; height:50px;"></img>
                                                                                                                                        @endif    
                                                                                                                                        <div class="media-body" style="padding-right:10px;">                                        
                                                                                                                                            <div style="padding-top:10px;"></div>
                                                                                                                                            <small class="text-muted"></small>
                                                                                                                                            <div class="well" style="border-color: #009688;">
                                                                                                                                                <small class="pull-right">
                                                                                                                                                    {!! $comment->created_at !!}
                                                                                                                                                </small>                                                                                                                                                        
                                                                                                                                                <strong>From: </strong>                                                             
                                                                                                                                                {!! strtoupper($comment->user->first_name.' '.$comment->user->last_name) !!}                                                              
                                                                                                                                                <br /><br />
                                                                                                                                                <strong>Message: </strong><br/>
                                                                                                                                                    {!! $comment->message !!}                                                                                                                                                                                       
                                                                                                                                                <br />
                                                                                                                                                @if(count($comment->files) > 0)
                                                                                                                                                    <div style="padding-top:20px;">
                                                                                                                                                        <strong>Attachment: </strong><br/>                                                                      
                                                                                                                                                        @forelse($comment->files as $comment_file)                                                                                                                                                                   
                                                                                                                                                            {!! Form::open(array('route'=>'job_order_thread_comment_files.download','method'=>'POST')) !!}
                                                                                                                                                            {!! Form::hidden('encname',$comment_file->encrpytname) !!}
                                                                                                                                                            {!! Form::submit($comment_file->filename, array('type' => 'submit', 'class' => 'btn btn-primary btn-xs')) !!}                                                       
                                                                                                                                                            {!! Form::close() !!}                                                           
                                                                                                                                                        @empty
                                                                                                                                                        @endforelse                                                 
                                                                                                                                                    </div>      
                                                                                                                                                @else                                                                                       
                                                                                                                                                @endif                                                          
                                                                                                                                            </div>                                                            
                                                                                                                                        </div>                                                          
                                                                                                                                    </div>
                                                                                                                                @endforeach
                                                                                                                                @else
                                                                                                                            @endif                                                         
                                                                                                                        </div>                                                                      
                                                                                                                      <br />                                                            
                                                                                                                    </div>
                                                                                                                    @empty
                                                                                                                    @endforelse
                                                                                                                </div>                                     
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>                                                                        
                                                                                            </div>
                                                                                        </div>
                                                                                    @else
                                                                                    <div id="tab-{!! $task->tab !!}" class="tab-pane">
                                                                                            <div class="panel-body">                                                            
                                                                                                <div class="col-lg-12">
                                                                                                    <div class="ibox float-e-margins">
                                                                                                        <div class="ibox-title" style="background-color:#009688">
                                                                                                                                                                                                                                                                                
                                                                                                        </div>
                                                                                                        <div class="ibox-content ibox-heading">
                                                                                                            <center><h3><i class="fa fa-comment">{!! $task->task !!}</i></h3></center>
                                                                                                        </div>
                                                                                                        <div class="ibox-content">
                                                                                                            <div class="feed-activity-list">                                
                                                                                                                <div class="feed-element box" id="flowTimeProj">
                                                                                                                    @forelse($task->threads as $thread)
                                                                                                                    @if(!empty($thread->user->image))                                           
                                                                                                                        {!! Html::image('/uploads/users/'.$thread->user->employee_number.'/'.$thread->user->image,'',['class'=>'img-circle pull-left','style'=>'width:50px; height:50px;']) !!}
                                                                                                                    @else                                           
                                                                                                                        <img src="/uploads/users/default.jpg" class="img-circle pull-left" style="width:50px; height:50px;"></img>
                                                                                                                    @endif                                       
                                                                                                                    <div class="media-body" style="padding-right:10px;">                                        
                                                                                                                        <div style="padding-top:10px;"></div>
                                                                                                                        <small class="text-muted"></small>
                                                                                                                        @if($date_now > $thread->end_date AND $thread->status == 1)
                                                                                                                            <div class="well" style="background-color: #fab1a0">                  
                                                                                                                        @elseif($thread->status > 1 AND $thread->return_date > $thread->end_date)
                                                                                                                            <div class="well" style="background-color: #fab1a0">                  
                                                                                                                        @else
                                                                                                                            <div class="well">                  
                                                                                                                        @endif                                   
                                                                                                                            <small class="pull-right">
                                                                                                                                {!! $thread->created_at->format('M-d-Y H:i:s A') !!}
                                                                                                                            </small>                         
                                                                                                                            <strong>Status:  </strong> 
                                                                                                                                @if($thread->status === 1)
                                                                                                                                    <span class="label label-info"><i class="fa fa-clock-o"></i> Ongoing</span>
                                                                                                                                @elseif($thread->status === 2)
                                                                                                                                    <span class="label label-success"><i class="fa fa-check"></i> Returned</span>
                                                                                                                                @elseif($thread->status === 3)
                                                                                                                                    <span class="label label-primary"> <i class="fa fa-user"></i>Closed</span>                                                                               
                                                                                                                                @endif              
                                                                                                                                <br />
                                                                                                                                @if($thread->status > 1)                                                                                              
                                                                                                                                    <strong>Return Date: </strong>{!! $thread->return_date !!}
                                                                                                                                @endif                                                                                                             
                                                                                                                            <br/><br />                                                                                                                                         
                                                                                                                            <strong>From: </strong>
                                                                                                                                {!! strtoupper($thread->user->first_name.' '.$thread->user->last_name) !!} <br/>                                            
                                                                                                                            <strong>Rating: </strong>
                                                                                                                                @if($thread->status == 2)
                                                                                                                                    @if($uid == $thread->created_by)
                                                                                                                                        <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#ratingtModal" id="{{$thread->id}}}" value="{{$thread->id}}}" onclick="RatingFunction(this)">
                                                                                                                                            <i class="fa fa-star"></i>
                                                                                                                                        </button>    
                                                                                                                                    @else
                                                                                                                                        N/A
                                                                                                                                    @endif
                                                                                                                                @elseif($thread->status == 3)
                                                                                                                                    @for($i=0; $i < $thread->rating; $i++)
                                                                                                                                        <i class="fa fa-star"></i>
                                                                                                                                    @endfor
                                                                                                                                    @for($j=$i; $j < 10; $j++)
                                                                                                                                        <i class="fa fa-star-o"></i>
                                                                                                                                    @endfor
                                                                                                                                @else
                                                                                                                                    N/A
                                                                                                                                @endif 
                                                                                                                            <br/>
                                                                                                                            @if($thread->status == 3)
                                                                                                                            <strong>Rating Remarks: </strong><br />
                                                                                                                                {!! $thread->rating_remarks !!}
                                                                                                                            <br />
                                                                                                                            @endif
                                                                                                                            <br />                                                                                                                                                        
                                                                                                                            <strong>Job Order #: </strong> {!! $thread->id !!} <br/>
                                                                                                                            <strong>Scope: </strong>
                                                                                                                            @if($thread->type < 1)
                                                                                                                                Internal
                                                                                                                            @else
                                                                                                                                External
                                                                                                                            @endif
                                                                                                                            <strong>Assigned To: </strong> {!! strtoupper($thread->assigned.'-'.$thread->assigned_name) !!} <br/>                                 
                                                                                                                            <strong>Date: </strong> {!! strtoupper($thread->start_date.' - '.$thread->end_date) !!} <br/>                                                                                                                            
                                                                                                                            <br />
                                                                                                                            <strong>Details: </strong><br/>
                                                                                                                            {!! $thread->details!!}                                                                                                                                                         
                                                                                                                            @if(count($thread->files) > 0)
                                                                                                                                <div style="padding-top:30px;">
                                                                                                                                    <strong>Attachment: </strong><br/>                                                    
                                                                                                                                    @forelse($thread->files as $thread_file)                                                                                                                                                                 
                                                                                                                                        {!! Form::open(array('route'=>'job_orders.download_files','method'=>'POST')) !!}
                                                                                                                                        {!! Form::hidden('encname',$thread_file->encrpytname) !!}
                                                                                                                                        {!! Form::submit($thread_file->filename, array('type' => 'submit', 'class' => 'btn btn-primary btn-xs')) !!}                                                        
                                                                                                                                        {!! Form::close() !!}                                                           
                                                                                                                                    @empty
                                                                                                                                    @endforelse                                                 
                                                                                                                                </div>      
                                                                                                                            @else                                                                                       
                                                                                                                            @endif               
                                                                                                                            @if(!empty($thread->emails))
                                                                                                                                <div style="padding-top: 20px;"></div>
                                                                                                                                <strong>*Sent to: </strong>
                                                                                                                                <p>{!! $thread->emails !!}</p>
                                                                                                                            @endif
                                                                                                                            <br /><br />                                                                            
                                                                                                                            <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#addJobOrderCommentModal" id="{{$thread->mainthreadid}}" value="{{$thread->mainthreadid}}" onclick="ThreadJobCommentFunction(this)">
                                                                                                                                <i class="fa fa-comment"></i>
                                                                                                                            </button>
                                                                                                                            @if($thread->status < 2)
                                                                                                                            <button type="button" class="btn btn-success btn-xs" id="returnjo"><i class="fa fa-reply"></i></button>                                                                                                           
                                                                                                                            @endif
                                                                                                                            @if($thread->status < 2)
                                                                                                                                {!! Form::open(array('route'=>array('job_orders.update_status'),'method'=>'POST'))!!}
                                                                                                                                    {!! Form::hidden('job_id',$thread->id) !!}                                                                                                                                    
                                                                                                                                    {!! Form::button('<i class="fa fa-save"></i> Return', array('value'=>'2','type' => 'submit', 'class' => 'btn btn-primary hidden', 'id'=>'returnJObtn','name'=>'status')) !!}                            
                                                                                                                                {!! Form::close() !!}
                                                                                                                            @endif                   
                                                                                                                            @if(!empty($thread->comments))
                                                                                                                                @foreach($thread->comments as $comment)                                                    
                                                                                                                                    <div style="padding-top: 10px;">                                                                           
                                                                                                                                        @if(!empty($comment->user->image))                                           
                                                                                                                                            {!! Html::image('/uploads/users/'.$comment->user->employee_number.'/'.$comment->user->image,'',['class'=>'img-circle pull-left','style'=>'width:50px; height:50px;']) !!}
                                                                                                                                        @else                                           
                                                                                                                                            <img src="/uploads/users/default.jpg" class="img-circle pull-left" style="width:50px; height:50px;"></img>
                                                                                                                                        @endif    
                                                                                                                                        <div class="media-body" style="padding-right:10px;">                                        
                                                                                                                                            <div style="padding-top:10px;"></div>
                                                                                                                                            <small class="text-muted"></small>
                                                                                                                                            <div class="well" style="border-color: #009688;">
                                                                                                                                                <small class="pull-right">
                                                                                                                                                    {!! $comment->created_at !!}
                                                                                                                                                </small>                                                                                                                                                        
                                                                                                                                                <strong>From: </strong>                                                             
                                                                                                                                                {!! strtoupper($comment->user->first_name.' '.$comment->user->last_name) !!}                                                              
                                                                                                                                                <br /><br />
                                                                                                                                                <strong>Message: </strong><br/>
                                                                                                                                                    {!! $comment->message !!}                                                                                                                                                                                       
                                                                                                                                                <br />
                                                                                                                                                @if(count($comment->files) > 0)
                                                                                                                                                    <div style="padding-top:20px;">
                                                                                                                                                        <strong>Attachment: </strong><br/>                                                                      
                                                                                                                                                        @forelse($comment->files as $comment_file)                                                                                                                                                                   
                                                                                                                                                            {!! Form::open(array('route'=>'job_order_thread_comment_files.download','method'=>'POST')) !!}
                                                                                                                                                            {!! Form::hidden('encname',$comment_file->encrpytname) !!}
                                                                                                                                                            {!! Form::submit($comment_file->filename, array('type' => 'submit', 'class' => 'btn btn-primary btn-xs')) !!}                                                       
                                                                                                                                                            {!! Form::close() !!}                                                           
                                                                                                                                                        @empty
                                                                                                                                                        @endforelse                                                 
                                                                                                                                                    </div>      
                                                                                                                                                @else                                                                                       
                                                                                                                                                @endif 
                                                                                                                                                @if(!empty($thread->emails))
                                                                                                                                                    <div style="padding-top: 20px;"></div>
                                                                                                                                                    <strong>*Sent to: </strong>
                                                                                                                                                    <p>{!! $thread->emails !!}</p>
                                                                                                                                                @endif                                                         
                                                                                                                                            </div>                                                            
                                                                                                                                        </div>                                                          
                                                                                                                                    </div>
                                                                                                                                @endforeach
                                                                                                                                @else
                                                                                                                            @endif                                                         
                                                                                                                        </div>                                                                      
                                                                                                                      <br />                                                            
                                                                                                                    </div>
                                                                                                                    @empty
                                                                                                                    @endforelse
                                                                                                                </div>                                     
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>                                                                        
                                                                                            </div>
                                                                                        </div>
                                                                                    @endif
                                                                                    <?php $cn2++;?>
                                                                                @endforeach
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            @endif                                                                                  
                                                        <?php $cnt++;?>      
                                                    @endforeach                                                                                                       
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>                                                                                                  
                            </div>    
                        </div>
                    </div>
                </div>
                <div id="tab-workgroup" class="tab-pane">
                    <div class="panel-body">
                        <div class="tabs-container">
                            <div class="col-lg-12">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-title" style="background-color:#009688">
                                        <div class="pull-right">                                                                            
                                            <button type="button" class="btn btn-white btn-xs" data-toggle="modal" data-target="#workgroupAddModal">
                                                Add Member
                                            </button>                                                                      
                                        </div>                          
                                    </div>
                                    <div class="ibox-content ibox-heading">
                                        <center><h3><i class="fa fa-users"> Workgroup Members!</i></h3></center>
                                    </div>
                                    <div class="ibox-content">
                                        <table class="table table-hover no-margins">
                                            <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Department</th>
                                                <th>Position</th>
                                                <th>Action</th>                                                    
                                            </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($members as $member)
                                                    <tr>
                                                        <td>{!! strtoupper($member->user->first_name.' '.$member->user->last_name) !!}</td>
                                                        <td>{!! strtoupper($member->position) !!}</td>                                    
                                                    </tr>                   
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>   
                        </div>
                    </div>
                </div>               
                <div id="tab-outgoing" class="tab-pane">
                    <div class="panel-body">
                        <div class="tabs-container">
                            <div class="row">
                                <div class="col-lg-8">
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-title" style="background-color:#009688">                                            
                                            <div class="pull-right">                                                        
                                                <button type="button" class="btn btn-white btn-xs" data-toggle="modal" data-target="#emailModal">
                                                    Create Email
                                                </button>
                                            </div>         
                                        </div>
                                        <div class="ibox-content ibox-heading">
                                            <center><h3><i class="fa fa-comment"> Email Threads!</i></h3></center>
                                        </div>
                                        <div class="ibox-content">
                                            <div class="feed-activity-list">                                
                                                <div class="feed-element box" id="flowTimeProj">    
                                                    @foreach($email_outs as $email_out)
                                                    <img src="/uploads/users/default.jpg" class="img-circle pull-left" style="width:50px; height:50px;"></img>
                                                    <div class="media-body" style="padding-right:10px;">                                        
                                                        <div style="padding-top:10px;"></div>
                                                        <small class="text-muted"></small>                                                        
                                                            <div class="well">                                                                                                                                                                                                                            
                                                            <strong>To: </strong>{!! $email_out->to !!}<br/>
                                                            <strong>Cc: </strong>{!! $email_out->cc !!}                                                          
                                                            <br />                                                                                                    
                                                            <br />
                                                            <strong>Subject: </strong>{!! $email_out->subject !!}<br/>
                                                            <strong>Details: </strong>{!! $email_out->details !!}<br/>                                                            
                                                            
                                                                <div style="padding-top:30px;">
                                                                    <strong>Attachment: </strong><br/>                                                                                                                        
                                                                </div>                                                                
                                                            <br /><br />                                                        
                                                            <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#addJobOrderCommentModal">
                                                                <i class="fa fa-comment"></i>
                                                            </button>                                                                                                                                                                                                                                                                
                                                            <div style="padding-top: 10px;">                                                                           
                                                                    <img src="/uploads/users/default.jpg" class="img-circle pull-left" style="width:50px; height:50px;"></img>
                                                                
                                                                <div class="media-body" style="padding-right:10px;">                                        
                                                                    <div style="padding-top:10px;"></div>
                                                                    <small class="text-muted"></small>
                                                                    <div class="well" style="border-color: #009688;">
                                                                        <small class="pull-right">
                                                                                                                                                           </small>                                                                                                                                                        
                                                                        <strong>From: </strong>                                                             
                                                                                                                                     
                                                                        <br /><br />
                                                                        <strong>Message: </strong><br/>
                                                                                                                                                                                                                                                            
                                                                        <br />
                                                                        
                                                                            <div style="padding-top:20px;">
                                                                                <strong>Attachment: </strong><br/>                                                                      
                                                                                                                                       
                                                                                                                               
                                                                            </div>      
                                                                         
                                                                                                                    
                                                                    </div>                                                            
                                                                </div>                                                          
                                                            </div>
                                                                                                                  
                                                        </div>                                                                      
                                                      <br />                                                            
                                                    </div>    
                                                    @endforeach                                           
                                                </div>                                     
                                            </div>
                                        </div>
                                    </div>
                                </div>                               
                            </div> 
                        </div>
                    </div>
                </div>
                <div id="tab-alljo" class="tab-pane">
                    <div class="panel-body">
                        <div class="tabs-container">
                            <div class="col-lg-12">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-title" style="background-color:#009688">
                                        <h5 style="color: white">Job Orders</h5> <span class="label label-primary">*</span>
                                        {!! Form::open(array('route'=>'job_orders.exportInProjects','method'=>'POST')) !!}
                                        {!! Form::hidden('project_id',$project->id) !!}
                                        {!! Form::button('<i class="fa fa-download"></i> Download Excel', array('type' => 'submit', 'class' => 'btn btn-warning btn-xs pull-right', 'id'=>'urbtn','value'=>'2','name'=>'type' )) !!}                                          
                                        {!! Form::close() !!}
                                    </div>
                                    <div class="ibox-content">                            
                                        <div class="table-responsive">
                                            <table class="table table-striped table-hover dataTables-example">
                                                <thead>
                                                    <tr>                                                                    
                                                        <th>#</th>      
                                                        <th>Subject</th>                                                        
                                                        <th>Assigned By</th>
                                                        <th>Assigned To</th>
                                                        <th>Start Date</th>                               
                                                        <th>End Date</th>
                                                        <th>Date Return</th>
                                                        <th>Status</th>
                                                        <th>Rating</th>                        
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                       @forelse($jobs as $job)
                                                            @if($date_now > $job->end_date AND $job->status == 1)
                                                                <tr style="background-color: #fab1a0">                  
                                                            @elseif($job->status > 1 AND $job->return_date > $job->end_date)
                                                                <tr style="background-color: #fab1a0">                  
                                                            @else
                                                                <tr>
                                                            @endif                                          
                                                                <td>{!! Html::decode(link_to_route('job_orders.overview', $job->id,$job->encryptname, array())) !!}</td>    
                                                                <td>{!! $job->subject !!}</td>
                                                                <td>
                                                                    {!! strtoupper($job->user->first_name.' '.$job->user->last_name) !!}
                                                                </td>
                                                                <td>
                                                                    {!! $job->userName !!}
                                                                </td>
                                                                <td>{!! $job->start_date !!}</td>
                                                                <td>{!! $job->end_date !!}</td>
                                                                <td>
                                                                    @if(!empty($job->return_date))
                                                                        {!! $job->return_date !!}
                                                                    @else
                                                                        N/A
                                                                    @endif
                                                                </td>
                                                                <td>
                                                                    @if($job->status === 1)
                                                                        <span class="label label-info"><i class="fa fa-clock-o"></i> Ongoing</span>
                                                                    @elseif($job->status === 2)
                                                                        <span class="label label-success"><i class="fa fa-check"></i> Returned</span>
                                                                    @elseif($job->status === 3)
                                                                        <span class="label label-primary"> <i class="fa fa-user"></i>Closed</span>
                                                                    @endif
                                                                </td>                                   
                                                                <td>
                                                                    @if($job->status == 3)
                                                                        @for($i=0; $i < $job->rating; $i++)
                                                                            <i class="fa fa-star"></i>
                                                                        @endfor
                                                                        @for($j=$i; $j < 10; $j++)
                                                                            <i class="fa fa-star-o"></i>
                                                                        @endfor
                                                                    @else
                                                                    N/A
                                                                    @endif
                                                                </td>                                                           
                                                            </tr>
                                                        @empty
                                                        @endforelse
                                                    </tbody>  
                                            </table>
                                        </div>                            
                                    </div>
                                </div>                                                        
                            </div>  
                        </div>
                    </div>
                </div>
                <div id="tab-request" class="tab-pane">
                    <div class="panel-body">
                        <div class="tabs-container">
                            <div class="col-lg-12">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-title" style="background-color:#009688">
                                        <h5 style="color: white">Requests</h5> <span class="label label-primary">*</span>
                                        <div class="ibox-tools">
                                            <a class="collapse-link">
                                                <i class="fa fa-chevron-up"></i>
                                            </a>                               
                                        </div>
                                    </div>
                                    <div class="ibox-content">                            
                                        <div class="table-responsive">
                                            <table class="table table-striped table-hover dataTables-example">
                                                <thead>
                                                    <tr>                                                                    
                                                        <th>REQUEST CODE</th>                                            
                                                        <th>REQUESTOR</th>
                                                        <th>STATUS</th>
                                                        <th>ACTION</th>                        
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($reqs as $reqtome)
                                                        <tr>
                                                            <td>{!! $reqtome->request_no !!}</td>
                                                            <td>{!! strtoupper($reqtome->user->first_name.' '.$reqtome->user->last_name) !!}</td>                                                
                                                            <td>
                                                                @if($reqtome->status == 1)
                                                                    <span class="label label-warning label-xs"> <i class="fa fa-angellist"></i>Pending</span>
                                                                @elseif($reqtome->status == 2)
                                                                    <span class="label label-success label-xs"> <i class="fa fa-angellist"></i>Returned</span>
                                                                @else
                                                                    <span class="label label-primary label-xs"> <i class="fa fa-angellist"></i>Closed</span>
                                                                @endif
                                                            </td>
                                                            <td>                                                           
                                                                {!! Html::decode(link_to_Route('my_requests.show','<i class="fa fa-list"></i> Details', $reqtome->request_code, array('class' => 'btn btn-success btn-xs','target'=>'__blank')))!!}
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>  
                                            </table>
                                        </div>                            
                                    </div>
                                </div>                                                        
                            </div>  
                        </div>
                    </div>
                </div>
                <div id="tab-incident" class="tab-pane">
                    <div class="panel-body">
                        <div class="tabs-container">
                            <div class="row">
                                <div class="col-lg-8">
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-title" style="background-color:#009688">                           
                                            {!! Form::open(array('route'=>'incident_reports.export','method'=>'POST')) !!}                 
                                            <div class="pull-right">                                                                                                        
                                                {!! Form::hidden('project_id',$project->id) !!}
                                                {!! Form::submit('Export', array('type' => 'submit', 'class' => 'btn btn-warning btn-xs')) !!}                                                                                                        
                                                <button type="button" class="btn btn-white btn-xs" data-toggle="modal" data-target="#incidentModal">
                                                    Create IR
                                                </button>
                                            </div>         
                                            {!! Form::close() !!}   
                                        </div>
                                        <div class="ibox-content ibox-heading">
                                            <center><h3><i class="fa fa-comment"> Incident Reports!</i></h3></center>
                                        </div>
                                        <div class="ibox-content">
                                            <div class="feed-activity-list">                                
                                                <div class="feed-element box" id="flowTimeProj">    
                                                    @foreach($incidents as $incident)
                                                    <img src="/uploads/users/default.jpg" class="img-circle pull-left" style="width:50px; height:50px;"></img>
                                                    <div class="media-body" style="padding-right:10px;">                                        
                                                        <div style="padding-top:10px;"></div>
                                                        <small class="text-muted"></small>                                                        
                                                            <div class="well">
                                                            <strong>IR#: </strong> {!! $incident->id !!}<br/>
                                                            <strong>Filed By: </strong> {!! strtoupper($incident->user->first_name.' '.$incident->user->last_name) !!}<br/>
                                                            <strong>Date Created: </strong> {!! $incident->created_at !!}<br/> 
                                                            <strong>Status: </strong>
                                                                @if($incident->status == 1)
                                                                    <span class="label label-info">NEW</span>
                                                                @elseif($incident->status == 2)
                                                                    <span class="label label-warning">PENDING</span>
                                                                @elseif($incident->status == 3)
                                                                    <span class="label label-success">RETURNED</span>
                                                                @else
                                                                    <span class="label label-primary">CLOSED</span>
                                                                @endif
                                                            <br/> 
                                                            <br />                                                            
                                                            <strong>Company: </strong>{!! strtoupper($incident->contact->company) !!} <br/>
                                                            <strong>Project: </strong>{!! strtoupper($incident->project->project_name) !!} <br/>
                                                            <strong>Client Coordinator: </strong>{!! strtoupper($incident->client_coor) !!} <br/><br/> 
                                                            <strong>Subject: </strong>{!! strtoupper($incident->subject) !!} <br/>
                                                            <strong>Details: </strong> <br/><br/>                                                            
                                                                {!! $incident->details !!}
                                                            @if(count($incident->files) > 0)
                                                                <div style="padding-top:30px;">
                                                                    <strong>Attachment: </strong><br/>                                                    
                                                                    @forelse($incident->files as $thread_file)                                                                                                                                                                 
                                                                        {!! Form::open(array('route'=>'incident_reports.download_files','method'=>'POST')) !!}
                                                                        {!! Form::hidden('encname',$thread_file->encryptname) !!}
                                                                        {!! Form::submit($thread_file->filename, array('type' => 'submit', 'class' => 'btn btn-primary btn-xs')) !!}                                                        
                                                                        {!! Form::close() !!}                                                           
                                                                    @empty
                                                                    @endforelse                                                    
                                                                </div>      
                                                            @else                                                                                       
                                                            @endif                                                                              
                                                            <br /><br />           
                                                            @if($incident->status < 2)                                             
                                                                <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#incidentCommentModal" id="{{$incident->id}}" value="{{$incident->id}}" onclick="IncidentCommentFunction(this)">
                                                                    <i class="fa fa-comment"></i>
                                                                </button> 
                                                                <button type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#incidentSolutionModal" id="{{$incident->id}}" value="{{$incident->id}}" onclick="IncidentSolutionFunction(this)">
                                                                    <i class="fa fa-reply"></i>
                                                                </button>            
                                                            @endif
                                                            @if($incident->status == 3 AND $allow_close == 1)
                                                                <button type="button" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#incidentClosedModal" id="{{$incident->id}}" value="{{$incident->id}}" onclick="IncidentClosedFunction(this)">
                                                                    <i class="fa fa-stop"></i>
                                                                </button>     
                                                            @endif
                                                            @if(!empty($incident->solution))                                    
                                                                <div style="padding-top: 10px;">                                                                           
                                                                        <img src="/uploads/users/default.jpg" class="img-circle pull-left" style="width:50px; height:50px;"></img>                                                                        
                                                                    <div class="media-body" style="padding-right:10px;">                                        
                                                                        <div style="padding-top:10px;"></div>
                                                                        <small class="text-muted"></small>
                                                                        <div class="well" style="border-color: #009688;">
                                                                            <small class="pull-right">                                                                                    
                                                                                {!! $incident->resolved_date !!}
                                                                            </small>                                                                                                                                                        
                                                                            <strong><h3 style="color: green;">Final Solution </h3></strong><br/>     
                                                                            <strong>From: </strong>{!! strtoupper($incident->resolved->first_name.' '.$incident->resolved->last_name) !!}                                                                                                                                                          
                                                                            <br /><br />
                                                                            <strong>Final Solution: </strong><br/>                                                                            
                                                                            {!! $incident->solution !!}
                                                                            <br />
                                                                            @if(count($incident->solution_file) > 0)
                                                                                <div style="padding-top:30px;">
                                                                                    <strong>Attachment: </strong><br/>                                                    
                                                                                    @forelse($incident->solution_file as $solution_file)                                                                                                                                                                 
                                                                                        {!! Form::open(array('route'=>'incident_reports.download_solution_files','method'=>'POST')) !!}
                                                                                        {!! Form::hidden('encname',$solution_file->encryptname) !!}
                                                                                        {!! Form::hidden('ir_id',$incident->id) !!}
                                                                                        {!! Form::submit($solution_file->filename, array('type' => 'submit', 'class' => 'btn btn-primary btn-xs')) !!}                                                        
                                                                                        {!! Form::close() !!}                                                           
                                                                                    @empty
                                                                                    @endforelse                                                    
                                                                                </div>      
                                                                            @else                                                                                       
                                                                            @endif     
                                                                            @if($incident->closed_by > 0)                                                                                                                
                                                                            <br /><br />
                                                                            <strong>Closed By: </strong>{!! strtoupper($incident->closed->first_name.' '.$incident->closed->last_name) !!}<br />
                                                                            <strong>Rating: </strong>
                                                                            @for($i=0; $i < $incident->ratings; $i++)
                                                                            <i class="fa fa-star"></i>
                                                                            @endfor
                                                                            @for($j=$i; $j < 10; $j++)
                                                                                <i class="fa fa-star-o"></i>
                                                                            @endfor
                                                                            <br />
                                                                            <strong>Remarks: </strong><br />
                                                                            {!! $incident->final_remarks !!}

                                                                            @endif                                                                            
                                                                        </div>                                                            
                                                                    </div>                                                          
                                                                </div>
                                                            @endif
                                                            @if(!empty($incident->threads))
                                                                @foreach($incident->threads as $comment)                                                                                                                                                                                                                                                             
                                                                    <div style="padding-top: 10px;">                                                                           
                                                                            <img src="/uploads/users/default.jpg" class="img-circle pull-left" style="width:50px; height:50px;"></img>                                                                        
                                                                        <div class="media-body" style="padding-right:10px;">                                        
                                                                            <div style="padding-top:10px;"></div>
                                                                            <small class="text-muted"></small>
                                                                            <div class="well" style="border-color: #009688;">
                                                                                <small class="pull-right">                                                                                    
                                                                                    {!! $comment->created_at->format('M-d-Y H:i:s A') !!}
                                                                                </small>                                                                                                                                                        
                                                                                <strong>From: </strong>{!! strtoupper($comment->user->first_name.' '.$comment->user->last_name) !!}                                                                                                                                                          
                                                                                <br /><br />
                                                                                <strong>Message: </strong><br/>                                                                            
                                                                                {!! $comment->details !!}
                                                                                <br />
                                                                                @if(count($comment->thread_file) > 0)
                                                                                    <div style="padding-top:30px;">
                                                                                        <strong>Attachment: </strong><br/>                                                    
                                                                                        @forelse($comment->thread_file as $thread_file2)                                                                                                                                                                 
                                                                                            {!! Form::open(array('route'=>'incident_reports.download_thread_files','method'=>'POST')) !!}
                                                                                            {!! Form::hidden('encname',$thread_file2->encryptname) !!}
                                                                                            {!! Form::hidden('ir_id',$incident->id) !!}
                                                                                            {!! Form::submit($thread_file2->filename, array('type' => 'submit', 'class' => 'btn btn-primary btn-xs')) !!}                                                        
                                                                                            {!! Form::close() !!}                                                           
                                                                                        @empty
                                                                                        @endforelse                                                    
                                                                                    </div>      
                                                                                @else                                                                                       
                                                                                @endif                                             
                                                                            </div>                                                            
                                                                        </div>                                                          
                                                                    </div>
                                                                @endforeach
                                                            @endif
                                                        </div>                                                                      
                                                      <br />                                                            
                                                    </div>    
                                                    @endforeach                                           
                                                </div>                                     
                                            </div>
                                        </div>
                                    </div>
                                </div>                               
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 
</div>
    

<div class="modal inmodal" id="workgroupAddModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-users modal-icon"></i>
                <h4 class="modal-title">ADD WORKGROUP MEMBER</h4>                    
            </div>              
            {!! Form::open(array('route'=>'project_workgroups.store','method'=>'POST','files'=>true)) !!}                                                                               
            <div class="modal-body">                                                 
                <div class="row" style="padding-top:10px">
                    <!-- <div class="col-lg-6">
                        {!! Form::label('department','Department')!!}
                        {!! Form::select('department[]',$departments,'',['class'=>'form-control','id'=>'deptid','multiple'=>'multiple']) !!}                                  
                    </div>   -->    
                    <div class="col-lg-6">
                        {!! Form::label('user_id','Name')!!}
                        {!! Form::select('user_id[]',$users,'',['class'=>'form-control','id'=>'user_id','multiple'=>'multiple']) !!}                     
                    </div>  
                </div>                                    
            </div>
            <div class="modal-footer">   
                {!! Form::hidden('contact_id',$project->contact_id) !!}
                {!! Form::hidden('project_id',$project->id,['id'=>'proj_id']) !!}
                {!! Form::button('Close', array('type' => 'submit', 'class' => 'btn btn-white','data-dismiss'=>'modal')) !!}            
                {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'button', 'class' => 'btn btn-primary hidden sbalert','id'=>'aurbtn')) !!}
                {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'submit', 'class' => 'btn btn-primary hidden', 'id'=>'aurbtn2')) !!}                    
            </div>   
            {!! Form::close() !!}                           
        </div>
    </div>
</div>
<div class="modal inmodal" id="workgroupRemoveModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-users modal-icon"></i>
                <h4 class="modal-title">REMOVE WORKGROUP MEMBER</h4>                    
            </div>              
            {!! Form::open(array('route'=>'project_workgroups.remove_member','method'=>'POST','files'=>true)) !!}                                                                               
            <div class="modal-body">                                                 
                <div class="row" style="padding-top:10px">                    
                    <div class="col-lg-6">
                        {!! Form::label('user_id','Name')!!}                        
                    </div>  
                </div>                                    
            </div>
            <div class="modal-footer">   
                {!! Form::hidden('contact_id',$project->contact_id) !!}
                {!! Form::hidden('project_id',$project->id,['id'=>'proj_id']) !!}
                {!! Form::button('Close', array('type' => 'submit', 'class' => 'btn btn-white','data-dismiss'=>'modal')) !!}            
                {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'button', 'class' => 'btn btn-primary hidden sbalert','id'=>'urbtn')) !!}
                {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'submit', 'class' => 'btn btn-primary hidden', 'id'=>'urbtn2')) !!}                    
            </div>   
            {!! Form::close() !!}                           
        </div>
    </div>
</div>
<div class="modal inmodal" id="threadModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-comment modal-icon"></i>
                <h4 class="modal-title">New Thread</h4>                    
            </div>              
            {!! Form::open(array('route'=>'project_threads.store','method'=>'POST','files'=>true,'enctype'=>'multipart/form-data')) !!}
                <div class="modal-body">                            
                    <div class="row">                  
                        {!! Form::label('message','Message') !!}                    
                        {!! Form::textarea('message','',['class'=>'form-control','placeholder'=>'Enter it here....','required'=>'required','id'=>'threadmessageid']) !!}
                    </div>                             
                    <div class="row" style="padding-top:5px;">
                        <div class="col-lg-12">                  
                            {!! Form::label('attached','Attached File')!!}
                            {!! Form::file('attached[]', array('id' => 'thread_inputs', 'class' => 'photo_files', 'accept' => 'pdf|docx')) !!}                   
                        </div>
                    </div>                                    
                </div>
                <div class="modal-footer">                 
                    {!! Form::hidden('proj_id',$project->id) !!}                                                 
                    {!! Form::button('Close', array('type' => 'submit', 'class' => 'btn btn-white','data-dismiss'=>'modal')) !!}                    
                    {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'button', 'class' => 'btn btn-primary sbalert','id'=>'threadbtn')) !!}                    
                    <button class="btn btn-primary hidden" id="threadbtn2" value="Submit" onclick="this.disabled=true; this.form.submit();">Submit</button>                    
                </div>                  
            {!! Form::close() !!}
        </div>
    </div>
</div>
<div class="modal inmodal" id="addCommentModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content animated fadeIn">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <i class="fa fa-comment modal-icon"></i>
                    <h4 class="modal-title">Comment</h4>                    
                </div>              
                {!! Form::open(array('route'=>'project_threads.comment','method'=>'POST','files'=>true,'enctype'=>'multipart/form-data')) !!}
                    <div class="modal-body">                                 
                        <div class="row">                  
                            {!! Form::label('message','Comment') !!}                    
                            {!! Form::textarea('message','',['class'=>'form-control','placeholder'=>'Enter it here....','required'=>'required','id'=>'commentmessageid']) !!}
                        </div>                                                                              
                        <div class="row" style="padding-top:5px;">
                            <div class="col-lg-12">                  
                                {!! Form::label('attached','Attached File')!!}
                                {!! Form::file('attached[]', array('id' => 'comment_inputs', 'class' => 'photo_files', 'accept' => 'pdf|docx')) !!}                   
                            </div>
                        </div>       
                    </div>
                    <div class="modal-footer">                                              
                        {!! Form::hidden('thread_id','',['id'=>'mainthread_id']) !!}
                        {!! Form::button('Close', array('type' => 'submit', 'class' => 'btn btn-white','data-dismiss'=>'modal')) !!}              
                        {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'button', 'class' => 'btn btn-primary sbalert','id'=>'commentbtn')) !!}                              
                        <button class="btn btn-primary hidden" id="commentbtn2" value="Submit" onclick="this.disabled=true; this.form.submit();">Submit</button>                    
                    </div>                  
                {!! Form::close() !!}
        </div>

    </div>
</div>
<div class="modal inmodal" id="contactpersonModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content animated fadeIn">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <i class="fa fa-comment modal-icon"></i>
                    <h4 class="modal-title">New Contact Person</h4>                    
                </div>              
                {!! Form::open(array('route'=>'contacts.add_person','method'=>'POST','files'=>true,'enctype'=>'multipart/form-data')) !!}
                    <div class="modal-body">                                 
                        <div class="row">                  
                            <div class="col-lg-6">                                                            
                                {!! Form::label('contact_person','Name') !!}                    
                                {!! Form::text('contact_person','',['class'=>'form-control','placeholder'=>'Enter it here....','required'=>'required','id'=>'commentmessageid']) !!}
                            </div>
                        </div>                            
                        <div class="row">              
                            <div class="col-lg-6">                                                                
                            {!! Form::label('function','Function') !!}                    
                            {!! Form::text('function','',['class'=>'form-control','placeholder'=>'Enter it here....','required'=>'required','id'=>'commentmessageid']) !!}
                            </div>
                        </div>                                                                              
                        <div class="row">                  
                            <div class="col-lg-6">                                                            
                            {!! Form::label('email','Email') !!}                    
                            {!! Form::text('email','',['class'=>'form-control','placeholder'=>'Enter it here....','required'=>'required','id'=>'commentmessageid']) !!}
                            </div>
                        </div>                            
                        <div class="row">                  
                            <div class="col-lg-6">                                                            
                            {!! Form::label('mobile_number','Mobile Number') !!}                    
                            {!! Form::text('mobile_number','',['class'=>'form-control','placeholder'=>'Enter it here....','required'=>'required','id'=>'commentmessageid']) !!}
                            </div>
                        </div>                                                    
                    </div>
                    <div class="modal-footer">                                              
                        {!! Form::hidden('contact_id',$project->contact_id) !!}
                        {!! Form::hidden('project_id',$project->id) !!}
                        {!! Form::button('Close', array('type' => 'submit', 'class' => 'btn btn-white','data-dismiss'=>'modal')) !!}              
                        {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'button', 'class' => 'btn btn-primary sbalert','id'=>'addpersonbtn')) !!}                              
                        <button class="btn btn-primary hidden" id="addpersonbtn2" value="Submit" onclick="this.disabled=true; this.form.submit();">Submit</button>                    
                    </div>                  
                {!! Form::close() !!}
        </div>
    </div>
</div>
<div class="modal inmodal" id="jobOrderModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-briefcase modal-icon"></i>
                <h4 class="modal-title">Job Order</h4>                    
            </div>              
                <div class="modal-body">                 
                    {!! Form::open(array('route'=>'job_orders.store','method'=>'POST','files'=>true)) !!}    
                        <div class="row" style="padding-top:50">                    
                            <div class="col-lg-8">
                                {!! Form::label('timetable_id','Topic')!!}    
                                {!! Form::select('timetable_id',$topics,'',['class'=>'form-control','required'=>'required','id'=>'topic_id','placeholder'=>'Select Topic']) !!}                    
                            </div>                      
                        </div>                                                                                 
                        <div class="row" style="padding-top:10">                    
                            <div class="col-lg-8">
                                {!! Form::label('task_id','Task')!!}    
                                {!! Form::select('task_id',$taskList,'',['class'=>'form-control','required'=>'required','id'=>'task_id']) !!}                    
                            </div>                      
                        </div>                                                                                 
                        <div class="row" style="padding-top:10;">                                    
                            <div class="col-lg-8">
                                {!! Form::label('reciever','Send To') !!}
                                {!! Form::select('reciever[]',$users_jo,'',['class'=>'form-control','multiple'=>'multiple','id'=>'userID','required'=>'required']) !!}                                    
                            </div>                           
                        </div>               
                        <div class="row" style="padding-top:10px ">
                            <div class="col-lg-6">
                                {!! Form::label('settings','Settings')!!}                 
                                {!! Form::select('settings',$settings,'',['class'=>'form-control','id'=>'settings'])!!}
                            </div>      
                        </div>          
                        <div class="row" style="padding-top:10px ">
                            <div class="col-lg-6">
                                {!! Form::label('type2','Scope')!!}                 
                                {!! Form::select('type2',$type2,'',['class'=>'form-control','id'=>'type2'])!!}
                            </div>      
                        </div>                                                
                        <div class="row-1" style="padding-top:10px;" id="normdate">
                            {!!Form::label('date','Date')!!}         
                            <div class="form-group" id="date_range">                                                
                                <div class="input-daterange input-group" id="datepicker">
                                    <input type="text" class="input-md form-control" name="start_date" value="{{$date_now}}" required="" />
                                    <span class="input-group-addon">to</span>
                                    <input type="text" class="input-md form-control" name="end_date" value="{{$date_now}}" required="" />
                                </div>
                            </div>                                
                        </div>     
                        <div class="row hidden" style="padding-top:10px;" id="week">
                            <div class="col-lg-6">
                                {!! Form::label('weeks','What Day?')!!}                 
                                {!! Form::select('weeks',$everyweek,'',['class'=>'form-control'])!!}
                            </div>                            
                        </div>            
                        <div class="row" style="padding-top:30px;">
                            <div class="col-lg-12">
                                {!!Form::label('subject','Subject')!!}         
                                {!! Form::text('subject','',['class'=>'form-control','required'=>'required']) !!}
                            </div>
                        </div>                                                        
                        <div class="row" style="padding-top:10px;">
                            <div class="col-lg-12">
                                {!!Form::label('details','Details')!!}         
                                {!! Form::textarea('details','',['class'=>'form-control','id'=>'job_order_details','required'=>'required']) !!}
                            </div>
                        </div>                    
                        <div class="row" style="padding-top:10px;">
                            <div class="col-lg-12">                            
                                {!!Form::label('attached','Attached File')!!}                             
                                {!! Form::file('attached[]', array('id' => 'joinputs', 'class' => 'photo_files', 'accept' => 'pdf|docx')) !!}                   
                            </div>
                        </div>                    
                </div>
                <div class="modal-footer">                                           
                    {!! Form::hidden('contact_id',$project->contact_id) !!}
                    {!! Form::hidden('project_id',$project->id) !!}                                
                    {!! Form::button('Close', array('type' => 'submit', 'class' => 'btn btn-white','data-dismiss'=>'modal')) !!}            
                    {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'button', 'class' => 'btn btn-primary','id'=>'jobtn')) !!}
                    {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'submit', 'class' => 'btn btn-primary hidden', 'id'=>'jobtn2')) !!}                    
                </div>   
            {!! Form::close() !!}                           
        </div>
    </div>
</div>
<div class="modal inmodal" id="ratingtModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-users modal-icon"></i>
                <h4 class="modal-title">RATE THIS JOB ORDER BEFORE CLOSING IT</h4>                    
            </div>              
            {!! Form::open(array('route'=>'job_orders.rating','method'=>'POST','files'=>true)) !!}                                                                               
            <div class="modal-body">                                                 
                <div class="row" style="padding-top:10px">                    
                    <div class="col-lg-4">
                        {!! Form::label('rating','Rate')!!}                        
                        {!! Form::select('rating',$ratings,'',['class'=>'form-control']) !!}
                    </div>                      
                    <div class="col-lg-12">
                        {!! Form::label('rating_remarks','Remarks')!!}                        
                        {!! Form::textarea('rating_remarks','',['class'=>'form-control','placeholder'=>'Remarks','required'=>'required','id'=>'rateremID']) !!}
                    </div>  
                </div>                                    
            </div>
            <div class="modal-footer">                   
                {!! Form::hidden('job_id','',['id'=>'jobID']) !!}
                {!! Form::hidden('status','3') !!}
                {!! Form::button('Close', array('type' => 'submit', 'class' => 'btn btn-white','data-dismiss'=>'modal')) !!}            
                {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'button', 'class' => 'btn btn-primary sbalert','id'=>'rtbtn')) !!}
                {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'submit', 'class' => 'btn btn-primary hidden', 'id'=>'rtbtn2')) !!}                    
            </div>   
            {!! Form::close() !!}                           
        </div>
    </div>
</div>
<div class="modal inmodal" id="addJobOrderCommentModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content animated fadeIn">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <i class="fa fa-comment modal-icon"></i>
                    <h4 class="modal-title">Job Order Comment</h4>                    
                </div>              
                {!! Form::open(array('route'=>'job_order_threads.comment','method'=>'POST','files'=>true,'enctype'=>'multipart/form-data')) !!}
                    <div class="modal-body">                           
                        <div class="row">
                            <div class="col-lg-4">
                                {!! Form::label('email','Option') !!}
                                {!! Form::select('option',$wemail,'',['class'=>'form-control','id'=>'jobcomoptionID']) !!}
                            </div>                                                
                        </div>     
                        <div class="row">
                            <div class="col-lg-6 hidden" id="jobcomEMAIL">
                                {!! Form::label('email','Email') !!}                    
                                {!! Form::label('email','*For multiple email, please use ";" at the end of each email address',['style'=>'color:red']) !!}                    
                                {!! Form::text('email','',['class'=>'form-control','placeholder'=>'Enter email. Use ; for multiple email','required'=>'required','id'=>'jobcomemailID']) !!}    
                            </div>                        
                        </div>
                        <div class="row">
                            <div class="col-lg-6 hidden" id="jobcomSUBJ">
                                {!! Form::label('subject','Subject') !!}                    
                                {!! Form::text('subject','',['class'=>'form-control','placeholder'=>'Enter Subject.','required'=>'required','id'=>'jobcomsubjID']) !!}    
                            </div>                        
                        </div>      
                        <div class="row">                  
                            {!! Form::label('message','Comment') !!}                    
                            {!! Form::textarea('message','',['class'=>'form-control','placeholder'=>'Enter it here....','required'=>'required','id'=>'jobcommentmessageid']) !!}
                        </div>                                                                              
                        <div class="row" style="padding-top:5px;">
                            <div class="col-lg-12">                  
                                {!! Form::label('attached','Attached File')!!}
                                {!! Form::file('attached[]', array('id' => 'comment_inputs', 'class' => 'photo_files', 'accept' => 'pdf|docx')) !!}                   
                            </div>
                        </div>       
                    </div>
                    <div class="modal-footer">                                              
                        {!! Form::hidden('thread_id','',['id'=>'joborderthread_id']) !!}
                        {!! Form::button('Close', array('type' => 'submit', 'class' => 'btn btn-white','data-dismiss'=>'modal')) !!}              
                        {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'button', 'class' => 'btn btn-primary sbalert','id'=>'jobcommentbtn')) !!}                              
                        <button class="btn btn-primary hidden" id="jobcommentbtn2" value="Submit" onclick="this.disabled=true; this.form.submit();">Submit</button>                    
                    </div>                  
                {!! Form::close() !!}
        </div>

    </div>
</div>
<div class="modal inmodal" id="emailModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-comment modal-icon"></i>
                <h4 class="modal-title">Email</h4>                    
            </div>              
            {!! Form::open(array('route'=>'email_outs.store','method'=>'POST','files'=>true,'enctype'=>'multipart/form-data')) !!}
                <div class="modal-body">                                                   
                    <div class="row">
                        <div class="col-lg-12">                                            
                            {!! Form::label('to','To') !!}
                            {!! Form::label('to','*For multiple email, please use ";" at the end of each email address',['style'=>'color:red']) !!}                    
                            {!! Form::text('to','',['class'=>'form-control','placeholder'=>'Enter email. Use ; for multiple email','required'=>'required','id'=>'emailtoID']) !!}    
                        </div>                        
                        
                    </div>
                    <div class="row">
                        <div class="col-lg-12">                            
                            {!! Form::label('cc','CC') !!}
                            {!! Form::label('cc','*For multiple email, please use ";" at the end of each email address',['style'=>'color:red']) !!}                    
                            {!! Form::text('cc','',['class'=>'form-control','placeholder'=>'Enter email. Use ; for multiple email','id'=>'emailccID']) !!}    
                        </div>                        
                    </div>
                    <div class="row">
                        <div class="col-lg-6" id="comSUBJ">
                            {!! Form::label('subject','Subject') !!}                    
                            {!! Form::text('subject','',['class'=>'form-control','placeholder'=>'Enter Subject.','required'=>'required','id'=>'comsubjID']) !!}    
                        </div>                        
                    </div>      
                    <div class="row">                  
                        {!! Form::label('message','Content') !!}                    
                        {!! Form::textarea('message','',['class'=>'form-control','placeholder'=>'Enter it here....','required'=>'required','id'=>'emailMessageID']) !!}
                    </div>                                                                              
                    <div class="row" style="padding-top:5px;">
                        <div class="col-lg-12">                  
                            {!! Form::label('attached','Attached File')!!}
                            {!! Form::file('attached[]', array('id' => 'comment_inputs', 'class' => 'photo_files', 'accept' => 'pdf|docx')) !!}                   
                        </div>
                    </div>       
                </div>
                <div class="modal-footer">                                              
                    {!! Form::hidden('contact_id',$project->contact_id) !!}
                    {!! Form::hidden('project_id',$project->id) !!}
                    {!! Form::button('Close', array('type' => 'submit', 'class' => 'btn btn-white','data-dismiss'=>'modal')) !!}              
                    {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'button', 'class' => 'btn btn-primary','id'=>'messageBtn')) !!}                              
                    <button class="btn btn-primary hidden" id="emailsentBtn" value="Submit" onclick="this.disabled=true; this.form.submit();">Submit</button>                    
                </div>                  
            {!! Form::close() !!}
        </div>
    </div>
</div>
<div class="modal inmodal" id="incidentModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-comment modal-icon"></i>
                <h4 class="modal-title">Incident Report</h4>                    
            </div>              
            {!! Form::open(array('route'=>'incident_reports.store','method'=>'POST','files'=>true,'enctype'=>'multipart/form-data')) !!}
                <div class="modal-body">                                                   
                    
                    <div class="row">                        
                        {!! Form::label('client_coor','Client Coordinator') !!}                    
                        {!! Form::text('client_coor','',['class'=>'form-control','placeholder'=>'Enter Client Coordinator.','required'=>'required','id'=>'incidentCoor']) !!}                            
                    </div>      
                    <div class="row">                        
                        {!! Form::label('subject','Subject') !!}                    
                        {!! Form::text('subject','',['class'=>'form-control','placeholder'=>'Enter Subject.','required'=>'required','id'=>'incidentSubj']) !!}                            
                    </div>      
                    <div class="row">                  
                        {!! Form::label('details','Details') !!}                    
                        {!! Form::textarea('details','',['class'=>'form-control','placeholder'=>'Enter it here....','required'=>'required','id'=>'incidentDetails']) !!}
                    </div>                                                                              
                    <div class="row" style="padding-top:5px;">
                        <div class="col-lg-12">                  
                            {!! Form::label('attached','Attached File')!!}
                            {!! Form::file('attached[]', array('id' => 'comment_inputs', 'class' => 'photo_files', 'accept' => 'pdf|docx')) !!}                   
                        </div>
                    </div>       
                </div>
                <div class="modal-footer">                                              
                    {!! Form::hidden('contact_id',$project->contact_id) !!}
                    {!! Form::hidden('project_id',$project->id) !!}
                    {!! Form::button('Close', array('type' => 'submit', 'class' => 'btn btn-white','data-dismiss'=>'modal')) !!}              
                    {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'button', 'class' => 'btn btn-primary','id'=>'incidentBtn')) !!}                              
                    <button class="btn btn-primary hidden" id="incidenttBtn2" value="Submit" onclick="this.disabled=true; this.form.submit();">Submit</button>                    
                </div>                  
            {!! Form::close() !!}
        </div>
    </div>
</div>
<div class="modal inmodal" id="incidentCommentModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-comment modal-icon"></i>
                <h4 class="modal-title">Incident Comment</h4>                    
            </div>              
            {!! Form::open(array('route'=>'incident_reports.comment_store','method'=>'POST','files'=>true,'enctype'=>'multipart/form-data')) !!}
                <div class="modal-body">                                                                                       
                    <div class="row">                  
                        {!! Form::label('details','Details') !!}                    
                        {!! Form::textarea('details','',['class'=>'form-control','placeholder'=>'Enter it here....','required'=>'required','id'=>'incidentThreadDetails']) !!}
                    </div>                                                                              
                    <div class="row" style="padding-top:5px;">
                        <div class="col-lg-12">                  
                            {!! Form::label('attached','Attached File')!!}
                            {!! Form::file('attached[]', array('id' => 'comment_inputs', 'class' => 'photo_files', 'accept' => 'pdf|docx')) !!}                   
                        </div>
                    </div>       
                </div>
                <div class="modal-footer">                                              
                    {!! Form::hidden('contact_id',$project->contact_id) !!}
                    {!! Form::hidden('project_id',$project->id) !!}
                    {!! Form::hidden('ir_id','',['id'=>'IRID']) !!}
                    {!! Form::button('Close', array('type' => 'submit', 'class' => 'btn btn-white','data-dismiss'=>'modal')) !!}              
                    {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'button', 'class' => 'btn btn-primary','id'=>'incidentThreadBtn')) !!}                              
                    <button class="btn btn-primary hidden" id="incidentThreadBtn2" value="Submit" onclick="this.disabled=true; this.form.submit();">Submit</button>                    
                </div>                  
            {!! Form::close() !!}
        </div>
    </div>
</div>
<div class="modal inmodal" id="incidentSolutionModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-comment modal-icon"></i>
                <h4 class="modal-title">Incident Final Solution</h4>                    
            </div>              
            {!! Form::open(array('route'=>'incident_reports.solution_store','method'=>'POST','files'=>true,'enctype'=>'multipart/form-data')) !!}
                <div class="modal-body">                                                                                       
                    <div class="row">                  
                        {!! Form::label('solution','Details') !!}                    
                        {!! Form::textarea('solution','',['class'=>'form-control','placeholder'=>'Enter it here....','required'=>'required','id'=>'incidentThreadDetails']) !!}
                    </div>                                                                              
                    <div class="row" style="padding-top:5px;">
                        <div class="col-lg-12">                  
                            {!! Form::label('attached','Attached File')!!}
                            {!! Form::file('attached[]', array('id' => 'comment_inputs', 'class' => 'photo_files', 'accept' => 'pdf|docx')) !!}                   
                        </div>
                    </div>       
                </div>
                <div class="modal-footer">                                              
                    {!! Form::hidden('contact_id',$project->contact_id) !!}
                    {!! Form::hidden('project_id',$project->id) !!}
                    {!! Form::hidden('ir_id','',['id'=>'IRSID']) !!}
                    {!! Form::button('Close', array('type' => 'submit', 'class' => 'btn btn-white','data-dismiss'=>'modal')) !!}              
                    {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'button', 'class' => 'btn btn-primary','id'=>'incidentThreadSBtn')) !!}                              
                    <button class="btn btn-primary hidden" id="incidentThreadSBtn2" value="Submit" onclick="this.disabled=true; this.form.submit();">Submit</button>                    
                </div>                  
            {!! Form::close() !!}
        </div>
    </div>
</div>
<div class="modal inmodal" id="incidentClosedModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-comment modal-icon"></i>
                <h4 class="modal-title">Incident Closing</h4>                    
            </div>              
            {!! Form::open(array('route'=>'incident_reports.closing_store','method'=>'POST','files'=>true,'enctype'=>'multipart/form-data')) !!}
                <div class="modal-body">        
                    <div class="row">
                        <div class="col-lg-6">
                            {!! Form::label('ratings','Rating') !!}
                            {!! Form::select('ratings',$ratings,'',['class' => 'form-control']) !!}
                        </div>                        
                    </div>                                                                               
                    <div class="row">                  
                        {!! Form::label('final_remarks','Final Remarks') !!}                    
                        {!! Form::textarea('final_remarks','',['class'=>'form-control','placeholder'=>'Enter it here....','required'=>'required','id'=>'incidentThreadDetails']) !!}
                    </div>                                                                                                  
                </div>
                <div class="modal-footer">                                              
                    {!! Form::hidden('contact_id',$project->contact_id) !!}
                    {!! Form::hidden('project_id',$project->id) !!}
                    {!! Form::hidden('ir_id','',['id'=>'IRCID']) !!}
                    {!! Form::button('Close', array('type' => 'submit', 'class' => 'btn btn-white','data-dismiss'=>'modal')) !!}              
                    {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'button', 'class' => 'btn btn-primary','id'=>'incidentThreadCBtn')) !!}                              
                    <button class="btn btn-primary hidden" id="incidentThreadCBtn2" value="Submit" onclick="this.disabled=true; this.form.submit();">Submit</button>                    
                </div>                  
            {!! Form::close() !!}
        </div>
    </div>
</div>
@stop
@section('page-script')

$('#deptid,#user_id,#ex_user_id,#handle_by,#leaddeptid,#userID').multiselect({
    maxHeight: 200,    
    includeSelectAllOption: true,
    enableCaseInsensitiveFiltering: true,
    enableFiltering: true,
    buttonWidth: '100%',
    buttonClass: 'form-control'        
});
$('#topic_id,#task_id').multiselect({
    maxHeight: 200,    
    includeSelectAllOption: false,
    enableCaseInsensitiveFiltering: true,
    enableFiltering: true,
    buttonWidth: '100%',
    buttonClass: 'form-control'        
});
$('#comment_inputs,#thread_inputs').filer({

    showThumbs:true,
    addMore:true
});


$('#deptid').on("change", function(){

    var dept_id = $('#deptid').val();                           
    var proj_id = $('#proj_id').val();
    $.ajax({
        type:"POST",
        dataType: 'json',
        data: {dept_id: dept_id, proj_id: proj_id},
        url: "../../projects/getusers",
        success: function(data){            

            $('select#user_id').empty();
            
            $.each(data, function(i, text) {
                $('<option />',{value: i, text: text}).appendTo($('select#user_id'));
            });          
            var user = $('#user_id').val();       
            
            if(user != ""){                
                $('#aurbtn').removeClass('hidden');
            }else{
                $('#aurbtn').addClass('hidden');
            }        

            $('select#user_id').multiselect('rebuild');
        }
    });      
});    
$('#user_id').on("change", function(){

   var user2 = $('#user_id').val();       
            
    if(user2 != ""){                
        $('#aurbtn').removeClass('hidden');
    }else{
        $('#aurbtn').addClass('hidden');
    }        
});    
$('#aurbtn').click(function () {

    if($('#user_id').val() != ""){
        
        swal({
            title: "Allright!",
            text: "I'm now going to add this to workgroup!",
            type: "success"
            });            
        $('#aurbtn2').click();            
    }else{

        swal({
            title: "Warning",
            text: "Oh Come on! Select at least one member before you submit!",
            type: "warning"
        });      
    }                            
});
$('#leaddeptid').on("change", function(){

    var dept_id = $('#leaddeptid').val();                               
    $.ajax({
        type:"POST",
        dataType: 'json',
        data: {dept_id: dept_id},
        url: "../../projects/getleadusers",
        success: function(data){            

            $('select#handle_by').empty();
            
            $.each(data, function(i, text) {
                $('<option />',{value: i, text: text}).appendTo($('select#handle_by'));
            });                          
        
            var user = $('#handle_by').val();                 
            if(user != ""){                
                $('#leadbtn').removeClass('hidden');
            }else{
                $('#leadbtn').addClass('hidden');
            }

            $('select#handle_by').multiselect('rebuild');
        }
    });      
}); 

$('#handle_by').on("change", function(){

   var user2 = $('#handle_by').val();       
            
    if(user2 != ""){                
        $('#leadbtn').removeClass('hidden');
    }else{
        $('#leadbtn').addClass('hidden');
    }        
});  
$('#leadbtn').click(function () {

    if($('#handle_by').val() != ""){
        
        swal({
            title: "Got it!",
            text: "I'm now going to assign this topic!",
            type: "success"
            });            
        $('#leadbtn2').click();            
    }else{

        swal({
            title: "Warning",
            text: "Hey! Select a lead for this topic before you submit!",
            type: "warning"
        });      
    }                            
});

$('#bombtn').click(function () {
    
    
    if($('#bomfileid').val() != ""){
        
        swal({
            title: "Got it!",
            text: "Your BOM are now Uploading!",
            type: "success"
            });            
        $('#bombtn2').click();            
    }else{

        swal({
            title: "Warning",
            text: "Yow! Attach Your BOM File!",
            type: "warning"
        });      
    }                            
});

$('#bomupdatebtn').click(function () {
       
    swal({
        title: "Got it!",
        text: "Your BOM are now Uploading!",
        type: "success"
        });            
    $('#bomupdatebtn2').click();    
});

$('#threadbtn').click(function () {

    if($('#threadmessageid').val() != ""){        
        
        swal({
            title: "That's Great!",
            text: "Please wait....!",
            type: "success"
        });            
        $('#threadbtn2').click();                         
    }else{
        swal({
            title: "Warning",
            text: "Oooopss! You forgot to put a message!",
            type: "warning"
        });      
    }                            
});
$('#commentbtn').click(function () {

    if($('#commentmessageid').val() != ""){        
        
        swal({
            title: "That's Great!",
            text: "Please wait....!",
            type: "success"
        });            
        $('#commentbtn2').click();                         
    }else{
        swal({
            title: "Warning",
            text: "Oooopss! You forgot to put a message!",
            type: "warning"
        });      
    }                            
});
$('#messageBtn').click(function () {
    
    if($('#emailtoID').val() != ""){        
        alert('y');
        swal({
            title: "That's Great!",
            text: "Please wait....!",
            type: "success"
        });            
        $('#emailsentBtn').click();                         
    }else{
        swal({
            title: "Warning",
            text: "Oooopss! You forgot to put a reciepient!",
            type: "warning"
        });      
    }                            
});

$('#incidentBtn').click(function () {
    
    if($('#incidentSubj').val() != ""){        
        
        swal({
            title: "That's Great!",
            text: "Please wait....!",
            type: "success"
        });            
        $('#incidenttBtn2').click();                         
    }else{
        swal({
            title: "Warning",
            text: "Oooopss! You forgot to put a subject!",
            type: "warning"
        });      
    }                            
});

$('#incidentThreadBtn').click(function () {
    
    if($('#incidentThreadDetails').val() != ""){        
        
        swal({
            title: "That's Great!",
            text: "Please wait....!",
            type: "success"
        });            
        $('#incidentThreadBtn2').click();                         
    }else{
        swal({
            title: "Warning",
            text: "Oooopss! You forgot to put a details!",
            type: "warning"
        });      
    }                            
});

$('#incidentThreadSBtn').click(function () {
    
    if($('#incidentThreadSDetails').val() != ""){        
        
        swal({
            title: "That's Great!",
            text: "Please wait....!",
            type: "success"
        });            
        $('#incidentThreadSBtn2').click();                         
    }else{
        swal({
            title: "Warning",
            text: "Oooopss! You forgot to put a details!",
            type: "warning"
        });      
    }                            
});

$('#incidentThreadCBtn').click(function () {
    
    if($('#incidentThreadSDetails').val() != ""){        
        
        swal({
            title: "That's Great!",
            text: "Please wait....!",
            type: "success"
        });            
        $('#incidentThreadCBtn2').click();                         
    }else{
        swal({
            title: "Warning",
            text: "Oooopss! You forgot to put a details!",
            type: "warning"
        });      
    }                            
});
$('#addpersonbtn').click(function () {
    
    swal({
        title: "That's Great!",
        text: "Please wait....!",
        type: "success"
    });            
    $('#addpersonbtn2').click();                         
            
});
$('#settings').on("change", function(){

    var x = $('#settings').val();               
    $('#week').addClass('hidden');
    if(x == 3){

        $('#week').removeClass('hidden');
    }
    
});  

$('#date_range .input-daterange').datepicker({
    keyboardNavigation: false,
    forceParse: false,
    autoclose: true
});   

$('#jobtn').click(function () {    
    if($('#topic_id').val() != ""){
        if($('#userID').val() != ""){

            if($('#job_order_details').val() != ""){

                swal({
                    title: "Good job!",
                    text: "I'm now going to send this job order!",
                    type: "success"
                    });            
                $('#jobtn2').click();            
                $('#jobtn').addClass('hidden');
            }else{

                swal({
                    title: "Warning",
                    text: "Hey people! I think you should put some details for this job order!",
                    type: "warning"
                });     
            }            
        }else{

            swal({
                title: "Warning",
                text: "Oh Come on! You forgot to assign somebody to this job order!",
                type: "warning"
            });      
            $('#jobtn2').click();            
        }                            
    }
    else{
        swal({
            title: "Warning",
            text: "Hey! You forgot to select a topic.",
            type: "warning"            
        });      
        $('#jobtn2').click();            
    }    
});  
$('#joinputs').filer({

    showThumbs:true,
    addMore:true
}); 

$('#topic_id').on("change", function(){

    var topicID = $('#topic_id').val();                           

    $.ajax({
        type:"POST",
        dataType: 'json',
        data: {topicID: topicID},
        url: "../../timetableTask/getList",
        success: function(data){            

            $('select#task_id').empty();
            
            $.each(data, function(i, text) {
                $('<option />',{value: i, text: text}).appendTo($('select#task_id'));
            });                                          

            $('select#task_id').multiselect('rebuild');
        }
    });      
});  
$('#rtbtn').click(function () {

    var x = $("#ionrange_2").html();
    $('#rangeID').val(x);

    if($('#rateremID').val() != ""){        
        
        swal({
            title: "That's Great!",
            text: "Please wait....!",
            type: "success"
        });            
        $('#rtbtn2').click();                         
    }else{
        swal({
            title: "Warning",
            text: "Oooopss! You forgot to put a remarks!",
            type: "warning"
        });      
    }                            
});
$('#jobcommentbtn').click(function () {

    if($('#jobcommentmessageid').val() != ""){        
        
        swal({
            title: "That's Great!",
            text: "Please wait....!",
            type: "success"
        });            
        $('#jobcommentbtn2').click();                         
    }else{
        swal({
            title: "Warning",
            text: "Oooopss! You forgot to put a message!",
            type: "warning"
        });      
    }                            
});

$('#jobcomoptionID').on("change", function(){

    var x = $('#jobcomoptionID').val();    
    
    if(x > 0 ){
        
        $('#jobcomsubjID').val('');            
        $('#jobcomemailID').val('');            
        $('#jobcomsubjID').addClass('required');            
        $('#jobcomemailID').addClass('required');     
        $('#jobcomEMAIL').removeClass('hidden');            
        $('#jobcomSUBJ').removeClass('hidden');           
    }else{
        $('#jobcomsubjID').val(''); 
        $('#jobcomemailID').val(''); 
        $('#jobcomsubjID').removeClass('required');            
        $('#jobcomemailID').removeClass('required');    
        $('#jobcomEMAIL').addClass('hidden');        
        $('#jobcomSUBJ').addClass('hidden');      

    }
});
$('#returnjo').click(function () {            
    swal({
        title: "Are you sure?",
        text: "You will about to return the job order",
        type: "info",
        showCancelButton: true,
        confirmButtonColor: "#1abc9c",
        confirmButtonText: "Yes, return it!",
        closeOnConfirm: false
    }, function () {
        swal("Got it!", "Please wait...", "success");
        $('#returnJObtn').click();
    });      
});
@endsection
<script type="text/javascript">
    
    function leadTopicFunction(elem){

        $('#leadtimetableid').val(elem.value);
        $('#topicLeadAddModal').modal('show');
    }

    function bomEditFunction(elem){

        var x = elem.id;

        $.ajax({

            type:"POST",
            dataType: 'json',
            data: {bomid: x},
            url: "../../getBOMDetails",
            success: function(data){            
                            
                $('#BOMCategory').val(data['category']);
                $('#BOMItem').val(data['item']);
                $('#BOMDesc').val(data['description']);
                $('#BOMQty').val(data['qty']);
                $('#BOMID').val(data['id']);
                $('#BOMSpec').val(data['specs']);
                                                
                $('#editBOMModal').modal('show');                
            }    
        });
    }    
    
    function ThreadCommentFunction(val){

    var x = val.value;
        if(x != ""){
            $('#mainthread_id').val(x);
        }    
        else{
            alert("Please Reload The Page And Try Again.")
        }
    }

    function ThreadJobCommentFunction(val){

    var x = val.value;
        if(x != ""){
            $('#joborderthread_id').val(x);
        }    
        else{
            alert("Please Reload The Page And Try Again.")
        }
    }

</script>
<script type="text/javascript">
    
    function RatingFunction(val){

    var x = val.value;
        if(x != ""){
            $('#jobID').val(x);
        }    
        else{
            alert("Please Reload The Page And Try Again.")
        }
    }

</script>

<script type="text/javascript">
    
    function IncidentCommentFunction(elem){

        $('#IRID').val(elem.value);
    }

</script>

<script type="text/javascript">
    
    function IncidentSolutionFunction(elem){

        $('#IRSID').val(elem.value);
    }

</script>

<script type="text/javascript">
    
    function IncidentClosedFunction(elem){

        $('#IRCID').val(elem.value);
    }

</script>
