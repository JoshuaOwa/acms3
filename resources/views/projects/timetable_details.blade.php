@extends('layouts.master')
@section('main-body')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2><i class="fa fa-building-o"></i> <i class="fa fa-eye"></i> TIMETABLE DETAILS</h2>        
    </div>
</div>        
&nbsp;
<div class="row">
    <div class="col-lg-12">
        {!! Html::decode(link_to_Route('projects.overview','<i class="fa fa-arrow-left"></i> Back', $project->encryptname, array('class' => 'btn btn-white btn-lg')))!!}                                                    
    </div><br />
</div>        
<div class="row">    
    <div class="col-lg-12 animated flash">
        <?php if (session('is_success_jo')): ?>
            <div class="alert alert-success alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
            </button>
                <center><h4>Job Order has been created!<i class="fa fa-check"></i></h4></center>                
            </div>
        <?php endif;?>            
    </div>            
</div>  
<div class="wrapper wrapper-content animated fadeInRight">  
    <div class="row">     
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title" style="background-color:#009688">                                                
                        <div class="pull-right">
                            <button type="button" class="btn btn-white btn-xs" data-toggle="modal" data-target="#jobOrderModal">
                                Create Job Order
                            </button>   
                        </div>                        
                    </div>                    
                    <div class="ibox-content ibox-heading">
                        <center><h3><i class="fa fa-tasks"> {!! $time->timetable->timetable !!} SUB TOPICS</i></h3></center>
                    </div>
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example">
                                <thead>
                                <tr>                                
                                    <th>Sub topic count</th>
                                    <th style="text-align: center">Job Order Count</th>
                                    <th style="text-align: center">Ongoing</th>
                                    <th style="text-align: center">Returned</th>
                                    <th style="text-align: center">Closed</th>                                
                                    <th style="text-align: center">Total OT</th>
                                    <th style="text-align: center">Total Expense</th>                                
                                </tr>
                                </thead>
                                <tbody>
                                   @foreach($timetable as $t)
                                    <tr>
                                        <td>{!! Html::decode(link_to_route('project_tasks.overview', $t->task,$t->encryptname, array())) !!}</td>
                                        <td style="text-align: center">{!! $t->jobcount !!}</td>
                                        <td style="text-align: center">{!! $t->ongoing !!}</td>
                                        <td style="text-align: center">{!! $t->returned !!}</td>
                                        <td style="text-align: center">{!! $t->closed !!}</td>
                                        <td style="text-align: center"></td>
                                        <td style="text-align: center"></td>                                    
                                    </tr>
                                   @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>             
        </div>        
    </div>
</div>
<!-- Modals -->
<div class="modal inmodal" id="jobOrderModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-briefcase modal-icon"></i>
                <h4 class="modal-title">Job Order</h4>                    
            </div>              
                <div class="modal-body">                 
                    {!! Form::open(array('route'=>'job_orders.store','method'=>'POST','files'=>true)) !!}    
                        <div class="row" style="padding-top:50">                    
                            <div class="col-lg-12">
                                {!! Form::label('task_id','Task')!!}    
                                {!! Form::select('task_id',$taskList,'',['class'=>'form-control','required'=>'required','id'=>'task_Id','multple'=>'multiple']) !!}                    
                            </div>                      
                        </div>                                                                                 
                        <div class="row" style="padding-top:10;">                                    
                            <div class="col-lg-6">
                                {!! Form::label('assigned_to','Send To') !!}
                                {!! Form::select('assigned_to',$users_jo,'',['class'=>'form-control','id'=>'assignedto_id','multiple'=>'multiple','required'=>'required']) !!}                                    
                            </div>                           
                        </div>               
                        <div class="row" style="padding-top:10px ">
                            <div class="col-lg-6">
                                {!! Form::label('settings','Settings')!!}                 
                                {!! Form::select('settings',$settings,'',['class'=>'form-control','id'=>'settings'])!!}
                            </div>      
                        </div>                                                
                        <div class="row-1" style="padding-top:10px;" id="normdate">
                            {!!Form::label('date','Date')!!}         
                            <div class="form-group" id="date_range">                                                
                                <div class="input-daterange input-group" id="datepicker">
                                    <input type="text" class="input-md form-control" name="start_date" value="{{$date_now}}" required="" />
                                    <span class="input-group-addon">to</span>
                                    <input type="text" class="input-md form-control" name="end_date" value="{{$date_now}}" required="" />
                                </div>
                            </div>                                
                        </div>     
                        <div class="row hidden" style="padding-top:10px;" id="week">
                            <div class="col-lg-6">
                                {!! Form::label('weeks','What Day?')!!}                 
                                {!! Form::select('weeks',$everyweek,'',['class'=>'form-control'])!!}
                            </div>                            
                        </div>                                                
                        <div class="row" style="padding-top:30px;">
                            <div class="col-lg-12">
                                {!!Form::label('details','Details')!!}         
                                {!! Form::textarea('details','',['class'=>'form-control','id'=>'job_order_details','required'=>'required']) !!}
                            </div>
                        </div>                    
                        <div class="row" style="padding-top:10px;">
                            <div class="col-lg-12">                            
                                {!!Form::label('attached','Attached File')!!}                             
                                {!! Form::file('attached[]', array('id' => 'joinputs', 'class' => 'photo_files', 'accept' => 'pdf|docx')) !!}                   
                            </div>
                        </div>                    
                </div>
                <div class="modal-footer">                       
                    {!! Form::hidden('timetable_id',$time->timetable_id) !!}
                    {!! Form::hidden('contact_id',$time->contact_id) !!}
                    {!! Form::hidden('project_id',$time->project_id) !!}                                
                    {!! Form::button('Close', array('type' => 'submit', 'class' => 'btn btn-white','data-dismiss'=>'modal')) !!}            
                    {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'button', 'class' => 'btn btn-primary','id'=>'jobtn')) !!}
                    {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'submit', 'class' => 'btn btn-primary hidden', 'id'=>'jobtn2')) !!}                    
                </div>   
            {!! Form::close() !!}                           
        </div>
    </div>
</div>
@stop
@section('page-script')

$('#joinputs').filer({

    showThumbs:true,
    addMore:true
});   

$('#deptid,#user_id,#ex_user_id,#jouserid,#task_Id,#assignedto_id').multiselect({
    maxHeight: 200,    
    includeSelectAllOption: true,
    enableCaseInsensitiveFiltering: true,
    enableFiltering: true,
    buttonWidth: '100%',
    buttonClass: 'form-control'        
});

$('#deptid').on("change", function(){

    var dept_id = $('#deptid').val();                           
    var proj_id = $('#proj_id').val();
    $.ajax({
        type:"POST",
        dataType: 'json',
        data: {dept_id: dept_id, proj_id: proj_id},
        url: "../../projects/getusers",
        success: function(data){            

            $('select#user_id').empty();
            
            $.each(data, function(i, text) {
                $('<option />',{value: i, text: text}).appendTo($('select#user_id'));
            });          
            var user = $('#user_id').val();       
            
            if(user != ""){                
                $('#urbtn').removeClass('hidden');
            }else{
                $('#urbtn').addClass('hidden');
            }        

            $('select#user_id').multiselect('rebuild');
        }
    });      
});  
$('#settings').on("change", function(){

    var x = $('#settings').val();               
    $('#week').addClass('hidden');
    if(x == 3){

        $('#week').removeClass('hidden');
    }
    
});  

$('#date_range .input-daterange').datepicker({
    keyboardNavigation: false,
    forceParse: false,
    autoclose: true
});   

$('#jobtn').click(function () {    
    if($('#assignedto_id').val() != ""){

        if($('#job_order_details').val() != ""){

            swal({
                title: "Good job!",
                text: "I'm now going to send this job order!",
                type: "success"
                });            
            $('#jobtn2').click();            
            $('#jobtn').addClass('hidden');
        }else{

            swal({
                title: "Warning",
                text: "Hey people! I think you should put some details for this job order!",
                type: "warning"
            });     
        }            
    }else{

        swal({
            title: "Warning",
            text: "Oh Come on! You forgot to assign somebody to this job order!",
            type: "warning"
        });      
    }                            
});  

@endsection