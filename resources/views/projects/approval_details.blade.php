@extends('layouts.master')
@section('main-body')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2>PROJECT DETAILS</h2>        
    </div>
</div>        
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-8">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Status</h5>
                    <div class ="pul-right">
                        @if($project->status === 0)
                            <span class="label label-info">For Approval</span>
                        @elseif($project->status === 1)
                            <span class="label label-primary">Approved</span>
                        @elseif($project->status === 2)
                            <span class="label label-default">Assigned</span>
                        @elseif($project->status === 3)
                            <span class="label label-danger">Denied</span>
                        @elseif($project->status === 4)
                            <span class="label label-warning">For Re-Approve</span>
                        @endif
                        {!! Html::decode(link_to_Route('projects.edit','<i class="fa fa-pencil"></i> Edit', $project->encryptname, array('class' => 'btn btn-warning btn-xs')))!!}                                                                                                                                                               
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="form-group" style="padding-top:20px;">
                        <h2><center><strong>{!! $project->project_name !!}</strong></center></h2>
                        <center><span><small><i>Project Name</i></small></span></center>
                    </div><div class="hr-line-dashed"></div>
                    <table class="table table-hover">
                        <tr>
                            <td class="action" style="width:30%"><i>SO NUMBER:</i></td><td>
                                @if(!empty($project->so_number))
                                    {!! $project->so_number !!}
                                @else
                                    N/A
                                @endif
                            </td>
                        </tr>
                    </table>
                    <table class="table table-hover">
                        <tr>
                            <td class="action" style="width:30%"><i>DATE REPORTED:</i></td><td>{!! strtoupper($project->date_reported) !!}</td>
                        </tr>
                    </table>
                    <table class="table table-hover">
                        <tr>
                            <td class="action" style="width:30%"><i>PROJECT COST:</i></td><td>{!! $project->project_cost !!}</td>
                        </tr>
                    </table>                    
                    <table class="table table-hover">
                        <tr>
                            <td class="action" style="width:30%"><i>ALLOWED OT:</i></td><td>{!! strtoupper($project->allowed_ot).' HOURS'!!}</td>
                        </tr>
                    </table>
                    <table class="table table-hover">
                         <tr>
                            <td class="action" style="width:30%"><i>PROJECT NAME:</i></td><td>{!! strtoupper($project->project_name) !!}</td>
                        </tr>   
                    </table>
                    <table class="table table-hover">
                         <tr>
                            <td class="action" style="width:30%"><i>BRANCH:</i></td><td>{!! strtoupper($project->branch) !!}</td>
                        </tr>   
                    </table>                    
                    <table class="table table-hover">
                        <tr>
                            <td class="action" style="width:30%"><i>CONTACT PERSON:</i></td><td>{!! strtoupper($project->contact->contact_person) !!}</td>
                        </tr>    
                    </table>  
                    <table class="table table-hover">
                        <tr>
                            <td class="action" style="width:30%"><i>CONTACT DETAILS:</i></td>
                            <td>                                
                                {!! strtoupper($project->contact->project_address.' - '.'/'.$project->contact->tel_no.'/'.$project->contact->fax_number.'/'.$project->contact->mobile_number) !!}                                
                                <br />                                
                                {!! $project->contact->email !!}
                            </td>
                        </tr>    
                    </table>                                        
                    <table class="table table-hover">                                                                                
                        <tr>
                            <td class="action" style="width:30%"><i>START DATE:</i></td><td>{!! strtoupper($project->start_date) !!}</td>
                        </tr>     
                    </table>  
                    <table class="table table-hover">                                 
                        <tr>
                            <td class="action" style="width:30%"><i>END DATE:</i></td><td>{!! strtoupper($project->end_date) !!}</td>
                        </tr>   
                    </table>
                    <table class="table table-hover">
                        <tr>
                            <td class="action" style="width:30%"><i>PROJECT DETAILS:</i></td><td>{!! strtoupper($project->project_details) !!}</td>
                        </tr>   
                    </table>
                    <table class="table table-hover">
                        <tr>
                            <td class="action" style="width:30%"><i>PROJECT STATUS:</i></td>
                            <td>
                                @if($project->status === 0)
                                    FOR APPROVAL
                                @elseif($project->status === 1)
                                    APPROVED
                                @elseif($project->status === 2)
                                    ASSIGNED
                                @elseif($project->status === 3)
                                    DENIED
                                @elseif($project->status === 4)
                                    FOR RE-APPROVE
                                @endif                                
                            </td>
                        </tr>   
                    </table>
                    <table class="table table-hover">
                        <tr>
                            <td class="action" style="width:30%"><i>PROJECT STAGE:</i></td>
                            <td>
                                @if($project->stage === 0)
                                    PENDING
                                @elseif($project->stage === 1)
                                    OPEN
                                @elseif($project->stage === 2)
                                    ONGOING
                                @elseif($project->stage === 3)
                                    CLOSED/CANCELED
                                @endif                                
                            </td>
                        </tr>   
                    </table>
                    <table class="table table-hover">
                        <tr>
                            <td class="action" style="width:30%"><i>DATE CREATED:</i></td><td>{!! strtoupper($project->created_at) !!}</td>
                        </tr>  
                    </table>                          
                    <table class="table table-hover">
                        <tr>
                            <td class="action" style="width:30%"><i>PREPARED BY:</i></td><td>{!! strtoupper($project->user->first_name. ' '.$project->user->last_name) !!}</td>
                        </tr>                     
                    </table>      
                    <table class="table table-hover">
                        <tr>
                            <td class="action" style="width:30%"><i>ATTACHED FILES:</i></td>
                            <td>                            
                                @foreach($files as $filer)
                                    {!! Form::open(array('route'=>'projects.download_files','method'=>'POST')) !!}
                                        {!! Form::hidden('encname',$filer->encrpytname) !!}
                                        {!! Form::submit($filer->filename, array('type' => 'submit', 'class' => 'btn btn-primary btn-xs')) !!}                                                                                  
                                    {!! Form::close() !!}         
                                @endforeach
                            </td>
                        </tr>                     
                    </table>      
                    <table>
                       {!!Form::open(array('route'=>array('project_approvals.update_status',$project->id),'method'=>'POST'))!!}
                        @if($project->status < 1 || $project->status === 4)      
                            
                            <div class="form-group col-lg-12">
                                <div class="row">
                                    <div class="form-group col-lg-12">
                                        {!!Form::label('remarks','Remarks')!!}                                                             
                                        {!!Form::textarea('remarks','',['class'=>'form-control col-lg-12','placeholder'=>'Enter your Remarks Here']) !!}
                                        {!!Form::hidden('project_id',$project->id) !!}
                                    </div>
                                </div>
                            </div>
                        @endif
                        <tr>
                            <td>
                                <a href="javascript:history.back()"><button type="button" class="btn btn-default"><i class="fa fa-arrow-left"></i> Back </button></a>
                            </td><td>&nbsp;</td>
                            @if($project->status < 1 || $project->status === 4)                                
                                    <td>                            
                                        <button type="button" class="btn btn-success btn-sm" id="sbalert1"><i class="fa fa-check"></i> Approved</button>                                                                   
                                    </td><td>&nbsp;</td>
                                    <td>                            
                                        <button type="button" class="btn btn-danger btn-sm" id="sbalert2"><i class="fa fa-close"></i> Denied</button>                                                                                                           
                                    </td>                                    
                                    {!! Form::button('<i class="fa fa-save"></i> Approved', array('value'=>'1','type' => 'submit', 'class' => 'btn btn-primary hidden', 'id'=>'appbtn','name'=>'status')) !!}                            
                                    {!! Form::button('<i class="fa fa-save"></i> Denied', array('value'=>'2','type' => 'submit', 'class' => 'btn btn-primary hidden', 'id'=>'denbtn','name'=>'status')) !!}                            
                                {!! Form::close() !!}
                            @endif
                        </tr>
                    </table>           
                </div>
            </div>
        </div>    
        <div class="col-lg-4" id="logflow">        
            <div class="panel panel-default">
                <div class="panel-heading">
                     <strong>Project activity</strong>  
                </div>
                <div class="panel-body">
                    <div id="vertical-timeline" class="vertical-container dark-timeline">
                        @foreach($logs as $log)
                        <div class="vertical-timeline-block">
                            <div class="vertical-timeline-icon gray-bg">
                                <i class="fa fa-coffee"></i>
                            </div>
                            <div class="vertical-timeline-content">
                                <p>{!! $log->log !!}</p>
                                @if(!empty($log->remarks))
                                <br />                        
                                <br />
                                <p>Remarks: <br/>
                                {!! $log->remarks !!}</p>                        
                                @endif
                                <span class="vertical-date small text-muted"> {!! $log->created_at->diffForHumans() !!} </span>
                            </div>
                        </div>
                        @endforeach
                    </div>  
                </div>                
            </div>                        
        </div> 
    </div>
</div>  
@stop
@section('page-script')
$('#sbalert1').click(function () {            
        swal({
            title: "Are you sure?",
            text: "You will about to approve the project",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#1abc9c",
            confirmButtonText: "Yes, approve it!",
            closeOnConfirm: false
        }, function () {
            swal("Got it!", "Please wait...", "success");
            $('#appbtn').click();
        });      
    });

    $('#sbalert2').click(function () {            
        swal({
            title: "Are you sure?",
            text: "You will about to denied the project",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#1abc9c",
            confirmButtonText: "Yes, denied it!",
            closeOnConfirm: false
        }, function () {
            swal("Got it!", "Please wait...", "success");
            $('#denbtn').click();
        });      
    });
    
@stop