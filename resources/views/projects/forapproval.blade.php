@extends('layouts.master')
@section('main-body')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2><i class="fa fa-folder"></i> PROJECTS FOR APPROVAL</h2>        
    </div>
</div>        
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12 animated flash">
            <?php if (session('is_success')): ?>
                <div class="alert alert-success alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                    <center><h4>Project was successfully added!<i class="fa fa-check"></i></h4></center>                
                </div>
            <?php endif;?>
            <?php if (session('is_update')): ?>
                <div class="alert alert-success alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                    <center><h4>Project was successfully updated!<i class="fa fa-check"></i></h4></center>                
                </div>
            <?php endif;?>
            <?php if (session('is_assigned')): ?>
                <div class="alert alert-success alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                    <center><h4>Project was successfully assigned!<i class="fa fa-check"></i></h4></center>                
                </div>
            <?php endif;?>
            <?php if (session('not_assigned')): ?>
                <div class="alert alert-danger alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                    <center><h4>Please Try To Assigned The Again!<i class="fa fa-close"></i></h4></center>                
                </div>
            <?php endif;?>
            <?php if (session('is_transfer_proj')): ?>
                <div class="alert alert-success alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                    <center><h4>Project was successfully transfered! <i class="fa fa-check"></i></h4></center>                
                </div>
            <?php endif;?>     
            <?php if (session('error_transfer_proj')): ?>
                <div class="alert alert-danger alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                    <center><h4>Project Transfer Failed! <i class="fa fa-wrong"></i></h4></center>                
                </div>
            <?php endif;?>     
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="form-group"><label class="col-sm-12 control-label">Filter by:</label>
                    {!!Form::open(array('route'=>'project_approvals.filter','method'=>'POST'))!!}
                    <div class="col-lg-6">
                        <button type="submit" class="btn btn-info btn-sm" value="0" name="filter"><i class="fa fa-clock-o"></i> For Approval</button>                       
                        <button type="submit" class="btn btn-success btn-sm" value="1" name="filter"><i class="fa fa-check"></i> Approved</button>
                        <button type="submit" class="btn btn-danger btn-sm" value="3" name="filter"><i class="fa fa-close"></i> Denied</button>                            
                    </div>
                    {!! Form::close() !!}                                
                </div>
            </div>
        </div>
        &nbsp;
        <div class="col-lg-12">
            <div class="panel blank-panel">
                <div class="panel-heading">                            
                    <div class="panel-options">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#tab-1"> <i class="fa fa-bullseye"></i> MAIN</a></li>                            
                        </ul>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div id="tab-1" class="tab-pane active">                                                	                        	
                            &nbsp;
                            <center><h2>PROJECTS</h2></center>
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <table class="table table-striped table-hover dataTables-example">
                                    <thead>
                                        <tr>                                            
                                            <th>SO#</th>
                                            <th>PROJECT NAME</th>
                                            <th>COMPANY</th>
                                            <th>CONTACT PERSON</th>
                                            <th>EMAIL</th>                                            
                                            <th>CONTACT NUMBER</th>                                            
                                            <th>ACTION</th>
                                        </tr>
                                    </thead>
                                    <tbody>        
                                        @forelse($projects as $project)    
                                            <tr>                    
                                                <td>
                                                    @if(!empty($project->so_number))
                                                        {!! $project->so_number !!}
                                                    @else
                                                        N/A
                                                    @endif
                                                </td>                                     
                                                <td>{!! $project->project_name !!}</td>
                                                <td>{!! strtoupper($project->contact->company) !!}</td>
                                                <td>                                                    
                                                    @foreach($project->person as $person)
                                                        @if(!empty($person->contact_person))
                                                            {!! $person->contact_person !!}                                         
                                                        @endif
                                                    @endforeach                                                    
                                                </td>
                                                <td>                                                    
                                                    @foreach($project->person as $person)
                                                        @if(!empty($person->email))
                                                            {!! $person->email !!}                                         
                                                        @endif
                                                    @endforeach                                                    
                                                </td>
                                                <td>
                                                    @foreach($project->person as $person)  
                                                        @if(!empty($person->mobile_number))
                                                            {!! $person->mobile_number.'/' !!}
                                                        @endif
                                                        @if(!empty($person->tel_no))
                                                            {!! $person->tel_no.'/' !!}
                                                        @endif
                                                    @endforeach                           
                                                </td> 
                                                <td class="action">                                                      
                                                    {!! Html::decode(link_to_Route('project_approvals.show','<i class="fa fa-eye"></i> Details', $project->encryptname, array('class' => 'btn btn-primary btn-xs')))!!}                                                    
                                                </td>
                                            </tr>                           
                                        @empty
                                        @endforelse
                                    </tbody>                                  
                                    </table>
                                </div>
                            </div>
                        </div>                      
                    </div>
                </div>
            </div>
        </div>   
    </div>                 
</div>	
@stop