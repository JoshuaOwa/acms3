@extends('layouts.master')
@section('main-body')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2><i class="fa fa-users"></i> TIMETABLE FOR {!! strtoupper($project->project_name) !!}</h2>       
    </div>
</div>        
<div class="wrapper wrapper-content animated fadeInRight">		
    <div class="row">
    	<div class="col-md-12 animated flash">
    		<?php if (session('is_success')): ?>
                <div class="alert alert-success alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                    <center><h4>Timetable was successfully added!<i class="fa fa-check"></i></h4></center>                
                </div>
            <?php endif;?>
            @if( count($errors) > 0)
                <div class="alert alert-danger alert-dismissible flash" role="alert">            
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
            </button>
                    <center><h4>Oh snap! You got an error!</h4></center>   
                    <center><h4>Select a module before you save!</h4></center>
                </div>
            @endif
        </div>    
        <div class="row-1">
        	<div class="col-lg-12">
        		{!! Html::decode(link_to_Route('projects.overview','<i class="fa fa-arrow-left"></i> Back', $project->encryptname, array('class' => 'btn btn-white btn-lg')))!!}                                                    
        	</div><br />
        </div>        
        <div class="col-lg-12">
	        <div class="ibox float-e-margins">
	            <div class="ibox-title" style="background-color:#009688">
	                <h5 style="color:white"><i class="fa fa-users"></i> Timetable List</h5>	                
	            </div>
	            <div class="ibox-content">
	            	<div class="table-responsive">
		            	<table class="table table-striped table-hover" >
			            <thead>
				            <tr>				                
				            	<th style="text-align: center;"></th>					                
				                <th style="text-align: center;">Timetable</th>
				                <th style="text-align: center;">Task Count</th>
				            </tr>
			            </thead>
			            <tbody>
			            	{!! Form::open(array('route'=>'project_timetables.store', 'method'=>'POST','enctype'=>'multipart/form-data')) !!}
					            @forelse($timetables as $timetable)				           				           								
				           			<tr>
				           				@if(count($proj_timetable) < 1)				           			
						           			<td style="text-align: center;"><input type="checkbox" name="timetable_id[]" value="{!! $timetable->id !!}"></td>						           			
						           			<td style="text-align: center;">{!! $timetable->timetable !!}</td>
						           			<td style="text-align: center;">{!! $timetable->taskcount !!}</td>						           		
						           		@elseif(count($proj_timetable) > 0)
						           			
					           				@if(in_array($timetable->id, $proj_timetable))
						           				<td style="text-align: center;"><input type="checkbox" name="timetable_id[]" value="{!! $timetable->id !!}" checked></td>							           			
						           			@else
						           				<td style="text-align: center;"><input type="checkbox" name="timetable_id[]" value="{!! $timetable->id !!}"></td>							           			
						           			@endif												           			
						           			<td style="text-align: center;">{!! $timetable->timetable !!}</td>
						           			<td style="text-align: center;">{!! $timetable->taskcount !!}</td>	           		
						           		@endif
					           			
					           		</tr>
					           	@empty
					           	@endforelse
			            </tbody>			            
		            	</table>
		            </div>		            
		            <div class="ibox-footer">		         
		            	{!! Form::hidden('project_id',$project->id) !!}   							
						{!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'button', 'class' => 'btn btn-primary','id'=>'addbtn')) !!}
                		{!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'submit', 'class' => 'btn btn-primary hidden', 'id'=>'addbtn2')) !!}                    
					</div>		
	            </div>	            
				{!! Form::close() !!}	
	        </div>
	    </div>	    
    </div>
</div>	
@stop
@section('page-script')

$('#addbtn').click(function () {
	
	var cnt = $('input[name="timetable_id[]"]:checked').length;	
    if(cnt > 0){
        
        swal({
            title: "Yeah!",
            text: "Your Timetable will be updated!",
            type: "success"
            });            
        $('#addbtn2').click();            
    }else{

        swal({
            title: "Warning",
            text: "Wooopppsss! Select atleast one topic before you submit!",
            type: "warning"
        });      
    }                            
});
@endsection