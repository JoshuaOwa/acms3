@extends('layouts.master')
@section('main-body')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2><i class="fa fa-building-o"></i> <i class="fa fa-eye"></i> PROJECT DETAILS</h2>        
    </div>
</div>        
<div class="row  border-bottom white-bg dashboard-header">
    <center>
        <h2><strong>{!! strtoupper($project->project_name) !!}</strong></h2>
        <small>Project Details:</small>
    </center>
    <div class="col-md-6">                
        <ul class="list-group clear-list m-t">
            <li class="list-group-item fist-item">
                <span class="pull-right">
                    {!! strtoupper($project->approver->first_name.' '.$project->approver->last_name) !!}
                </span>
                <span class="label label-info">*</span> Project Approver:
            </li>
            <li class="list-group-item">
                <span class="pull-right">
                    {!! $project->contact->contact_person !!}
                </span>
                <span class="label label-info">*</span> Contact Person:
            </li>
            <li class="list-group-item">
                <span class="pull-right">
                    {!! $project->contact->mobile_number.' / '.$project->contact->tel_no !!}
                </span>
                <span class="label label-info">*</span> Contact Number:
            </li>
            <li class="list-group-item">
                <span class="pull-right">
                    {!! $project->contact->email !!}
                </span>
                <span class="label label-info">*</span> Email:
            </li>         
            <li class="list-group-item">
                <span class="pull-right">
                    @if($project->status === 0)
                        <span class="label label-info"><i class="fa fa-clock-o"></i> For Approval</span>
                    @elseif($project->status === 1)
                        <span class="label label-success"><i class="fa fa-check"></i> Approved</span>
                    @elseif($project->status === 2)
                        <span class="label label-primary"> <i class="fa fa-user"></i>Assigned</span>                                       
                    @elseif($project->status === 3)
                        <span class="label label-danger"> <i class="fa fa-angellist"></i>Denied</span>                                       
                    @elseif($project->status === 4)
                        <span class="label label-warning"> <i class="fa fa-refresh"></i> For Re-Approve</span>                                       
                    @endif
                </span>
                <span class="label label-info">*</span> Project Status:
            </li>  
            <li class="list-group-item">
                <span class="pull-right">
                    {!! $project->allowed_ot.' HOURS' !!}
                </span>
                <span class="label label-info">*</span> Allowed OT:
            </li>                
            <li class="list-group-item">
                <span class="pull-right">
                    {!! number_format($project->project_cost,2) !!}
                </span>
                <span class="label label-info">*</span> Project Cost:
            </li> 
        </ul>
    </div>
    <div class="col-md-6">                
        <ul class="list-group clear-list m-t">
            <li class="list-group-item fist-item">
                <span class="pull-right">
                    {!! strtoupper($project->user->first_name.' '.$project->user->last_name) !!}
                </span>
                <span class="label label-info">*</span> Created By:
            </li>                        
            <li class="list-group-item">
                <span class="pull-right">
                    {!! $project->project_address !!}
                </span>
                <span class="label label-info">*</span> Project Address:
            </li>      
            <li class="list-group-item">
                <span class="pull-right">
                    {!! $project->project_details !!}
                </span>
                <span class="label label-info">*</span> Project Details:
            </li>                          
            <li class="list-group-item">
                <span class="pull-right">
                    {!! $project->start_date !!}
                </span>
                <span class="label label-info">*</span> Start Date:
            </li>                
            <li class="list-group-item">
                <span class="pull-right">
                    {!! $project->end_date !!}
                </span>
                <span class="label label-info">*</span> End Date:
            </li>                            
            <li class="list-group-item">
                <span class="pull-right">
                    
                </span>
                <span class="label label-info">*</span> Attachments:
            </li>
            
        </ul>
    </div>
</div>&nbsp;
<div class="row">
    <div class="col-lg-12 animated flash">
        <?php if (session('is_added')): ?>
            <div class="alert alert-success alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
            </button>
                <center><h4>New Member has been added!<i class="fa fa-check"></i></h4></center>                
            </div>
        <?php endif;?>            
    </div>            
</div>  
<div class="wrapper wrapper-content animated fadeInRight">  
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title" style="background-color:#009688">
                    <h5 style="color: white">Workgroup Members</h5>                    
                    <div class="pull-right">                                                                            
                        <button type="button" class="btn btn-white btn-xs" data-toggle="modal" data-target="#workgroupAddModal">
                            Add Member
                        </button>                                                                      
                    </div>                          
                </div>
                <div class="ibox-content">
                    <table class="table table-hover no-margins">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Position</th>
                            <th style="text-align: center">Pending JO</th>
                            <th style="text-align: center">Returned JO</th>
                            <th style="text-align: center">Closed JO</th>
                            <th style="text-align: center">Total OT</th>
                            <th style="text-align: center">Total Expense</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($members as $member)
                                <tr>
                                    <td>{!! strtoupper($member->user->first_name.' '.$member->user->last_name) !!}</td>
                                    <td>{!! strtoupper($member->position) !!}</td>
                                    <td style="text-align: center">{!! $member->pendJO !!}</td>                                    
                                    <td style="text-align: center">{!! $member->retJO !!}</td>                                    
                                    <td style="text-align: center">{!! $member->cloJO !!}</td> 
                                    <td></td>                                   
                                    <td></td>
                                    <td></td>
                                </tr>                   
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
                
        <div class="col-lg-6">
            <div class="ibox float-e-margins">
                <div class="ibox-title" style="background-color:#009688">
                    <h5 style="color: white">Close Sales</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <table class="table table-hover no-margins">
                        <thead>
                        <tr>
                            <th>Close sales ID</th>
                            <th>Amount</th>                                                        
                            <th>Done by</th>                            
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td><small>Pending...</small></td>
                            <td><i class="fa fa-clock-o"></i> 11:20pm</td>
                            <td>Samantha</td>                            
                        </tr>                   
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="ibox float-e-margins">
                <div class="ibox-title" style="background-color:#009688">
                    <h5 style="color: white">Collection</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <table class="table table-hover no-margins">
                        <thead>
                        <tr>
                            <th>Collection ID</th>
                            <th>Close Sales ID</th>
                            <th>Amount</th>                                                        
                            <th>Done by</th>                            
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td><small>Pending...</small></td>
                            <td><i class="fa fa-clock-o"></i> 11:20pm</td>
                            <td>Samantha</td>                            
                        </tr>                   
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-lg-8">
            <div class="ibox float-e-margins">
                <div class="ibox-title" style="background-color:#009688">
                    <h5 style="color:white"><i class="fa fa-futbol-o"></i> Message Threads</h5>                                                     
                        <div class="pull-right">
                            <button type="button" class="btn btn-white btn-xs" data-toggle="modal" data-target="#myModal">
                                Create New Thread
                            </button>                                        
                            <button type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#jobOrderModel">
                                Create Job Order
                            </button>                                                                      
                        </div>                                
                </div>
                <div class="ibox-content">
                    <div class="feed-activity-list">                                
                        <div class="feed-element box" id="flowTimeProj">
                            <div class="media-body" style="padding-right:10px;">                                        
                                <div style="padding-top:10px;"></div>
                                <small class="text-muted"></small>
                                <div class="well">                  
                                    <small class="pull-right">
                                        
                                    </small>                                                                                                                                                        
                                    <strong>From: </strong>
                                     <br/>                                            
                                    <br />
                                    <strong>Message: </strong><br/>
                                                                                                                                                                                                                             
                                    <br /><br />
                                    <strong>Recipient/s: </strong><br /> 
                                    

                                    <br /><br />
                                    <button type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#myModalForward" id="" value="" onclick="forwardFunction(this)">
                                        <i class="fa fa-forward"></i>
                                    </button>
                                    <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#addCommentModal" id="" value="" onclick="ThreadCommentFunction(this)">
                                        <i class="fa fa-comment"></i>
                                    </button>                                           

                
                                    <small class="pull-right">Seen:                                                    
                                    </small><br />                                                                        
                                </div>                                                                      
                              <br />                                                            
                            </div>
                        </div>                                     
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title"  style="background-color:#009688">
                    <h5 style="color: white;">Timeline Activity</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>                                
                    </div>
                </div>
                <div class="ibox-content ibox-heading">
                    <center><h3><i class="fa fa-book"> Project logs!</i></h3></center>
                </div>
                <div class="ibox-content inspinia-timeline" id="flowTimeProj">                                                
                    <div class="timeline-item">
                        @foreach($logs as $log)
                        <div class="row">
                            <div class="col-xs-3 date">
                                <i class="fa fa-book"></i>
                                {!! $log->created_at->format('H:i a') !!}
                                <br/>
                                <small class="text-navy">{!! $log->created_at->diffForHumans() !!}</small>
                            </div>
                            <div class="col-xs-7 content">
                                <p class="m-b-xs"><strong>{!! strtoupper($log->user->first_name.' '.$log->user->last_name) !!}</strong></p>
                                <p>
                                    {!! $log->log !!}
                                </p>
                                <p>
                                    @if(!empty($log->remarks))
                                    <br />                        
                                    <br />
                                    <p>Remarks: <br/>
                                    {!! $log->remarks !!}</p>                        
                                    @endif
                                </p>
                            </div>
                        </div>
                        @endforeach
                    </div>                    
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal" id="workgroupAddModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-users modal-icon"></i>
                <h4 class="modal-title">ADD WORKGROUP MEMBER</h4>                    
            </div>              
            {!! Form::open(array('route'=>'project_workgroups.store','method'=>'POST','files'=>true)) !!}                                                                               
            <div class="modal-body">                                                 
                <div class="row" style="padding-top:10px">
                    <div class="col-lg-6">
                        {!! Form::label('department','Department')!!}
                        {!! Form::select('department[]',$departments,'',['class'=>'form-control','id'=>'deptid','multiple'=>'multiple']) !!}                                  
                    </div>      
                    <div class="col-lg-6">
                        {!! Form::label('user_id','Name')!!}
                        {!! Form::select('user_id[]',$users,'',['class'=>'form-control','id'=>'user_id','multiple'=>'multiple']) !!}                     
                    </div>  
                </div>                                    
            </div>
            <div class="modal-footer">   
                {!! Form::hidden('contact_id',$project->contact_id) !!}
                {!! Form::hidden('project_id',$project->id,['id'=>'proj_id']) !!}
                {!! Form::button('Close', array('type' => 'submit', 'class' => 'btn btn-white','data-dismiss'=>'modal')) !!}            
                {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'button', 'class' => 'btn btn-primary hidden sbalert','id'=>'urbtn')) !!}
                {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'submit', 'class' => 'btn btn-primary hidden', 'id'=>'urbtn2')) !!}                    
            </div>   
            {!! Form::close() !!}                           
        </div>
    </div>
</div>
<div class="modal inmodal" id="workgroupRemoveModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-users modal-icon"></i>
                <h4 class="modal-title">REMOVE WORKGROUP MEMBER</h4>                    
            </div>              
            {!! Form::open(array('route'=>'project_workgroups.remove_member','method'=>'POST','files'=>true)) !!}                                                                               
            <div class="modal-body">                                                 
                <div class="row" style="padding-top:10px">                    
                    <div class="col-lg-6">
                        {!! Form::label('user_id','Name')!!}                        
                    </div>  
                </div>                                    
            </div>
            <div class="modal-footer">   
                {!! Form::hidden('contact_id',$project->contact_id) !!}
                {!! Form::hidden('project_id',$project->id,['id'=>'proj_id']) !!}
                {!! Form::button('Close', array('type' => 'submit', 'class' => 'btn btn-white','data-dismiss'=>'modal')) !!}            
                {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'button', 'class' => 'btn btn-primary hidden sbalert','id'=>'urbtn')) !!}
                {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'submit', 'class' => 'btn btn-primary hidden', 'id'=>'urbtn2')) !!}                    
            </div>   
            {!! Form::close() !!}                           
        </div>
    </div>
</div>
@stop
@section('page-script')

$('#deptid,#user_id,#ex_user_id').multiselect({
    maxHeight: 200,    
    includeSelectAllOption: true,
    enableCaseInsensitiveFiltering: true,
    enableFiltering: true,
    buttonWidth: '100%',
    buttonClass: 'form-control'        
});

$('#deptid').on("change", function(){

    var dept_id = $('#deptid').val();                           
    var proj_id = $('#proj_id').val();
    $.ajax({
        type:"POST",
        dataType: 'json',
        data: {dept_id: dept_id, proj_id: proj_id},
        url: "../../projects/getusers",
        success: function(data){            

            $('select#user_id').empty();
            
            $.each(data, function(i, text) {
                $('<option />',{value: i, text: text}).appendTo($('select#user_id'));
            });          
            var user = $('#user_id').val();       
            
            if(user != ""){                
                $('#urbtn').removeClass('hidden');
            }else{
                $('#urbtn').addClass('hidden');
            }        

            $('select#user_id').multiselect('rebuild');
        }
    });      
});    
$('#user_id').on("change", function(){

   var user2 = $('#user_id').val();       
            
    if(user2 != ""){                
        $('#urbtn').removeClass('hidden');
    }else{
        $('#urbtn').addClass('hidden');
    }        
});    
$('.sbalert').click(function () {

    if($('#user_id').val() != ""){
        
        swal({
            title: "Allright!",
            text: "I'm now going to add this to workgroup!",
            type: "success"
            });            
        $('#urbtn2').click();            
    }else{

        swal({
            title: "Warning",
            text: "Oh Come on! Select at least one member before you submit!",
            type: "warning"
        });      
    }                            
});
@endsection
