@extends('layouts.master')
@section('main-body')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2><i class="fa fa-building-o"></i> <i class="fa fa-eye"></i> {!! $task->task->task !!} JOB ORDER LISTS</h2>        
    </div>
</div>        
&nbsp;
<div class="row">
    <div class="col-lg-12">
        {!! Html::decode(link_to_Route('project_timetables.details','<i class="fa fa-arrow-left"></i> Back', $task->projtimetable->encryptname, array('class' => 'btn btn-white btn-lg')))!!}                                                    
    </div><br />
</div>        
<div class="row">
    <div class="col-lg-12 animated flash">
        <?php if (session('is_rated')): ?>
            <div class="alert alert-success alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
            </button>
                <center><h4>Job Order Has Been Rated And Closed! <i class="fa fa-check"></i></h4></center>                
            </div>
        <?php endif;?>            
    </div>        
</div>
<div class="wrapper wrapper-content animated fadeInRight">  
    <div class="row">     
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title" style="background-color:#009688">                        
                    </div>                    
                    <div class="ibox-content ibox-heading">
                        <center><h3><i class="fa fa-tasks"> JOB ORDERS</i></h3></center>
                    </div>
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example">
                                <thead>
                                <tr>                                
                                    <th>JO #</th>
                                    <th style="text-align: center">Assigned By</th>
                                    <th style="text-align: center">Assigned To</th>
                                    <th style="text-align: center">Status</th>
                                    <th style="text-align: center">Date Created</th>
                                    <th style="text-align: center">Last Update</th>
                                    <th style="text-align: center">OT</th>
                                    <th style="text-align: center">Expense</th>
                                    <th style="text-align: center"></th>
                                </tr>
                                </thead>
                                <tbody>
                                   @foreach($jobs as $job)
                                    <tr>
                                        <td>{!! Html::decode(link_to_route('job_orders.overview', $job->id,$job->encryptname, array())) !!}</td>
                                        <td style="text-align: center">{!! strtoupper($job->user->first_name.' '.$job->user->last_name) !!}</td>
                                        <td style="text-align: center">{!! strtoupper($job->assigned->first_name.' '.$job->assigned->last_name) !!}</td>
                                        <td style="text-align: center">
                                            @if($job->status === 1)
                                                <span class="label label-info"><i class="fa fa-clock-o"></i> Ongoing</span>
                                            @elseif($job->status === 2)
                                                <span class="label label-success"><i class="fa fa-check"></i> Returned</span>
                                            @elseif($job->status === 3)
                                                <span class="label label-primary"> <i class="fa fa-user"></i>Closed</span>                                                                               
                                            @endif
                                        </td>
                                        <td style="text-align: center">{!! $job->created_at->format('M d Y h:i a') !!}</td>
                                        <td style="text-align: center">{!! $job->updated_at->format('M d Y h:i a') !!}</td>
                                        <td style="text-align: center"></td>     
                                        <td style="text-align: center"></td>                                    
                                        <td style="text-align: center">
                                            @if($job->status == 2)
                                                <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#ratingtModal" id="{{$job->id}}}" value="{{$job->id}}}" onclick="RatingFunction(this)">
                                                    <i class="fa fa-star"></i>
                                                </button>    
                                            @elseif($job->status == 3)
                                                @for($i=0; $i < $job->rating; $i++)
                                                    <i class="fa fa-star"></i>
                                                @endfor
                                            @endif

                                        </td>     
                                    </tr>
                                   @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>             
        </div>        
    </div>
</div>
<div class="modal inmodal" id="ratingtModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-users modal-icon"></i>
                <h4 class="modal-title">RATE THIS JOB ORDER BEFORE CLOSING IT</h4>                    
            </div>              
            {!! Form::open(array('route'=>'job_orders.rating','method'=>'POST','files'=>true)) !!}                                                                               
            <div class="modal-body">                                                 
                <div class="row" style="padding-top:10px">                    
                    <div class="col-lg-4">
                        {!! Form::label('rating','Rate')!!}                        
                        {!! Form::select('rating',$ratings,'',['class'=>'form-control']) !!}
                    </div>                      
                    <div class="col-lg-12">
                        {!! Form::label('rating_remarks','Remarks')!!}                        
                        {!! Form::textarea('rating_remarks','',['class'=>'form-control','placeholder'=>'Remarks','required'=>'required','id'=>'rateremID']) !!}
                    </div>  
                </div>                                    
            </div>
            <div class="modal-footer">                   
                {!! Form::hidden('job_id','',['id'=>'jobID']) !!}
                {!! Form::hidden('status','3') !!}
                {!! Form::button('Close', array('type' => 'submit', 'class' => 'btn btn-white','data-dismiss'=>'modal')) !!}            
                {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'button', 'class' => 'btn btn-primary sbalert','id'=>'rtbtn')) !!}
                {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'submit', 'class' => 'btn btn-primary hidden', 'id'=>'rtbtn2')) !!}                    
            </div>   
            {!! Form::close() !!}                           
        </div>
    </div>
</div>
@stop
@section('page-script')
$("#ionrange_2").ionRangeSlider({
    min: 1,
    max: 5,
    type: 'single',
    step: 1,
    postfix: " stars",
    prettify: false,
    hasGrid: true
});

$('#rtbtn').click(function () {

    var x = $("#ionrange_2").html();
    $('#rangeID').val(x);

    if($('#rateremID').val() != ""){        
        
        swal({
            title: "That's Great!",
            text: "Please wait....!",
            type: "success"
        });            
        $('#rtbtn2').click();                         
    }else{
        swal({
            title: "Warning",
            text: "Oooopss! You forgot to put a remarks!",
            type: "warning"
        });      
    }                            
});

@endsection
<script type="text/javascript">
    
    function RatingFunction(val){

    var x = val.value;
        if(x != ""){
            $('#jobID').val(x);
        }    
        else{
            alert("Please Reload The Page And Try Again.")
        }
    }

</script>