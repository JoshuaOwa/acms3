@extends('layouts.master')
@section('main-body')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2><i class="fa fa-folder"></i> MY PROJECT</h2>        
    </div>
</div>        
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12 animated flash">
            <?php if (session('is_success')): ?>
                <div class="alert alert-success alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                    <center><h4>Project was successfully added!<i class="fa fa-check"></i></h4></center>                
                </div>
            <?php endif;?>
            <?php if (session('is_update')): ?>
                <div class="alert alert-success alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                    <center><h4>Project was successfully updated!<i class="fa fa-check"></i></h4></center>                
                </div>
            <?php endif;?>
            <?php if (session('is_assigned')): ?>
                <div class="alert alert-success alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                    <center><h4>Project was successfully assigned!<i class="fa fa-check"></i></h4></center>                
                </div>
            <?php endif;?>
            <?php if (session('not_assigned')): ?>
                <div class="alert alert-danger alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                    <center><h4>Please Try To Assigned The Again!<i class="fa fa-close"></i></h4></center>                
                </div>
            <?php endif;?>
            <?php if (session('is_transfer_proj')): ?>
                <div class="alert alert-success alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                    <center><h4>Project was successfully transfered! <i class="fa fa-check"></i></h4></center>                
                </div>
            <?php endif;?>     
            <?php if (session('error_transfer_proj')): ?>
                <div class="alert alert-danger alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                    <center><h4>Project Transfer Failed! <i class="fa fa-wrong"></i></h4></center>                
                </div>
            <?php endif;?>     
        </div>
        <div class="col-lg-12">
            <div class="panel blank-panel">
                <div class="panel-heading">                            
                    <div class="panel-options">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#tab-1"> <i class="fa fa-bullseye"></i> MAIN</a></li>                            
                        </ul>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div id="tab-1" class="tab-pane active">                                                	
                            <div class="form-group pull-right">
                            	<div class="col-lg-12">
                                	{!! Html::decode(link_to_Route('projects.create', '<i class="fa fa-plus"></i> New Project', [], ['class' => 'btn btn-primary'])) !!}
                                    <button class="btn btn-success" data-toggle="modal" data-target="#TransferProjectModal"><i class="fa fa-refresh"></i> Transfer Project</button>
                                </div>
                            </div>                    
                        	</br>
                            &nbsp;
                            <center><h2></h2></center>
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <table class="table table-striped table-hover dataTables-example">
                                    <thead>
                                        <tr>                                            
                                            <th>SO#</th>
                                            <th>PROJECT NAME</th>
                                            <th>BRANCH</th>
                                            <th>TAG</th>
                                            <th>CLIENT NAME</th>                                            
                                            <th>CONTACT PERSON</th>
                                            <th>CONTACT NUMBER</th>
                                            <th>EMAIL</th>
                                            <th>STATUS</th>                                                            
                                            <th>ACTION</th>                        
                                        </tr>
                                    </thead>
                                    <tbody>        
                                        @forelse($projects as $project)    
                                            <tr>                             
                                                <td>
                                                    @if(!empty($project->so_number))
                                                        {!! $project->so_number !!}
                                                    @else
                                                        N/A
                                                    @endif
                                                </td>                   
                                                <td>{!! $project->project_name !!}</td>
                                                <td>{!! $project->branch !!}</td>
                                                <td>{!! $project->tag !!}</td>
                                                <td>{!! strtoupper($project->contact->company) !!}</td>
                                                <td>                                                    
                                                    @foreach($project->person as $person)
                                                        @if(!empty($person->contact_person))
                                                            {!! $person->contact_person !!}                                         
                                                        @endif
                                                    @endforeach                                                    
                                                </td>
                                                <td>
                                                    @foreach($project->person as $person)  
                                                        @if(!empty($person->mobile_number))
                                                            {!! $person->mobile_number.'/' !!}
                                                        @endif
                                                        @if(!empty($person->tel_no))
                                                            {!! $person->tel_no.'/' !!}
                                                        @endif
                                                    @endforeach                           
                                                </td>                                                
                                                <td>                                                    
                                                    @foreach($project->person as $person)
                                                        @if(!empty($person->email))
                                                            {!! $person->email !!}                                         
                                                        @endif
                                                    @endforeach                                                    
                                                </td>
                                                <td>
                                                    @if($project->status === 0)
                                                        <span class="label label-info"><i class="fa fa-clock-o"></i> For Approval</span>
                                                    @elseif($project->status === 1)
                                                        <span class="label label-success"><i class="fa fa-check"></i> Approved</span>
                                                    @elseif($project->status === 2)
                                                        <span class="label label-primary"> <i class="fa fa-user"></i>Assigned</span>                                       
                                                    @elseif($project->status === 3)
                                                        <span class="label label-danger"> <i class="fa fa-angellist"></i>Denied</span>                                       
                                                    @elseif($project->status === 4)
                                                        <span class="label label-warning"> <i class="fa fa-refresh"></i> For Re-Approve</span>                                       
                                                    @endif
                                                </td>
                                                <td class="action">         
                                                    @if($project->status == 1 || $project->status == 4)                                             
                                                    {!! Html::decode(link_to_Route('projects.overview','<i class="fa fa-eye"></i> Overview', $project->encryptname, array('class' => 'btn btn-primary btn-xs')))!!}                                                    
                                                    @endif
                                                    {!! Html::decode(link_to_Route('projects.edit','<i class="fa fa-pencil"></i> Edit', $project->encryptname, array('class' => 'btn btn-warning btn-xs')))!!}                                                                                                                                                               
                                                </td>
                                            </tr>                           
                                        @empty
                                        @endforelse
                                    </tbody>                                  
                                    </table>
                                </div>
                            </div>
                        </div>                      
                    </div>
                </div>
            </div>
        </div>   
    </div>                 
</div>	
<div class="modal inmodal" id="TransferProjectModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInUp">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-refresh modal-icon"></i>
                <h4 class="modal-title">Transfer Project</h4>                    
            </div>              
            {!! Form::open(array('route'=>'projects.transfer','method'=>'POST')) !!}
                <div class="modal-body">                                        
                    <div class="row" style="padding-top:20px;">
                        <div class="form-group col-lg-12">
                            <div class="row">
                                <div class="col-lg-12">
                                    {!!Form::label('created_by','Transfer To')!!}                 
                                    {!!Form::select('created_by',$employees,null,['class'=>'form-control','id'=>'employee_id'])!!} 
                                    @if ($errors->has('created_by')) <p class="help-block" style="color:red;">{{ $errors->first('created_by') }}</p> @endif
                                </div> 
                                <div class="col-lg-12" style="padding-top:20px;">
                                    {!!Form::label('project_id','Project Name')!!}                 
                                    {!!Form::select('project_id[]',$project_list,null,['class'=>'form-control','id'=>'project_id', 'multiple'=>true])!!} 
                                    @if ($errors->has('project_id')) <p class="help-block" style="color:red;">{{ $errors->first('project_id') }}</p> @endif
                                </div> 
                            </div>
                        </div>                                                                                                     
                    </div>                            
                </div>
                <div class="modal-footer">                        
                    {!! Form::button('Close', array('type' => 'submit', 'class' => 'btn btn-white','data-dismiss'=>'modal')) !!}
                    {!! Form::submit('Submit', array('type' => 'submit', 'class' => 'btn btn-primary')) !!}
                </div>                  
            {!! Form::close() !!}            
        </div>
    </div>
</div>
@stop
@section('page-script')        

    $('#project_id').multiselect({
    maxHeight: 200,
    includeSelectAllOption: true,
    enableCaseInsensitiveFiltering: true,
    enableFiltering: true,
    buttonWidth: '100%',
    buttonClass: 'form-control',
    });       
    
   
@stop