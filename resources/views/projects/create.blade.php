@extends('layouts.master')
@section('main-body')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2><i class="fa fa-plus"></i> NEW PROJECT</h2>        
    </div>
</div>        
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-md-12 animated flash">
            @if(count($errors) > 0 )
                <div class="alert alert-danger alert-dismissible flash" role="alert">            
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
            </button>
                    <center><h4>Oh snap! You got an error!</h4></center>   
                    @foreach ($errors->all() as $error)
                    <center>
                        <div>{{ $error }}</div>
                    </center>
                    @endforeach
                </div>
            @endif
        </div>    
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title" style="background-color:#009688">
                    <h5 style="color:white"><i class="fa fa-plus"></i> Project Form <small></small></h5>          
                </div> 
                <div class="ibox-content">
                    <div class="row">                    
                        <div class="col-lg-12">
                            <div class="row">                        
                                {!!Form::open(array('route'=>'projects.store','method'=>'POST', 'id'=>'myproject','files'=>true))!!}
                                <div class="form-group col-lg-12">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            {!!Form::label('date_reported','Date Reported')!!}                 
                                            <span>{!! Form::text('date_reported', '', array('class' => 'form-control date_input', 'id' => 'date_reported', 'data-date-format' => 'yyyy-mm-dd', 'placeholder' => 'mm/dd/yyyy','required'=>'required')) !!}</span>
                                            @if ($errors->has('date_reported')) <p class="help-block" style="color:red;">{{ $errors->first('date_reported') }}</p> @endif
                                        </div>      
                                        <div class="col-lg-6">
                                            {!!Form::label('tag','Tag')!!}                 
                                            {!!Form::select('tag',$tagging,null,['class'=>'form-control','id'=>'tag_id','required'=>'required'])!!} 
                                            @if ($errors->has('tag')) <p class="help-block" style="color:red;">{{ $errors->first('contact_id') }}</p> @endif
                                        </div>                                          
                                    </div>       
                                </div>           
                                <div class="form-group col-lg-12">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            {!!Form::label('so_number','SO Number')!!}                 
                                            {!!Form::text('so_number','',['class'=>'form-control','placeholder'=>'Enter SO Number','id'=>'so_number'])!!}                                               
                                            @if ($errors->has('so_number')) <p class="help-block" style="color:red;">{{ $errors->first('so_number') }}</p> @endif
                                        </div>      
                                        <div class="col-lg-4">
                                            {!!Form::label('project_name','Project Name')!!}                 
                                            {!!Form::text('project_name','',['class'=>'form-control','placeholder'=>'Enter Project Name','required'=>'required','id'=>'project_name'])!!}                                               
                                            @if ($errors->has('project_name')) <p class="help-block" style="color:red;">{{ $errors->first('project_name') }}</p> @endif
                                        </div>      
                                        <div class="col-lg-4">
                                            {!!Form::label('contact_id','Client Name')!!}                 
                                            {!!Form::select('contact_id',$contacts,null,['class'=>'form-control','id'=>'contact_id','required'=>'required'])!!} 
                                            @if ($errors->has('contact_id')) <p class="help-block" style="color:red;">{{ $errors->first('contact_id') }}</p> @endif
                                        </div>                                    
                                    </div>       
                                </div>                                                                                                   
                                <div class="form-group col-lg-12">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            {!!Form::label('project_range','Project Range')!!} 
                                            <div class="form-group" id="date_range">                                                
                                                <div class="input-daterange input-group" id="datepicker">
                                                    <input type="text" class="input-md form-control" name="start_date" value="{{$date_now}}" required="" />
                                                    <span class="input-group-addon">to</span>
                                                    <input type="text" class="input-md form-control" name="end_date" value="{{$date_now}}" required="" />
                                                </div>
                                            </div>                                
                                        </div>                                        
                                    </div>       
                                </div>        
                                <div class="form-group col-lg-12">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            {!!Form::label('branch','Branch')!!}                 
                                            {!!Form::text('branch','',['class'=>'form-control','placeholder'=>'Enter Branch','required'=>'required'])!!}                                               
                                            @if ($errors->has('branch')) <p class="help-block" style="color:red;">{{ $errors->first('branch') }}</p> @endif
                                        </div>                                     
                                    </div>       
                                </div>         
                                <div class="form-group col-lg-12">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            {!!Form::label('project_details','Project Details')!!}                 
                                            {!!Form::textarea('project_details','',['class'=>'form-control','placeholder'=>'Enter Project Details'])!!}                                               
                                            @if ($errors->has('project_details')) <p class="help-block" style="color:red;">{{ $errors->first('project_details') }}</p> @endif
                                        </div>                                     
                                    </div>       
                                </div>        
                                <div class="form-group col-lg-12">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            {!!Form::label('project_address','Project Address')!!}                 
                                            {!!Form::text('project_address','',['class'=>'form-control','placeholder'=>'Enter Project Address'])!!}                                               
                                            @if ($errors->has('project_address')) <p class="help-block" style="color:red;">{{ $errors->first('project_address') }}</p> @endif
                                        </div>                                     
                                    </div>       
                                </div>           
                                <div class="form-group col-lg-12">
                                    <div class="row">
                                        <div class="form-group col-lg-6">
                                            {!! Form::label('project_cost','Project Cost') !!}
                                            {!! Form::text('project_cost','',['class'=>'form-control','data-mask'=>'$ 999,999.99','placeholder'=>'Project cost...','id'=>'pr_cost']) !!}
                                        </div>
                                        <div class="form-group col-lg-6">
                                            {!! Form::label('allowed_ot','Allowed OT') !!}
                                            {!! Form::number('allowed_ot','',['class'=>'form-control','placeholder'=>'Allowed OT...']) !!}                                        
                                        </div>
                                    </div>
                                </div>                                                                                                                               
                                <div class="form-group col-lg-12">
                                        <div class="form-group">                                
                                            {!! Form::label('attached','Attached File')!!}
                                            {!! Form::file('attached[]', array('id' => 'attach_file', 'class' => 'photo_files')) !!}                                                   
                                        </div>
                                    </div>
                                </div>           
                                <div class="form-group col-lg-12 pull-right">
                                    <div class="row">                                                   
                                        <div class="col-lg-12 pull-right">                                            
                                            {!! Form::hidden('status','0') !!}
                                            {!! Html::decode(link_to_Route('projects.index', '<i class="fa fa-arrow-left"></i> Cancel', [], ['class' => 'btn btn-default'])) !!}
                                            {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'button', 'class' => 'btn btn-primary sbalert')) !!}
                                            {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'submit', 'class' => 'btn btn-primary hidden', 'id'=>'urbtn')) !!}                            
                                        </div>      
                                    </div>      
                                </div>                                                            							                                                                                                
                            </div>
                        </div>                                                                                                        
                        {!! Form::close() !!}       
                    </div>
                </div>                   
            </div>             	                        
        </div>              
    </div> 
</div>    

@stop
@section('page-script')       
    $("#date_reported").datepicker({
        dateFormat: "yy-mm-dd",
        
    });
    
    $('#date_range .input-daterange').datepicker({
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true
    });

    $('#attach_file').filer({

        showThumbs:true,
        addMore:true
    });

    $('.sbalert').click(function () {
        
        if($('#date_reported').val() != "" && $('#project_name').val() != "" && $('#start_date').val() != "" && $('#end_date').val() != ""){
            swal({
                title: "Are you sure?",
                text: "You will now submit your new project",
                type: "info",
                showCancelButton: true,
                confirmButtonColor: "#1abc9c",
                confirmButtonText: "Yes, submit it!",
                closeOnConfirm: false
            }, function () {
                swal("Submitted!", "I will now submit your new project.", "success");
                $('#urbtn').click();
            });
        }
        else{
            $('#urbtn').click();
        }
    });

@stop

