<?php 
use App\AccessControl;
use App\Modules;
?>
<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>ACMS | V3</title>

    <link href="/../../assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/../../assets/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="/../../assets/css/plugins/iCheck/custom.css" rel="stylesheet">

    <link href="/../../assets/css/plugins/dataTables/datatables.min.css" rel="stylesheet">    
    

    <link href="/../../assets/css/animate.css" rel="stylesheet">    
    <link href="/../../assets/css/jquery.filer.css" rel="stylesheet">    
    <link href="/../../assets/css/themes/jquery.filer-dragdropbox-theme.css" rel="stylesheet">
    <link href="/../../assets/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
    <link href="/../../assets/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">       
    {!! Html::style('/assets/datepicker/bootstrap-datetimepicker.min.css') !!}
    {!! Html::style('/assets/css/datepicker/datepicker3.css') !!}
    <link href="/../../assets/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
    <link rel="stylesheet" href="../../assets/bootstrap-multiselect-0.9.13/css/bootstrap-multiselect.css">    
    <link href="/../../assets/css/plugins/ionRangeSlider/ion.rangeSlider.css" rel="stylesheet">
    <link href="/../../assets/css/plugins/ionRangeSlider/ion.rangeSlider.skinFlat.css" rel="stylesheet">

    <link href="/../../assets/css/plugins/fullcalendar/fullcalendar.css" rel="stylesheet">
    <link href="/../../assets/css/plugins/fullcalendar/fullcalendar.print.css" rel='stylesheet' media='print'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/clockpicker/0.0.7/bootstrap-clockpicker.css">

    <link href="/../../assets/css/styleowa.css" rel="stylesheet">
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
            'pusherKey' => config('broadcasting.connections.pusher.key'),
            'pusherCluster' => config('broadcasting.connections.pusher.options.cluster')
        ]) !!};
    </script>
</head>
<body>    
    <?php $routeUri = Route::currentRouteName();?>
    <?php $user = Auth::user(); ?>    
    <?php $imgURL = (!$user->image ? 'default.jpg' : $user->image); ?> 
    <div id="app">
        <div id="wrapper">
            <nav class="navbar-default navbar-static-side" role="navigation">
                <div class="sidebar-collapse">
                    <ul class="nav metismenu" id="side-menu">
                        <li class="nav-header">
                            <div class="dropdown profile-element"> 
                                <span>
                                    <?php if(!empty($user->image)){?>
                                        {!! Html::image('/uploads/users/'.$user->employee_number.'/'.$imgURL,'',['class'=>'img-circle','style'=>'width:50px; height:50px;']) !!}
                                        <?php }
                                    else{?>
                                        <img alt="image" class="img-circle" src="/uploads/users/default.jpg" style="width:50px; height:50px;" />
                                        <?php }?>
                                </span>
                                <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                    <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">{!! strtoupper($user->first_name.' '.$user->last_name) !!}</strong><br />                                                           
                                    <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">{!! strtoupper($user->position->position) !!}</strong>
                                    </span> <span class="text-muted text-xs block"><b class="caret"></b></span> </span> </a>
                                <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                    <li><a href="#">Profile</a></li>                                
                                    <li><a href="../../user/reset/password.php">Change Password</a></li>
                                    <li><a href="#">Change Status</a></li>                                
                                    <li class="divider"></li>
                                    <li></li>   
                                </ul>
                            </div>                        
                            <div class="logo-element">
                                ACMS
                            </div>
                        </li>            
                        <?php 
                            $access_controls = AccessControl::where('user_id',$user->id)->get(); 
                        ?>    
                        <?php $accccc = []; ?>
                        @foreach($access_controls as $ac)
                            <?php $accccc[] = $ac->module_id; ?>                    
                        @endforeach                        

                        <?php 
                            //maintenance
                            $fm = [1,2,3,4,5,6,14,18];                                                 
                            $fms = array_intersect($fm, $accccc);  

                            //reports
                            $rep = [21,22,23];
                            $reps = array_intersect($rep, $accccc);
                        ?>                    
                        <li class="<?php echo ($routeUri=='dashboard') ? 'active' : '' ?>">{!! Html::decode(link_to_route('dashboard.index', '
                            <i class="fa fa-dashboard fa-2x"></i>
                            <span class="nav-label">Dashboard</span>', array(), ['class' => 'large button'])) !!}
                        </li>       
                        @if(count($fms) > 0)
                        <!-- maintenance -->
                        <?php $filemaccess = AccessControl::where('user_id',$user->id)->whereIn('module_id',$fms)->get(); ?>
                        <li>
                            <a href="#"><i class="fa fa-gears fa-2x"></i> <span class="nav-label">File Maintenance </span><span class="fa arrow fa-2x"></span></a>                        
                            <ul class="nav nav-second-level collapse">       
                                @foreach($filemaccess as $fma)                     
                                <li class="{{ ( $routeUri == $fma->module->routeUri) ? 'active' : ' ' }} ">
                                    <a href="{{url($fma->module->routeUri)}}">
                                        <i class="{{$fma->module->icon}}"></i>
                                        <span class="nav-label">{{$fma->module->module}}</span>                                
                                    </a>
                                </li>                      
                                @endforeach
                            </ul>
                        </li>
                        @endif 
                        @if(count($reps) > 0)
                        <?php $repsaccess = AccessControl::where('user_id',$user->id)->whereIn('module_id',$reps)->get(); ?>
                        <li>
                            <a href="#"><i class="fa fa-gears fa-2x"></i> <span class="nav-label">Reports </span><span class="fa arrow fa-2x"></span></a>                        
                            <ul class="nav nav-second-level collapse">       
                                @foreach($repsaccess as $repa)                     
                                <li class="{{ ( $routeUri == $repa->module->routeUri) ? 'active' : ' ' }} ">
                                    <a href="{{url($repa->module->routeUri)}}">
                                        <i class="{{$repa->module->icon}}"></i>
                                        <span class="nav-label">{{$repa->module->module}}</span>                                
                                    </a>
                                </li>                      
                                @endforeach
                            </ul>
                        </li>
                        @endif
                        <?php $allaccessc = AccessControl::where('user_id',$user->id)
                                                ->whereNotIn('module_id',$fms)                                            
                                                ->whereNotIn('module_id',$reps)   
                                                ->get(); ?>
                        @foreach($allaccessc as $allacc)
                            <li class="{{ ( $routeUri == $allacc->module->routeUri) ? 'active' : ' ' }} ">
                                <a href="{{url($allacc->module->routeUri)}}">
                                    <i class="{{$allacc->module->icon}}"></i>
                                    <span class="nav-label">{{$allacc->module->module}}</span>                                
                                </a>
                            </li>                        
                        @endforeach             
                    </ul>
                </div>
            </nav>

            <div id="page-wrapper" class="gray-bg">
                <div class="row border-bottom">
                    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                        <div class="navbar-header">
                            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
                            <form role="search" class="navbar-form-custom" action="#">
                                <div class="form-group">
                                    <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search">
                                </div>
                            </form>
                        </div>
                        <ul class="nav navbar-top-links navbar-right">
                            <li>
                                <span class="m-r-sm text-muted welcome-message">Welcome to ACMS.</span>
                            </li>
                            <!-- <li class="dropdown">
                                <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                                    <i class="fa fa-envelope"></i>  <span class="label label-warning">16</span>
                                </a>
                                <ul class="dropdown-menu dropdown-messages">
                                    <li>
                                        <div class="dropdown-messages-box">
                                            <a href="profile.html" class="pull-left">
                                                <img alt="image" class="img-circle" src="/assets/img/a7.jpg">
                                            </a>
                                            <div class="media-body">
                                                <small class="pull-right">46h ago</small>
                                                <strong>Mike Loreipsum</strong> started following <strong>Monica Smith</strong>. <br>
                                                <small class="text-muted">3 days ago at 7:58 pm - 10.06.2014</small>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <div class="dropdown-messages-box">
                                            <a href="profile.html" class="pull-left">
                                                <img alt="image" class="img-circle" src="/assets/img/a4.jpg">
                                            </a>
                                            <div class="media-body ">
                                                <small class="pull-right text-navy">5h ago</small>
                                                <strong>Chris Johnatan Overtunk</strong> started following <strong>Monica Smith</strong>. <br>
                                                <small class="text-muted">Yesterday 1:21 pm - 11.06.2014</small>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <div class="dropdown-messages-box">
                                            <a href="profile.html" class="pull-left">
                                                <img alt="image" class="img-circle" src="/assets/img/profile.jpg">
                                            </a>
                                            <div class="media-body ">
                                                <small class="pull-right">23h ago</small>
                                                <strong>Monica Smith</strong> love <strong>Kim Smith</strong>. <br>
                                                <small class="text-muted">2 days ago at 2:30 am - 11.06.2014</small>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <div class="text-center link-block">
                                            <a href="mailbox.html">
                                                <i class="fa fa-envelope"></i> <strong>Read All Messages</strong>
                                            </a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                                    <i class="fa fa-bell"></i>  <span class="label label-primary">8</span>
                                </a>
                                <ul class="dropdown-menu dropdown-alerts">
                                    <li>
                                        <a href="mailbox.html">
                                            <div>
                                                <i class="fa fa-envelope fa-fw"></i> You have 16 messages
                                                <span class="pull-right text-muted small">4 minutes ago</span>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a href="profile.html">
                                            <div>
                                                <i class="fa fa-twitter fa-fw"></i> 3 New Followers
                                                <span class="pull-right text-muted small">12 minutes ago</span>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a href="grid_options.html">
                                            <div>
                                                <i class="fa fa-upload fa-fw"></i> Server Rebooted
                                                <span class="pull-right text-muted small">4 minutes ago</span>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <div class="text-center link-block">
                                            <a href="notifications.html">
                                                <strong>See All Alerts</strong>
                                                <i class="fa fa-angle-right"></i>
                                            </a>
                                        </div>
                                    </li>
                                </ul>
                            </li> -->
                            <li>{!! Html::decode(link_to_route('logout', '<i class="fa fa-sign-out"></i> Log out',array(), ['class' => 'large button'])) !!}</li>                    
                        </ul>
                    </nav>
                    <div class="page-inner">
                        <div class="col-lg-12">
                            @section('main-body')
                            @show  
                        </div>
                    </div>
                    <div class="footer">
                        <div class="pull-right">
                            <strong>PROVERBS 16:3</strong>
                        </div>
                        <div>
                            <strong>Copyright</strong> CHASE TECHNOLOGIES CORPORATION 2016
                        </div>
                    </div>
                </div>
            
            </div>  
            
            
          
        </div>
    </div>

    <!-- Mainly scripts -->    
    <script src="/../../assets/js/plugins/fullcalendar/moment.min.js"></script>
    <script src="/../../assets/js/jquery-3.1.1.min.js"></script>
    <script src="/../../assets/js/bootstrap.min.js"></script>
    <script src="/../../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="/../../assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>    
    <!-- Full Calendar -->
    <script src="/../../assets/js/plugins/fullcalendar/fullcalendar.min.js"></script>
    <script src="/../../assets/js/plugins/dataTables/datatables.min.js"></script>
    <script src="/../../assets/js/jquery.filer.js"></script>    

    <!-- Custom and plugin javascript -->
    <script src="/../../assets/js/inspinia.js"></script>
    <script src="/assets/js/plugins/pace/pace.min.js"></script>    
    <script src="/../../assets/js/plugins/iCheck/icheck.min.js"></script>       
     <!-- Data picker -->
    <script src="/../../assets/js/plugins/datapicker/bootstrap-datepicker.js"></script>
    <script src="/../../assets/js/plugins/daterangepicker/daterangepicker.js"></script>
    {!! Html::script('/assets/js/datepicker/bootstrap-datepicker.js') !!}
    {!! Html::script('/assets/datepicker/bootstrap-datetimepicker.js') !!}
    {!! Html::script('/assets/datepicker/bootstrap-datetimepicker.es.js') !!}
    <script src="/../../assets/js/plugins/sweetalert/sweetalert.min.js"></script>
    <script src="/../../assets/bootstrap-multiselect-0.9.13/js/bootstrap-multiselect.js"></script>          
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pusher/4.2.2/pusher.js"></script>          
    <script src="https://cdnjs.cloudflare.com/ajax/libs/clockpicker/0.0.7/bootstrap-clockpicker.js"></script>          

     <script type="text/javascript">
          $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
          });
      </script>    
    <script type="text/javascript">
        
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,                                            
            });

            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });

           @section('page-script')                
           @show
        });                      
    </script>
    @section('page-calendar')
    @show    
</body>
</html>
