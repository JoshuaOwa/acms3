@extends('layouts.master')
@section('main-body')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2><i class="fa fa-plus"></i> REQUEST DETAILS</h2>    
    </div>
</div>        
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-md-12 animated flash">
            <?php if (session('is_success')): ?>
                <div class="alert alert-success alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                    <center><h4>Remarks has been Added!<i class="fa fa-check"></i></h4></center>                
                </div>
            <?php endif;?>
        </div>
        <div class="col-md-12 animated flash">
            <?php if (session('is_success_jo')): ?>
                <div class="alert alert-success alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                    <center><h4>Job Order has been Added!<i class="fa fa-check"></i></h4></center>                
                </div>
            <?php endif;?>
        </div>
        <div class="col-md-12 animated flash">
            @if( count ($errors) > 0 )
                <div class="alert alert-danger alert-dismissible flash" role="alert">            
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
            </button>
                    <center><h4>Oh snap! You got an error!</h4></center>   
                </div>
            @endif
        </div>    
                   
    </div>    
</div>    
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">        
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title" style="background-color:#009688">
                    <h5 style="color:white"><i class="fa fa-plus"></i> REQUEST DETAILS <small></small></h5>          
                    @if($req->status < 3)
                    <div class="pull-right">                                                                            
                        <button type="button" class="btn btn-white btn-xs" data-toggle="modal" data-target="#jobOrderModel">
                            Create Job Order
                        </button>                                                                      
                    </div>      
                    @endif
                </div> 
                <div class="ibox-content">                    
                    <table class="table table-hover table-bordered">
                        <tr>
                            <td style="width:30%"><i><strong>REQUESTED BY:</strong></i></td><td>{!! strtoupper($req->user->first_name.' '.$req->user->last_name) !!}</td>
                        </tr>                       
                        <tr>
                            <td style="width:30%"><i><strong>REQUEST #:</strong></i></td><td>{!! $req->request_no !!}</td>
                        </tr>
                        <tr>
                            <td style="width:30%"><i><strong>REQUEST CODE:</strong></i></td><td>{!! $req->request_code !!}</td>
                        </tr>      
                        <tr>
                            <td style="width:30%"><i><strong>CONTACT:</strong></i></td><td>{!! $req->contact !!}</td>
                        </tr>      
                        <tr>
                            <td style="width:30%"><i><strong>PROJECT:</strong></i></td><td>{!! $req->project !!}</td>
                        </tr>      
                                                         
                        <tr>
                            <td style="width:30%"><i><strong>STATUS:</strong></i></td>
                            <td>
                                @if($req->status == 1)
                                    <span class="label label-warning"> <i class="fa fa-angellist"></i>Pending</span>
                                @elseif($req->status == 2)
                                    <span class="label label-success"> <i class="fa fa-angellist"></i>Returned</span>
                                @else
                                    <span class="label label-primary"> <i class="fa fa-angellist"></i>Closed</span>
                                @endif
                            </td>
                        </tr>                        
                        <tr>
                            <td style="width:30%"><i><strong>DETAILS:</strong></i></td><td>{!! strtoupper($req->details) !!}</td>
                        </tr>    
                        <tr>
                            <td style="width: 30%"><i><strong>ATTACHMENTS:</strong></i></td>
                            <td>
                                @foreach($files as $file)                                                                         
                                    
                                    {!! Form::hidden('encname',$file->encryptname) !!}
                                    {!! Form::submit($file->filename, array('type' => 'submit', 'class' => 'btn btn-primary btn-xs')) !!}                                              
                                @endforeach     
                            </td>
                        </tr>                        
                    </table>                                         
                </div>                   
            </div>                                      
        </div> 
        <div class="row">
            <div class="col-lg-6">            
                <div class="ibox float-e-margins">
                    <div class="ibox-title" style="background-color:#009688">
                        <h5 style="color:white"><i class="fa fa-plus"></i> ADD REMARKS <small></small></h5>          
                    </div>                
                    {!! Form::open(array('route'=>'my_requests.add_remarks','method'=>'POST','files'=>true)) !!}
                    <div class="ibox-content">
                        <div class="form-group" >
                            {!! Form::label('remarks','Add Your Remarks') !!}
                            {!! Form::textarea('remarks','',['class'=>'form-control','placeholder'=>'You Remarks Here...','required'=>'required','id'=>'remarkid']) !!}                       
                        </div>
                        <div class="row" style="padding-top:5px;">
                            <div class="col-lg-12">                  
                                {!! Form::label('attached','Attached File')!!}
                                {!! Form::file('attached[]', array('id' => 'filer_inputs', 'class' => 'photo_files', 'accept' => 'pdf|docx')) !!}                   
                            </div>
                        </div>
                        <div class="form-group">  
                            {!! Form::hidden('request_id',$req->id) !!}                                                                                                   
                            <button class="btn btn-warning hidden" id="sub1" name ="sub" value="1" this.form.submit();">Remarks Only</button>
                            <button class="btn btn-primary hidden" id="sub2" name ="sub" value="2" this.form.submit();">Return</button>
                            <button class="btn btn-primary hidden" id="sub3" name ="sub" value="3" this.form.submit();">Reopen</button>
                            <button class="btn btn-primary hidden" id="sub4" name ="sub" value="4" this.form.submit();">Close</button>
                            @if($req->status < 3)
                                {!! Form::button('<i class="fa fa-comment"></i> Remarks Only', array('type' => 'button', 'class' => 'btn btn-warning','id'=>'rembtn')) !!}                        
                            @elseif($req->status != 1)                        
                                {!! Form::button('<i class="fa fa-exchange"></i> Re-open', array('type' => 'button', 'class' => 'btn btn-primary','id'=>'reobtn')) !!}
                            @endif
                            @if($req->status < 2)
                                {!! Form::button('<i class="fa fa-exchange"></i> Return', array('type' => 'button', 'class' => 'btn btn-primary','id'=>'retbtn')) !!}
                            @elseif($req->status < 3)
                                {!! Form::button('<i class="fa fa-stop"></i> Close', array('type' => 'button', 'class' => 'btn btn-primary','id'=>'clobtn')) !!}
                            @endif
                        </div>                      
                    </div> 
                    {!! Form::close() !!}                                  
                </div> 
            </div>
            <div class="col-lg-6">
                <div class="ibox float-e-margins">
                     <div class="ibox-title" style="background-color:#009688">
                        <h5 style="color:white">Remarks</h5>                    
                    </div>

                    <div class="ibox-content inspinia-timeline" id="flow2">
                    @forelse($remarks as $remark)
                        <div class="timeline-item">
                            <div class="row">
                                <div class="col-xs-3 date">
                                    <i class="fa fa-briefcase"></i>
                                    {!! $remark->created_at->format('M-d-Y h:i a') !!}
                                    <br/>
                                    <small class="text-navy">{!! $remark->created_at->diffForHumans() !!}</small>
                                </div>
                                <div class="col-xs-7 content no-top-border">
                                    <p class="m-b-xs"><strong>{!! strtoupper($remark->user->first_name.' '.$remark->user->last_name ) !!}</strong></p>
                                    <p>{!! $remark->remarks !!}</p>                                
                                    <div>
                                        @if(!empty($remark->files))
                                            @foreach($remark->files as $filer)
                                                {!! Form::open(array('route'=>'my_requests.remarks_download_files','method'=>'POST')) !!}
                                                    {!! Form::hidden('encname',$filer->encryptname) !!}
                                                    {!! Form::submit($filer->filename, array('type' => 'submit', 'class' => 'btn btn-primary btn-xs')) !!}                                                          
                                                    {!! Form::hidden('request_id',$req->id) !!}        
                                                {!! Form::close() !!}                                                                                                                                         
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    @empty
                        ....No Remarks Found
                    @endforelse   
                    </div>
                </div>
            </div> 
            <div class="col-lg-12">                
                <div class="ibox float-e-margins">
                    <div class="ibox-title" style="background-color:#009688">
                        <h5 style="color:white"><i class="fa fa-briefcase"></i> JOB ORDERS LINK TO THIS REQUEST <small></small></h5>    
                        @if($req->status < 3)
                        <div class="pull-right">                                                    
                            <button type="button" class="btn btn-white btn-xs" data-toggle="modal" data-target="#jobOrderModel">
                                Create Job Order
                            </button>                                                                      
                        </div>               
                        @endif       
                    </div>                                    
                    <div class="ibox-content">
                        <div class="table-responsive">                                        
                            <table class="table table-striped table-hover dataTables-example" >
                                <thead>
                                    <tr>                                                                    
                                        <th>JO #</th>
                                        <th>CREATED BY</th>
                                        <th>ASSIGNED TO</th>
                                        <th>TOTAL EXPENSE</th>                                                
                                        <th>STATUS</th>
                                        <th>ACTION</th>                        
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($joborders as $job)
                                        <tr>
                                            <td>{!! $job->id !!}</td>
                                            <td>{!! strtoupper($job->user->first_name.' '.$job->user->last_name) !!}</td>
                                            <td>{!! strtoupper($job->assigned->first_name.' '.$job->assigned->last_name) !!}</td>
                                            <td></td>
                                            <td>
                                                @if($job->status == 1)
                                                    <span class="label label-warning"> <i class="fa fa-angellist"></i>Pending</span>
                                                @elseif($job->status == 2)
                                                    <span class="label label-success"> <i class="fa fa-angellist"></i>Returned</span>
                                                @elseif($job->status == 3)
                                                    <span class="label label-primary"> <i class="fa fa-angellist"></i>Closed</span>
                                                @elseif($job->status == 4)
                                                    <span class="label label-info"> <i class="fa fa-angellist"></i>Not Assigned</span>
                                                @endif
                                            </td>
                                            <td></td>
                                        </tr>
                                    @endforeach
                                </tbody>                        
                            </table>
                        </div>          
                    </div>                     
                </div> 
            </div>
        </div>        
    </div>    
</div>   
<!-- Modals -->
<div class="modal inmodal" id="jobOrderModel" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-briefcase modal-icon"></i>
                <h4 class="modal-title">Job Order</h4>                    
            </div>              
                <div class="modal-body">                 
                    {!! Form::open(array('route'=>'job_orders.store','method'=>'POST','files'=>true)) !!}                                                                               
                        <div class="row" style="padding-top:50px;">                                    
                            <div class="col-lg-6">
                                {!! Form::label('assigned_to','Send To') !!}
                                {!! Form::select('assigned_to[]',$users,'',['class'=>'form-control','id'=>'assignedto_id','multiple'=>'multiple','required'=>'required']) !!}                                    
                            </div>                           
                        </div>               
                        <div class="row" style="padding-top:10px ">
                            <div class="col-lg-6">
                                {!! Form::label('settings','Settings')!!}                 
                                {!! Form::select('settings',$settings,'',['class'=>'form-control','id'=>'settings'])!!}
                            </div>      
                        </div>                                                
                        <div class="row-1" style="padding-top:10px;" id="normdate">
                            {!!Form::label('date','Date')!!}         
                            <div class="form-group" id="date_range">                                                
                                <div class="input-daterange input-group" id="datepicker">
                                    <input type="text" class="input-md form-control" name="start_date" value="{{$date_now}}" required="" />
                                    <span class="input-group-addon">to</span>
                                    <input type="text" class="input-md form-control" name="end_date" value="{{$date_now}}" required="" />
                                </div>
                            </div>                                
                        </div>     
                        <div class="row hidden" style="padding-top:10px;" id="week">
                            <div class="col-lg-6">
                                {!! Form::label('weeks','What Day?')!!}                 
                                {!! Form::select('weeks',$everyweek,'',['class'=>'form-control'])!!}
                            </div>                            
                        </div>                                                
                        <div class="row" style="padding-top:30px;">
                            <div class="col-lg-12">
                                {!!Form::label('details','Details')!!}         
                                {!! Form::textarea('details','',['class'=>'form-control','id'=>'job_order_details','required'=>'required']) !!}
                            </div>
                        </div>                    
                        <div class="row" style="padding-top:10px;">
                            <div class="col-lg-12">                            
                                {!!Form::label('attached','Attached File')!!}                             
                                {!! Form::file('attached[]', array('id' => 'joinputs', 'class' => 'photo_files', 'accept' => 'pdf|docx')) !!}                   
                            </div>
                        </div>                    
                </div>
                <div class="modal-footer">   
                    {!! Form::hidden('request_enc',$encrypt) !!}                                   
                    {!! Form::button('Close', array('type' => 'submit', 'class' => 'btn btn-white','data-dismiss'=>'modal')) !!}
                    {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'button', 'class' => 'btn btn-primary pull-right sbalert')) !!}
                    {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'submit', 'class' => 'btn btn-primary hidden', 'id'=>'urbtn')) !!}                    
                </div>   
            {!! Form::close() !!}                           
        </div>
    </div>
</div>
@endsection
@section('page-script')
    

    $('#filer_inputs,#joinputs').filer({

        showThumbs:true,
        addMore:true
    });   

    $('#assignedto_id').multiselect({
        maxHeight: 200,    
        includeSelectAllOption: true,
        enableCaseInsensitiveFiltering: true,
        enableFiltering: true,
        buttonWidth: '100%',
        buttonClass: 'form-control'        
    });

    $('#rembtn').click(function () {

        if($('#remarkid').val() != ""){
            swal({
                title: "Good job!",
                text: "Your remarks will be save!",
                type: "success"
            });            
            $('#sub1').click();
        }else{

            $('#sub1').click();
        }                            
    });

    $('#retbtn').click(function () {

        if($('#remarkid').val() != ""){
            swal({
                title: "Good job!",
                text: "Your remarks will be save and the request will be returned!",
                type: "success"
            });            
            $('#sub2').click();
        }else{

            $('#sub2').click();
        }                            
    });

    $('#reobtn').click(function () {

        if($('#remarkid').val() != ""){
            swal({
                title: "Good job!",
                text: "Your remarks will be save and the request will be re-open!",
                type: "success"
            });            
            $('#sub3').click();
        }else{

            $('#sub3').click();
        }                          
    });

    $('#clobtn').click(function () {

        if($('#remarkid').val() != ""){
            swal({
                title: "Good job!",
                text: "Your remarks will be save and the request will be closed!",
                type: "success"
            });            
            $('#sub4').click();
        }else{

            $('#sub4').click();
        }                            
    });

    $('#settings').on("change", function(){

        var x = $('#settings').val();               
        $('#week').addClass('hidden');
        if(x == 3){

            $('#week').removeClass('hidden');
        }
        
    });  

    $('#date_range .input-daterange').datepicker({
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true
    });   

    $('.sbalert').click(function () {

        if($('#assignedto_id').val() != ""){

            if($('#job_order_details').val() != ""){

                swal({
                    title: "Good job!",
                    text: "I'm now going to send this job order!",
                    type: "success"
                    });            
                $('#urbtn').click();
            }else{

                swal({
                    title: "Warning",
                    text: "Hey people! I think you should put some details for this job order!",
                    type: "warning"
                });     
            }            
        }else{

            swal({
                title: "Warning",
                text: "Oh Come on! You forgot to assign somebody to this job order!",
                type: "warning"
            });      
        }                            
    });
@stop
