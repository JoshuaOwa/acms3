@extends('layouts.master')
@section('main-body')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2><i class="fa fa-plus"></i> REQUEST DETAILS</h2>    
    </div>
</div>        
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-md-12 animated flash">
            <?php if (session('is_success')): ?>
                <div class="alert alert-success alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                    <center><h4>Remarks has been Added!<i class="fa fa-check"></i></h4></center>                
                </div>
            <?php endif;?>
        </div>        
        <div class="col-md-12 animated flash">
            @if( count ($errors) > 0 )
                <div class="alert alert-danger alert-dismissible flash" role="alert">            
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
            </button>
                    <center><h4>Oh snap! You got an error!</h4></center>   
                </div>
            @endif
        </div>    
                   
    </div>    
</div>    
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">        
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title" style="background-color:#009688">
                    <h5 style="color:white"><i class="fa fa-plus"></i> REQUEST DETAILS <small></small></h5>                              
                </div> 
                <div class="ibox-content">                    
                    <table class="table table-hover table-bordered">
                        <tr>
                            <td style="width:30%"><i><strong>REQUESTED BY:</strong></i></td><td>{!! strtoupper($req->user->first_name.' '.$req->user->last_name) !!}</td>
                        </tr>                       
                        <tr>
                            <td style="width:30%"><i><strong>REQUEST #:</strong></i></td><td>{!! $req->request_no !!}</td>
                        </tr>  
                        <tr>
                            <td style="width:30%"><i><strong>INVOLVED DEPARTMENT</strong></i></td><td>{!! $req->dept !!}</td>
                        </tr>  
                        <tr>
                            <td style="width:30%"><i><strong>SUBJECT:</strong></i></td>
                            <td>
                                @if(!empty($req->subject))
                                    {!! $req->subject !!}
                                @else
                                    N/A
                                @endif
                            </td>
                        </tr>                        
                        <tr>
                            <td style="width:30%"><i><strong>COMPANY:</strong></i></td><td>{!! $req->contact !!}</td>
                        </tr>      
                        <tr>
                            <td style="width:30%"><i><strong>PROJECT:</strong></i></td><td>{!! $req->project !!}</td>
                        </tr>      
                                                         
                        <tr>
                            <td style="width:30%"><i><strong>STATUS:</strong></i></td>
                            <td>
                                @if($req->status == 1)
                                    <span class="label label-warning"> <i class="fa fa-angellist"></i>Pending</span>
                                @elseif($req->status == 2)
                                    <span class="label label-success"> <i class="fa fa-angellist"></i>Returned</span>
                                @else
                                    <span class="label label-primary"> <i class="fa fa-angellist"></i>Closed</span>
                                @endif
                            </td>
                        </tr>                        
                        <tr>
                            <td style="width:30%"><i><strong>DETAILS:</strong></i></td><td>{!! strtoupper($req->details) !!}</td>
                        </tr>    
                        <tr>
                            <td style="width:30%"><i><strong>DATE RANGE: </strong></i></td><td>{!! $req->start_date.' - '.$req->end_date !!}</td>
                        </tr>    
                        <tr>
                            <td style="width:30%"><i><strong>NEED UPDATES: </strong></i></td>
                            <td>
                                @if($req->recuring == 1)
                                    <span class="label label-info"> <i class="fa fa-angellist"></i>DAILY</span>
                                @elseif($req->recuring == 2)
                                    <span class="label label-info"> <i class="fa fa-angellist"></i>WEEKLY</span>
                                @else
                                    <span class="label label-info"> <i class="fa fa-angellist"></i>MONTHLY</span>
                                @endif
                            </td>
                        </tr>    
                        <tr>
                            <td style="width: 30%"><i><strong>ATTACHMENTS:</strong></i></td>
                            <td>
                                @foreach($files as $filer)                                                                         
                                    
                                    {!! Form::open(array('route'=>'my_requests.download','method'=>'POST')) !!}
                                        {!! Form::hidden('encname',$filer->encrpytname) !!}
                                        {!! Form::submit($filer->filename, array('type' => 'submit', 'class' => 'btn btn-primary btn-xs')) !!}                                                          
                                        {!! Form::hidden('request_id',$req->id) !!}        
                                    {!! Form::close() !!}     
                                @endforeach                                     
                            </td>
                        </tr>                        
                    </table>                                         
                </div>                   
            </div>                                      
        </div> 
        <div class="row">
            <div class="col-lg-12 animated flash">
                <?php if (session('is_added')): ?>
                    <div class="alert alert-success alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                        <center><h4>New Member has been added!<i class="fa fa-check"></i></h4></center>                
                    </div>
                <?php endif;?>            
            </div>        
        </div>    
        <div class="row">
            <div class="col-lg-6">
                <div class="ibox float-e-margins">
                    <div class="ibox-title" style="background-color:#009688">
                        <div class="pull-right">                                                                            
                            <button type="button" class="btn btn-white btn-xs" data-toggle="modal" data-target="#workgroupAddModal">
                                Add Member
                            </button>                                                                      
                        </div>                          
                    </div>
                    <div class="ibox-content ibox-heading">
                        <center><h3><i class="fa fa-users"> Participants</i></h3></center>
                    </div>
                    <div class="ibox-content">
                        <table class="table table-hover no-margins">
                            <thead>
                            <tr>
                                <th>Name</th>                                
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($members as $member)
                                    <tr>
                                        <td>{!! strtoupper($member->user->first_name.' '.$member->user->last_name) !!}</td>
                                    </tr>                   
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div> 
            <div class="col-lg-6">
                <div class="ibox float-e-margins">
                    <div class="ibox-title" style="background-color:#009688">
                        <div class="pull-right">                                                                            
                            <button type="button" class="btn btn-white btn-xs" data-toggle="modal" data-target="#workgroupAddCCModal">
                                Add CC
                            </button>       
                            <button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#workgroupRemoveCCModal">
                                Remove CC
                            </button>                                                                      
                        </div>                          
                    </div>
                    <div class="ibox-content ibox-heading">
                        <center><h3><i class="fa fa-users"> CC</i></h3></center>
                    </div>
                    <div class="ibox-content">
                        <table class="table table-hover no-margins">
                            <thead>
                            <tr>
                                <th>Name</th>                                
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($ccs as $cc)
                                    <tr>
                                        <td>{!! strtoupper($cc->full_name) !!}</td>
                                    </tr>                   
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div> 
        </div>
        <div class="row">
            <div class="col-lg-6">            
                <div class="ibox float-e-margins">
                    <div class="ibox-title" style="background-color:#009688">
                        <h5 style="color:white"><i class="fa fa-plus"></i> ADD REMARKS <small></small></h5>          
                    </div>                
                    {!! Form::open(array('route'=>'my_requests.add_remarks','method'=>'POST','files'=>true)) !!}
                    <div class="ibox-content">
                        <div class="form-group" >
                            {!! Form::label('remarks','Add Your Remarks') !!}
                            {!! Form::textarea('remarks','',['class'=>'form-control','placeholder'=>'You Remarks Here...','required'=>'required','id'=>'remarkid']) !!}                       
                        </div>
                        <div class="row" style="padding-top:5px;">
                            <div class="col-lg-12">                  
                                {!! Form::label('attached','Attached File')!!}
                                {!! Form::file('attached[]', array('id' => 'filer_inputs', 'class' => 'photo_files', 'accept' => 'pdf|docx')) !!}                   
                            </div>
                        </div>
                        <div class="form-group">  
                            {!! Form::hidden('request_id',$req->id) !!}                                                                                                   
                            <button class="btn btn-warning hidden" id="sub1" name ="sub" value="1" this.form.submit();">Remarks Only</button>                            
                            <button class="btn btn-primary hidden" id="sub2" name ="sub" value="2" this.form.submit();">Return</button>
                            <button class="btn btn-primary hidden" id="sub3" name ="sub" value="3" this.form.submit();">Reopen</button>
                            <button class="btn btn-primary hidden" id="sub4" name ="sub" value="4" this.form.submit();">Close</button>
                            @if($req->status < 3)
                                {!! Form::button('<i class="fa fa-comment"></i> Remarks Only', array('type' => 'button', 'class' => 'btn btn-warning','id'=>'rembtn')) !!}                        
                            @endif                                
                            @if($req->status == 2)      
                                @if($uid == $req->requestor)                  
                                    {!! Form::button('<i class="fa fa-exchange"></i> Re-open', array('type' => 'button', 'class' => 'btn btn-primary','id'=>'reobtn')) !!}
                                @endif                                    
                            @endif
                            @if($req->status < 2)
                                @if($uid != $req->requestor)
                                    {!! Form::button('<i class="fa fa-exchange"></i> Return', array('type' => 'button', 'class' => 'btn btn-primary','id'=>'retbtn')) !!}
                                @endif
                            @elseif($req->status < 3)
                                @if($uid == $req->requestor)
                                    {!! Form::button('<i class="fa fa-stop"></i> Close', array('type' => 'button', 'class' => 'btn btn-primary','id'=>'clobtn')) !!}
                                @endif
                            @endif
                        </div>                      
                    </div> 
                    {!! Form::close() !!}                                  
                </div> 
            </div>
            <div class="col-lg-6">
                <div class="ibox float-e-margins">
                     <div class="ibox-title" style="background-color:#009688">
                        <h5 style="color:white">Remarks</h5>                    
                    </div>

                    <div class="ibox-content inspinia-timeline" id="flow2">
                    @forelse($remarks as $remark)
                        <div class="timeline-item">
                            <div class="row">
                                <div class="col-xs-3 date">
                                    <i class="fa fa-briefcase"></i>
                                    {!! $remark->created_at->format('M-d-Y h:i a') !!}
                                    <br/>
                                    <small class="text-navy">{!! $remark->created_at->diffForHumans() !!}</small>
                                </div>
                                <div class="col-xs-7 content no-top-border">
                                    <p class="m-b-xs"><strong>{!! strtoupper($remark->user->first_name.' '.$remark->user->last_name ) !!}</strong></p>
                                    <p>{!! $remark->remarks !!}</p>                                
                                    <div>
                                        @if(!empty($remark->files))
                                            @foreach($remark->files as $filer)
                                                {!! Form::open(array('route'=>'my_requests.remarks_download_files','method'=>'POST')) !!}
                                                    {!! Form::hidden('encname',$filer->encryptname) !!}
                                                    {!! Form::submit($filer->filename, array('type' => 'submit', 'class' => 'btn btn-primary btn-xs')) !!}                                                          
                                                    {!! Form::hidden('request_id',$req->id) !!}        
                                                {!! Form::close() !!}                                                                                                                                         
                                            @endforeach
                                        @endif
                                    </div>
                                    @if($req->requestor != $remark->user_id)
                                    <p><b>Rating: </b>
                                        @if($remark->rating < 1)                                                                
                                            @if($req->requestor == $uid)
                                                <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" value = "{{$remark->id}}" id = "{{$remark->id}}" onclick="remarkRatingFunction(this)"><i class="fa fa-smile-o"></i> Rate</button>                   
                                            @else
                                            N/A                     
                                            @endif
                                        @else
                                            @if($remark->rating == 1)
                                                <i class="fa fa-frown-o fa-2x"></i>
                                            @elseif($remark->rating == 2)
                                                <i class="fa fa-meh-o fa-2x"></i>
                                            @elseif($remark->rating == 3)        
                                                <i class="fa fa-smile-o fa-2x"></i>
                                            @endif                                    
                                        @endif
                                    </p>                                    
                                    @endif
                                </div>
                            </div>
                        </div>
                    @empty
                        ....No Remarks Found
                    @endforelse   
                    </div>
                </div>
            </div>            
        </div>        
    </div>    
</div>   
<!-- Modals -->
<div class="modal inmodal" id="remarkRatingModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-smile-o fa-spin modal-icon"></i>
                <h4 class="modal-title">Rating</h4>                    
            </div>              
            {!! Form::open(array('route'=>'my_requests.add_rating','method'=>'POST','files'=>true)) !!}                                                                               
            <div class="modal-body">                                 
                <div class="row" style="padding-top:50px;">                                    
                    <div class="col-lg-6">
                        {!! Form::label('rate','Rate:') !!}                                                
                        <div class="i-checks"><label><i class="fa fa-frown-o fa-2x"> - Poor <input type="radio" checked="" value="1" name="rating"></i></label></div>
                        <div class="i-checks"><label><i class="fa fa-meh-o fa-2x"> - Satisfactory <input type="radio" value="2" name="rating"></i></label></div>
                        <div class="i-checks"><label><i class="fa fa-smile-o fa-2x"> - Very Good <input type="radio" value="3" name="rating"></i></label></div>
                    </div>                           
                </div>
            </div>
            <div class="modal-footer">   
                {!! Form::hidden('request_id','',['id'=>'requestID']) !!}                                                   
                {!! Form::button('Close', array('type' => 'submit', 'class' => 'btn btn-white','data-dismiss'=>'modal')) !!}            
                {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'button', 'class' => 'btn btn-primary sbalert','id'=>'ratebtn')) !!}
                {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'submit', 'class' => 'btn btn-primary hidden', 'id'=>'ratebtn2')) !!}                    
            </div>   
            {!! Form::close() !!}                           
        </div>
    </div>
</div>
<div class="modal inmodal" id="workgroupAddModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-users modal-icon"></i>
                <h4 class="modal-title">Add Participant</h4>                    
            </div>              
            {!! Form::open(array('route'=>'my_request.add_member','method'=>'POST','files'=>true)) !!}                                                                               
            <div class="modal-body">                                                 
                <div class="row" style="padding-top:10px">                    
                    <div class="col-lg-6">
                        {!! Form::label('reciever','Name')!!}
                        {!! Form::select('reciever[]',$users,'',['class'=>'form-control','id'=>'user_id','multiple'=>'multiple']) !!}                     
                    </div>  
                </div>                                    
            </div>
            <div class="modal-footer">                   
                {!! Form::hidden('request_id',$req->id) !!}
                {!! Form::button('Close', array('type' => 'submit', 'class' => 'btn btn-white','data-dismiss'=>'modal')) !!}            
                {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'button', 'class' => 'btn btn-primary hidden sbalert','id'=>'urbtn')) !!}
                {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'submit', 'class' => 'btn btn-primary hidden', 'id'=>'urbtn2')) !!}                    
            </div>   
            {!! Form::close() !!}                           
        </div>
    </div>
</div>
<div class="modal inmodal" id="workgroupAddCCModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-users modal-icon"></i>
                <h4 class="modal-title">Add CC</h4>                    
            </div>              
            {!! Form::open(array('route'=>'my_request.add_member_cc','method'=>'POST','files'=>true)) !!}                                                                               
            <div class="modal-body">                                                 
                <div class="row" style="padding-top:10px">                    
                    <div class="col-lg-6">
                        {!! Form::label('reciever','Name')!!}
                        {!! Form::select('reciever[]',$notInCC,'',['class'=>'form-control','id'=>'user_idcc','multiple'=>'multiple']) !!}                     
                    </div>  
                </div>                                    
            </div>
            <div class="modal-footer">                   
                {!! Form::hidden('request_id',$req->id) !!}
                {!! Form::button('Close', array('type' => 'submit', 'class' => 'btn btn-white','data-dismiss'=>'modal')) !!}            
                {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'button', 'class' => 'btn btn-primary hidden','id'=>'sbalert2')) !!}
                {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'submit', 'class' => 'btn btn-primary hidden', 'id'=>'urbtn22')) !!}                    
            </div>   
            {!! Form::close() !!}                           
        </div>
    </div>
</div>
<div class="modal inmodal" id="workgroupRemoveCCModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-users modal-icon"></i>
                <h4 class="modal-title">Remove CC</h4>                    
            </div>              
            {!! Form::open(array('route'=>'my_request.remove_member_cc','method'=>'POST','files'=>true)) !!}                                                                               
            <div class="modal-body">                                                 
                <div class="row" style="padding-top:10px">                    
                    <div class="col-lg-6">
                        {!! Form::label('reciever','Name')!!}
                        {!! Form::select('reciever[]',$inCC,'',['class'=>'form-control','id'=>'user_idrecc','multiple'=>'multiple']) !!}                     
                    </div>  
                </div>                                    
            </div>
            <div class="modal-footer">                   
                {!! Form::hidden('request_id',$req->id) !!}
                {!! Form::button('Close', array('type' => 'submit', 'class' => 'btn btn-white','data-dismiss'=>'modal')) !!}            
                {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'button', 'class' => 'btn btn-primary hidden','id'=>'sbalert22')) !!}
                {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'submit', 'class' => 'btn btn-primary hidden', 'id'=>'urbtn222')) !!}                    
            </div>   
            {!! Form::close() !!}                           
        </div>
    </div>
</div>
@stop
@section('page-script')
    
    $('#user_id,#user_idcc,#user_idrecc').multiselect({
        maxHeight: 200,    
        includeSelectAllOption: true,
        enableCaseInsensitiveFiltering: true,
        enableFiltering: true,
        buttonWidth: '100%',
        buttonClass: 'form-control'        
    });

    $('#filer_inputs,#joinputs').filer({

        showThumbs:true,
        addMore:true
    });       

    $('#rembtn').click(function () {

        if($('#remarkid').val() != ""){
            swal({
                title: "Good job!",
                text: "Your remarks will be save!",
                type: "success"
            });            
            $('#sub1').click();
        }else{

            $('#sub1').click();
        }                            
    });

    $('#retbtn').click(function () {

        if($('#remarkid').val() != ""){
            swal({
                title: "Good job!",
                text: "Your remarks will be save and the request will be returned!",
                type: "success"
            });            
            $('#sub2').click();
        }else{

            $('#sub2').click();
        }                            
    });

    $('#reobtn').click(function () {

        if($('#remarkid').val() != ""){
            swal({
                title: "Good job!",
                text: "Your remarks will be save and the request will be re-open!",
                type: "success"
            });            
            $('#sub3').click();
        }else{

            $('#sub3').click();
        }                          
    });

    $('#clobtn').click(function () {

        if($('#remarkid').val() != ""){
            swal({
                title: "Good job!",
                text: "Your remarks will be save and the request will be closed!",
                type: "success"
            });            
            $('#sub4').click();
        }else{

            $('#sub4').click();
        }                            
    });    

    $('#ratebtn').click(function () {

        if($('#requestID').val() != ""){
            swal({
                title: "Good job!",
                text: "Your rating will be save!",
                type: "success"
            });            
            $('#ratebtn2').click();
        }else{

            swal({
                title: "Waaiitt!",
                text: "Please refresh the page",
                type: "info"
            });       
        }                            
    });    

    $('#user_id').on("change", function(){

       var user2 = $('#user_id').val();       
                
        if(user2 != ""){                
            $('#urbtn').removeClass('hidden');
        }else{
            $('#urbtn').addClass('hidden');
        }        
    });    
    $('.sbalert').click(function () {

        if($('#user_id').val() != ""){
            
            swal({
                title: "Allright!",
                text: "I'm now going to add this to workgroup!",
                type: "success"
                });            
            $('#urbtn2').click();            
        }else{

            swal({
                title: "Warning",
                text: "Oh Come on! Select at least one member before you submit!",
                type: "warning"
            });      
        }                            
    });

    $('#user_idcc').on("change", function(){

       var user2 = $('#user_idcc').val();       
                
        if(user2 != ""){                
            $('#sbalert2').removeClass('hidden');
        }else{
            $('#sbalert2').addClass('hidden');
        }        
    });    
    $('#sbalert2').click(function () {

        if($('#user_idcc').val() != ""){
            
            swal({
                title: "Allright!",
                text: "I'm now going to add this to workgroup!",
                type: "success"
                });            
            $('#urbtn22').click();            
        }else{

            swal({
                title: "Warning",
                text: "Oh Come on! Select at least one member before you submit!",
                type: "warning"
            });      
        }                            
    });

    $('#user_idrecc').on("change", function(){

       var user2 = $('#user_idrecc').val();       
                
        if(user2 != ""){                
            $('#sbalert22').removeClass('hidden');
        }else{
            $('#sbalert22').addClass('hidden');
        }        
    });    
    $('#sbalert22').click(function () {

        if($('#user_idccc').val() != ""){
            
            swal({
                title: "Allright!",
                text: "I'm now going to remove this to CC!",
                type: "success"
                });            
            $('#urbtn222').click();            
        }else{

            swal({
                title: "Warning",
                text: "Oh Come on! Select at least one member before you submit!",
                type: "warning"
            });      
        }                            
    });
@stop
<script type="text/javascript">
    
    function remarkRatingFunction(elem){
    
        var x = elem.value;        
        $('#requestID').val(x);        

        $('#remarkRatingModal').modal('show'); 
    }

</script>