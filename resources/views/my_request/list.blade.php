@extends('layouts.master')
@section('main-body')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2><i class="fa fa-play-circle-o"></i> MY REQUEST</h2>
    </div>        
</div>      
<div class="wrapper wrapper-content animated fadeInRight">    
    <div class="row">
        <div class="col-lg-12 animated flash">
            <?php if (session('is_linked')): ?>
                <div class="alert alert-success alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                    <center><h4>Request was successfully linked!<i class="fa fa-check"></i></h4></center>                
                </div>
            <?php endif;?>            
        </div>        
        <div class="col-lg-12 animated flash">
            <?php if (session('is_linked_failed')): ?>
                <div class="alert alert-danger alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                    <center><h4>Something went wrong...please try again!</h4></center>                
                </div>
            <?php endif;?>            
        </div>
    </div>  
    <div class="row">
        <div class="col-lg-12">            
            &nbsp;
             <div class="panel blank-panel">
                <div class="panel-heading">                            
                    <div class="panel-options">    
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#made"> <i class="fa fa-bullseye"></i> Request I Made</a></li>
                            <li><a data-toggle="tab" href="#tome"> <i class="fa fa-bullseye"></i> Request For Me</a></li>
                            <li><a data-toggle="tab" href="#emreq"> <i class="fa fa-bullseye"></i> Email Request</a></li>
                            <li><a data-toggle="tab" href="#clreq"> <i class="fa fa-bullseye"></i> Client Request</a></li>
                        </ul>
                    </div>
                </div>
                &nbsp;
                &nbsp;
                <div class="tab-content">
                    <div id="made" class="tab-pane active">   
                        <div></div>                                    
                        </br>
                        &nbsp;
                        <center><h2>LIST OF REQUESTS I MADE</h2></center>
                        <div class="col-lg-12">    
                            <div class="table-responsive">                                        
                                <table class="table table-striped table-hover dataTables-example" >
                                    <thead>
                                        <tr>                                                                    
                                            <th>REQUEST CODE</th>
                                            <th>SUBJECT</th>                                       
                                            <th>RECIEVER</th>
                                            <th>PREVIEW</th>                                         
                                            <th>TYPE</th>
                                            <th>STATUS</th>                                            
                                            <th>ACTION</th>                        
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($my_requests as $myreq)
                                            <tr>
                                                <td>{!! $myreq->request_no !!}</td>
                                                <td>
                                                    @if(!empty($myreq->subject))
                                                        {!! $myreq->subject !!}
                                                    @else
                                                        N/A
                                                    @endif
                                                </td>
                                                <td>{!! $myreq->reciever !!}</td>   
                                                <td>
                                                    {!! substr($myreq->details, 0, 20); !!}
                                                </td>            
                                                <td>
                                                    @if(!empty($myreq->reqtype))
                                                        {!! $myreq->reqtype !!}
                                                    @else
                                                        N/A
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($myreq->status == 1)
                                                        <span class="label label-warning"> <i class="fa fa-angellist"></i>Pending</span>
                                                    @elseif($myreq->status == 2)
                                                        <span class="label label-success"> <i class="fa fa-angellist"></i>Returned</span>
                                                    @else
                                                        <span class="label label-primary"> <i class="fa fa-angellist"></i>Closed</span>
                                                    @endif
                                                </td>
                                                <td>                                                    
                                                    {!! Html::decode(link_to_Route('my_requests.show','<i class="fa fa-list"></i> Details', $myreq->request_code, array('class' => 'btn btn-success btn-xs')))!!}
                                                </td>                                                
                                            </tr>
                                        @endforeach
                                    </tbody>                        
                                </table>
                            </div>
                        </div>                       
                    </div> 
                    <div id="tome" class="tab-pane">   
                        <div></div>                                    
                        </br>
                        &nbsp;
                        <center><h2>LIST OF REQUESTS FOR ME</h2></center>
                        <div class="col-lg-12">    
                            <div class="table-responsive">                                        
                                <table class="table table-striped table-hover dataTables-example">
                                    <thead>
                                        <tr>                                                                    
                                            <th>REQUEST CODE</th>     
                                            <th>SUBJECT</th>                                       
                                            <th>REQUESTOR</th>
                                            <th>COMPANY</th>
                                            <th>PROJECT</th>
                                            <th>PREVIEW</th>
                                            <th>TYPE</th>   
                                            <th>STATUS</th>
                                            <th>ACTION</th>                        
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($requests_for_me as $reqtome)
                                            <tr>
                                                <td>{!! $reqtome->request_no !!}</td>
                                                <td>
                                                    @if(!empty($reqtome->subject))
                                                        {!! $reqtome->subject !!}
                                                    @else
                                                        N/A
                                                    @endif
                                                </td>
                                                <td>{!! $reqtome->requestor !!}</td>            
                                                <td>
                                                    @if($reqtome->contact_id > 0)
                                                        {!! $reqtome->con->company !!}
                                                    @else
                                                        ----
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($reqtome->project_id > 0)
                                                        {!! $reqtome->proj->project_name !!}
                                                    @else
                                                        ----
                                                    @endif
                                                </td>
                                                <td>
                                                    {!! substr($reqtome->details, 0, 20) !!}
                                                </td>
                                                <td>
                                                    @if(!empty($reqtome->reqtype))
                                                        {!! $reqtome->reqtype !!}
                                                    @else
                                                        N/A
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($reqtome->status == 1)
                                                        <span class="label label-warning label-xs"> <i class="fa fa-angellist"></i>Pending</span>
                                                    @elseif($reqtome->status == 2)
                                                        <span class="label label-success label-xs"> <i class="fa fa-angellist"></i>Returned</span>
                                                    @else
                                                        <span class="label label-primary label-xs"> <i class="fa fa-angellist"></i>Closed</span>
                                                    @endif
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" value = "{{$reqtome->request_code}}" id = "{{$reqtome->request_code}}" onclick="userRequestlinkFunction(this)"><i class="fa fa-chain"></i> Link</button>                   
                                                    {!! Html::decode(link_to_Route('my_requests.show','<i class="fa fa-list"></i> Details', $reqtome->request_code, array('class' => 'btn btn-success btn-xs')))!!}
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>                        
                                </table>
                            </div>
                        </div>                       
                    </div>
                    <div id="emreq" class="tab-pane">   
                        <div></div>                                    
                        </br>
                        &nbsp;
                        <center><h2>EMAIL REQUEST</h2></center>
                        <div class="col-lg-12">    
                            <div class="table-responsive">                                        
                                <table class="table table-striped table-hover dataTables-example">
                                    <thead>
                                        <tr>                                                                    
                                            <th>REQUEST CODE</th>     
                                            <th>SUBJECT</th>                                       
                                            <th>REQUESTOR</th>
                                            <th>COMPANY</th>
                                            <th>PROJECT</th>
                                            <th>PREVIEW</th>
                                            <th>TYPE</th>   
                                            <th>STATUS</th>
                                            <th>ACTION</th>                        
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                    </tbody>                        
                                </table>
                            </div>
                        </div>                       
                    </div>                                                    
                    <div id="clreq" class="tab-pane">   
                        <div></div>                                    
                        </br>
                        &nbsp;
                        <center><h2>Client Request </h2></center>
                        <div class="col-lg-12">    
                            <div class="table-responsive">                                        
                                <table class="table table-striped table-hover dataTables-example">
                                    <thead>
                                        <tr>                                                                    
                                            <th>REQUEST CODE</th>     
                                            <th>SUBJECT</th>                                       
                                            <th>REQUESTOR</th>
                                            <th>COMPANY</th>
                                            <th>PROJECT</th>
                                            <th>PREVIEW</th>
                                            <th>TYPE</th>   
                                            <th>STATUS</th>
                                            <th>ACTION</th>                        
                                        </tr>
                                    </thead>
                                    <tbody>
                                       
                                    </tbody>                        
                                </table>
                            </div>
                        </div>                       
                    </div>
                </div>
            </div>
        </div>      
    </div>
</div>
<!-- Modals -->
<div class="modal inmodal" id="requestLinkModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-chain fa-spin modal-icon"></i>
                <h4 class="modal-title">LINKING</h4>                    
            </div>              
            {!! Form::open(array('route'=>'my_requests.linking','method'=>'POST','files'=>true)) !!}                                                                               
            <div class="modal-body">                                 
                <div class="row" style="padding-top:50px;">                                    
                    <div class="col-lg-6">
                        {!! Form::label('linkto','Link to:') !!}                        
                        {!! Form::select('linkto',$types,'',['class'=>'form-control','id'=>'linktotype','placeholder'=>'Select link method']) !!}
                    </div>                           
                </div>               
                <div class="row hidden" style="padding-top:10px" id="contactrow">
                    <div class="col-lg-6">
                        {!! Form::label('contact_person','Company')!!}
                        {!! Form::select('contact_person',$contacts,'',['class'=>'form-control','id'=>'linkcontactperson','placeholder'=>'Select Contact Person']) !!}                                  
                    </div>      
                </div>     
                <div class="row hidden" style="padding-top:10px" id="projectrow">
                    <div class="col-lg-6">
                        {!! Form::label('project_name','Project Name')!!}
                        {!! Form::select('project_id',$projects,'',['class'=>'form-control','id'=>'linkprojectname','placeholder'=>'Select Project']) !!}                     
                    </div>      
                </div>                                                                        
            </div>
            <div class="modal-footer">   
                {!! Form::hidden('request_code','',['id'=>'linkrequestcode']) !!}                                   
                {!! Form::hidden('type','',['id'=>'linktypid']) !!}
                {!! Form::button('Close', array('type' => 'submit', 'class' => 'btn btn-white','data-dismiss'=>'modal')) !!}            
                {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'button', 'class' => 'btn btn-primary hidden sbalert','id'=>'urbtn')) !!}
                {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'submit', 'class' => 'btn btn-primary hidden', 'id'=>'urbtn2')) !!}                    
            </div>   
            {!! Form::close() !!}                           
        </div>
    </div>
</div>
@endsection
@section('page-script')

$('#linktotype').on("change", function(){

    var x = $('#linktotype').val();               

    $('#contactrow').addClass('hidden');
    $('#projectrow').addClass('hidden');
    $('#urbtn').addClass('hidden');
    $('#linktypid').val(x);
    if(x > 0){
        $('select#linkcontactperson').val("");
        $('select#linkprojectname').val("");
        $('#contactrow').removeClass('hidden');
        $('#projectrow').removeClass('hidden');        

        $('#linkcontactperson').on("change", function(){

            var con = $('#linkcontactperson').val();                           
            
            $.ajax({
                type:"POST",
                dataType: 'json',
                data: {con: con},
                url: "../my_requests/getProjects",
                success: function(data){            

                    $('select#linkprojectname').empty();
                    
                    $.each(data, function(i, text) {
                        $('<option />',{value: i, text: text}).appendTo($('select#linkprojectname'));
                    });          
                    var proj = $('#linkprojectname').val();                           
                    if(proj != null){                
                        $('#urbtn').removeClass('hidden');
                    }else{
                        $('#urbtn').addClass('hidden');
                    }        
                }
            });  

            $('#linkcontactperson').on("change", function(){

                var proj = $('#linkprojectname').val();                           
                if(proj != ""){                
                    $('#urbtn').removeClass('hidden');
                }else{
                    $('#urbtn').addClass('hidden');
                }            
            });                   
        });    

    }if(x < 1){
        $('#contactrow').removeClass('hidden');
        $('select#linkcontactperson').val("");
        $('select#linkprojectname').val("");
        var linkto = $('#linkcontactperson').val();

        $('#linkcontactperson').on("change", function(){

            var con = $('#linkcontactperson').val();                           
            if(con != ""){                
                $('#urbtn').removeClass('hidden');
            }else{
                $('#urbtn').addClass('hidden');
            }            
        });                 
    }
    if(x == ""){

        $('#contactrow').addClass('hidden');
        $('#projectrow').addClass('hidden');
        $('#urbtn').addClass('hidden');
    }
    
}); 

$('#urbtn').click(function () {

    swal({
        title: "Got it!",
        text: "Give me a second to link this!",
        type: "success"
        });            
    $('#urbtn2').click();            
}); 

@stop
<script type="text/javascript">
    
    function userRequestlinkFunction(elem){
    
        var x = elem.value;        
        $('#linkrequestcode').val(x);
        $('#contactrow').addClass('hidden');
        $('#projectrow').addClass('hidden');
        $('select#linkcontactperson').val("");
        $('select#linkprojectname').val("");

        $('#requestLinkModal').modal('show'); 
    }

</script>