@extends('layouts.master')
@section('main-body')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2><i class="fa fa-play-circle-o"></i> WORKGROUPS MAINTENANCE</h2>
    </div>        
</div>      
<div class="wrapper wrapper-content animated fadeInRight">    
    <div class="row">
        <div class="col-lg-12 animated flash">
            <?php if (session('is_success')): ?>
                <div class="alert alert-success alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                    <center><h4>Workgroup was successfully added!<i class="fa fa-check"></i></h4></center>                
                </div>
            <?php endif;?>
            <?php if (session('is_update')): ?>
                <div class="alert alert-success alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                    <center><h4>Workgroup was successfully updated!<i class="fa fa-check"></i></h4></center>                
                </div>
            <?php endif;?>
        </div>
    </div>  
    <div class="row">
        <div class="col-lg-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title" style="background-color:#009688">
                    <h5 style="color:white"><i class="fa fa-plus"></i> Create New Workgroup</h5>
                </div>
                <div class="ibox-content">                    
                {!! Form::open(array('route'=>'workgroups.store','methd'=>'POST')) !!}
                    <div class="form-group">
                        {!! Form::label('workgroup_name','Workgroup Name') !!}
                        {!! Form::text('workgroup_name','',['class'=>'form-control','required'=>'required','placeholder'=>'Enter Wrokgroup Name Here']) !!} 
                    </div>       
                    <div class="form-group">
                        {!! Form::label('head','Workgroup Head') !!}
                        {!! Form::select('head',$users,'',['class'=>'form-control','required'=>'required']) !!} 
                    </div>                    
                    <div class="form-group">                        
                        {!! Form::button('<i class="fa fa-save"></i> Save', array('type' => 'submit', 'class' => 'btn btn-primary')) !!}
                    </div>                          
                </div>                
                {!! Form::close() !!}
            </div>
        </div>
        <div class="col-lg-8">
                <div class="ibox float-e-margins">
                    <div class="ibox-title" style="background-color:#009688">
                        <h5 style="color:white"><i class="fa fa-play-circle-o"></i> Workgroups Table</h5>                    
                    </div>
                    <div class="ibox-content"> 
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example" >
                                <thead>
                                    <tr>                                                                          
                                        <th>WORKGROUP NAME</th>                                          
                                        <th>HEAD</th>
                                        <th>ACTION</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($workgroups as $workgroup)
                                    <tr>                                        
                                        <td>{!! $workgroup->workgroup_name !!}</td>                                        
                                        <td>{!! strtoupper($workgroup->mngr->first_name.' '.$workgroup->mngr->last_name) !!}</td>
                                        <td>
                                        <button name="rbtn" class="rolebuttn btn btn-info btn-xs" id="{{$workgroup->id}}" value="{{$workgroup->id}}" onclick="rolesEditFunction(this)">
                                                <i class="fa fa-pencil"></i> Edit
                                        </button>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>   
                        </div>                    
                    </div>                                    
                </div>                
            </div>
        </div>
    </div>
</div>
<!-- Trigger the modal with a button -->

<!-- Modal -->
<div id="editWorkgroupModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        {!! Form::open(array('route'=>'workgroups.updates','method'=>'POST')) !!}
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                {!!Form::label('name','Edit Workgroup')!!}
            </div>
            <div class="modal-body">
                <div class="form-group">
                    {!!Form::label('workgroup_name','Workgroup Name')!!}                       
                    {!!Form::text('workgroup_name','',['class'=>'form-control','placeholder'=>'Enter Name of Workgroup','id'=>'workgroupname'])!!}                                              
                    @if ($errors->has('workgroup')) <p class="help-block" style="color:red;">{{ $errors->first('workgroup') }}</p> @endif
                </div>                  
                <div class="form-group">
                    {!! Form::label('head','Workgroup Head') !!}
                    {!! Form::select('head',$users,'',['class'=>'form-control','required'=>'required','id'=>'workhead']) !!} 
                </div>               
            </div>
            <div class="modal-footer">
                {!! Form::hidden('workgroup_id','',['id'=>'workgroupID']) !!}
                {!! Form::button('<i class="fa fa-save"></i> Update', array('type' => 'submit', 'class' => 'btn btn-primary')) !!}
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-arrow-left"></i> Close</button>
            </div>
        </div>
        {!!Form::close()!!}
    </div>
</div>
@endsection
<script type="text/javascript">
    function rolesEditFunction(elem){

        var x = elem.id;

        $.ajax({

            type:"POST",
            dataType: 'json',
            data: {workgroupid: x},
            url: "../getWorkgroupDetails",
            success: function(data){            
            
                $('#workgroupname').val(data['workgroup_name']);                
                $('#workgroupID').val(data['id']);
                $('#workhead').val(data['head']);

                $('#editWorkgroupModal').modal('show');
            }    
        });
    }
</script>