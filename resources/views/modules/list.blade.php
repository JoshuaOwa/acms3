@extends('layouts.master')
@section('main-body')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2><i class="fa fa-university"></i> MODULES LIST</h2>       
    </div>
</div>        
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-lg-12 animated flash">
		    <?php if (session('is_success')): ?>
		        <div class="alert alert-success alert-dismissible fade in" role="alert">
		            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
		        </button>
		            <center><h4>Module was successfully added!<i class="fa fa-check"></i></h4></center>                
		        </div>
		    <?php endif;?>
		    <?php if (session('is_update')): ?>
		        <div class="alert alert-success alert-dismissible fade in" role="alert">
		            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
		        </button>
		            <center><h4>Module was successfully updated!<i class="fa fa-check"></i></h4></center>                
		        </div>
		    <?php endif;?>
		</div>
	</div>
    <div class="row">
        <div class="col-lg-12">
	        <div class="ibox float-e-margins">
	            <div class="ibox-title" style="background-color:#009688">
	                <h5 style="color:white"><i class="fa fa-university"></i> Modules Table</h5>
	                {!! Html::decode(link_to_Route('modules.create', '<i class="fa fa-plus"></i> New Module', [], ['class' => 'btn btn-white btn-xs pull-right'])) !!}
	            </div>
	            <div class="ibox-content">
	            	<div class="table-responsive">
		            	<table class="table table-striped table-bordered table-hover dataTables-example" >
			            <thead>
				            <tr>
				                <th>#</th>
				                <th>Module</th>
				                <th>Description</th>
				                <th>Route</th>
				                <th>Default Url</th>
				                <th>Icon</th>
				                <th>Action</th>			               
				            </tr>
			            </thead>
			            <tbody>
				           @forelse($modules as $module)
				           		<tr>
				           			<td>{!! $module->id !!}</td>
				           			<td>{!! $module->module !!}</td>
				           			<td>{!! $module->description !!}</td>
				           			<td>{!! $module->routeUri !!}</td>
				           			<td>{!! $module->default_url !!}</td>
				           			<td>{!! $module->icon !!}</td>
				           			<td>{!! Html::decode(link_to_Route('modules.edit','<i class="fa fa-pencil"></i> Edit', $module->id, array('class' => 'btn btn-info btn-xs')))!!}</td>
				           		</tr>
				           	@empty
				           	@endforelse
			            </tbody>			            
		            	</table>
		            </div>
	            </div>
	        </div>
	    </div>	    
    </div>
</div>	
@stop