@extends('layouts.master')
@section('main-body')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>DASHBOARD</h2>        
    </div>
    <div class="col-sm-8">
        <div class="title-action">            
            <button class="btn btn-warning btn-md" onclick="taskFunction()"><i class="fa fa-plus"> Notes</i> </button>
        </div>
    </div>
</div>
<div class="wrapper wrapper-content">    
    <div class="row">
        <div class="col-lg-12">
            <div class="tabs-container">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#tab-reminders">REMINDERS</a></li>                  
                    <li class=""><a data-toggle="tab" href="#tab-schedule">SCHEDULE</a></li>  
                    <li class=""><a data-toggle="tab" href="#tab-announcements">ANNOUNCEMENTS</a></li>
                </ul>
                <div class="tab-content">
                    <div id="tab-reminders" class="tab-pane active">
                        <div class="panel-body">
                            <div class="tabs-container">
                                <div class="col-lg-4">
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-title">
                                            <h5><i class="fa fa-pencil">Needs Your Daily Update</i></h5>
                                            <div class="ibox-tools">
                                                <span class="label label-warning-light pull-right">{!! count($daily) !!}</span>
                                               </div>
                                        </div>
                                        <div class="ibox-content dupd">
                                            <div>
                                                <div class="feed-activity-list">
                                                    @foreach($daily as $d)
                                                    <div class="feed-element">                                
                                                        <div class="media-body ">
                                                            <small class="pull-right">{!! $d->updated_at->diffForHumans() !!}</small>
                                                            <strong>
                                                                @if($d->type < 1)
                                                                    {!! Html::decode(link_to_route('my_requests.show', $d->info,$d->link, array())) !!}
                                                                @else
                                                                    {!! Html::decode(link_to_route('job_orders.overview', $d->info,$d->link, array())) !!}
                                                                @endif
                                                            </strong> <br>
                                                            <small class="text-muted">Subject: 
                                                                @if(!empty($d->subject))
                                                                    {!! $d->subject !!}
                                                                @else
                                                                    N/A
                                                                @endif
                                                            </small><br />
                                                            <small class="text-muted">Last Update: {!! $d->updated_at !!}</small><br />
                                                            <small class="text-muted">Update Needed: {!! $d->cnt_need !!}</small><br />
                                                            <small class="text-muted">Update Done: {!! $d->cnt_donw !!}</small><br />
                                                            <small class="text-muted">Status: {!! $d->status !!}</small>
                                                        </div>
                                                    </div>                            
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-title">
                                            <h5><i class="fa fa-pencil"> Needs Your Weekly Update</i></h5>
                                            <div class="ibox-tools">
                                                <span class="label label-warning-light pull-right">{!! count($weekly) !!}</span>
                                               </div>
                                        </div>
                                        <div class="ibox-content dupd">
                                            <div>
                                                <div class="feed-activity-list">
                                                    @foreach($weekly as $w)
                                                    <div class="feed-element">                                
                                                        <div class="media-body ">
                                                            <small class="pull-right">{!! $w->updated_at->diffForHumans() !!}</small>
                                                            <strong>{!! Html::decode(link_to_route('job_orders.overview', $w->info,$w->link, array())) !!}</strong> <br>
                                                            <small class="text-muted">Subject: 
                                                                @if(!empty($w->subject))
                                                                    {!! $w->subject !!}
                                                                @else
                                                                    N/A
                                                                @endif
                                                            </small><br />
                                                            <small class="text-muted">Last Update: {!! $w->updated_at !!}</small><br />
                                                            <small class="text-muted">Update Needed: {!! $w->cnt_need !!}</small><br />
                                                            <small class="text-muted">Update Done: {!! $w->cnt_donw !!}</small><br />   
                                                            <small class="text-muted">Status: {!! $d->status !!}</small>
                                                        </div>
                                                    </div>                   
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-title">
                                            <h5><i class="fa fa-pencil"> Needs Your Monthly Update</i></h5>
                                            <div class="ibox-tools">
                                                <span class="label label-warning-light pull-right">{!! count($monthly) !!}</span>
                                               </div>
                                        </div>
                                        <div class="ibox-content dupd">
                                            <div>
                                                <div class="feed-activity-list">
                                                    @foreach($monthly as $m)
                                                    <div class="feed-element">                                
                                                        <div class="media-body ">
                                                            <small class="pull-right">{!! $m->updated_at->diffForHumans() !!}</small>
                                                            <strong>{!! Html::decode(link_to_route('my_requests.show', $m->info,$m->link, array())) !!}</strong> <br>
                                                            <small class="text-muted">Subject: 
                                                                @if(!empty($m->subject))
                                                                    {!! $m->subject !!}
                                                                @else
                                                                    N/A
                                                                @endif
                                                            </small><br />
                                                            <small class="text-muted">Last Update: {!! $m->updated_at !!}</small><br />
                                                            <small class="text-muted">Update Needed: {!! $m->cnt_need !!}</small><br />
                                                            <small class="text-muted">Update Done: {!! $m->cnt_donw !!}</small>
                                                        </div>
                                                    </div>                   
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>   
                                <div class="col-lg-4">
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-title">
                                            <h5><i class="fa fa-pencil">CC JO</i></h5>
                                            <div class="ibox-tools">
                                                <span class="label label-warning-light pull-right">{!! count($copieds2) !!}</span>
                                               </div>
                                        </div>
                                        <div class="ibox-content dupd">
                                            <div>
                                                <div class="feed-activity-list">
                                                    @foreach($copieds2 as $copy2)
                                                    <div class="feed-element">                                
                                                        <div class="media-body ">
                                                            <small class="pull-right">{!! $copy2->updated_at->diffForHumans() !!}</small>
                                                            <strong>{!! Html::decode(link_to_route('job_orders.overview', $copy2->info,$copy2->encryptname, array())) !!}</strong> <br>
                                                            <small class="text-muted">Subject: 
                                                                @if(!empty($copy2->subject))
                                                                    {!! $copy2->subject !!}
                                                                @else
                                                                    N/A
                                                                @endif
                                                            </small><br />
                                                            <small class="text-muted">Last Update: {!! $copy2->updated_at !!}</small><br />
                                                        </div>
                                                    </div>                            
                                                    @endforeach                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-8" id="noteflow">                                    
                                    <ul class="notes">
                                        @foreach($notes as $note)
                                        <li>
                                            <div>
                                                <small>{!! $note->created_at !!}</small>
                                                <h4>{!! $note->title !!}</h4>
                                                
                                                    <p id="noteflows">{!! $note->note !!}</p>                        
                                                <a href="#">
                                                {!! Form::open(array('route'=>'sticky_notes.delt','method'=>'POST')) !!}
                                                    {!!Form::hidden('noted',$note->id) !!}
                                                    {!! Form::button('<i class="fa fa-trash-o"></i>', array('type' => 'submit', 'class' => 'btn')) !!}
                                                {!! Form::close() !!}
                                                </a>       
                                            </div>
                                        </li>
                                        @endforeach
                                    </ul>                                    
                                </div>                                                                           
                            </div>
                        </div>
                    </div>                                     
                    <div id="tab-schedule" class="tab-pane">
                        <div class="panel-body">
                            <div class="tabs-container">
                                {!! Form::open(array('route'=>'dashboard.calendar','method'=>'POST')) !!}
                                <div class="col-lg-4">
                                
                                    {!! Form::label('department_id','Department') !!}
                                    {!! Form::select('department_id',$departments,'',['class'=>'form-control','placeholder'=>'Select Department','required'=>'required']) !!}                                                                        
                                </div>
                                &nbsp;
                                <div class="col-lg-12" style="padding-top: 20px;">
                                     <div class="form-group">                        
                                        {!! Form::button('<i class="fa fa-save"></i> Process', array('type' => 'submit', 'class' => 'btn btn-primary')) !!}
                                    </div>      
                                </div>
                                {!! Form::close() !!}
                                <div class="col-lg-12">
                                    <center>
                                        <h4>Legends:</h4> 
                                        <h3>
                                            <span class="label" style="background-color: #fab1a0">Overdue</span>
                                            <span class="label" style="background-color: #1ab394">On-Track</span>
                                        </h3>
                                    </center>
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-title">                                                                                                                                                            
                                        </div>
                                        <div class="ibox-content">
                                            {!! $calendar->calendar() !!}
                                            <br />
                                        </div>
                                    </div>
                                </div>           
                            </div>
                        </div>
                    </div>
                    <div id="tab-announcements" class="tab-pane">
                        <div class="panel-body">
                            <div class="tabs-container">
                                <div class="col-lg-12">
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-title" style="background-color:#009688">
                                            <h5 style="color:white"><i class="fa fa-bullhorn"></i> Announcement</h5>                    
                                            <div class="ibox-tools">
                                                <a class="collapse-link">
                                                    <i class="fa fa-chevron-up"></i>
                                                </a>                        
                                            </div>
                                        </div>
                                        <div class="ibox-content ibox-heading">
                                            <h3><i class="fa fa-envelope-o"></i> Recent Announcements</h3>
                                            <small><i class="fa fa-tim"></i> There are {!! $ann_cntr !!} Announcements</small>
                                        </div>
                                        <div class="ibox-content">
                                            <div class="feed-activity-list">
                                                @foreach($announcements as $announcement)
                                                <div class="feed-element">
                                                    <div>
                                                        <small class="pull-right text-navy">{!! $announcement->created_at->diffForHumans() !!}</small>                                
                                                        <strong><a href="/memo/{{$announcement->id}}/details" target="__blank" id="{{$announcement->id}}" value="{{$announcement->id}}">{!! $announcement->subject !!}</a></strong>
                                                        <div>
                                                            <small class="text-muted">{!! $announcement->created_at->format('M-D-Y H:i:s a') !!}</small>
                                                        </div>                           
                                                    </div>
                                                </div>                       
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    	
<!-- Modal -->
<div id="memoModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">        
        <div class="modal-content">
            <div class="modal-header" style="background-color:#009688">                
                <button type="button" class="close" data-dismiss="modal">&times;</button>                
                <center><h2 id="titlememo" style="color: white;"></h2></center>
            </div>
            <div class="modal-body">
                <p id="memobody">
                    
                </p>
            </div>
            <div class="modal-footer">                
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-arrow-left"></i> Close</button>
            </div>
        </div>        
    </div>
</div>
<div id="taskModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">        
        <div class="modal-content">
            <div class="modal-header" style="background-color:#009688">                
                <button type="button" class="close" data-dismiss="modal">&times;</button>                
                <center><h2 style="color: white;">Sticky Notes</h2></center>
            </div>
            {!! Form::open(array('route'=>'sticky_notes.store','method'=>'POST')) !!}
            <div class="modal-body">
                <div class="form-group">
                    {!! Form::label('title','Title') !!}
                    {!! Form::text('title','',['class'=>'form-control','placeholder'=>'your title','required'=>'required']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('notes','Notes') !!}
                    {!! Form::textarea('notes','',['class'=>'form-control','placeholder'=>'text here...','required'=>'required']) !!}
                </div>
            </div>
            <div class="modal-footer">          
                {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'submit', 'class' => 'btn btn-primary')) !!}      
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-arrow-left"></i> Close</button>
            </div>
            {!! Form::close() !!}
        </div>        
    </div>
</div>
@endsection
@section('page-calendar')
{!! $calendar->script() !!}

@stop
<script type="text/javascript">
    function taskFunction(){
        
        $('#taskModal').modal('show');
        
    }
    function rolesEditFunction(elem){

        var x = elem.id;
     
        $.ajax({

            type:"POST",
            dataType: 'json',
            data: {memoid: x},
            url: "../getMemoDetails",
            success: function(data){            
                
                document.getElementById("titlememo").innerHTML = data['subject'].toUpperCase();
                document.getElementById("memobody").innerHTML = data['message'];
                

                $('#memoModal').modal('show');
            }    
        });
    }
</script>
