@extends('layouts.master')
@section('main-body')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2><i class="fa fa-play-circle-o"></i> TIMETABLE MAINTENANCE</h2>
    </div>        
</div>      
<div class="wrapper wrapper-content animated fadeInRight">    
    <div class="row">
        <div class="col-lg-12 animated flash">
            <?php if (session('is_success')): ?>
                <div class="alert alert-success alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                    <center><h4>Timetable was successfully added!<i class="fa fa-check"></i></h4></center>
                </div>
            <?php endif;?>
            <?php if (session('is_update')): ?>
                <div class="alert alert-success alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                    <center><h4>Timetable was successfully updated!<i class="fa fa-check"></i></h4></center>                
                </div>
            <?php endif;?>
        </div>
    </div>  
    <div class="row">
        <div class="col-lg-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title" style="background-color:#009688">
                    <h5 style="color:white"><i class="fa fa-plus"></i> Create New Timetable</h5>
                </div>
                <div class="ibox-content">                    
                {!! Form::open(array('route'=>'timetables.store','methd'=>'POST')) !!}
                    <div class="form-group">
                        {!! Form::label('timetable','Type') !!}
                        {!! Form::text('timetable','',['class'=>'form-control','required'=>'required','placeholder'=>'Enter timetable here']) !!} 
                    </div>                               
                    <div class="form-group">                        
                        {!! Form::button('<i class="fa fa-save"></i> Save', array('type' => 'submit', 'class' => 'btn btn-primary')) !!}
                    </div>                          
                </div>                
                {!! Form::close() !!}
            </div>
        </div>
        <div class="col-lg-8">
            <div class="ibox float-e-margins">
                <div class="ibox-title" style="background-color:#009688">
                    <h5 style="color:white"><i class="fa fa-play-circle-o"></i> Timetable Table</h5>                    
                </div>
                <div class="ibox-content"> 
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead>
                                <tr>                                                                          
                                    <th>TIMETABLE</th>
                                    <th>NO. OF TASK</th>                                                                              
                                    <th>ACTION</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($timetables as $timetable)
                                <tr>                                        
                                    <td>{!! $timetable->timetable !!}</td>                                                                         
                                    <td>{!! $timetable->taskcount !!}</td>
                                    <td>
                                    <button name="rbtn" class="timetablebuttn btn btn-info btn-xs" id="{{$timetable->id}}" value="{{$timetable->id}}" onclick="timeEditFunction(this)">
                                            <i class="fa fa-pencil"></i> Edit
                                    </button>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>   
                    </div>                    
                </div>                                                    
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 animated flash">
            <?php if (session('is_success_task')): ?>
                <div class="alert alert-success alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                    <center><h4>Task was successfully added!<i class="fa fa-check"></i></h4></center>
                </div>
            <?php endif;?>
            <?php if (session('is_update_task')): ?>
                <div class="alert alert-success alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                    <center><h4>Task was successfully updated!<i class="fa fa-check"></i></h4></center>                
                </div>
            <?php endif;?>
        </div>
    </div>  
    <div class="row">
        <div class="col-lg-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title" style="background-color:#009688">
                    <h5 style="color:white"><i class="fa fa-plus"></i> Create New Timetable Task</h5>
                </div>
                <div class="ibox-content">                    
                {!! Form::open(array('route'=>'timetable_tasks.store','methd'=>'POST')) !!}
                    <div class="form-group">
                        {!! Form::label('timetable','Type') !!}
                        {!! Form::select('timetable_id',$timetable_list,'',['class'=>'form-control','required'=>'required','placeholder'=>'Select Timetable']) !!} 
                    </div>      
                     <div class="form-group">
                        {!! Form::label('task','Task') !!}
                        {!! Form::text('task','',['class'=>'form-control','required'=>'required','placeholder'=>'Enter task here']) !!} 
                    </div>                               
                    <div class="form-group">                        
                        {!! Form::button('<i class="fa fa-save"></i> Save', array('type' => 'submit', 'class' => 'btn btn-primary')) !!}
                    </div>                          
                </div>                
                {!! Form::close() !!}
            </div>
        </div>
        <div class="col-lg-8">
            <div class="ibox float-e-margins">
                <div class="ibox-title" style="background-color:#009688">
                    <h5 style="color:white"><i class="fa fa-play-circle-o"></i> Timetable Task Table</h5>                    
                </div>
                <div class="ibox-content"> 
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead>
                                <tr>                                                                          
                                    <th>TIMETABLE</th>
                                    <th>TASK</th>                                                                              
                                    <th>ACTION</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($tasks as $task)
                                <tr>                                        
                                    <td>{!! $task->timetable->timetable !!}</td>                                                                         
                                    <td>{!! $task->task !!}</td>
                                    <td>
                                    <button name="rbtn" class="timetabletaskbuttn btn btn-info btn-xs" id="{{$task->id}}" value="{{$task->id}}" onclick="taskEditFunction(this)">
                                            <i class="fa fa-pencil"></i> Edit
                                    </button>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>   
                    </div>                    
                </div>                                    
            </div>                            
        </div>
    </div>
</div>
<!-- Trigger the modal with a button -->

<!-- Modal -->
<div id="editTimeModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        {!! Form::open(array('route'=>'timetables.updates','method'=>'POST')) !!}
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                {!!Form::label('name','Edit Timetable')!!}
            </div>
            <div class="modal-body">
                <div class="form-group">
                    {!! Form::label('timetable','Type') !!}
                    {!! Form::text('timetable','',['class'=>'form-control','required'=>'required','placeholder'=>'Enter timetable here','id'=>'timetableid']) !!} 
                </div>                                    
            </div>
            <div class="modal-footer">
                {!! Form::hidden('timeid','',['id'=>'timeid']) !!}
                {!! Form::button('<i class="fa fa-save"></i> Update', array('type' => 'submit', 'class' => 'btn btn-primary')) !!}
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-arrow-left"></i> Close</button>
            </div>
        </div>
        {!!Form::close()!!}
    </div>
</div>
<!-- Modal -->
<div id="editTaskModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        {!! Form::open(array('route'=>'timetable_tasks.updates','method'=>'POST')) !!}
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                {!!Form::label('name','Edit Timetable Task')!!}
            </div>
            <div class="modal-body">
               <div class="form-group">
                    {!! Form::label('timetable','Type') !!}
                    {!! Form::select('timetable_id',$timetable_list,'',['class'=>'form-control','required'=>'required','placeholder'=>'Select Timetable','id'=>'taskid']) !!} 
                </div>      
                 <div class="form-group">
                    {!! Form::label('task','Task') !!}
                    {!! Form::text('task','',['class'=>'form-control','required'=>'required','placeholder'=>'Enter task here','id'=>'taskname']) !!} 
                </div>                                              
            </div>
            <div class="modal-footer">
                {!! Form::hidden('taskid','',['id'=>'taskID']) !!}
                {!! Form::button('<i class="fa fa-save"></i> Update', array('type' => 'submit', 'class' => 'btn btn-primary')) !!}
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-arrow-left"></i> Close</button>
            </div>
        </div>
        {!!Form::close()!!}
    </div>
</div>
@endsection
<script type="text/javascript">
    function timeEditFunction(elem){

        var x = elem.id;

        $.ajax({

            type:"POST",
            dataType: 'json',
            data: {timeid: x},
            url: "../getTimetableDetails",
            success: function(data){            
                
                $('#timetableid').val(data['timetable']);                
                $('#timeid').val(data['id']);                

                $('#editTimeModal').modal('show');
            }    
        });
    }

    function taskEditFunction(elem){

        var x = elem.id;

        $.ajax({

            type:"POST",
            dataType: 'json',
            data: {taskid: x},
            url: "../getTaskDetails",
            success: function(data){            
                
                $('#taskname').val(data['task']);              
                $('#taskid').val(data['timetable_id']);
                $('#taskID').val(data['id']);                

                $('#editTaskModal').modal('show');
            }    
        });
    }
</script>