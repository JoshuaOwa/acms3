@extends('layouts.master')
@section('main-body')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2><i class="fa fa-users"></i> USERS LIST</h2>        
    </div>
</div>        
<div class="wrapper wrapper-content animated fadeInRight">	
	<div class="row">
		<div class="col-lg-12 animated flash">
		    <?php if (session('is_success')): ?>
		        <div class="alert alert-success alert-dismissible fade in" role="alert">
		            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
		        </button>
		            <center><h4>User was successfully added!<i class="fa fa-check"></i></h4></center>                
		        </div>
		    <?php endif;?>
		</div>
		<div class="col-lg-12 animated flash">
		    <?php if (session('is_update')): ?>
		        <div class="alert alert-success alert-dismissible fade in" role="alert">
		            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
		        </button>
		            <center><h4>User was successfully updated!<i class="fa fa-check"></i></h4></center>                
		        </div>
		    <?php endif;?>
		</div>
		<div class="col-lg-12 animated flash">
		    <?php if (session('is_reset')): ?>
		        <div class="alert alert-success alert-dismissible fade in" role="alert">
		            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
		        </button>
		            <center><h4>User password was successfully updated!<i class="fa fa-check"></i></h4></center>                
		        </div>
		    <?php endif;?>
		</div>
		<div class="col-lg-12 animated flash">
		    <?php if (session('is_workgroup')): ?>
		        <div class="alert alert-success alert-dismissible fade in" role="alert">
		            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
		        </button>
		            <center><h4>User workgroup was successfully updated!<i class="fa fa-check"></i></h4></center>                
		        </div>
		    <?php endif;?>
		</div>
	</div>
    <div class="row">
        <div class="col-lg-12">
	        <div class="ibox float-e-margins">
	            <div class="ibox-title" style="background-color:#009688">
	                <h5 style="color:white"><i class="fa fa-users"></i> Users Table</h5>
	                {!! Html::decode(link_to_Route('users.create', '<i class="fa fa-plus"></i> New User', [], ['class' => 'btn btn-white btn-xs pull-right'])) !!}	                
	            </div>
	            <div class="ibox-content">
	            	<div class="table-responsive">
			            <table class="table table-striped table-bordered table-hover dataTables-example">
				            <thead>
					            <tr>				                
					                <th>IMAGE</th>
					                <th>EMP #</th>
					                <th>FULLNAME</th>					           		
					           		<th>POSITION</th>
					                <th>STATUS</th>
					                <th>ACTION</th>			               
					            </tr>
				            </thead>
				            <tbody align="center">
					           @forelse($users as $user)
					           		<?php $imgURL = (!$user->image ? 'default.jpg' : $user->image);?>				           		
									@if($user->active < 1)
									<tr class="danger" style="opacity:0.6; filter:alpha(opacity=40);">
									@else
					           		<tr>
					           		@endif
					           			<td>
					           				@if(!empty($user->image))				           					
					           					{!! Html::image('uploads/users/'.$user->employee_number.'/'.$imgURL,'',['class'=>'img-circle','style'=>'width:50px; height:50px;']) !!}
					           					
					           				@else				           					
					           					<img src="/uploads/users/default.jpg" class="img-circle" style="width:50px; height:50px;"></img>
					           				@endif				           				
					           			</td>
					           			<td>{!! $user->empid !!}</td>
					           			<td>{!! strtoupper($user->first_name.' '.$user->last_name) !!}</td>					           			
					           			<td>{!! strtoupper($user->position->position) !!}</td>							
					           			<td>
					           				@if($user->active > 0)
					           					{!! Html::decode(link_to_Route('users.activate','<i class="fa fa-close"></i> Deactivate Account', $user->id, array('class' => 'btn btn-danger btn-xs')))!!}
					           				@else
					           					{!! Html::decode(link_to_Route('users.activate','<i class="fa fa-check"></i> Activate Account', $user->id, array('class' => 'btn btn-primary btn-xs')))!!}
					           				@endif					           				
					           			</td>				           			
					           			<td>
					           				@if($user->active > 0)
				           						{!! Html::decode(link_to_Route('users.edit','<i class="fa fa-pencil"></i> Edit Profile', $user->id, array('class' => 'btn btn-primary btn-xs')))!!}
				           						{!! Html::decode(link_to_Route('access_controls.manage_access','<i class="fa fa-pencil"></i> Module Access', $user->id, array('class' => 'btn btn-info btn-xs')))!!}
					           				@endif
					           			</td>
					           		</tr>
					           	@empty
					           	@endforelse
				            </tbody>			            
			            </table>
		            </div>
	            </div>
	        </div>
	    </div>	    
    </div>
</div>	

@endsection
