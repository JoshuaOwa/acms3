<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>AHEAD Contact Management System</title>

    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="/assets/css/plugins/dataTables/datatables.min.css" rel="stylesheet">    


    <link href="/assets/css/animate.css" rel="stylesheet">
    <link href="/assets/css/styleowa.css" rel="stylesheet">

</head>
<body class="gray-bg">
    
    <div class="animated fadeInDown">
        <div>
            <div style="margin-top: 40px;">                
                <center>
                    <h3 style="font-size: 70px;">JOB ORDER MONITORING</h3>
                    <p>(External Job Order Only)</p>
                </center>
            </div>                                                
        </div>
    </div>
    <div class="animated fadeInUp">
        <div class="row" style="margin-top: 80px;">
            <div class="col-lg-12">                                    
                <ul class="notes" id="notelist">
                    @foreach($jobs as $note)
                    <li>
                        <div>
                            <small>{!! $note->created_at !!}</small>
                            <h4>{!! "JO#: ".$note->job_id !!}</h4>                            
                                <p><strong> {!! $note->assigned_name !!}</strong></p>
                                <p><strong> Subject: </strong>{!! $note->subject !!}</p>
                                <p><strong> Start Date: </strong>{!! $note->start_date !!}</p>
                                <p><strong> End Date:</strong>{!! $note->end_date !!}</p>                        
                            <a href="#">                            
                            </a>       
                        </div>
                    </li>
                    @endforeach
                </ul>                                    
            </div>                     
        </div>        
    </div>
        

    <!-- Mainly scripts -->
    <script src="/assets/js/jquery-2.1.1.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
</body>
</html>
<!-- Software Developer: JOSHUA M. FRADEJAS -->

<script type="text/javascript">
    
function loadrealtime(){  
    
    setTimeout(function(){
       $( "#notelist" ).load( "/job_monitoring #notelist" );
    }); 
}

setInterval(function(){
    loadrealtime()
}, 5000);
</script>