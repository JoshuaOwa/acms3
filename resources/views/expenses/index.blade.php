@extends('layouts.master')
@section('main-body')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2><i class="fa fa-folder"></i> EXPENSES FOR APPROVAL</h2>        
    </div>
</div>        
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12 animated flash">
            <?php if (session('is_approved')): ?>
                <div class="alert alert-success alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                    <center><h4>Expense was approved!<i class="fa fa-check"></i></h4></center>                
                </div>
            <?php endif;?>
            <?php if (session('is_denied')): ?>
                <div class="alert alert-success alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                    <center><h4>Expense was denied!<i class="fa fa-check"></i></h4></center>                
                </div>
            <?php endif;?>
            <?php if (session('is_assigned')): ?>
                <div class="alert alert-success alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                    <center><h4>Expense was successfully assigned!<i class="fa fa-check"></i></h4></center>                
                </div>
            <?php endif;?>
            <?php if (session('not_assigned')): ?>
                <div class="alert alert-danger alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                    <center><h4>Please Try To Assigned The Again!<i class="fa fa-close"></i></h4></center>                
                </div>
            <?php endif;?>
            <?php if (session('is_error_not_found')): ?>
                <div class="alert alert-danger alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                    <center><h4>Error in Opening Expense! Please report this to the admin<i class="fa fa-close"></i></h4></center>                
                </div>
            <?php endif;?>
            <?php if (session('is_transfer_proj')): ?>
                <div class="alert alert-success alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                    <center><h4>Expense was successfully transfered! <i class="fa fa-check"></i></h4></center>                
                </div>
            <?php endif;?>     
            <?php if (session('error_transfer_proj')): ?>
                <div class="alert alert-danger alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                    <center><h4>Expense Approval Failed! <i class="fa fa-wrong"></i></h4></center>                
                </div>
            <?php endif;?>     
        </div>
        <div class="ibox-content">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover dataTables-example">
                    <thead>
                    <tr>
                        <th>Project Name</th>
                        <th>JO#</th>
                        <th>Employee Name</th>
                        <th>Date</th>
                        <th>Particular</th>                      
                        <th>Type</th>                                  
                        <th>Total Amount</th>
                        <th>Status</th>
                        <th>Action</th>                               
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($expenses as $expense)
                            <tr>
                                <td>{!! $expense->project->project_name !!}</td>
                                <td>{!! $expense->job_order_id !!}</td>
                                <td>{!! strtoupper($expense->user->first_name.' '.$expense->user->last_name) !!}</td>
                                <td>{!! $expense->date !!}</td>
                                <td>{!! $expense->particular !!}</td>
                                <td>{!! $expense->type->type !!}</td>
                                <td>{!! $expense->amount*$expense->qty !!}</td>
                                <td>
                                    @if($expense->status === 1)
                                        <span class="label label-info"><i class="fa fa-clock-o"></i> For Approval</span>
                                    @elseif($expense->status === 2)
                                        <span class="label label-success"><i class="fa fa-check"></i> Approved</span>
                                    @elseif($expense->status === 3)
                                        <span class="label label-primary"> <i class="fa fa-user"></i>Denied</span>
                                    @endif
                                </td>
                                <td>
                                    {!! Html::decode(link_to_route('expenses.overview', '<i class="fa fa-eye"></i> Details',$expense->encryptname, array('class' => 'btn btn-xs btn-info'))) !!}
                                </td>                                        
                            </tr>                   
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>   
    </div>                 
</div>	
@stop
@section('page-script')        

    $('#project_id').multiselect({
    maxHeight: 200,
    includeSelectAllOption: true,
    enableCaseInsensitiveFiltering: true,
    enableFiltering: true,
    buttonWidth: '100%',
    buttonClass: 'form-control',
    });       
    
   
@stop