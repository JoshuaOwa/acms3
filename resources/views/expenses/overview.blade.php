@extends('layouts.master')
@section('main-body')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12 animated flash">
        <?php if (session('error_in_action')): ?>
            <div class="alert alert-success alert-danger fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
            </button>
                <center><h4>Error in approving/denying expense!<i class="fa fa-check"></i></h4></center>                
            </div>
        <?php endif;?>
        <?php if (session('is_not_found')): ?>
            <div class="alert alert-success alert-danger fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
            </button>
                <center><h4>Error in download file!<i class="fa fa-check"></i></h4></center>                
            </div>
        <?php endif;?>
    </div>
    <div class="col-lg-12">
        <h2><i class="fa fa-building-o"></i> <i class="fa fa-eye"></i> EXPENSE DETAILS</h2>        
    </div>
</div>
&nbsp;
<div class="row">
    <div class="col-lg-12">      
        <div class="panel-body">
            <div class="tabs-container">
                <div class="row  border-bottom white-bg dashboard-header">
                    <div class="ibox-content ibox-heading">
                        <center>
                            <h2><strong>{!! strtoupper($project->project_name) !!}</strong></h2>
                            <small>Project Name</small>
                        </center>
                    </div>   
                    <div class="col-md-6">                
                        <ul class="list-group clear-list m-t">
                            <li class="list-group-item fist-item">
                                <span class="pull-right">
                                    @if($project->status === 0)
                                        <span class="label label-info"><i class="fa fa-clock-o"></i> For Approval</span>
                                    @elseif($project->status === 1)
                                        <span class="label label-success"><i class="fa fa-check"></i> Approved</span>
                                    @elseif($project->status === 2)
                                        <span class="label label-primary"> <i class="fa fa-user"></i>Assigned</span>                                       
                                    @elseif($project->status === 3)
                                        <span class="label label-danger"> <i class="fa fa-angellist"></i>Denied</span>                                       
                                    @elseif($project->status === 4)
                                        <span class="label label-warning"> <i class="fa fa-refresh"></i> For Re-Approve</span>                                       
                                    @endif
                                </span>
                                <span class="label label-info">*</span> <strong>Project Status:</strong>
                            </li>  
                            <li class="list-group-item ">
                                <span class="pull-right">
                                    {!! $project->tag !!}
                                </span>
                                <span class="label label-info">*</span> <strong>Tag:</strong>
                            </li>      
                            <li class="list-group-item">
                                <span class="pull-right">
                                    {!! strtoupper($project->approver->first_name.' '.$project->approver->last_name) !!}
                                </span>
                                <span class="label label-info">*</span> <strong>Project Approver:</strong>
                            </li>
                            <li class="list-group-item">
                                <span class="pull-right">
                                    {!! $project->project_address !!}
                                </span>
                                <span class="label label-info">*</span> <strong>Project Address:</strong>
                            </li>      
                            <li class="list-group-item">
                                <span class="pull-right">
                                    {!! $project->branch !!}
                                </span>
                                <span class="label label-info">*</span> <strong>Branch:</strong>
                            </li>                                                                                                  
                        </ul>                                    
                    </div>
                    <div class="col-md-6">                
                        <ul class="list-group clear-list m-t">
                            <li class="list-group-item fist-item">
                                <span class="pull-right">
                                    {!! strtoupper($project->user->first_name.' '.$project->user->last_name) !!}
                                </span>
                                <span class="label label-info">*</span> <strong>Created By:</strong>
                            </li>                        
                            
                            <li class="list-group-item fist-item">
                                <span class="pull-right">
                                    {!! $project->start_date !!}
                                </span>
                                <span class="label label-info">*</span> <strong>Start Date:</strong>
                            </li>                
                            <li class="list-group-item">
                                <span class="pull-right">
                                    {!! $project->end_date !!}
                                </span>
                                <span class="label label-info">*</span> <strong>End Date:</strong>
                            </li>
                            <li class="list-group-item">
                                <span class="pull-right">
                                    {!! $project->allowed_ot.' HOURS' !!}
                                </span>
                                <span class="label label-info">*</span> <strong>Allowed OT:</strong>
                            </li>                
                            <li class="list-group-item">
                                <span class="pull-right">
                                    {!! $project->project_cost !!}
                                </span>
                                <span class="label label-info">*</span> <strong>Project Cost:</strong>
                            </li>                                                  
                        </ul>
                    </div>
                </div>&nbsp;
                <div class="row  border-bottom white-bg dashboard-header">
                    <div class="ibox-content ibox-heading">
                        <center>
                            <h2><strong>Expense Details</strong></h2>                        
                        </center>        
                    </div>
                    <div class="col-md-6">                
                        <ul class="list-group clear-list m-t">
                            <li class="list-group-item fist-item">
                                <span class="pull-right">
                                    {!! strtoupper($expense->user->first_name.' '.$expense->user->last_name) !!}
                                </span>
                                <span class="label label-info">*</span> <strong>Filed By:</strong>
                            </li> 
                            <li class="list-group-item fist-item">
                                <span class="pull-right">
                                    JOB ORDER #{!! $expense->job_order_id !!}
                                </span>
                                <span class="label label-info">*</span> <strong>Job Order #:</strong>
                            </li>
                            <li class="list-group-item fist-item">
                                <span class="pull-right">
                                    {!! strtoupper($job->user->first_name.' '.$job->user->last_name) !!}
                                </span>
                                <span class="label label-info">*</span> <strong>Job Order Creator:</strong>
                            </li>
                            <li class="list-group-item fist-item">
                                <span class="pull-right">
                                    {!! $expense->particular !!}
                                </span>
                                <span class="label label-info">*</span> <strong>Particular:</strong>
                            </li>
                            <li class="list-group-item fist-item">
                                <span class="pull-right">
                                    {!! $expense->type->type !!}
                                </span>
                                <span class="label label-info">*</span> <strong>Type:</strong>
                            </li>                                                                        
                        </ul>
                    </div>
                    <div class="col-md-6">                
                        <ul class="list-group clear-list m-t">
                            <li class="list-group-item fist-item">
                                <span class="pull-right">
                                    {!! date('F d, Y',strtotime($expense->date)) !!}
                                </span>
                                <span class="label label-info">*</span> <strong>Expense Date:</strong>
                            </li>
                            <li class="list-group-item fist-item">
                                <span class="pull-right">
                                    {!! number_format($expense->amount,2) !!}
                                </span>
                                <span class="label label-info">*</span> <strong>Amount:</strong>
                            </li>
                            <li class="list-group-item fist-item">
                                <span class="pull-right">
                                    {!! $expense->qty !!}
                                </span>
                                <span class="label label-info">*</span> <strong>Qty:</strong>
                            </li>
                            <li class="list-group-item fist-item">
                                <span class="pull-right">
                                    {!! number_format($expense->amount*$expense->qty,2) !!}
                                </span>
                                <span class="label label-info">*</span> <strong>Total Amount:</strong>
                            </li>
                            <li class="list-group-item fist-item">
                                <span class="pull-right">
                                    @if($expense->status === 1)
                                        <span class="label label-info pull-right"><i class="fa fa-clock-o"></i> For Approval</span>
                                    @elseif($expense->status === 2)
                                        <span class="label label-success pull-right"><i class="fa fa-check"></i> Approved</span>
                                    @elseif($expense->status === 3)
                                        <span class="label label-primary pull-right"> <i class="fa fa-user"></i>Denied</span>
                                    @endif
                                </span>
                                <span class="label label-info">*</span> <strong>Status:</strong>
                            </li>
                            <li class="list-group-item fist-item">
                                <span class="pull-right">
                                    @foreach($files as $file)
                                        {!! Form::open(array('route'=>'expenses.download','method'=>'POST')) !!}
                                        {!! Form::hidden('id',$file->id) !!}
                                        <button class="btn btn-success btn-xs" type="submit">{!! $file->filename !!}</button>
                                        {!! Form::close() !!}
                                    @endforeach
                                </span>
                                <span class="label label-info">*</span> <strong>Files:</strong>
                            </li>                                                                       
                        </ul>
                    </div>&nbsp;
                    <hr>
                    <div class="col-md-12">
                        {!! Form::open(array('route'=>'expenses.approval','method'=>'POST')) !!}
                        <input type="hidden" name="id" value="{{$expense->id}}">
                        <button class="btn btn-md btn-success" id="btnaprove" type="button">Approve</button>
                        <button class="btn hidden" id="approved" name="type" value="1" type="submit"></button>
                        <button class="btn btn-md btn-danger" id="btndeny">Deny</button>
                        <button class="btn hidden" id="denied" name="type" value="2" type="submit"></button>
                        {!! Form::close() !!}
                    </div>&nbsp;
                </div>                                                        
            </div>
        </div>
    </div>
</div>
@stop
@section('page-script')
$('#btnaprove').click(function () {
    swal({
        title: "Are you sure?",
        text: "This Expense will be approved!",
        icon: "warning",
        dangerMode: true,
        showCancelButton: true,
        confirmButtonText: "Yes, i am sure!"
    },function(){
        $('#approved').click();
    });
});
$('#btndeny').click(function () {
    swal({
        title: "Are you sure?",
        text: "This Expense will be denied!",
        icon: "warning",
        dangerMode: true,
        showCancelButton: true,
        confirmButtonText: "Yes, i am sure!"
    },function(){
        $('#denied').click();
    });
});
@endsection
