@extends('layouts.master')
@section('main-body')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2><i class="fa fa-users"></i> MY CONTACTS</h2>        
    </div>
</div>        
<div class="wrapper wrapper-content animated fadeInRight">	
	<div class="row">
		<div class="col-lg-12 animated flash">
		    <?php if (session('is_success')): ?>
		        <div class="alert alert-success alert-dismissible fade in" role="alert">
		            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
		        </button>
		            <center><h4>Contact was successfully added!<i class="fa fa-check"></i></h4></center>                
		        </div>
		    <?php endif;?>
		</div>
		<div class="col-lg-12 animated flash">
		    <?php if (session('is_update')): ?>
		        <div class="alert alert-success alert-dismissible fade in" role="alert">
		            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
		        </button>
		            <center><h4>Contact was successfully updated!<i class="fa fa-check"></i></h4></center>                
		        </div>
		    <?php endif;?>
		</div>
		<div class="col-lg-12 animated flash">
		    <?php if (session('is_reset')): ?>
		        <div class="alert alert-success alert-dismissible fade in" role="alert">
		            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
		        </button>
		            <center><h4>Contact password was successfully updated!<i class="fa fa-check"></i></h4></center>                
		        </div>
		    <?php endif;?>
		</div>
		<div class="col-lg-12 animated flash">
		    <?php if (session('is_workgroup')): ?>
		        <div class="alert alert-success alert-dismissible fade in" role="alert">
		            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
		        </button>
		            <center><h4>Contact workgroup was successfully updated!<i class="fa fa-check"></i></h4></center>                
		        </div>
		    <?php endif;?>
		</div>
        <div class="col-lg-12 animated flash">
            <?php if (session('is_transferred')): ?>
                <div class="alert alert-success alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                    <center><h4>Contact has been transferred!<i class="fa fa-check"></i></h4></center>                
                </div>
            <?php endif;?>
        </div>
        <div class="col-lg-12 animated flash">
            <?php if (session('not_transferred')): ?>
                <div class="alert alert-danger alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                    <center><h4>Contact not found!<i class="fa fa-check"></i></h4></center>                
                </div>
            <?php endif;?>
        </div>
	</div>	
    <div class="row">
        <div class="col-lg-12">
        	<div class="row">
                <div class="col-lg-12">
                    <div class="form-group"><label class="col-sm-12 control-label">Filter by:</label>
                        {!!Form::open(array('route'=>'contacts.filter','method'=>'POST'))!!}
                        <div class="col-lg-6">
                            <button type="submit" class="btn btn-info btn-sm" value="0" name="filter"><i class="fa fa-clock-o"></i> For Approval</button>
                            <button type="submit" class="btn btn-warning btn-sm" value="3" name="filter"><i class="fa fa-refresh"></i> For Re-Approve</button>
                            <button type="submit" class="btn btn-success btn-sm" value="1" name="filter"><i class="fa fa-check"></i> Approved</button>
                            <button type="submit" class="btn btn-danger btn-sm" value="2" name="filter"><i class="fa fa-close"></i> Denied</button>                            
                        </div>
                        {!! Form::close() !!}                                
                    </div>
                </div>
            </div>
            &nbsp;
	         <div class="panel blank-panel">
                <div class="panel-heading">                            
                    <div class="panel-options">    
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#tab-main"> <i class="fa fa-bullseye"></i> MAIN</a></li>                                                        
                        </ul>
                    </div>
                </div>
                &nbsp;
                &nbsp;
                <div class="tab-content">
                    <div id="tab-main" class="tab-pane active">   
                        <div></div>
                        <div class="form-group pull-right">
                            <div class="col-lg-12">
                                {!! Html::decode(link_to_Route('contacts.create', '<i class="fa fa-plus"></i> New Contact', [], ['class' => 'btn btn-primary'])) !!}                                
                            </div>
                        </div>                    
                        </br>
                        &nbsp;
                         <center><h2>CLIENT CONTACTS</h2></center>
                        <div class="col-lg-12">    
                            <div class="table-responsive">                                        
                                <table class="table table-striped table-hover dataTables-example" >
						            <thead>
							            <tr>
                                            <th>COMPANY</th>			                					                		
							                <th>ADDRESS</th>
                                            <th>CONTACT PERSON</th>
							           		<th>MOBILE NUMBER</th>
							                <th>PHONE NUMBER</th>							                
							                <th>STATUS</th>
							                <th>ACTION</th>			               
							            </tr>
						            </thead>
						            <tbody align="center">
							        	@forelse($contacts as $contact)
							           		<tr>
							           			<td>{!! $contact->company !!}</td>                                                
							           			<td>{!! $contact->comp_address !!}</td>
                                                <td>                                                    
                                                    @foreach($contact->person as $person)
                                                        @if(!empty($person->contact_person))
                                                            {!! $person->contact_person.'/' !!}                                         
                                                        @endif
                                                    @endforeach                                                    
                                                </td>
							           			<td>
                                                    @foreach($contact->person as $person)  
                                                        @if(!empty($person->mobile_number))
                                                            {!! $person->mobile_number.'/' !!}
                                                        @endif
                                                    @endforeach                           
                                                </td>
							           			<td>
                                                    @foreach($contact->person as $person)  
                                                        @if(!empty($person->tel_no))
                                                            {!! $person->tel_no.'/' !!}
                                                        @endif
                                                    @endforeach                             
                                                </td>							           			
		                                        <td>
		                                            @if($contact->status === 0)
		                                                <span class="label label-info"><i class="fa fa-clock-o"></i> For Approval</span>
		                                            @elseif($contact->status === 1)
		                                                <span class="label label-success"><i class="fa fa-check"></i> Approved</span>
		                                            @elseif($contact->status === 2)
		                                                <span class="label label-danger"> <i class="fa fa-close"></i> Denied</span>
                                                    @elseif($contact->status === 3)
                                                        <span class="label label-warning"> <i class="fa fa-close"></i> For Re-Approve</span>
		                                            @endif
		                                        </td>
		                                        <td class="action" style="width:15%">  
		                                            @if($contact->status === 0)
		                                                {!! Html::decode(link_to_Route('contacts.show','<i class="fa fa-eye"></i> Details', $contact->encryptname, array('class' => 'btn btn-primary btn-xs')))!!}
                                                        @if($contact->created_by == $id)
		                                                  {!! Html::decode(link_to_Route('contacts.edit','<i class="fa fa-pencil"></i> Edit', $contact->encryptname, array('class' => 'btn btn-warning btn-xs')))!!}
                                                        @endif
		                                            @elseif($contact->status === 1)                                                        
		                                                {!! Html::decode(link_to_Route('contacts.overview','<i class="fa fa-eye"></i> Overview', $contact->encryptname, array('class' => 'btn btn-primary btn-xs')))!!}
		                                                @if($contact->created_by == $id)
                                                          {!! Html::decode(link_to_Route('contacts.edit','<i class="fa fa-pencil"></i> Edit', $contact->encryptname, array('class' => 'btn btn-warning btn-xs')))!!}
                                                        @endif
		                                            @elseif($contact->status === 2)
		                                                {!! Html::decode(link_to_Route('contacts.show','<i class="fa fa-eye"></i> Details', $contact->encryptname, array('class' => 'btn btn-primary btn-xs')))!!}
                                                    @elseif($contact->status === 3)
                                                        {!! Html::decode(link_to_Route('contacts.overview','<i class="fa fa-eye"></i> Overview', $contact->encryptname, array('class' => 'btn btn-primary btn-xs')))!!}
		                                            @endif                                                
		                                        </td>
							           		</tr>
							           	@empty
							           	@endforelse
						            </tbody>			            
					            </table>
					        </div>
					    </div>
		            </div>		                                              
	            </div>
	        </div>
	    </div>	    
    </div>
</div>	
<div class="modal inmodal" id="TransferClientModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated rollIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-refresh fa-spin modal-icon"></i>
                <h4 class="modal-title">Transfer Contact</h4>                    
            </div>              
            {!! Form::open(array('route'=>'contacts.transfer','method'=>'POST')) !!}
                <div class="modal-body">                                        
                    <div class="row" style="padding-top:20px;">
                        <div class="form-group col-lg-12">
                            <div class="row">
                                <div class="col-lg-12">
                                    {!!Form::label('created_by','Transfer To')!!}                 
                                    {!!Form::select('created_by',$employees,null,['class'=>'form-control','id'=>'employees_id'])!!} 
                                    @if ($errors->has('created_by')) <p class="help-block" style="color:red;">{{ $errors->first('created_by') }}</p> @endif
                                </div> 
                                <div class="col-lg-12" style="padding-top:20px;">
                                    {!!Form::label('contact_id','Contact Name')!!}                 
                                    {!!Form::select('contact_id[]',$contact_list,null,['class'=>'form-control','id'=>'contact_id', 'multiple'=>true])!!} 
                                    @if ($errors->has('contact_id')) <p class="help-block" style="color:red;">{{ $errors->first('client_id') }}</p> @endif
                                </div> 
                            </div>
                        </div>                                                                                                     
                    </div>                            
                </div>
                <div class="modal-footer">                        
                    {!! Form::button('Close', array('type' => 'submit', 'class' => 'btn btn-white','data-dismiss'=>'modal')) !!}
                    {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'button', 'class' => 'btn btn-primary pull-right sbalert')) !!}
                    {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'submit', 'class' => 'btn btn-primary hidden', 'id'=>'urbtn')) !!}              
                </div>                  
            {!! Form::close() !!}            
        </div>
    </div>
</div>
@endsection
@section('page-script')   
    $('#employees_id, #contact_id').multiselect({
    maxHeight: 200,
    includeSelectAllOption: true,
    enableCaseInsensitiveFiltering: true,
    enableFiltering: true,
    buttonWidth: '100%',
    buttonClass: 'form-control',
    });   

    $('.sbalert').click(function () {

        var $nonemployee = $('#employees_id').filter(function() {
            return this.value != ''
        });

        var $noncontact = $('#contact_id').filter(function() {
            return this.value != ''
        });

        if ($nonemployee.length > 0) {

            if ($noncontact.length > 0) {
                swal({
                    title: "Are you sure?",
                    text: "You will now transfer the selected contacts",
                    type: "info",
                    showCancelButton: true,
                    confirmButtonColor: "#1abc9c",
                    confirmButtonText: "Yes, transfer it!",
                    closeOnConfirm: false
                }, function () {
                    swal("Submitted!", "Allright! Selected contacts are now transferring", "success");
                    $('#urbtn').click();
                });
            }else{            
                swal({
                    title: "Warning",
                    text: "Ooops! I'm Not Intellegient Enough To Know Which Contact You Want To Transfer!",
                    type: "warning"
                });        
            }        
        }
        else{            
            swal({
                title: "Warning",
                text: "Ooops! I'm Not Intellegient Enough To Know To Whom Do You Want To Transfer The Contact/s!",
                type: "warning"
            });        
        }               
    });
@stop