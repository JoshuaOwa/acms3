@extends('layouts.master')
@section('main-body')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2><i class="fa fa-building-o"></i> <i class="fa fa-eye"></i> CONTACT DETAILS</h2>        
    </div>
</div>        
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">        
        <div class="col-lg-8">
            <div class="ibox float-e-margins">
                <div class="ibox-title" style="background-color:#009688">
                    <h5 style="color:white">Status</h5>
                    <div class ="pul-right">
                        @if($contact->status === 0)
                            <span class="label label-info">For Approval</span>
                        @elseif($contact->status === 1)
                            <span class="label label-primary">Approved</span>
                        @elseif($contact->status === 2)
                            <span class="label label-danger">Denied</span>
                        @elseif($contact->status === 3)
                            <span class="label label-warning"> For Re-Approve</span>
                        @endif
                    </div>                    
                </div>
                <div class="ibox-content">
                    <div class="form-group" style="padding-top:20px;">
                        <h2><center><strong>{!! strtoupper($contact->company) !!}</strong></center></h2>
                        <center><span><small><i>Company Name</i></small></span></center>   
                    </div><div class="hr-line-dashed"></div>                               
                                
                    <table class="table table table-hover">
                        <tr>
                            <td class="action" style="width:30%"><i>INDUSTRY:</i></td><td>{!! strtoupper($contact->industry) !!}</td>
                        </tr>    
                    </table>
                    <table class="table table table-hover">
                        <tr>
                            <td class="action" style="width:30%"><i>COMPANY ADDRESS:</i></td><td>{!! strtoupper($contact->comp_address) !!}</td>
                        </tr>    
                    </table>                    
                    <table class="table table table-hover">
                        <tr>
                            <td class="action" style="width:30%"><i>CONTACT TYPE:</i></td>
                            <td>                                    
                                {!! strtoupper($contact->contacttype->type) !!}                                    
                            </td>
                        </tr>    
                    </table>                                                         
                    <table class="table table table-hover">
                        <tr>
                            <td class="action" style="width:30%"><i>CONTACT PERSON:</i></td><td>{!! strtoupper($contact->person->contact_person) !!}</td>
                        </tr>    
                    </table>      
                    <table class="table table table-hover">
                        <tr>
                            <td class="action" style="width:30%"><i>FUNCTION OF CONTACT PERSON:</i></td><td>{!! strtoupper($contact->person->function) !!}</td>
                        </tr>    
                    </table>                                        
                    <table class="table table table-hover">
                        <tr>
                            <td class="action" style="width:30%"><i>EMAIL ADDRESS:</i></td>
                            <td>
                                @if($contact->person->email)
                                    {!! $contact->person->email!!}                                
                                @else
                                    N/A
                                @endif
                            </td>                            
                        </tr>
                    </table>
                        <table class="table table table-hover">         
                            <tr>
                                <td class="action" style="width:30%"><i>TELEPHONE NUMBER:</i></td>
                                <td>
                                    @if($contact->person->tel_no)
                                        {!! $contact->person->tel_no !!}
                                    @else
                                        N/A
                                    @endif
                                </td>
                            </tr>
                        </table>
                        <table class="table table table-hover">
                            <tr>
                                <td class="action" style="width:30%"><i>MOBILE NUMBER:</i></td>
                                <td>
                                    @if($contact->person->mobile_number)
                                        {!! $contact->person->mobile_number !!}
                                    @else
                                        N/A
                                    @endif
                                </td>
                            </tr>
                        </table> 
                         <table class="table table table-hover">
                            <tr>
                                <td class="action" style="width:30%"><i>FAX NUMBER:</i></td>
                                <td>
                                    @if($contact->person->fax_number)
                                        {!! strtoupper($contact->person->fax_number) !!}
                                    @else
                                        N/A
                                    @endif
                                </td>
                            </tr>   
                        </table>           
                                                                                                
                        <table class="table table table-hover">                 
                            <tr>
                                <td class="action" style="width:30%"><i>PREPARED BY:</i></td>
                                 <td>
                                    
                                    {!! strtoupper($contact->user->first_name.' '.$contact->user->last_name) !!}                                
                                </td>
                            </tr>
                        </table>
                        <table class="table table table-hover">
                            <tr>
                                <td class="action" style="width:30%"><i>DATE CREATED:</i></td>
                                 <td>
                                    
                                    {!! strtoupper($contact->updated_at) !!}                                
                                </td>
                            </tr>                        
                        </table><div class="hr-line-dashed"></div>         
                        <table>                                                
                        <tr>
                            <td>
                                <a href="javascript:history.back()"><button type="button" class="btn btn-default"><i class="fa fa-arrow-left"></i> Back </button></a>
                            </td><td>&nbsp;</td>                            
                        </tr>
                    </table>           
                </div>
            </div>
        </div>  
        <div class="col-lg-4" id="logflow">        
            <div class="panel panel-default">
                <div class="panel-heading">
                     <strong>Contact activity</strong>  
                </div>
                <div class="panel-body">
                    <div id="vertical-timeline" class="vertical-container dark-timeline">
                        @foreach($logs as $log)
                        <div class="vertical-timeline-block">
                            <div class="vertical-timeline-icon gray-bg">
                                <i class="fa fa-coffee"></i>
                            </div>
                            <div class="vertical-timeline-content">
                                <p>{!! $log->log !!}</p>
                                @if(!empty($log->remarks))
                                <br />                        
                                <br />
                                <p>Remarks: <br/>
                                {!! $log->remarks !!}</p>                        
                                @endif
                                <span class="vertical-date small text-muted"> {!! $log->created_at->diffForHumans() !!} </span>
                            </div>
                        </div>
                        @endforeach
                    </div>  
                </div>                
            </div>                        
        </div>     
    </div>
</div>
@stop
