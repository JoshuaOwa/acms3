@extends('layouts.master')
@section('main-body')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2><i class="fa fa-building-o"></i> <i class="fa fa-eye"></i> CONTACT DETAILS</h2>        
    </div>
</div>        
<div class="row  border-bottom white-bg dashboard-header">
    <center>
        <h2><strong>{!! strtoupper($contact->contact_person) !!}</strong></h2>
        <small>Contact Details:</small>
    </center>
    <div class="col-md-6">                
        <ul class="list-group clear-list m-t">
            <li class="list-group-item fist-item">
                <span class="pull-right">
                    {!! strtoupper($contact->approver->first_name.' '.$contact->approver->last_name) !!}
                </span>
                <span class="label label-info">*</span> Approver:
            </li>
            <li class="list-group-item">
                <span class="pull-right">
                    {!! $contact->mobile_number.' / '.$contact->tel_no !!}
                </span>
                <span class="label label-info">*</span> Contact Number:
            </li>
            <li class="list-group-item">
                <span class="pull-right">
                    {!! $contact->email !!}
                </span>
                <span class="label label-info">*</span> Email:
            </li>
            <li class="list-group-item">
                <span class="pull-right">
                    {!! $contact->address !!}
                </span>
                <span class="label label-info">*</span> Address:
            </li>                
        </ul>
    </div>
    <div class="col-md-6">                
        <ul class="list-group clear-list m-t">
            <li class="list-group-item fist-item">
                <span class="pull-right">
                    {!! strtoupper($contact->user->first_name.' '.$contact->user->last_name) !!}
                </span>
                <span class="label label-info">*</span> Created By:
            </li>
            <li class="list-group-item">
                <span class="pull-right">
                    @if($contact->status === 0)
                        <span class="label label-info"><i class="fa fa-clock-o"></i> For Approval</span>
                    @elseif($contact->status === 1)
                        <span class="label label-success"><i class="fa fa-check"></i> Approved</span>
                    @elseif($contact->status === 2)
                        <span class="label label-danger"> <i class="fa fa-close"></i> Denied</span>
                    @elseif($contact->status === 3)
                        <span class="label label-warning"> <i class="fa fa-close"></i> For Re-Approve</span>
                    @endif
                </span>
                <span class="label label-info">*</span> Status:
            </li>
            <li class="list-group-item">
                <span class="pull-right">
                    {!! $contact->email !!}
                </span>
                <span class="label label-info">*</span> Attachments:
            </li>
            
        </ul>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content">
                <div class="row">
                <div class="col-lg-8">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title" style="background-color:#009688">
                            <h5 style="color: white">Projects</h5> <span class="label label-primary">*</span>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>                               
                            </div>
                        </div>
                        <div class="ibox-content">                            
                            <div class="table-responsive">
                                <table class="table table-striped table-hover dataTables-example">
                                    <thead>
                                    <tr>                                            
                                        <th>Project</th>                                                           
                                        <th>Cost</th>
                                        <th>Start</th>                                            
                                        <th>End</th>
                                        <th style="text-align: center">JO count</th>
                                        <th style="text-align: center">Request count</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($projects as $project)
                                    <tr>
                                        <td>
                                            {!! Html::decode(link_to_route('projects.overview', $project->project_name,$project->encryptname, array())) !!}                                            
                                        </td>
                                        <td>{!! $project->project_cost !!}</td>
                                        <td>{!! $project->start_date !!}</td>
                                        <td>{!! $project->end_date !!}</td>
                                        <td style="text-align: center">{!! $project->jo_count !!}</td>
                                        <td style="text-align: center">{!! $project->req_count !!}</td>
                                        <td></td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>                            
                        </div>
                    </div>
                     <div class="ibox float-e-margins">
                        <div class="ibox-title" style="background-color:#009688">
                            <h5 style="color: white">Job Orders not yet link to any project</h5> <span class="label label-primary">*</span>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>                               
                            </div>
                        </div>
                        <div class="ibox-content">                            
                            <div class="table-responsive">
                                <table class="table table-striped table-hover dataTables-example">
                                    <thead>
                                    <tr>                                            
                                        <th>JO #</th>                                                           
                                        <th>Created By</th>
                                        <th>Assigned To</th>                                            
                                        <th>Status</th>                                        
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                   
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>                                        
                                    </tr>
                                   
                                    </tbody>
                                </table>
                            </div>                            
                        </div>
                    </div>
                     <div class="ibox float-e-margins">
                        <div class="ibox-title" style="background-color:#009688">
                            <h5 style="color: white">Requests not yet link to any project</h5> <span class="label label-primary">*</span>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>                               
                            </div>
                        </div>
                        <div class="ibox-content">                            
                            <div class="table-responsive">
                                <table class="table table-striped table-hover dataTables-example">
                                    <thead>
                                    <tr>                                            
                                        <th>Request #</th>                                                           
                                        <th>Created By</th>                                        
                                        <th>Status</th>                                        
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($float_request as $freq)
                                    <tr>
                                        <td>{!! $freq->request_no !!}</td>
                                        <td>{!! strtoupper($freq->user->first_name.' '.$freq->user->last_name) !!}</td>
                                        <td>
                                            @if($freq->status == 1)
                                                <span class="label label-warning"> <i class="fa fa-angellist"></i>Pending</span>
                                            @elseif($freq->status == 2)
                                                <span class="label label-success"> <i class="fa fa-angellist"></i>Returned</span>
                                            @else
                                                <span class="label label-primary"> <i class="fa fa-angellist"></i>Closed</span>
                                            @endif
                                        </td>
                                        <td>{!! $freq->request_no !!}</td>                                        
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>                            
                        </div>
                    </div>
                    <div class="col-lg-12" style="padding-top:10px;">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title" style="background-color:#009688">
                                <h5 style="color:white"><i class="fa fa-futbol-o"></i> Message Threads</h5>                                                     
                                    <div class="pull-right">
                                        <button type="button" class="btn btn-white btn-xs" data-toggle="modal" data-target="#myModal">
                                            Create New Thread
                                        </button>                                        
                                        <button type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#jobOrderModel">
                                            Create Job Order
                                        </button>                                                                      
                                    </div>                                
                            </div>
                            <div class="ibox-content">
                                <div class="feed-activity-list">                                
                                    <div class="feed-element box" id="flow2">
                                        <div class="media-body" style="padding-right:10px;">                                        
                                            <div style="padding-top:10px;"></div>
                                            <small class="text-muted"></small>
                                            <div class="well">                  
                                                <small class="pull-right">
                                                    
                                                </small>                                                                                                                                                        
                                                <strong>From: </strong>
                                                 <br/>                                            
                                                <br />
                                                <strong>Message: </strong><br/>
                                                                                                                                                                                                                                         
                                                <br /><br />
                                                <strong>Recipient/s: </strong><br /> 
                                                

                                                <br /><br />
                                                <button type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#myModalForward" id="" value="" onclick="forwardFunction(this)">
                                                    <i class="fa fa-forward"></i>
                                                </button>
                                                <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#addCommentModal" id="" value="" onclick="ThreadCommentFunction(this)">
                                                    <i class="fa fa-comment"></i>
                                                </button>                                           

                            
                                                <small class="pull-right">Seen:
                                                    
                                                </small><br />                                                                        
                                            </div>                                                                      
                                          <br />                                                            
                                        </div>
                                    </div>                                     
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                
                <div class="col-lg-4">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title"  style="background-color:#009688">
                            <h5 style="color: white;">Timeline Activity</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>                                
                            </div>
                        </div>
                        <div class="ibox-content ibox-heading">
                            <h3>You have meeting today!</h3>
                            <small><i class="fa fa-map-marker"></i> Meeting is on 6:00am. Check your schedule to see detail.</small>
                        </div>
                        <div class="ibox-content inspinia-timeline" id="contactoverviewflow">                            
                            <div class="timeline-item">
                                <div class="row">
                                    <div class="col-xs-3 date">
                                        <i class="fa fa-coffee"></i>
                                        8:00 am
                                        <br/>
                                    </div>
                                    <div class="col-xs-7 content">
                                        <p class="m-b-xs"><strong>Coffee Break</strong></p>
                                        <p>
                                            Go to shop and find some products.
                                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="timeline-item">
                                <div class="row">
                                    <div class="col-xs-3 date">
                                        <i class="fa fa-phone"></i>
                                        11:00 am
                                        <br/>
                                        <small class="text-navy">21 hour ago</small>
                                    </div>
                                    <div class="col-xs-7 content">
                                        <p class="m-b-xs"><strong>Phone with Jeronimo</strong></p>
                                        <p>
                                            Lorem Ipsum has been the industry's standard dummy text ever since.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="timeline-item">
                                <div class="row">
                                    <div class="col-xs-3 date">
                                        <i class="fa fa-user-md"></i>
                                        09:00 pm
                                        <br/>
                                        <small>21 hour ago</small>
                                    </div>
                                    <div class="col-xs-7 content">
                                        <p class="m-b-xs"><strong>Go to the doctor dr Smith</strong></p>
                                        <p>
                                            Find some issue and go to doctor.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="timeline-item">
                                <div class="row">
                                    <div class="col-xs-3 date">
                                        <i class="fa fa-comments"></i>
                                        12:50 pm
                                        <br/>
                                        <small class="text-navy">48 hour ago</small>
                                    </div>
                                    <div class="col-xs-7 content">
                                        <p class="m-b-xs"><strong>Chat with Monica and Sandra</strong></p>
                                        <p>
                                            Web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
                                        </p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>    

@stop
