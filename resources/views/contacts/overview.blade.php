@extends('layouts.master')
@section('main-body')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2><i class="fa fa-building-o"></i> <i class="fa fa-eye"></i> CONTACT DETAILS</h2>        
    </div>
</div>        
&nbsp;
<div class="row">
    <div class="col-lg-12">
        <div class="tabs-container">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#tab-contactdetails"> DETAILS</a></li>
                <li class=""><a data-toggle="tab" href="#tab-projects">PROJECTS</a></li>
                <li class=""><a data-toggle="tab" href="#tab-request">REQUEST</a></li>
                <li class=""><a data-toggle="tab" href="#tab-timeline">TIMELINE</a></li>                
            </ul>
            <div class="tab-content">
                <div id="tab-contactdetails" class="tab-pane active">
                    <div class="panel-body">
                        <div class="tabs-container">
                            <div class="row  border-bottom white-bg dashboard-header">
                                <center>
                                    <h2><strong>{!! strtoupper($contact->company) !!}</strong></h2>
                                    <small>Client Details:</small>
                                </center>
                                <div class="col-md-12">                
                                    <ul class="list-group clear-list m-t">
                                        <li class="list-group-item">                                            
                                            <span class="label label-info">*</span> <strong>Status:</strong>
                                            @if($contact->status === 0)
                                                <span class="label label-info"><i class="fa fa-clock-o"></i> For Approval</span>
                                            @elseif($contact->status === 1)
                                                <span class="label label-success"><i class="fa fa-check"></i> Approved</span>
                                            @elseif($contact->status === 2)
                                                <span class="label label-danger"> <i class="fa fa-close"></i> Denied</span>
                                            @elseif($contact->status === 3)
                                                <span class="label label-warning"> <i class="fa fa-close"></i> For Re-Approve</span>
                                            @endif                                            
                                        </li>            
                                    </ul>
                                    <ul class="list-group clear-list m-t">
                                        <li class="list-group-item">                                            
                                            <span class="label label-info">*</span><strong> CREATED BY: </strong>{!! strtoupper($contact->user->first_name.' '.$contact->user->last_name) !!}
                                        </li>
                                    </ul>
                                    <ul class="list-group clear-list m-t">
                                        <li class="list-group-item">                                            
                                            <span class="label label-info">*</span> <strong>APPROVER:</strong> {!! strtoupper($contact->approver->first_name.' '.$contact->approver->last_name) !!}
                                        </li>            
                                    </ul>
                                    <ul class="list-group clear-list m-t">
                                        <li class="list-group-item fist-item">                                            
                                            <span class="label label-info">*</span> <strong>CLIENT TYPE:</strong> {!! strtoupper($contact->contacttype->type) !!}   
                                        </li>            
                                    </ul>
                                    <ul class="list-group clear-list m-t">
                                        <li class="list-group-item">                                            
                                            <span class="label label-info">*</span><strong> ADDRESS: </strong>{!! $contact->comp_address !!}
                                        </li>                
                                    </ul>           
                                    <ul class="list-group clear-list m-t">
                                        <li class="list-group-item">                                            
                                            <span class="label label-info">*</span><strong> INDUSTRY: </strong>{!! $contact->industry !!}
                                        </li>                
                                    </ul>                                
                                    <ul class="list-group clear-list m-t">
                                        <li class="list-group-item">                                            
                                            <span class="label label-info">*</span><strong> CONTACT PERSON/S: </strong><br /><br />
                                            <table class="table">
                                                <thead>
                                                    <th>NAME</th>
                                                    <th>FUNCTION</th>
                                                    <th>EMAIL</th>
                                                    <th>MOBILE #</th>
                                                    <th>TEL #</th>                                                        
                                                </thead>
                                                <tbody>
                                                     @foreach($contact->person as $person)
                                                    <tr>
                                                        <td>{!! $person->contact_person !!}</td>
                                                        <td>{!! $person->function !!}</td>
                                                        <td>{!! $person->email !!}</td>
                                                        <td>{!! $person->mobile_number !!}</td>
                                                        <td>{!! $person->tel_no!!}</td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>                                            
                                        </li>                
                                    </ul>           
                                </div>
                            </div>                                              
                        </div>
                    </div>
                </div>            
                <div id="tab-projects" class="tab-pane">
                    <div class="panel-body">
                        <div class="tabs-container">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="wrapper wrapper-content">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="ibox float-e-margins">
                                                    <div class="ibox-title" style="background-color:#009688">
                                                        <h5 style="color: white">Projects</h5> <span class="label label-primary">*</span>
                                                        <div class="ibox-tools">
                                                            <a class="collapse-link">
                                                                <i class="fa fa-chevron-up"></i>
                                                            </a>                               
                                                        </div>
                                                    </div>
                                                    <div class="ibox-content">                            
                                                        <div class="table-responsive">
                                                            <table class="table table-striped table-hover dataTables-example">
                                                                <thead>
                                                                <tr>                                            
                                                                    <th>Project</th>                                                           
                                                                    <th>Cost</th>
                                                                    <th>Start</th>                                            
                                                                    <th>End</th>
                                                                    <th style="text-align: center">JO count</th>
                                                                    <th style="text-align: center">Request count</th>
                                                                    <th>Action</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                @foreach($projects as $project)
                                                                <tr>
                                                                    <td>
                                                                        {!! Html::decode(link_to_route('projects.overview', $project->project_name,$project->encryptname, array())) !!}                                            
                                                                    </td>
                                                                    <td>{!! $project->project_cost !!}</td>
                                                                    <td>{!! $project->start_date !!}</td>
                                                                    <td>{!! $project->end_date !!}</td>
                                                                    <td style="text-align: center">{!! $project->jo_count !!}</td>
                                                                    <td style="text-align: center">{!! $project->req_count !!}</td>
                                                                    <td></td>
                                                                </tr>
                                                                @endforeach
                                                                </tbody>
                                                            </table>
                                                        </div>                            
                                                    </div>
                                                </div>                                                        
                                            </div>
                                        </div>
                                    </div>    
                                </div>                                                        
                            </div>
                        </div>
                    </div>
                </div>            
                <div id="tab-request" class="tab-pane">
                    <div class="panel-body">
                        <div class="tabs-container">
                            <div class="col-lg-12">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-title" style="background-color:#009688">
                                        <h5 style="color: white">Requests</h5> <span class="label label-primary">*</span>
                                        <div class="ibox-tools">
                                            <a class="collapse-link">
                                                <i class="fa fa-chevron-up"></i>
                                            </a>                               
                                        </div>
                                    </div>
                                    <div class="ibox-content">                            
                                        <div class="table-responsive">
                                            <table class="table table-striped table-hover dataTables-example">
                                                <thead>
                                                    <tr>                                                                    
                                                        <th>REQUEST CODE</th>                                            
                                                        <th>REQUESTOR</th>
                                                        <th>STATUS</th>
                                                        <th>ACTION</th>                        
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($reqs as $reqtome)
                                                        <tr>
                                                            <td>{!! $reqtome->request_no !!}</td>
                                                            <td>{!! strtoupper($reqtome->user->first_name.' '.$reqtome->user->last_name) !!}</td>                                                
                                                            <td>
                                                                @if($reqtome->status == 1)
                                                                    <span class="label label-warning label-xs"> <i class="fa fa-angellist"></i>Pending</span>
                                                                @elseif($reqtome->status == 2)
                                                                    <span class="label label-success label-xs"> <i class="fa fa-angellist"></i>Returned</span>
                                                                @else
                                                                    <span class="label label-primary label-xs"> <i class="fa fa-angellist"></i>Closed</span>
                                                                @endif
                                                            </td>
                                                            <td>                                                           
                                                                {!! Html::decode(link_to_Route('my_requests.show','<i class="fa fa-list"></i> Details', $reqtome->request_code, array('class' => 'btn btn-success btn-xs','target'=>'__blank')))!!}
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>  
                                            </table>
                                        </div>                            
                                    </div>
                                </div>                                                        
                            </div>  
                        </div>
                    </div>
                </div>            
                <div id="tab-timeline" class="tab-pane">
                    <div class="panel-body">
                        <div class="tabs-container">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="wrapper wrapper-content">
                                        <div class="row">                                                            
                                            <div class="col-lg-12">
                                                <div class="ibox float-e-margins">
                                                    <div class="ibox-title"  style="background-color:#009688">
                                                        <h5 style="color: white;">Timeline Activity</h5>
                                                        <div class="ibox-tools">
                                                            <a class="collapse-link">
                                                                <i class="fa fa-chevron-up"></i>
                                                            </a>                                
                                                        </div>
                                                    </div>
                                                    <div class="ibox-content ibox-heading">
                                                        <center><h3>CONTACT ACTIVITY!</h3></center>                            
                                                    </div>
                                                    <div class="ibox-content inspinia-timeline" id="contactoverviewflow"> 
                                                        @foreach($logs as $log)                           
                                                        <div class="timeline-item">
                                                            <div class="row">
                                                                <div class="col-xs-3 date">
                                                                    <i class="fa fa-phone"></i>
                                                                    {!! $log->created_at->diffForHumans() !!}
                                                                    <br/>
                                                                    <small class="text-navy">21 hour ago</small>
                                                                </div>
                                                                <div class="col-xs-9 content">
                                                                    <p class="m-b-xs"><strong>Details</strong></p>
                                                                    <p>
                                                                        {!! $log->log !!}
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>  
                                                        @endforeach                      
                                                    </div>
                                                </div>
                                            </div>                
                                        </div>            
                                    </div>
                                </div>
                            </div>   
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

 

@stop
