@extends('layouts.master')
@section('main-body')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2><i class="fa fa-plus"></i> NEW CONTACT</h2>        
    </div>
</div>        
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-md-12 animated flash">
            @if(count($errors) > 0)
                <div class="alert alert-danger alert-dismissible flash" role="alert">            
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
            </button>
                    <center><h4>Oh snap! You got an error!</h4></center>   
                </div>
            @endif
        </div>    
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title" style="background-color:#009688">
                    <h5 style="color:white"><i class="fa fa-plus"></i> Contact Form <small></small></h5>          
                </div> 
                <div class="ibox-content">
                    <div class="row">
                        {!!Form::open(array('route'=>'contacts.store','method'=>'POST','files'=>true))!!}     
                        <div class="form-group col-lg-12">
                            <div class="row">
                                <div class="col-lg-4">
                                    {!!Form::label('company','Client / Company Name*')!!}                       
                                    {!!Form::text('company','',['class'=>'form-control','placeholder'=>'Enter Client / Company Name','id'=>'company','required'=>'required'])!!}                                               
                                    @if ($errors->has('company')) <p class="help-block" style="color:red;">{{ $errors->first('company') }}</p> @endif                                             
                                </div>      
                                <div class="col-lg-4">
                                    {!! Form::label('industry','Industry*') !!}                       
                                    {!! Form::text('industry','',['class'=>'form-control','id'=>'industry']) !!}                                        
                                </div>      
                            </div>
                        </div>    
                        <div class="form-group col-lg-12">
                            <div class="row">
                                <div class="col-lg-8">
                                    {!!Form::label('comp_address','Company Address')!!}                 
                                    {!!Form::textarea('comp_address','',['class'=>'form-control','placeholder'=>'Enter Complete Client Address','id'=>'comp_address']) !!}
                                    @if ($errors->has('comp_address')) <p class="help-block" style="color:red;">{{ $errors->first('address') }}</p> @endif                                             
                                </div>      
                            </div>          
                        </div>           
                        <div class="form-group col-lg-12">
                            <div class="row">
                                <div class="col-lg-4">
                                    {!!Form::label('contact_person','Contact Person*')!!}                     
                                    {!!Form::text('contact_person','',['class'=>'form-control','placeholder'=>'Enter Contact Person','required'=>'required','id'=>'contact_person'])!!}                                             
                                    @if ($errors->has('contact_person')) <p class="help-block" style="color:red;">{{ $errors->first('contact_person') }}</p> @endif                                                                                     
                                </div>
                                 <div class="col-lg-4">
                                    {!!Form::label('function','Function*')!!}                     
                                    {!!Form::text('function','',['class'=>'form-control','placeholder'=>'Enter Function Of Contact Person'])!!}
                                    @if ($errors->has('function')) <p class="help-block" style="color:red;">{{ $errors->first('function') }}</p> @endif
                                </div>
                            </div>
                        </div>                 
                        <div class="form-group col-lg-12">
                            <div class="row">
                                <div class="col-lg-4">
                                    {!!Form::label('email','Email')!!}                       
                                    {!!Form::text('email','',['class'=>'form-control','placeholder'=>'Enter Client Email','required'=>'required','id'=>'email'])!!}          
                                    @if ($errors->has('email')) <p class="help-block" style="color:red;">{{ $errors->first('email') }}</p> @endif                                                                                  
                                </div>                                        
                            </div>
                        </div>                           
                        <div class="form-group col-lg-12">
                            <div class="row">
                                <div class="col-lg-4">
                                    {!!Form::label('tel_no','Telephone Number')!!}                       
                                    {!!Form::text('tel_no','',['class'=>'form-control','placeholder'=>'Enter Client Telephone Number'])!!}                                               
                                    @if ($errors->has('tel_no')) <p class="help-block" style="color:red;">{{ $errors->first('tel_no') }}</p> @endif                                             
                                </div>     
                                <div class="col-lg-4">
                                    {!!Form::label('fax_number','Fax Number')!!}                       
                                    {!!Form::text('fax_number','',['class'=>'form-control','placeholder'=>'Enter Client Fax Number'])!!}               
                                    @if ($errors->has('fax_number')) <p class="help-block" style="color:red;">{{ $errors->first('fax_number') }}</p> @endif                                                                             
                                </div>        
                                <div class="col-lg-4">
                                    {!!Form::label('mobile_number','Mobile Number')!!}                       
                                    {!!Form::text('mobile_number','',['class'=>'form-control','placeholder'=>'Enter Client Mobile Number'])!!}       
                                    @if ($errors->has('mobile_number')) <p class="help-block" style="color:red;">{{ $errors->first('mobile_number') }}</p> @endif                                                                                     
                                </div>      
                            </div>
                        </div>        
                        <div class="form-group col-lg-12">
                            <div class="row">
                                <div class="col-lg-4">
                                {!!Form::label('contact_type_id','Contact Type*')!!}                       
                                {!!Form::select('contact_type_id',$types,null,['class'=>'form-control','id'=>'contact_type_id','required'=>'required'])!!}                                    
                                @if ($errors->has('contact_type_id')) <p class="help-block" style="color:red;">{{ $errors->first('contact_type') }}</p> @endif
                                </div>      
                            </div>
                        </div>                                                                                                                         
                        <div class="form-group col-lg-12">
                        </div>
                        <div class="form-group col-lg-12">
                        </div>                        
                        <div class="col-lg-12">                            
                            <div class="form-group">                                
                                {!! Form::label('attached','Attached File')!!}
                                {!! Form::file('attached[]', array('id' => 'attach_file', 'class' => 'photo_files')) !!}                                                   
                            </div>                            
                        </div>    
                        <div class="col-lg-12">
                            <div class="form-group">
                                {!! Form::label('remarks','Remarks (OPTIONAL)') !!}
                                {!! Form::textarea('remarks','',['class'=>'form-control','placeholder'=>'Your remarks here...']) !!}
                            </div>
                        </div>                                                                          
                        <div class="form-group pull-right">
                            {!! Form::hidden('created_by',$user_id) !!}
                            {!! Form::hidden('contact_id',0) !!}                                                        
                            <a href="javascript:history.back()"><button type="button" class="btn btn-default"><i class="fa fa-arrow-left"></i> Cancel </button></a>            
                            {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'button', 'class' => 'btn btn-primary pull-right sbalert')) !!}
                            {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'submit', 'class' => 'btn btn-primary hidden', 'id'=>'urbtn')) !!}                            
                        </div>      
                     </div>									                                                                   
                    {!! Form::close() !!}       
                    </div>
                </div>                   
            </div>             	                        
        </div>              
    </div> 
</div>    
@stop
@section('page-script')   
    $('#contact_type_id').multiselect({
    maxHeight: 200,
    includeSelectAllOption: true,
    enableCaseInsensitiveFiltering: true,
    enableFiltering: true,
    buttonWidth: '100%',
    buttonClass: 'form-control',
    });   

    $('#with_company_id').on("change", function(){

        var x = $('#with_company_id').val();    
        
        if(x > 0 ){
            
            $('#with_comp').removeClass('hidden');

            $('#client_name').val('');
            $('#industry').val('');
            $('#region').val('');
            $('#country').val('');
            $('#zip_code').val('');
            $('#website').val('');
            $('#facebook').val('');
            $('#twitter').val('');
            $('#instagram').val('');
            $('#comp_address').val('');
        }
        else{
            
            $('#with_comp').addClass('hidden');
            $('#client_name').val('');
            $('#industry').val('');
            $('#region').val('');
            $('#country').val('');
            $('#zip_code').val('');
            $('#website').val('');
            $('#facebook').val('');
            $('#twitter').val('');
            $('#instagram').val('');
            $('#comp_address').val('');
        }
    });  

    $('.sbalert').click(function () {

        if($('#contact_person').val() != "" && $('#email').val() != ""){
            swal({
                title: "Are you sure?",
                text: "You will now submit your new contact",
                type: "info",
                showCancelButton: true,
                confirmButtonColor: "#1abc9c",
                confirmButtonText: "Yes, submit it!",
                closeOnConfirm: false
            }, function () {
                swal("Submitted!", "I will now submit your new contact.", "success");
                $('#urbtn').click();
            });
        }
        else{

            $('#urbtn').click();
        }                     
    });

    $('#attach_file').filer({

        showThumbs:true,
        addMore:true
    });
@stop

