@extends('layouts.master')
@section('main-body')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2><i class="fa fa-building-o"></i> <i class="fa fa-eye"></i> CONTACT APPROVAL DETAILS</h2>        
    </div>
</div>        
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12 animated flash">            
            <?php if (session('is_update_com')): ?>
                <div class="alert alert-success alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                    <center><h4>Contact Information was successfully updated!<i class="fa fa-check"></i></h4></center>                
                </div>
            <?php endif;?>            
        </div>        
        <div class="col-lg-8">
            <div class="ibox float-e-margins">
                <div class="ibox-title" style="background-color:#009688">
                    <h5 style="color:white">Status</h5>
                    <div class ="pul-right">
                        @if($contact->status === 0)
                            <span class="label label-info">For Approval</span>
                        @elseif($contact->status === 1)
                            <span class="label label-primary">Approved</span>
                        @elseif($contact->status === 2)
                            <span class="label label-danger">Denied</span>
                        @elseif($contact->status === 3)
                            <span class="label label-warning"> For Re-Approve</span>
                        @endif
                    </div>
                    <div class="pull-right">
                        <button class="btn btn-white btn-xs" data-toggle="modal" data-target="#ApproverEditModal"><i class="fa fa-pencil"></i> EDIT INFO</button>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="form-group" style="padding-top:20px;">
                        <h2><center><strong>{!! strtoupper($contact->company) !!}</strong></center></h2>
                        <center><span><small><i>Company Name</i></small></span></center>   
                    </div><div class="hr-line-dashed"></div>                               
                                
                    <table class="table table table-hover">
                        <tr>
                            <td class="action" style="width:30%"><i>INDUSTRY:</i></td><td>{!! strtoupper($contact->industry) !!}</td>
                        </tr>    
                    </table>
                    <table class="table table table-hover">
                        <tr>
                            <td class="action" style="width:30%"><i>COMPANY ADDRESS:</i></td><td>{!! strtoupper($contact->comp_address) !!}</td>
                        </tr>    
                    </table>                    
                    <table class="table table table-hover">
                        <tr>
                            <td class="action" style="width:30%"><i>CONTACT TYPE:</i></td>
                            <td>                                    
                                {!! strtoupper($contact->contacttype->type) !!}                                    
                            </td>
                        </tr>    
                    </table>                                                         
                    <table class="table table table-hover">
                        <tr>
                            <td class="action" style="width:30%"><i>CONTACT PERSON:</i></td><td>{!! strtoupper($contact->person->contact_person) !!}</td>
                        </tr>    
                    </table>      
                    <table class="table table table-hover">
                        <tr>
                            <td class="action" style="width:30%"><i>FUNCTION OF CONTACT PERSON:</i></td><td>{!! strtoupper($contact->person->function) !!}</td>
                        </tr>    
                    </table>                                        
                    <table class="table table table-hover">
                        <tr>
                            <td class="action" style="width:30%"><i>EMAIL ADDRESS:</i></td>
                            <td>
                                @if($contact->person->email)
                                    {!! $contact->person->email!!}                                
                                @else
                                    N/A
                                @endif
                            </td>                            
                        </tr>
                    </table>
                        <table class="table table table-hover">         
                            <tr>
                                <td class="action" style="width:30%"><i>TELEPHONE NUMBER:</i></td>
                                <td>
                                    @if($contact->person->tel_no)
                                        {!! $contact->person->tel_no !!}
                                    @else
                                        N/A
                                    @endif
                                </td>
                            </tr>
                        </table>
                        <table class="table table table-hover">
                            <tr>
                                <td class="action" style="width:30%"><i>MOBILE NUMBER:</i></td>
                                <td>
                                    @if($contact->person->mobile_number)
                                        {!! $contact->person->mobile_number !!}
                                    @else
                                        N/A
                                    @endif
                                </td>
                            </tr>
                        </table> 
                         <table class="table table table-hover">
                            <tr>
                                <td class="action" style="width:30%"><i>FAX NUMBER:</i></td>
                                <td>
                                    @if($contact->person->fax_number)
                                        {!! strtoupper($contact->person->fax_number) !!}
                                    @else
                                        N/A
                                    @endif
                                </td>
                            </tr>   
                        </table>           
                                                                                                
                        <table class="table table table-hover">                 
                            <tr>
                                <td class="action" style="width:30%"><i>PREPARED BY:</i></td>
                                 <td>
                                    
                                    {!! strtoupper($contact->user->first_name.' '.$contact->user->last_name) !!}                                
                                </td>
                            </tr>
                        </table>
                        <table class="table table table-hover">
                            <tr>
                                <td class="action" style="width:30%"><i>DATE CREATED:</i></td>
                                 <td>
                                    
                                    {!! strtoupper($contact->updated_at) !!}                                
                                </td>
                            </tr>                        
                        </table><div class="hr-line-dashed"></div>         
                        <table>                        
                        {!!Form::open(array('route'=>'contact_approvals.update_status','method'=>'POST'))!!}
                        @if($contact->status < 1 || $contact->status === 3)
                            <div class="form-group col-lg-12">
                                <div class="row">
                                    <div class="form-group col-lg-12">
                                        {!!Form::label('remarks','Remarks')!!}                                                             
                                        {!!Form::textarea('remarks','',['class'=>'form-control col-lg-12','placeholder'=>'Enter your Remarks Here']) !!}
                                        {!!Form::hidden('contact_id',$contact->id) !!}
                                    </div>
                                </div>
                            </div>
                        @endif
                        <tr>
                            <td>
                                <a href="javascript:history.back()"><button type="button" class="btn btn-default"><i class="fa fa-arrow-left"></i> Back </button></a>
                            </td><td>&nbsp;</td>
                            @if($contact->status < 1 || $contact->status === 3)                                
                                    <td>                            
                                        <button type="button" class="btn btn-success btn-sm" id="sbalert1"><i class="fa fa-check"></i> Approved</button>                                                                   
                                    </td><td>&nbsp;</td>
                                    <td>                            
                                        <button type="button" class="btn btn-danger btn-sm" id="sbalert2"><i class="fa fa-close"></i> Denied</button>                                                                                                           
                                    </td>                                    
                                    {!! Form::button('<i class="fa fa-save"></i> Approved', array('value'=>'1','type' => 'submit', 'class' => 'btn btn-primary hidden', 'id'=>'appbtn','name'=>'status')) !!}                            
                                    {!! Form::button('<i class="fa fa-save"></i> Denied', array('value'=>'2','type' => 'submit', 'class' => 'btn btn-primary hidden', 'id'=>'denbtn','name'=>'status')) !!}                            
                                {!! Form::close() !!}
                            @endif
                        </tr>
                    </table>           
                </div>
            </div>
        </div>  
        <div class="col-lg-4" id="logflow">        
            <div class="panel panel-default">
                <div class="panel-heading">
                     <strong>Contact activity</strong>  
                </div>
                <div class="panel-body">
                    <div id="vertical-timeline" class="vertical-container dark-timeline">
                        @foreach($logs as $log)
                        <div class="vertical-timeline-block">
                            <div class="vertical-timeline-icon gray-bg">
                                <i class="fa fa-coffee"></i>
                            </div>
                            <div class="vertical-timeline-content">
                                <p>{!! $log->log !!}</p>
                                @if(!empty($log->remarks))
                                <br />                        
                                <br />
                                <p>Remarks: <br/>
                                {!! $log->remarks !!}</p>                        
                                @endif
                                <span class="vertical-date small text-muted"> {!! $log->created_at->diffForHumans() !!} </span>
                            </div>
                        </div>
                        @endforeach
                    </div>  
                </div>                
            </div>                        
        </div>     
    </div>
</div>
<div class="modal inmodal" id="ApproverEditModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-comment modal-icon"></i>
                <h4 class="modal-title">CONTACT DETAILS</h4>                    
            </div>              
            {!!Form::open(array('route'=>array('contact_approvals.head_overide',$contact->id),'method'=>'PUT','files'=>true))!!}                
                <div class="modal-body">                                        
                    <div class="row" style="padding-top:20px;">
                        <div class="form-group col-lg-12">
                            <div class="row">
                                <div class="col-lg-4">
                                    {!!Form::label('company','Client / Company Name*')!!}                       
                                    {!!Form::text('company',$contact->company,['class'=>'form-control','placeholder'=>'Enter Client / Company Name','id'=>'company'])!!}                                               
                                    @if ($errors->has('company')) <p class="help-block" style="color:red;">{{ $errors->first('company') }}</p> @endif                                             
                                </div>      
                                <div class="col-lg-4">
                                    {!! Form::label('industry','Industry*') !!}                       
                                    {!! Form::text('industry',$contact->industry,['class'=>'form-control','id'=>'industry']) !!}                                        
                                </div>      
                            </div>
                        </div>    
                        <div class="form-group col-lg-12">
                            <div class="row">
                                <div class="col-lg-8">
                                    {!!Form::label('comp_address','Company Address')!!}                 
                                    {!!Form::textarea('comp_address',$contact->comp_address,['class'=>'form-control','placeholder'=>'Enter Complete Client Address','id'=>'comp_address']) !!}
                                    @if ($errors->has('comp_address')) <p class="help-block" style="color:red;">{{ $errors->first('address') }}</p> @endif                                             
                                </div>      
                            </div>          
                        </div>                   
                        <div class="form-group col-lg-12">
                            <div class="row">
                                <div class="col-lg-4">
                                    {!!Form::label('contact_person','Contact Person*')!!}                     
                                    {!!Form::text('contact_person',$contact->person->contact_person,['class'=>'form-control','placeholder'=>'Enter Contact Person','required'=>'required','id'=>'contact_person'])!!}                                             
                                    @if ($errors->has('contact_person')) <p class="help-block" style="color:red;">{{ $errors->first('contact_person') }}</p> @endif                                                                                     
                                </div>
                                 <div class="col-lg-4">
                                    {!!Form::label('function','Function*')!!}                     
                                    {!!Form::text('function',$contact->person->function,['class'=>'form-control','placeholder'=>'Enter Function Of Contact Person'])!!}
                                    @if ($errors->has('function')) <p class="help-block" style="color:red;">{{ $errors->first('function') }}</p> @endif
                                </div>
                            </div>
                        </div>                 
                        <div class="form-group col-lg-12">
                            <div class="row">
                                <div class="col-lg-4">
                                    {!!Form::label('email','Email')!!}                       
                                    {!!Form::text('email',$contact->person->email,['class'=>'form-control','placeholder'=>'Enter Client Email','required'=>'required','id'=>'email'])!!}          
                                    @if ($errors->has('email')) <p class="help-block" style="color:red;">{{ $errors->first('email') }}</p> @endif                                                                                  
                                </div>                                        
                            </div>
                        </div>                           
                        <div class="form-group col-lg-12">
                            <div class="row">
                                <div class="col-lg-4">
                                    {!!Form::label('tel_no','Telephone Number')!!}                       
                                    {!!Form::text('tel_no',$contact->person->tel_no,['class'=>'form-control','placeholder'=>'Enter Client Telephone Number'])!!}                                               
                                    @if ($errors->has('tel_no')) <p class="help-block" style="color:red;">{{ $errors->first('tel_no') }}</p> @endif                                             
                                </div>     
                                <div class="col-lg-4">
                                    {!!Form::label('fax_number','Fax Number')!!}                       
                                    {!!Form::text('fax_number',$contact->person->fax_number,['class'=>'form-control','placeholder'=>'Enter Client Fax Number'])!!}               
                                    @if ($errors->has('fax_number')) <p class="help-block" style="color:red;">{{ $errors->first('fax_number') }}</p> @endif                                                                             
                                </div>        
                                <div class="col-lg-4">
                                    {!!Form::label('mobile_number','Mobile Number')!!}                       
                                    {!!Form::text('mobile_number',$contact->person->mobile_number,['class'=>'form-control','placeholder'=>'Enter Client Mobile Number'])!!}       
                                    @if ($errors->has('mobile_number')) <p class="help-block" style="color:red;">{{ $errors->first('mobile_number') }}</p> @endif                                                                                     
                                </div>      
                            </div>
                        </div>        
                        <div class="form-group col-lg-12">
                            <div class="row">
                                <div class="col-lg-4">
                                {!!Form::label('contact_type_id','Contact Type*')!!}                       
                                {!!Form::select('contact_type_id',$types,$contact->contact_type_id,['class'=>'form-control','id'=>'contact_type_id','required'=>'required'])!!}                                    
                                @if ($errors->has('contact_type_id')) <p class="help-block" style="color:red;">{{ $errors->first('contact_type') }}</p> @endif
                                </div>      
                            </div>
                        </div>                          
                    </div>                            
                </div>
                <div class="modal-footer">                        
                    {!! Form::button('Close', array('type' => 'submit', 'class' => 'btn btn-white','data-dismiss'=>'modal')) !!}
                    {!! Form::submit('Update', array('type' => 'submit', 'class' => 'btn btn-primary')) !!}
                </div>                  
            {!! Form::close() !!}            
        </div>
    </div>
</div>
@stop
@section('page-script')
$('#contact_type_id').multiselect({
    maxHeight: 200,
    includeSelectAllOption: true,
    enableCaseInsensitiveFiltering: true,
    enableFiltering: true,
    buttonWidth: '100%',
    buttonClass: 'form-control',
    });   

    $('#sbalert1').click(function () {            
        swal({
            title: "Are you sure?",
            text: "You will about to approve the project",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#1abc9c",
            confirmButtonText: "Yes, approve it!",
            closeOnConfirm: false
        }, function () {
            swal("Got it!", "Please wait...", "success");
            $('#appbtn').click();
        });      
    });

    $('#sbalert2').click(function () {            
        swal({
            title: "Are you sure?",
            text: "You will about to denied the project",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#1abc9c",
            confirmButtonText: "Yes, denied it!",
            closeOnConfirm: false
        }, function () {
            swal("Got it!", "Please wait...", "success");
            $('#denbtn').click();
        });      
    });
@stop