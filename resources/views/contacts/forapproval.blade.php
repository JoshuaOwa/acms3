@extends('layouts.master')
@section('main-body')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2><i class="fa fa-users"></i> CONTACTS FOR APPROVAL</h2>        
    </div>
</div>        
<div class="wrapper wrapper-content animated fadeInRight">	
	<div class="row">
		<div class="col-lg-12 animated flash">
		    <?php if (session('is_success')): ?>
		        <div class="alert alert-success alert-dismissible fade in" role="alert">
		            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
		        </button>
		            <center><h4>Contact was successfully added!<i class="fa fa-check"></i></h4></center>                
		        </div>
		    <?php endif;?>
		</div>
		<div class="col-lg-12 animated flash">
		    <?php if (session('is_update')): ?>
		        <div class="alert alert-success alert-dismissible fade in" role="alert">
		            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
		        </button>
		            <center><h4>Contact was successfully updated!<i class="fa fa-check"></i></h4></center>                
		        </div>
		    <?php endif;?>
		</div>
		<div class="col-lg-12 animated flash">
		    <?php if (session('is_reset')): ?>
		        <div class="alert alert-success alert-dismissible fade in" role="alert">
		            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
		        </button>
		            <center><h4>Contact password was successfully updated!<i class="fa fa-check"></i></h4></center>                
		        </div>
		    <?php endif;?>
		</div>
		<div class="col-lg-12 animated flash">
		    <?php if (session('is_workgroup')): ?>
		        <div class="alert alert-success alert-dismissible fade in" role="alert">
		            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
		        </button>
		            <center><h4>Contact workgroup was successfully updated!<i class="fa fa-check"></i></h4></center>                
		        </div>
		    <?php endif;?>
		</div>
	</div>	
	<div class="row">
        <div class="col-lg-12">
            <div class="form-group"><label class="col-sm-12 control-label">Filter by:</label>
                {!!Form::open(array('route'=>'contact_approvals.filter','method'=>'POST'))!!}
                <div class="col-lg-6">
                    <button type="submit" class="btn btn-info btn-sm" value="0" name="filter"><i class="fa fa-clock-o"></i> For Approval</button>                    
                    <button type="submit" class="btn btn-success btn-sm" value="1" name="filter"><i class="fa fa-check"></i> Approved</button>
                    <button type="submit" class="btn btn-danger btn-sm" value="2" name="filter"><i class="fa fa-close"></i> Denied</button>                            
                </div>
                {!! Form::close() !!}                                
            </div>
        </div>
    </div>
    &nbsp;
    <div class="row">    	
        <div class="col-lg-12">            
	         <div class="panel blank-panel">
                <div class="panel-heading">                            
                    <div class="panel-options">    
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#tab-main"> <i class="fa fa-bullseye"></i> MAIN</a></li>                                                        
                        </ul>
                    </div>
                </div>
                &nbsp;
                &nbsp;
                <div class="tab-content">
                    <div id="tab-main" class="tab-pane active">   
                        <div></div>                                
                        </br>
                        &nbsp;
                         <center><h2>CONTACTS</h2></center>
                        <div class="col-lg-12">    
                            <div class="table-responsive">                                        
                                <table class="table table-striped table-hover dataTables-example" >
						            <thead>
							            <tr>	
							            	<th>COMPANY</th>		
							            	<th style="text-align: center;">ADDRESS</th>							           		
							                <th style="text-align: center;">CONTACT PERSON</th>
                                            <th style="text-align: center;">CREATED BY</th>
							                <th style="text-align: center;">ACTION</th>			               
							            </tr>
						            </thead>
						            <tbody>
							        	@forelse($contacts as $contact)
							           		<tr>
							           			<td>{!! $contact->company !!}</td>
							           			<td style="text-align: center;">{!! $contact->comp_address !!}</td>		
							           			<td style="text-align: center;">{!! $contact->person->contact_person !!}</td>		   			
                                                <td style="text-align: center;">{!! strtoupper($contact->user->first_name.' '.$contact->user->last_name) !!}</td>	                                        
		                                        <td class="action" style="text-align: center;">  		                                            
	                                                {!! Html::decode(link_to_Route('contact_approvals.show','<i class="fa fa-eye"></i> Details', $contact->encryptname, array('class' => 'btn btn-primary btn-xs')))!!}
		                                        </td>
							           		</tr>
							           	@empty
							           	@endforelse
						            </tbody>			            
					            </table>
					        </div>
					    </div>
		            </div>		                          
	            </div>
	        </div>
	    </div>	    
    </div>
</div>	
@endsection
