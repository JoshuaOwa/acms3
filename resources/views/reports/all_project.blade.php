@extends('layouts.master')
@section('main-body')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2><i class="fa fa-university"></i> PROJECT LISTS</h2>       
    </div>
</div>        
<div class="wrapper wrapper-content animated fadeInRight">		
	<div class="row-1">
		{!! Form::open(array('route'=>'reports.projects_exports','method'=>'POST')) !!}
		<div class="col-lg-12">
			<div class="col-lg-12">
			{!! Form::button('<i class="fa fa-download"></i> Export to Excel', array('type' => 'submit', 'class' => 'btn btn-warning', 'id'=>'urbtn','value'=>'2','name'=>'type' )) !!}
			</div>
		</div>
		{!! Form::close() !!}
	</div>
	<div class="row">
        <div class="col-lg-12">
            <div class="panel-body">                
            	<div class="col-lg-12">
			        <div class="ibox float-e-margins">
			            <div class="ibox-title" style="background-color:#009688">
			                <h5 style="color:white"><i class="fa fa-university"></i> All Projects</h5>
			            </div>
			            <div class="ibox-content">
			            	<div class="table-responsive">
		                       	<table class="table table-striped table-hover dataTables-example">
						            <thead>
							            <tr>
							                <th>Project #</th>			
							                <th>Client</th>	                				                
							                <th>Project Name</th>	
							                <th>Start Date</th>						                
							                <th>End Date</th>						                							                
							                <th>Created By</th>
							                <th>Approved By</th>
							                <th>Tag</th>			
							                <th>Pending JO</th>
							            </tr>
						            </thead>
						            <tbody>
							           	@forelse($projects as $project)
							           		<tr>
							           			<td>{!! Html::decode(link_to_route('projects.overview', $project->id,$project->encryptname, array())) !!}</td>
							           			<td>{!! strtoupper($project->contact->company) !!}</td>
							           			<td>{!! $project->project_name !!}</td>
							           			<td>{!! $project->start_date !!}</td>
							           			<td>{!! $project->end_date !!}</td>							           			
							           			<td>{!! strtoupper($project->user->first_name.' '.$project->user->last_name) !!}</td>
							           			<td>{!! strtoupper($project->approver->first_name.' '.$project->approver->last_name) !!}</td>
							           			<td>{!! $project->tag !!}</td>
							           			<td style="text-align: center;">{!! $project->jo_pending !!}</td>
							           		</tr>
							           	@empty
							           	@endforelse
						            </tbody>			            
				            	</table>
				            </div>
			            </div>
			        </div>
			    </div>                
            </div>
        </div>
    </div>   
</div>	
@stop
@section('page-script')

    $('#uIDs,#dIDs').multiselect({
        maxHeight: 200,    
        includeSelectAllOption: true,
        enableCaseInsensitiveFiltering: true,
        enableFiltering: true,
        buttonWidth: '100%',
        buttonClass: 'form-control',
    });  

    $('#date_range .input-daterange').datepicker({
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true
    });

    $('#dIDs').on("change", function(){

        var dept_id = $('#dIDs').val();                           

        $.ajax({
            type:"POST",
            dataType: 'json',
            data: {dept_id: dept_id},
            url: "../../job_reports/filterUserDept",
            success: function(data){            

                $('select#uIDs').empty();
                
                $.each(data, function(i, text) {
                    $('<option />',{value: i, text: text}).appendTo($('select#uIDs'));
                });          
                                        
                $('select#uIDs').multiselect('rebuild');                
            }
        });      
        
    });    
    
@endsection