@extends('layouts.master')
@section('main-body')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2><i class="fa fa-university"></i> JOB ORDER LIST</h2>       
    </div>
</div>        
<div class="wrapper wrapper-content animated fadeInRight">	
	<div class="row">
		{!! Form::open(array('route'=>'job_orders.filter_report','methd'=>'POST','files'=>true)) !!}                    
		<div class="panel-body">                
        	<div class="col-lg-12">
		        <div class="ibox float-e-margins">		            
		            <div class="ibox-content">		
			            <div class="row">
			            	<div class="col-lg-12">
				                <div class="col-lg-4">
				                    <div class="form-group">
				                        {!! Form::label('department', 'Department') !!}
				                        {!! Form::select('department_id[]',$departments,'',['class'=>'form-control','id'=>'deptsID','multiple'=>'multiple','required'=>'required']) !!}
				                    </div>    
				                </div>
				                <div class="col-lg-4">
				                    <div class="form-group">
				                        {!! Form::label('users', 'Employee') !!}
				                        {!! Form::select('userfilt[]',$user_filt,'',['class'=>'form-control','multiple'=>'multiple','id'=>'usersID','required'=>'required']) !!}
				                    </div>        
				                </div>                
				                <div class="col-lg-4">
	                                    {!!Form::label('date_range','Date Range')!!} 
	                                    <div class="form-group" id="date_range">                                                
	                                        <div class="input-daterange input-group" id="datepicker">
	                                            <input type="text" class="input-md form-control" name="start_date" value="{{$date_now}}" required="" />
	                                            <span class="input-group-addon">to</span>
	                                            <input type="text" class="input-md form-control" name="end_date" value="{{$date_now}}" required="" />
	                                        </div>
	                                    </div>                                
	                                </div>         
				                <div class="col-lg-12">
					                <div class="form-group">                                                
					                    {!! Form::button('<i class="fa fa-save"></i> Process', array('type' => 'submit', 'class' => 'btn btn-primary', 'id'=>'urbtn', 'value'=>'1','name'=>'type' )) !!}				                    
					                    {!! Form::button('<i class="fa fa-download"></i> Download Excel', array('type' => 'submit', 'class' => 'btn btn-warning', 'id'=>'urbtn','value'=>'2','name'=>'type' )) !!}				                    
					                </div>                                          
					            </div>                     
				            </div>       			
			            </div>						                          
			        </div>
			    </div>
			</div>
		</div>
		{!! Form::close() !!}  
	</div>
	<div class="row">
        <div class="col-lg-12">
            <div class="panel-body">                
            	<div class="col-lg-12">
			        <div class="ibox float-e-margins">
			            <div class="ibox-title" style="background-color:#009688">
			                <h5 style="color:white"><i class="fa fa-university"></i> Summary Of Employees Job Orders</h5>
			            </div>
			            <div class="ibox-content">

			            	<div class="table-responsive">
		                       	<table class="table table-striped table-hover dataTables-example">
						            <thead>
							            <tr>							                
							                <th>Name</th>
							                <th>Department</th>                				                
							                <th style="text-align: center">Total Job Order</th>							                							                
							                <th style="text-align: center">Ongoing</th>					                
							                <th style="text-align: center">Returned</th>					                
							                <th style="text-align: center">Closed</th>					                
							                <th style="text-align: center">Overdue</th>					                
							            </tr>
						            </thead>
						            <tbody>
							           	@forelse($users as $user)
							           		<tr>
							           			<td>{!! Html::decode(link_to_route('job_orders.emp_detailed', $user->full_name,$user->id, array())) !!}</td>
							           			<td>{!! $user->department !!}</td>
							           			<td style="text-align: center">{!! $user->totalJO !!}</td>							           			
							           			<td style="text-align: center">{!! $user->ongoings !!}</td>
							           			<td style="text-align: center">{!! $user->returned !!}</td>
							           			<td style="text-align: center">{!! $user->closed !!}</td>
							           			<td style="text-align: center">{!! $user->overdue !!}</td>
							           		</tr>
						           		@empty
						           		@endforelse
						            </tbody>			            
				            	</table>
				            </div>
			            </div>
			        </div>
			    </div>                
            </div>
        </div>
    </div>   
</div>	
@stop
@section('page-script')

    $('#usersID,#deptsID').multiselect({
        maxHeight: 200,    
        includeSelectAllOption: true,
        enableCaseInsensitiveFiltering: true,
        enableFiltering: true,
        buttonWidth: '100%',
        buttonClass: 'form-control',
    });  

     $('#date_range .input-daterange').datepicker({
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true
    });

    $('#deptsID').on("change", function(){

        var dept_id = $('#deptsID').val();                           

        $.ajax({
            type:"POST",
            dataType: 'json',
            data: {dept_id: dept_id},
            url: "../../job_reports/filterUserDept",
            success: function(data){            

                $('select#usersID').empty();
                
                $.each(data, function(i, text) {
                    $('<option />',{value: i, text: text}).appendTo($('select#usersID'));
                });          
                                        
                $('select#usersID').multiselect('rebuild');                
            }
        });      
        
    });    
@stop