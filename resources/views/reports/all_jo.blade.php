@extends('layouts.master')
@section('main-body')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2><i class="fa fa-university"></i> JOB ORDER LIST</h2>       
    </div>
</div>        
<div class="wrapper wrapper-content animated fadeInRight">		
	<div class="row">
		{!! Form::open(array('route'=>'job_orders.extraction','methd'=>'POST','files'=>true)) !!}                    
		<div class="panel-body">                
        	<div class="col-lg-12">
		        <div class="ibox float-e-margins">		            
		            <div class="ibox-content">		
			            <div class="row">
			            	<div class="col-lg-12">
				                <div class="col-lg-4">
				                    <div class="form-group">
				                        {!! Form::label('filter_by', 'Filter By') !!}
				                       	{!! Form::select('filter_by',$filter_by,$filter_by_selected,['class'=>'form-control','placeholder'=>'Select Type','id'=>'filterByID']) !!}
				                    </div>    
				                </div>
				                <div class="col-lg-4">
				                    <div class="form-group">
				                        {!! Form::label('filtered', 'Option') !!}				                       
				                        {!! Form::select('filtered[]',[],$filtered,['class'=>'form-control','multiple'=>'multiple','id'=>'filteredID','required'=>'required']) !!}
				                    </div>        
				                </div>                				                
				                <div class="col-lg-12">
					                <div class="form-group">                                                
					                    {!! Form::button('<i class="fa fa-save"></i> Process', array('type' => 'submit', 'class' => 'btn btn-primary', 'id'=>'urbtn', 'value'=>'1','name'=>'type' )) !!}				                    
					                    {!! Form::button('<i class="fa fa-download"></i> Download Excel', array('type' => 'submit', 'class' => 'btn btn-warning', 'id'=>'urbtn','value'=>'2','name'=>'type' )) !!}				                    
					                </div>                                          
					            </div>                     
				            </div>       			
			            </div>						                          
			        </div>
			    </div>
			</div>
		</div>
		{!! Form::close() !!}  
	</div>
	<div class="row">
        <div class="col-lg-12">
            <div class="panel-body">                
            	<div class="col-lg-12">
			        <div class="ibox float-e-margins">
			            <div class="ibox-title" style="background-color:#009688">
			                <h5 style="color:white"><i class="fa fa-university"></i> All Employee Job Orders</h5>
			            </div>
			            <div class="ibox-content">
			            	<div class="table-responsive">
		                       	<table class="table table-striped table-hover dataTables-example">
						            <thead>
							            <tr>
							                <th>#</th>			
							                <th>Subject</th>	                				                
							                <th>Assigned By</th>
							                <th>Date Created</th>				                
							                <th>Date Return</th>
							                <th>Status</th>
							                <th>Rating</th>				                
							            </tr>
						            </thead>
						            <tbody>
							            @forelse($jobs as $job)
							           		@if($date_now > $job->end_date AND $job->status == 1)
			                                    <tr style="background-color: #fab1a0">                  
			                                @elseif($job->status > 1 AND $job->return_date > $job->end_date)
			                                    <tr style="background-color: #fab1a0">                  
			                                @else
			                                    <tr>
			                                @endif             				           		
							           			<td>{!! Html::decode(link_to_route('job_orders.overview', $job->id,$job->encryptname, array())) !!}</td>	
							           			<td>
							           				@if(empty($job->subject))
							           					N/A
							           				@else
							           					{!! $job->subject !!}
							           				@endif
							           			</td>
							           			<td>
							           				{!! strtoupper($job->user->first_name.' '.$job->user->last_name) !!}
							           			</td>
							           			<td>{!! $job->created_at !!}</td>
							           			<td>
							           				@if(!empty($job->return_date))
							           					{!! $job->return_date !!}
							           				@else
							           					N/A
							           				@endif
							           			</td>
							           			<td>
							           				@if($job->status === 1)
								                        <span class="label label-info"><i class="fa fa-clock-o"></i> Ongoing</span>
								                    @elseif($job->status === 2)
								                        <span class="label label-success"><i class="fa fa-check"></i> Returned</span>
								                    @elseif($job->status === 3)
								                        <span class="label label-primary"> <i class="fa fa-user"></i>Closed</span>
								                    @endif
							           			</td>				           			
							           			<td>
							           				@if($job->status == 3)
			                                            @for($i=0; $i < $job->rating; $i++)
			                                                <i class="fa fa-star"></i>
			                                            @endfor
			                                            @for($j=$i; $j < 10; $j++)
			                                            	<i class="fa fa-star-o"></i>
			                                            @endfor
			                                        @else
			                                        N/A
			                                        @endif
							           			</td>							           			
							           		</tr>
							           	@empty
							           	@endforelse
						            </tbody>			            
				            	</table>
				            </div>
			            </div>
			        </div>
			    </div>                
            </div>
        </div>
    </div>   
</div>	
@stop
@section('page-script')

    $('#uIDs,#dIDs,#filteredID').multiselect({
        maxHeight: 200,    
        includeSelectAllOption: true,
        enableCaseInsensitiveFiltering: true,
        enableFiltering: true,
        buttonWidth: '100%',
        buttonClass: 'form-control',
    });  

    $('#date_range .input-daterange').datepicker({
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true
    });

    $('#dIDs').on("change", function(){

        var dept_id = $('#dIDs').val();                           

        $.ajax({
            type:"POST",
            dataType: 'json',
            data: {dept_id: dept_id},
            url: "../../job_reports/filterUserDept",
            success: function(data){            

                $('select#uIDs').empty();
                
                $.each(data, function(i, text) {
                    $('<option />',{value: i, text: text}).appendTo($('select#uIDs'));
                });          
                                        
                $('select#uIDs').multiselect('rebuild');                
            }
        });      
        
    });    

    $('#filterByID').on("change", function(){

        var filter_by = $('#filterByID').val();                           

        $.ajax({
            type:"POST",
            dataType: 'json',
            data: {filter_by: filter_by},
            url: "../../job_reports/filterOption",
            success: function(data){            

                $('select#filteredID').empty();
                
                $.each(data, function(i, text) {
                    $('<option />',{value: i, text: text}).appendTo($('select#filteredID'));
                });          
                                        
                $('select#filteredID').multiselect('rebuild');                
            }
        });      
        
    });    
    
    
@endsection