@extends('layouts.master')
@section('main-body')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2><i class="fa fa-university"></i>EMPLOYEE ALL JOB ORDER LIST</h2>       
    </div>
</div>        
<div class="wrapper wrapper-content animated fadeInRight">	
	 <div class="row">
        <div class="col-lg-12">
            <div class="tabs-container">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#tab-ongoing">ONGOING JOB ORDER</a></li>                  
                    <li class=""><a data-toggle="tab" href="#tab-returned">RETURNED JOB ORDER</a></li>                      
                    <li class=""><a data-toggle="tab" href="#tab-closed">CLOSED JOB ORDER</a></li>                      
                </ul>
                <div class="tab-content">
                    <div id="tab-ongoing" class="tab-pane active">
                        <div class="panel-body">
                            <div class="tabs-container">
                            	<div class="col-lg-12">
							        <div class="ibox float-e-margins">
							            <div class="ibox-title" style="background-color:#009688">
							                <h5 style="color:white"><i class="fa fa-university"></i>Ongoing Job Orders</h5>	                
							            </div>
							            <div class="ibox-content">
							            	<div class="table-responsive">
						                       	<table class="table table-striped table-hover dataTables-example">
										            <thead>
											            <tr>
											                <th>#</th>				                				
											                <th>Assigned By</th>											                
											                <th>Date Created</th>				                		
											                <th>Start Date</th>
											                <th>End Date</th>
											            </tr>
										            </thead>
										            <tbody>
											            @forelse($jobs->jobOngoings as $ongoing)											           		
							                                @if($date_now > $ongoing->end_date AND $ongoing->status == 1)
							                                    <tr style="background-color: #fab1a0">                  
							                                @elseif($ongoing->status > 1 AND $ongoing->return_date > $ongoing->end_date)
							                                    <tr style="background-color: #fab1a0">                  
							                                @else
							                                    <tr>
							                                @endif    						                               	
											           			<td>{!! Html::decode(link_to_route('job_orders.overview', $ongoing->id,$ongoing->encryptname, array())) !!}</td>	
											           			<td>
											           				{!! strtoupper($ongoing->user->first_name.' '.$ongoing->user->last_name) !!}
											           			</td>
											           			<td>{!! $ongoing->created_at !!}</td>		
											           			<td>{!! $ongoing->start_date !!}</td>									           			
											           			<td>{!! $ongoing->end_date !!}</td>									           			
											           		</tr>
											           	@empty
											           	@endforelse
										            </tbody>			            
								            	</table>
								            </div>
							            </div>
							        </div>
							    </div>
                            </div>
                        </div>
                    </div>
                    <div id="tab-returned" class="tab-pane">
                        <div class="panel-body">
                            <div class="tabs-container">
                            	<div class="col-lg-12">
							        <div class="ibox float-e-margins">
							            <div class="ibox-title" style="background-color:#009688">
							                <h5 style="color:white"><i class="fa fa-university"></i> Returned Job Orders</h5>	                
							            </div>
							            <div class="ibox-content">
							            	<div class="table-responsive">
						                       	<table class="table table-striped table-hover dataTables-example">
										         	<thead>
											            <tr>
											                <th>#</th>				                				
											                <th>Assigned By</th>											                
											                <th>Date Created</th>		
											                <th>Start Date</th>		                				
											                <th>End Date</th>
											                <th>Returned Date</th>
											            </tr>
										            </thead>
										            <tbody>
											            @forelse($jobs->jobReturns as $return)											           		
							                                @if($date_now > $return->end_date AND $return->status == 1)
							                                    <tr style="background-color: #fab1a0">                  
							                                @elseif($return->status > 1 AND $return->return_date > $return->end_date)
							                                    <tr style="background-color: #fab1a0">                  
							                                @else
							                                    <tr>
							                                @endif    
											           			<td>{!! Html::decode(link_to_route('job_orders.overview', $return->id,$return->encryptname, array())) !!}</td>	
											           			<td>
											           				{!! strtoupper($return->user->first_name.' '.$return->user->last_name) !!}
											           			</td>
											           			<td>{!! $return->created_at !!}</td>	
											           			<td>{!! $return->start_date !!}</td>									           			
											           			<td>{!! $return->end_date !!}</td>									           													           			
											           			<td>{!! $return->return_date !!}</td>											           			
											           		</tr>
											           	@empty
											           	@endforelse
										            </tbody>			           
								            	</table>
								            </div>
							            </div>
							        </div>
							    </div>
                            </div>
                        </div>
                    </div>
                    <div id="tab-closed" class="tab-pane">
                        <div class="panel-body">
                            <div class="tabs-container">
                            	<div class="col-lg-12">
							        <div class="ibox float-e-margins">
							            <div class="ibox-title" style="background-color:#009688">
							                <h5 style="color:white"><i class="fa fa-university"></i>Closed Job Orders</h5>	                
							            </div>
							            <div class="ibox-content">
							            	<div class="table-responsive">
						                       	<table class="table table-striped table-hover dataTables-example">
										         	<thead>
											            <tr>
											                <th>#</th>				                				
											                <th>Assigned By</th>
											                <th>Date Created</th>				                				
											                <th>Start Date</th>		                				
											                <th>End Date</th>
											                <th>Returned Date</th>
											            </tr>
										            </thead>
										            <tbody>
											            @forelse($jobs->jobClosed as $closed)											           		
							                                @if($date_now > $closed->end_date AND $closed->status == 1)
							                                    <tr style="background-color: #fab1a0">                  
							                                @elseif($closed->status > 1 AND $closed->return_date > $closed->end_date)
							                                    <tr style="background-color: #fab1a0">                  
							                                @else
							                                    <tr>
							                                @endif    						                               	
											           			<td>{!! Html::decode(link_to_route('job_orders.overview', $closed->id,$closed->encryptname, array())) !!}</td>	
											           			<td>
											           				{!! strtoupper($closed->user->first_name.' '.$closed->user->last_name) !!}
											           			</td>
											           			<td>{!! $closed->created_at !!}</td>											           			
											           			<td>{!! $closed->start_date !!}</td>									           			
											           			<td>{!! $closed->end_date !!}</td>									           			
											           			<td>{!! $closed->return_date !!}</td>		
											           		</tr>
											           	@empty
											           	@endforelse
										            </tbody>			           
								            	</table>
								            </div>
								            </div>
							            </div>
							        </div>
							    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>   
</div>	

@stop