@extends('layouts.master')
@section('main-body')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2><i class="fa fa-university"></i> PROJECT WORKGROUPS</h2>       
    </div>
</div>        
<div class="wrapper wrapper-content animated fadeInRight">	
    <div class="row">
        <div class="col-lg-12">
	        <div class="ibox float-e-margins">
	            <div class="ibox-title" style="background-color:#009688">
	                <h5 style="color:white"><i class="fa fa-university"></i> Projects</h5>	                
	            </div>
	            <div class="ibox-content">
	            	<div class="table-responsive">
		            	<table class="table table-striped table-bordered table-hover dataTables-example" >
			            <thead>
				            <tr>
				                <th>#</th>
				                <th>Project Name</th>
				                <th>Branch</th>				                
				                <th>Status</th>				                
				            </tr>
			            </thead>
			            <tbody>
				           @forelse($projects as $project)
				           		<tr>
				           			<td>{!! Html::decode(link_to_route('projects.overview', $project->id,$project->encryptname, array())) !!}</td>
				           			<td>{!! $project->project_name !!}</td>
				           			<td>{!! $project->branch !!}</td>				           			
				           			<td>
				           				@if($project->status === 0)
                                            <span class="label label-info"><i class="fa fa-clock-o"></i> For Approval</span>
                                        @elseif($project->status === 1)
                                            <span class="label label-success"><i class="fa fa-check"></i> Approved</span>
                                        @elseif($project->status === 2)
                                            <span class="label label-primary"> <i class="fa fa-user"></i>Assigned</span>                                       
                                        @elseif($project->status === 3)
                                            <span class="label label-danger"> <i class="fa fa-angellist"></i>Denied</span>                                       
                                        @elseif($project->status === 4)
                                            <span class="label label-warning"> <i class="fa fa-refresh"></i> For Re-Approve</span>                                       
                                        @endif
				           			</td>				           			
				           		</tr>
				           	@empty
				           	@endforelse
			            </tbody>			            
		            	</table>
		            </div>
	            </div>
	        </div>
	    </div>	    
    </div>
</div>	
@stop