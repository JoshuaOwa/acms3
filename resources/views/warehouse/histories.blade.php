@extends('layouts.master')
@section('main-body')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2><i class="fa fa-users"></i> STOCK HISTORIES OF {!! strtoupper($item->item) !!}</h2>       
    </div>
</div>        
<div class="wrapper wrapper-content animated fadeInRight">		
    <div class="row">    	
        <div class="row-1">
        	<div class="col-lg-12">
        		{!! Html::decode(link_to_Route('warehouse_inventory.index','<i class="fa fa-arrow-left"></i> Back',[], array('class' => 'btn btn-white btn-lg')))!!}                                                    
        	</div><br />
        </div>        
        <div class="col-lg-12">
	        <div class="ibox float-e-margins">
	            <div class="ibox-title" style="background-color:#009688">
	                <h5 style="color:white"><i class="fa fa-users"></i> History Logs</h5>	                
	            </div>
	            <div class="ibox-content">
	            	<div class="table-responsive">
		            	<table class="table table-striped table-hover" >
			            <thead>
				            <tr>				                				            	
				                <th style="text-align: center;">Log</th>				                
				            </tr>
			            </thead>
			            <tbody>
			            	@foreach($histories as $history)
			            	<tr>
			            		<td>{!! $history->logs !!}</td>
			            	</tr>
			            	@endforeach
			            </tbody>			            
		            	</table>
		            </div>		            
		            <div class="ibox-footer">		         
					</div>		
	            </div>	            				
	        </div>
	    </div>	    
    </div>
</div>	
@stop
