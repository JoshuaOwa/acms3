@extends('layouts.master')
@section('main-body')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2><i class="fa fa-play-circle-o"></i> WAREHOUSE INVENTORY</h2>
    </div>        
</div>      
<div class="wrapper wrapper-content animated fadeInRight">    
    <div class="row">
        <div class="col-lg-12 animated flash">
            <?php if (session('is_success')): ?>
                <div class="alert alert-success alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                    <center><h4>Items was successfully added! <i class="fa fa-check"></i></h4></center>
                </div>
            <?php endif;?>
            <?php if (session('item_update')): ?>
                <div class="alert alert-success alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                    <center><h4>Items was successfully updated! <i class="fa fa-check"></i></h4></center>                
                </div>
            <?php endif;?>
            <?php if (session('is_upload')): ?>
                <div class="alert alert-success alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                    <center><h4>Items was successfully uploaded! <i class="fa fa-check"></i></h4></center>                
                </div>
            <?php endif;?>
            <?php if (session('release_exceed')): ?>
                <div class="alert alert-danger alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                    <center><h4>Your Out Of Stock! <i class="fa fa-check"></i></h4></center>                
                </div>
            <?php endif;?>
            <?php if (session('item_release')): ?>
                <div class="alert alert-success alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                    <center><h4>Items was successfully released! <i class="fa fa-check"></i></h4></center>                
                </div>
            <?php endif;?>
            <?php if (session('item_error')): ?>
                <div class="alert alert-danger alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                    <center><h4>Item was not found in project Or you reach the limit for that stocks! <i class="fa fa-check"></i></h4></center>                
                </div>
            <?php endif;?>
        </div>
    </div>  
    <div class="row">        
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title" style="background-color:#009688">
                    <h5 style="color:white"><i class="fa fa-play-circle-o"></i> Inventory Table</h5>  
                    <div class="pull-right">
                        <button type="button" class="btn btn-white btn-xs" data-toggle="modal" data-target="#uploadItemModal">
                            <i class="fa fa-upload"></i> Upload Item
                        </button>
                        <button type="button" class="btn btn-white btn-xs" data-toggle="modal" data-target="#addItemModal">
                            <i class="fa fa-pluse"></i> New Item
                        </button>
                    </div>                  
                </div>
                <div class="ibox-content"> 
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead>
                                <tr>                                                                          
                                    <th>BARCODE</th>
                                    <th>ITEM</th>                                                                              
                                    <th>DESCRIPTION</th>
                                    <th>QTY</th>
                                    <th>ACTION</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($items as $item)
                                @if($item->qty < 1)
                                <tr class="danger" style="opacity:10; filter:alpha(opacity=10);">                             
                                @else
                                <tr>                                        
                                @endif                                
                                    <td>{!! $item->barcode !!}</td>                                                                         
                                    <td>{!! $item->item !!}</td>
                                    <td>{!! $item->description !!}</td>
                                    <td>
                                        @if($item->qty < 1)
                                            OUT OF STOCK
                                        @else
                                            {!! $item->qty !!}
                                        @endif                                        
                                    </td>
                                    <td>
                                        <button name="rbtn" class="itembtn btn btn-info btn-xs" id="{{$item->id}}" value="{{$item->id}}" onclick="itemEditFunction(this)">
                                                <i class="fa fa-pencil"></i> Edit
                                        </button>
                                        @if($item->qty < 1)                                            
                                        @else
                                            <button name="rbtn" class="itembtn btn btn-warning btn-xs" id="{{$item->id}}" value="{{$item->id}}" onclick="itemReleaseFunction(this)">
                                                <i class="fa fa-pencil"></i> Release
                                            </button>
                                        @endif                                             
                                        {!! Html::decode(link_to_Route('warehouse_inventory.histories','<i class="fa fa-book"></i> Histories', $item->barcode, array('class' => 'btn btn-primary btn-xs','id'=>$item->barcode)))!!}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>   
                    </div>                    
                </div>                                                    
            </div>
        </div>
    </div>  
</div>
<!-- Trigger the modal with a button -->
<!-- Modal -->
<div id="addItemModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        {!! Form::open(array('route'=>'warehouse_inventory.store','method'=>'POST')) !!}
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                {!!Form::label('name','Add New Item')!!}
            </div>
            <div class="modal-body">
                <div class="form-group">
                    {!! Form::label('barcode','Barcode') !!}
                    {!! Form::text('barcode','',['class'=>'form-control','required'=>'required','placeholder'=>'Enter barcode here']) !!} 
                </div>    
                <div class="form-group">
                    {!! Form::label('item','Item') !!}
                    {!! Form::text('item','',['class'=>'form-control','required'=>'required','placeholder'=>'Enter item here']) !!} 
                </div>                               
                <div class="form-group">
                    {!! Form::label('description','Description') !!}
                    {!! Form::textarea('description','',['class'=>'form-control','required'=>'required','placeholder'=>'Enter description here']) !!} 
                </div>    <div class="form-group">
                    {!! Form::label('qty','Qty') !!}
                    {!! Form::number('qty','',['class'=>'form-control','required'=>'required','placeholder'=>'Enter Qty here','min'=>0]) !!} 
                </div>                                                                    
            </div>
            <div class="modal-footer">                
                {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'submit', 'class' => 'btn btn-primary')) !!}
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-arrow-left"></i> Close</button>
            </div>
        </div>
        {!!Form::close()!!}
    </div>
</div>
<!-- Modal -->
<div id="editItemModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        {!! Form::open(array('route'=>'warehouse_inventory.updates','method'=>'POST')) !!}
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                {!!Form::label('name','Edit Item')!!}
            </div>
            <div class="modal-body">
                <div class="form-group">
                    {!! Form::label('qty','Qty') !!}
                    {!! Form::number('qty','',['class'=>'form-control','required'=>'required','placeholder'=>'Enter Qty here','min'=>0,'id'=>'qtyID']) !!} 
                </div>                                   
            </div>
            <div class="modal-footer">
                {!! Form::hidden('itemid','',['id'=>'itemid']) !!}
                {!! Form::button('<i class="fa fa-save"></i> Update', array('type' => 'submit', 'class' => 'btn btn-primary')) !!}
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-arrow-left"></i> Close</button>
            </div>
        </div>
        {!!Form::close()!!}
    </div>
</div>
<!-- Modal -->
<div id="uploadItemModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        {!! Form::open(array('route'=>'warehouse_inventory.upload','method'=>'POST','files'=>true)) !!}
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                {!!Form::label('name','Edit Timetable Task')!!}
            </div>
            <div class="modal-body">
               <div class="form-group">
                    <div class="col-lg-12">                            
                        {!!Form::label('attached','Attached File')!!}                             
                        {!! Form::file('attached', array('id' => 'inventoryid', 'class' => 'photo_files', 'accept'=>'.csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel','required'=>'required')) !!}                   
                    </div>
                </div>                                              
            </div>
            <div class="modal-footer">                
                {!! Form::button('<i class="fa fa-save"></i> Upload', array('type' => 'submit', 'class' => 'btn btn-primary')) !!}
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-arrow-left"></i> Close</button>
            </div>
        </div>
        {!!Form::close()!!}
    </div>
</div>
<!-- Modal -->
<div id="releaseItemModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        {!! Form::open(array('route'=>'warehouse_inventory.release','method'=>'POST')) !!}
        <!-- Modal content-->
        <div class="modal-content bounceInRight">
            <div class="modal-header" style="background-color:#009688">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <center><h3 style="color:white;" class="modal-title"><i class="fa fa-cubes fa-2x modal-icon"></i> Stock Releasing</h3></center>
            </div>                          
            <div class="modal-header">                
                <span><h3 id="bcodeRID"></h3></span>
                <span><h3 id="itemRID"></h3></span>
                <span><h3 id="descRID"></h3></span>                
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group">
                        <div class="col-lg-4">
                        {!! Form::text('job_id','',['class'=>'form-control','placeholder'=>'Enter JO #','id'=>'jocheckID']) !!}
                        </div>
                        <div class="col-lg-4">
                        <button type="button" class="btn btn-primary btn-md" id="checkerBtn">Check Job Order</button>
                        </div>
                    </div>
                </div>   
                <br />
                <div class="row">
                    <div id="successJO" class="hidden">
                        <div class="form-group">                            
                            <h3 id="jobDetID"></h3>
                        </div>
                        <br />
                        <div class="form-group">
                            {!! Form::label('qty','Qty') !!}
                            {!! Form::number('qty','',['class'=>'form-control','required'=>'required','placeholder'=>'Enter Qty here','min'=>0,'id'=>'qtyRID']) !!} 
                        </div>                                   
                    </div>                
                </div>                             
            </div>
            <div class="modal-footer">
                {!! Form::hidden('itemid','',['id'=>'itemRid']) !!}
                {!! Form::button('<i class="fa fa-save"></i> Release', array('type' => 'submit', 'class' => 'btn btn-primary')) !!}
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-arrow-left"></i> Close</button>
            </div>
        </div>
        {!!Form::close()!!}
    </div>
</div>
@endsection
@section('page-script')
$('#checkerBtn').click(function () {            
    
    $('#successJO').addClass('hidden');

    var x = $('#jocheckID').val();
    $.ajax({

        type:"POST",
        dataType: 'json',
        data: {jobid: x},
        url: "../../getJobOrderDetails",
        success: function(data){            
            
            if(data != ""){

                document.getElementById('jobDetID').innerHTML = "JOB ORDER DETAILS: " + data['details'];                      
                $('#successJO').removeClass('hidden');
            }else{
                alert('JOB ORDER NOT FOUND!');
            }         
        }    
    });
});
@endsection
<script type="text/javascript">
    function itemEditFunction(elem){

        var x = elem.id;

        $.ajax({

            type:"POST",
            dataType: 'json',
            data: {itemid: x},
            url: "../getWarehouseItemDetails",
            success: function(data){            
                

                $('#bcodeID').val(data['barcode']);                
                $('#itemID').val(data['item']);                
                $('#descID').val(data['description']);                
                $('#qtyID').val(data['qty']);              
                $('#itemid').val(data['id']);                
                $('#editItemModal').modal('show');
            }    
        });
    }

    function itemReleaseFunction(elem){

        var x = elem.id;

        $.ajax({

            type:"POST",
            dataType: 'json',
            data: {itemid: x},
            url: "../getWarehouseItemDetails",
            success: function(data){            
                
                document.getElementById('bcodeRID').innerHTML = "BARCODE: " + data['barcode'];
                document.getElementById('itemRID').innerHTML = "ITEM: " + data['item'];
                document.getElementById('descRID').innerHTML = "DESCRIPTION: " +data['description'];

                $('#qtyRID').val(data['qty']);              
                $('#itemRid').val(data['id']);
                $('#qtyID').attr('max',data['qty']);                          

                $('#releaseItemModal').modal('show');
            }    
        });
    }
</script>