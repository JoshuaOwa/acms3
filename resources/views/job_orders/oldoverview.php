@extends('layouts.master')
@section('main-body')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2><i class="fa fa-building-o"></i> <i class="fa fa-eye"></i> JOB ORDER {!! $job->id !!} DETAILS</h2>        
    </div>
</div>        
<div class="row  border-bottom white-bg dashboard-header">
    <div class="ibox-content ibox-heading">
        <center>
            <h2><strong>{!! strtoupper($job->id) !!}</strong></h2>
            <small>Job Order No.</small>
        </center>
    </div>   
    <div class="col-md-6">                
        <ul class="list-group clear-list m-t">
            <li class="list-group-item fist-item">
                <span class="pull-right">
                    {!! strtoupper($job->user->first_name.' '.$job->user->last_name) !!}
                </span>
                <span class="label label-info">*</span> Assigned By:
            </li>
            <li class="list-group-item">
                <span class="pull-right">
                    {!! strtoupper($job->assigned->first_name.' '.$job->assigned->last_name) !!}
                </span>
                <span class="label label-info">*</span> Assigned To:
            </li>                                    
            <li class="list-group-item">
                <span class="pull-right">
                    {!! $job->start_date !!}
                </span>
                <span class="label label-info">*</span> Start Date:
            </li>                
            <li class="list-group-item">
                <span class="pull-right">
                    {!! $job->end_date !!}
                </span>
                <span class="label label-info">*</span> End Date:
            </li>   
            <li class="list-group-item">
                <span class="pull-right">
                    @if($job->status === 1)
                        <span class="label label-info"><i class="fa fa-clock-o"></i> Ongoing</span>
                    @elseif($job->status === 2)
                        <span class="label label-success"><i class="fa fa-check"></i> Returned</span>
                    @elseif($job->status === 3)
                        <span class="label label-primary"> <i class="fa fa-user"></i>Closed</span>
                    @endif
                </span>
                <span class="label label-info">*</span> Job Order Status:
            </li>  
        </ul>
    </div>
    <div class="col-md-6">                
        <ul class="list-group clear-list m-t">                                                                                
            <li class="list-group-item fist-item">
                <span class="pull-right">
                    {!! $job->created_at->format('M d Y h:i a') !!}
                </span>
                <span class="label label-info">*</span> Date Created:
            </li>   
            <li class="list-group-item">
                <span class="pull-right">
                    {!! $job->updated_at->format('M d Y h:i a') !!}
                </span>
                <span class="label label-info">*</span> Last Update:
            </li>   
            <li class="list-group-item">
                <span class="pull-right">
                    {!! number_format($expense_approved,2) !!}
                </span>
                <span class="label label-info">*</span> Total Expense:
            </li>                                                    
            <li class="list-group-item">
                <span class="pull-right">
                    
                </span>
                <span class="label label-info">*</span> Total Overtime:
            </li>            
        </ul>
    </div>
</div>&nbsp;
<div class="wrapper wrapper-content animated fadeInRight">  
    <div class="row">             
        <div class="row">                  
            <div class="col-lg-12 animated flash">
                <?php if (session('is_update_status')): ?>
                    <div class="alert alert-success alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                        <center><h4>Job Order has been returned!<i class="fa fa-check"></i></h4></center>                
                    </div>
                <?php endif;?>            
            </div>        
        </div>        
        <div class="row">
            <div class="col-lg-12 animated flash">
                <?php if (session('is_sent')): ?>
                    <div class="alert alert-success alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                        <center><h4>Message sent! <i class="fa fa-check"></i></h4></center>                
                    </div>
                <?php endif;?>            
            </div>        
        </div>
        <div class="row">
            <div class="col-lg-8">
                <div class="ibox float-e-margins">
                    <div class="ibox-title" style="background-color:#009688">
                                                                
                        <div class="pull-right">
                            <button type="button" class="btn btn-white btn-xs" data-toggle="modal" data-target="#threadModal">
                                Create New Thread
                            </button>                                        
                        </div>                                
                    </div>
                    <div class="ibox-content ibox-heading">
                        <center><h3><i class="fa fa-comment"> Message Threads!</i></h3></center>
                    </div>
                    <div class="ibox-content">
                        <div class="feed-activity-list">                                
                            <div class="feed-element box" id="flowTimeProj">
                                @forelse($threads as $thread)
                                @if(!empty($thread->user->image))                                           
                                    {!! Html::image('/uploads/users/'.$thread->user->employee_number.'/'.$thread->user->image,'',['class'=>'img-circle pull-left','style'=>'width:50px; height:50px;']) !!}
                                @else                                           
                                    <img src="/uploads/users/default.jpg" class="img-circle pull-left" style="width:50px; height:50px;"></img>
                                @endif                                       
                                <div class="media-body" style="padding-right:10px;">                                        
                                    <div style="padding-top:10px;"></div>
                                    <small class="text-muted"></small>
                                    <div class="well">                  
                                        <small class="pull-right">
                                            {!! $thread->created_at->format('M-d-Y H:i:s A') !!}
                                        </small>                                                                                                                                                        
                                        <strong>From: </strong>
                                            {!! strtoupper($thread->user->first_name.' '.$thread->user->last_name) !!} <br/>                                            
                                        <br/>
                                        <br />
                                        <strong>Message: </strong><br/>
                                        {!! $thread->message!!}                                                                                                                                                         
                                        @if(count($thread->files) > 0)
                                            <div style="padding-top:30px;">
                                                <strong>Attachment: </strong><br/>                                                    
                                                @forelse($thread->files as $thread_file)                                                                                                                                                                 
                                                    {!! Form::open(array('route'=>'job_order_thread_files.download','method'=>'POST')) !!}
                                                    {!! Form::hidden('encname',$thread_file->encrpytname) !!}
                                                    {!! Form::submit($thread_file->filename, array('type' => 'submit', 'class' => 'btn btn-primary btn-xs')) !!}                                                        
                                                    {!! Form::close() !!}                                                           
                                                @empty
                                                @endforelse                                                 
                                            </div>      
                                        @else                                                                                       
                                        @endif               
                                        @if(!empty($thread->emails))
                                            <div style="padding-top: 20px;"></div>
                                            <strong>*Sent to: </strong>
                                            <p>{!! $thread->emails !!}</p>
                                        @endif
                                        <br /><br />                                                                            
                                        <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#addCommentModal" id="{{$thread->id}}" value="{{$thread->id}}" onclick="ThreadCommentFunction(this)">
                                            <i class="fa fa-comment"></i>
                                        </button>                                           
                                        @if(!empty($thread->comments))
                                            @foreach($thread->comments as $comment)                                                    
                                                <div style="padding-top: 10px;">                                                                           
                                                    @if(!empty($comment->user->image))                                           
                                                        {!! Html::image('/uploads/users/'.$comment->user->employee_number.'/'.$comment->user->image,'',['class'=>'img-circle pull-left','style'=>'width:50px; height:50px;']) !!}
                                                    @else                                           
                                                        <img src="/uploads/users/default.jpg" class="img-circle pull-left" style="width:50px; height:50px;"></img>
                                                    @endif    
                                                    <div class="media-body" style="padding-right:10px;">                                        
                                                        <div style="padding-top:10px;"></div>
                                                        <small class="text-muted"></small>
                                                        <div class="well" style="border-color: #009688;">
                                                            <small class="pull-right">
                                                                {!! $comment->created_at !!}
                                                            </small>                                                                                                                                                        
                                                            <strong>From: </strong>                                                             
                                                            {!! strtoupper($comment->user->first_name.' '.$comment->user->last_name) !!}                                                              
                                                            <br /><br />
                                                            <strong>Message: </strong><br/>
                                                                {!! $comment->message !!}                                                                                                                                                                                       
                                                            <br />
                                                            @if(count($comment->files) > 0)
                                                                <div style="padding-top:20px;">
                                                                    <strong>Attachment: </strong><br/>                                                                      
                                                                    @forelse($comment->files as $comment_file)                                                                                                                                                                   
                                                                        {!! Form::open(array('route'=>'job_order_thread_comment_files.download','method'=>'POST')) !!}
                                                                        {!! Form::hidden('encname',$comment_file->encrpytname) !!}
                                                                        {!! Form::submit($comment_file->filename, array('type' => 'submit', 'class' => 'btn btn-primary btn-xs')) !!}                                                       
                                                                        {!! Form::close() !!}                                                           
                                                                    @empty
                                                                    @endforelse                                                 
                                                                </div>      
                                                            @else                                                                                       
                                                            @endif 
                                                            @if(!empty($thread->emails))
                                                                <div style="padding-top: 20px;"></div>
                                                                <strong>*Sent to: </strong>
                                                                <p>{!! $thread->emails !!}</p>
                                                            @endif                                                         
                                                        </div>                                                            
                                                    </div>                                                          
                                                </div>
                                            @endforeach
                                            @else
                                        @endif                                                         
                                    </div>                                                                      
                                  <br />                                                            
                                </div>
                                @empty
                                @endforelse
                            </div>                                     
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="ibox float-e-margins">
                    <div class="ibox-title"  style="background-color:#009688">                        
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>                                
                        </div>
                    </div>
                    <div class="ibox-content ibox-heading">
                        <center><h3><i class="fa fa-book"> Job Order logs!</i></h3></center>
                    </div>
                    <div class="ibox-content inspinia-timeline" id="flowTimeProj">
                        @foreach($logs as $log)                           
                            <div class="timeline-item">
                                <div class="row">
                                    <div class="col-xs-3 date">
                                        <i class="fa fa-book"></i>
                                        {!! $log->created_at->diffForHumans() !!}
                                        <br/>
                                        <small class="text-navy">21 hour ago</small>
                                    </div>
                                    <div class="col-xs-7 content">
                                        <p class="m-b-xs"><strong>Details</strong></p>
                                        <p>
                                            {!! $log->logs !!}
                                        </p>
                                    </div>
                                </div>
                            </div>  
                        @endforeach                            
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 animated flash">
                <?php if (session('is_expense')): ?>
                    <div class="alert alert-success alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                        <center><h4>Expense has been added!<i class="fa fa-check"></i></h4></center>                
                    </div>
                <?php endif;?>            
            </div>        
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title" style="background-color:#009688">
                        <div class="pull-right">                                                                            
                            <button type="button" class="btn btn-white btn-xs" data-toggle="modal" data-target="#expenseAddModal">
                                Add Expense
                            </button>                                                                      
                        </div>                          
                    </div>
                    <div class="ibox-content ibox-heading">
                        <center><h3><i class="fa fa-money"> Expenses!</i></h3></center>
                        <div class="pull-right">
                            <h3 style="color: green;">Total Approved Expense: {!! number_format($expense_approved,2) !!}</h4>
                            <h3 style="color: red;">Total Expense For-Approval: {!! number_format($expense_approval,2) !!}</h4>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Date</th>
                                    <th>Particular</th>                      
                                    <th>Type</th>
                                    <th>Qty</th>
                                    <th>Amount</th>                                    
                                    <th>Total Amount</th>
                                    <th>Status</th>                                
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($expenses as $expense)
                                        <tr>
                                            <td>{!! strtoupper($expense->user->first_name.' '.$expense->user->last_name) !!}</td>
                                            <td>{!! $expense->date !!}</td>
                                            <td>{!! $expense->particular !!}</td>
                                            <td>{!! $expense->type->type !!}</td>       
                                            <td>{!! $expense->qty !!}</td>
                                            <td>{!! $expense->amount !!}</td>
                                            <td>{!! $expense->amount*$expense->qty !!}</td>
                                            <td>
                                                @if($expense->status === 1)
                                                    <span class="label label-info"><i class="fa fa-clock-o"></i> For Approval</span>
                                                @elseif($expense->status === 2)
                                                    <span class="label label-success"><i class="fa fa-check"></i> Approved</span>
                                                @elseif($expense->status === 3)
                                                    <span class="label label-primary"> <i class="fa fa-user"></i>Denied</span>
                                                @endif
                                            </td>                                            
                                        </tr>                   
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title" style="background-color:#009688">
                        <div class="pull-right">                                                                            
                                                                                            
                        </div>                          
                    </div>
                    <div class="ibox-content ibox-heading">
                        <center><h3><i class="fa fa-clock-o"> Overtime!</i></h3></center>
                    </div>
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>                      
                                    <th>Start Time</th>
                                    <th>End Time</th>                                    
                                    <th>Status</th>                                
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($members as $member)
                                        <tr>
                                            <td>{!! strtoupper($member->user->first_name.' '.$member->user->last_name) !!}</td>
                                            <td>{!! strtoupper($member->position) !!}</td>                  
                                            <td></td>
                                            <td></td>
                                            <td></td>                                            
                                            <td></td>                                            
                                        </tr>                   
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal" id="workgroupAddModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-users modal-icon"></i>
                <h4 class="modal-title">ADD WORKGROUP MEMBER</h4>                    
            </div>              
            {!! Form::open(array('route'=>'job_order_workgroups.store','method'=>'POST','files'=>true)) !!}                                                                               
            <div class="modal-body">                                                 
                <div class="row" style="padding-top:10px">
                    <div class="col-lg-6">
                        {!! Form::label('department','Department')!!}
                        {!! Form::select('department[]',$departments,'',['class'=>'form-control','id'=>'deptid','multiple'=>'multiple']) !!}                                  
                    </div>      
                    <div class="col-lg-6">
                        {!! Form::label('user_id','Name')!!}
                        {!! Form::select('user_id[]',$users,'',['class'=>'form-control','id'=>'user_id','multiple'=>'multiple']) !!}                     
                    </div>  
                </div>                                    
            </div>
            <div class="modal-footer">   
                {!! Form::hidden('contact_id',$job->contact_id) !!}
                {!! Form::hidden('project_id',$job->project_id) !!}
                {!! Form::hidden('job_order_id',$job->id) !!}
                {!! Form::hidden('timetable_id',$job->timetable_id) !!}
                {!! Form::hidden('task_id',$job->task_id) !!}
                {!! Form::button('Close', array('type' => 'submit', 'class' => 'btn btn-white','data-dismiss'=>'modal')) !!}            
                {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'button', 'class' => 'btn btn-primary hidden sbalert','id'=>'urbtn')) !!}
                {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'submit', 'class' => 'btn btn-primary hidden', 'id'=>'urbtn2')) !!}                    
            </div>   
            {!! Form::close() !!}                           
        </div>
    </div>
</div>
<div class="modal inmodal" id="workgroupRemoveModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-users modal-icon"></i>
                <h4 class="modal-title">REMOVE WORKGROUP MEMBER</h4>                    
            </div>              
            {!! Form::open(array('route'=>'project_workgroups.remove_member','method'=>'POST','files'=>true)) !!}                                                                               
            <div class="modal-body">                                                 
                <div class="row" style="padding-top:10px">                    
                    <div class="col-lg-6">
                        {!! Form::label('user_id','Name')!!}                        
                    </div>  
                </div>                                    
            </div>
            <div class="modal-footer">   
                {!! Form::hidden('contact_id',$job->contact_id) !!}
                {!! Form::hidden('project_id',$job->project_id) !!}
                {!! Form::hidden('job_order_id',$job->id) !!}
                {!! Form::button('Close', array('type' => 'submit', 'class' => 'btn btn-white','data-dismiss'=>'modal')) !!}          
                {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'button', 'class' => 'btn btn-primary hidden sbalert','id'=>'urbtn')) !!}
                {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'submit', 'class' => 'btn btn-primary hidden', 'id'=>'urbtn2')) !!}                    
            </div>   
            {!! Form::close() !!}                           
        </div>
    </div>
</div>
<div class="modal inmodal" id="expenseAddModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-money modal-icon"></i>
                <h4 class="modal-title">ADD EXPENSE</h4>                    
            </div>              
            {!! Form::open(array('route'=>'expenses.store','method'=>'POST','files'=>true)) !!}                                                                               
            <div class="modal-body">                                                 
                <div class="row" style="padding-top:10px">
                    <div class="col-lg-6">
                        {!! Form::label('date','Date')!!}
                        <span>{!! Form::text('date', '', array('class' => 'form-control date_input', 'id' => 'dateid', 'data-date-format' => 'yyyy-mm-dd', 'placeholder' => 'mm/dd/yyyy','required'=>'required')) !!}</span>
                        @if ($errors->has('date')) <p class="help-block" style="color:red;">{{ $errors->first('date') }}</p> @endif                        
                    </div>                          
                </div>          
                <div class="row" style="padding-top: 10px">
                    <div class="col-lg-6">
                        {!! Form::label('particular','Particular') !!}
                        {!! Form::text('particular','',['class'=>'form-control','placeholder'=>'Enter Particular','required'=>'required','id'=>'particular_id']) !!}
                    </div>                    
                </div>    
                <div class="row" style="padding-top: 10px">
                    <div class="col-lg-6">
                        {!! Form::label('particular_type_id','Particular Type')!!}
                        {!! Form::select('particular_type_id',$types,'',['class'=>'form-control','id'=>'type_id','required'=>'required','id'=>'partType_id']) !!}                     
                    </div>                  
                </div>    
                <div class="row" style="padding-top: 10px">
                    <div class="col-lg-6">
                        {!! Form::label('amount','Amount') !!}
                        {!! Form::text('amount','',['class'=>'form-control','placeholder'=>'Enter Amount','required'=>'required','id'=>'amountID']) !!}
                    </div>                    
                </div>    
                <div class="row" style="padding-top: 10px">
                    <div class="col-lg-6">
                        {!! Form::label('qty','Qty') !!}
                        {!! Form::number('qty','',['class'=>'form-control','placeholder'=>'Enter Qty','required'=>'required','min'=>'0','id'=>'qtyID']) !!}
                    </div>                    
                </div>    
                <div class="row" style="padding-top:10px;">
                    <div class="col-lg-12">                            
                        {!!Form::label('attached','Attached File')!!}                             
                        {!! Form::file('attached[]', array('id' => 'expfileid', 'class' => 'photo_files', 'accept' => 'pdf|docx')) !!}                   
                    </div>
                </div>         
            </div>
            <div class="modal-footer">                   
                {!! Form::hidden('contact_id',$job->contact_id) !!}
                {!! Form::hidden('project_id',$job->project_id) !!}
                {!! Form::hidden('timetable_id',$job->timetable_id) !!}
                {!! Form::hidden('task_id',$job->task_id) !!}
                {!! Form::hidden('job_order_id',$job->id) !!}
                {!! Form::button('Close', array('type' => 'submit', 'class' => 'btn btn-white','data-dismiss'=>'modal')) !!}            
                {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'button', 'class' => 'btn btn-primary','id'=>'expbtn')) !!}
                {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'submit', 'class' => 'btn btn-primary hidden', 'id'=>'expbtn2')) !!}                    
            </div>   
            {!! Form::close() !!}                           
        </div>
    </div>
</div>
<div class="modal inmodal" id="threadModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-comment modal-icon"></i>
                <h4 class="modal-title">New Thread</h4>                    
            </div>              
            {!! Form::open(array('route'=>'job_order_threads.store','method'=>'POST','files'=>true,'enctype'=>'multipart/form-data')) !!}
                <div class="modal-body">                       
                    <div class="row">
                        <div class="col-lg-4">
                            {!! Form::label('email','Option') !!}
                            {!! Form::select('option',$wemail,'',['class'=>'form-control','id'=>'optionID']) !!}
                        </div>                                                
                    </div>     
                    <div class="row">
                        <div class="col-lg-6 hidden" id="colEMAIL">
                            {!! Form::label('email','Email') !!}                    
                            {!! Form::label('email','*For multiple email, please use ";" at the end of each email address',['style'=>'color:red']) !!}                    
                            {!! Form::text('email','',['class'=>'form-control','placeholder'=>'Enter email. Use ; for multiple email','required'=>'required','id'=>'emailID']) !!}    
                        </div>                        
                    </div>
                    <div class="row">
                        <div class="col-lg-6 hidden" id="colSUBJ">
                            {!! Form::label('subject','Subject') !!}                    
                            {!! Form::text('subject','',['class'=>'form-control','placeholder'=>'Enter Subject.','required'=>'required','id'=>'subjID']) !!}    
                        </div>                        
                    </div>
                    <div class="row">                  
                        {!! Form::label('message','Message') !!}                    
                        {!! Form::textarea('message','',['class'=>'form-control','placeholder'=>'Enter it here....','required'=>'required','id'=>'threadmessageid']) !!}
                    </div>                             
                    <div class="row" style="padding-top:5px;">
                        <div class="col-lg-12">                  
                            {!! Form::label('attached','Attached File')!!}
                            {!! Form::file('attached[]', array('id' => 'thread_inputs', 'class' => 'photo_files', 'accept' => 'pdf|docx')) !!}                   
                        </div>
                    </div>                                    
                </div>
                <div class="modal-footer">                 
                    {!! Form::hidden('job_id',$job->id) !!}                                                 
                    {!! Form::button('Close', array('type' => 'submit', 'class' => 'btn btn-white','data-dismiss'=>'modal')) !!}                    
                    {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'button', 'class' => 'btn btn-primary sbalert','id'=>'threadbtn')) !!}                    
                    <button class="btn btn-primary hidden" id="threadbtn2" value="Submit" onclick="this.disabled=true; this.form.submit();">Submit</button>                    
                </div>                  
            {!! Form::close() !!}
        </div>
    </div>
</div>
<div class="modal inmodal" id="addCommentModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content animated fadeIn">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <i class="fa fa-comment modal-icon"></i>
                    <h4 class="modal-title">Comment</h4>                    
                </div>              
                {!! Form::open(array('route'=>'job_order_threads.comment','method'=>'POST','files'=>true,'enctype'=>'multipart/form-data')) !!}
                    <div class="modal-body">                           
                        <div class="row">
                            <div class="col-lg-4">
                                {!! Form::label('email','Option') !!}
                                {!! Form::select('option',$wemail,'',['class'=>'form-control','id'=>'comoptionID']) !!}
                            </div>                                                
                        </div>     
                        <div class="row">
                            <div class="col-lg-6 hidden" id="comEMAIL">
                                {!! Form::label('email','Email') !!}                    
                                {!! Form::label('email','*For multiple email, please use ";" at the end of each email address',['style'=>'color:red']) !!}                    
                                {!! Form::text('email','',['class'=>'form-control','placeholder'=>'Enter email. Use ; for multiple email','required'=>'required','id'=>'comemailID']) !!}    
                            </div>                        
                        </div>
                        <div class="row">
                            <div class="col-lg-6 hidden" id="comSUBJ">
                                {!! Form::label('subject','Subject') !!}                    
                                {!! Form::text('subject','',['class'=>'form-control','placeholder'=>'Enter Subject.','required'=>'required','id'=>'comsubjID']) !!}    
                            </div>                        
                        </div>      
                        <div class="row">                  
                            {!! Form::label('message','Comment') !!}                    
                            {!! Form::textarea('message','',['class'=>'form-control','placeholder'=>'Enter it here....','required'=>'required','id'=>'commentmessageid']) !!}
                        </div>                                                                              
                        <div class="row" style="padding-top:5px;">
                            <div class="col-lg-12">                  
                                {!! Form::label('attached','Attached File')!!}
                                {!! Form::file('attached[]', array('id' => 'comment_inputs', 'class' => 'photo_files', 'accept' => 'pdf|docx')) !!}                   
                            </div>
                        </div>       
                    </div>
                    <div class="modal-footer">                                              
                        {!! Form::hidden('thread_id','',['id'=>'mainthread_id']) !!}
                        {!! Form::button('Close', array('type' => 'submit', 'class' => 'btn btn-white','data-dismiss'=>'modal')) !!}              
                        {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'button', 'class' => 'btn btn-primary sbalert','id'=>'commentbtn')) !!}                              
                        <button class="btn btn-primary hidden" id="commentbtn2" value="Submit" onclick="this.disabled=true; this.form.submit();">Submit</button>                    
                    </div>                  
                {!! Form::close() !!}
        </div>

    </div>
</div>
@stop
@section('page-script')

$('#deptid,#user_id,#ex_user_id,#jouserid').multiselect({
    maxHeight: 200,    
    includeSelectAllOption: true,
    enableCaseInsensitiveFiltering: true,
    enableFiltering: true,
    buttonWidth: '100%',
    buttonClass: 'form-control'        
});

$('#expfileid,#comment_inputs,#thread_inputs').filer({

    showThumbs:true,
    addMore:true
});

$('#deptid').on("change", function(){

    var dept_id = $('#deptid').val();                           
    var proj_id = $('#proj_id').val();
    $.ajax({
        type:"POST",
        dataType: 'json',
        data: {dept_id: dept_id, proj_id: proj_id},
        url: "../../projects/getusers",
        success: function(data){            

            $('select#user_id').empty();
            
            $.each(data, function(i, text) {
                $('<option />',{value: i, text: text}).appendTo($('select#user_id'));
            });          
            var user = $('#user_id').val();       
            
            if(user != ""){                
                $('#urbtn').removeClass('hidden');
            }else{
                $('#urbtn').addClass('hidden');
            }        

            $('select#user_id').multiselect('rebuild');
        }
    });      
});    
$('#user_id').on("change", function(){

   var user2 = $('#user_id').val();       
            
    if(user2 != ""){                
        $('#urbtn').removeClass('hidden');
    }else{
        $('#urbtn').addClass('hidden');
    }        
});    
$('.sbalert').click(function () {

    if($('#user_id').val() != ""){
        
        swal({
            title: "Allright!",
            text: "I'm now going to add this to workgroup!",
            type: "success"
            });            
        $('#urbtn2').click();            
    }else{

        swal({
            title: "Warning",
            text: "Oh Come on! Select at least one member before you submit!",
            type: "warning"
        });      
    }                            
});


$('#expbtn').click(function () {

    if($('#dateid').val() != ""){        
        if($('#particular_id').val() !=""){
            if($('#partType_id').val() !=""){

                if($('#amountID').val() !=""){

                    if($('#qtyID').val() !=""){
                    swal({
                        title: "Allright!",
                        text: "Please wait....!",
                        type: "success"
                        });            
                        $('#expbtn2').click();     
                    }else{
                        $('#expbtn2').click();
                    }         
                }else{
                    $('#expbtn2').click();
                }
            }else{
                $('#expbtn2').click();
            }   
        }else{
            $('#expbtn2').click();
        }             
    }else{
        swal({
            title: "Warning",
            text: "Oh Come on! Select a date!",
            type: "warning"
        });      
    }                            
});

$('#sbalert2').click(function () {            
    swal({
        title: "Are you sure?",
        text: "You will about to return the job order",
        type: "info",
        showCancelButton: true,
        confirmButtonColor: "#1abc9c",
        confirmButtonText: "Yes, return it!",
        closeOnConfirm: false
    }, function () {
        swal("Got it!", "Please wait...", "success");
        $('#appbtn').click();
    });      
});
 $("#dateid").datepicker({
        dateFormat: "yy-mm-dd",        
});

$('#threadbtn').click(function () {

    if($('#threadmessageid').val() != ""){        
        
        swal({
            title: "That's Great!",
            text: "Please wait....!",
            type: "success"
        });            
        $('#threadbtn2').click();                         
    }else{
        swal({
            title: "Warning",
            text: "Oooopss! You forgot to put a message!",
            type: "warning"
        });      
    }                            
});
$('#commentbtn').click(function () {

    if($('#commentmessageid').val() != ""){        
        
        swal({
            title: "That's Great!",
            text: "Please wait....!",
            type: "success"
        });            
        $('#commentbtn2').click();                         
    }else{
        swal({
            title: "Warning",
            text: "Oooopss! You forgot to put a message!",
            type: "warning"
        });      
    }                            
});

$('#optionID').on("change", function(){

    var x = $('#optionID').val();    
    
    if(x > 0 ){
        
        $('#subjID').val('');            
        $('#emailID').val('');            
        $('#subjID').addClass('required');            
        $('#emailID').addClass('required');     
        $('#colEMAIL').removeClass('hidden');            
        $('#colSUBJ').removeClass('hidden');           
    }else{
        $('#subjID').val(''); 
        $('#emailID').val(''); 
        $('#subjID').removeClass('required');            
        $('#emailID').removeClass('required');    
        $('#colEMAIL').addClass('hidden');        
        $('#colSUBJ').addClass('hidden');      

    }
});

$('#comoptionID').on("change", function(){

    var x = $('#comoptionID').val();    
    
    if(x > 0 ){
        
        $('#comsubjID').val('');            
        $('#comemailID').val('');            
        $('#comsubjID').addClass('required');            
        $('#comemailID').addClass('required');            
        $('#comEMAIL').removeClass('hidden');            
        $('#comSUBJ').removeClass('hidden');           
    }else{
        $('#comsubjID').val(''); 
        $('#comemailID').val(''); 
        $('#comsubjID').removeClass('required');            
        $('#comemailID').removeClass('required');    
        $('#comEMAIL').addClass('hidden');        
        $('#comSUBJ').addClass('hidden');                       
    }
});
@endsection
<script type="text/javascript">
    
    function ThreadCommentFunction(val){

    var x = val.value;
        if(x != ""){
            $('#mainthread_id').val(x);
        }    
        else{
            alert("Please Reload The Page And Try Again.")
        }
    }
</script>