@extends('layouts.master')
@section('main-body')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2><i class="fa fa-play-circle-o"></i> CREATE JOB ORDER</h2>
    </div>        
</div>      
<div class="wrapper wrapper-content animated fadeInRight">    
    <div class="row">
        <div class="col-lg-12 animated flash">
            <?php if (session('is_success_jo')): ?>
                <div class="alert alert-success alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                    <center><h4>Job Order was successfully submitted!<i class="fa fa-check"></i></h4></center>                
                </div>
            <?php endif;?>            
            <?php if (session('is_required')): ?>
                <div class="alert alert-danger alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                    <center><h4>You Forgot To Select Any User<i class="fa fa-warning"></i></h4></center>                
                </div>
            <?php endif;?> 
        </div>
    </div>  
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title" style="background-color:#009688">
                    <h5 style="color:white"><i class="fa fa-plus"></i> New Job Order</h5>
                </div>
                <div class="ibox-content">                    
                {!! Form::open(array('route'=>'job_orders.new_store','methd'=>'POST','files'=>true)) !!}
                    <div class="row">                                
                        <div class="col-lg-12">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    {!! Form::label('department', 'Department') !!}
                                    {!! Form::select('department_id',$departments,'',['class'=>'form-control','id'=>'deptID','placeholder'=>'Select Department']) !!}
                                </div>    
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    {!! Form::label('users', 'To') !!}
                                    {!! Form::select('reciever[]',$users,'',['class'=>'form-control deptusers','multiple'=>'multiple','id'=>'userID']) !!}
                                </div>        
                            </div>
                            <div class="col-lg-4">
                                    <div class="form-group">
                                    {!! Form::label('cc', 'CC') !!}
                                    {!! Form::select('cc[]',$users,'',['class'=>'form-control deptusers','multiple'=>'multiple','id'=>'ccID']) !!}
                                </div>
                            </div>                                                    
                        </div>                                                
                    </div>
                    <div class="row">
                        <div class="form-group col-lg-12">
                            <div class="row">
                                <div class="col-lg-12">
                                    {!!Form::label('request_range','Job Range')!!} 
                                    <div class="form-group" id="date_range">                                                
                                        <div class="input-daterange input-group" id="datepicker">
                                            <input type="text" class="input-md form-control" name="start_date" value="{{$date_now}}" required="" />
                                            <span class="input-group-addon">to</span>
                                            <input type="text" class="input-md form-control" name="end_date" value="{{$date_now}}" required="" />
                                        </div>
                                    </div>                                
                                </div>                                        
                            </div>       
                        </div>        
                        <div class="col-lg-6">                            
                            <div class="form-group">
                                {!! Form::label('subject','Subject') !!}
                                {!! Form::text('subject','',['class'=>'form-control','required'=>'required','placeholder'=>'Enter Subject','id'=>'ursub']) !!} 
                            </div>                            
                        </div>         
                        <div class="col-lg-12">
                            <div class="form-group">
                                {!! Form::label('details','Details') !!}
                                {!! Form::textarea('details','',['class'=>'form-control','required'=>'required','placeholder'=>'Enter Complete Details Here','id'=>'urdet']) !!} 
                            </div>
                        </div>         
                        <div class="col-lg-12">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    {!! Form::label('type2','Scope') !!}
                                    {!! Form::select('type2',$scopes,'',['class'=>'form-control','required'=>'required','id'=>'reqtypeI']) !!} 
                                </div>
                            </div>
                        </div>         
                        <div class="col-lg-12">
                            {!! Form::label('recuring','Update Me') !!}                                                
                            <div class="i-checks">
                                <label>Daily <input type="radio" checked="" value="1" name="recuring"></label>
                                <label>Weekly <input type="radio" value="2" name="recuring"></label>
                                <label>Monthly <input type="radio" value="3" name="recuring"></label>
                            </div>
                        </div>                           
                        <div class="col-lg-3">
                            <div class="form-group">                                
                                {!! Form::label('attached','Attached File')!!}
                                {!! Form::file('attached[]', array('id' => 'attach_file', 'class' => 'photo_files')) !!}                                                   
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group">                        
                                {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'button', 'class' => 'btn btn-primary pull-right sbalert')) !!}
                                {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'submit', 'class' => 'btn btn-primary hidden', 'id'=>'urbtn')) !!}
                            </div>                          
                        </div>                   
                    </div>            
                </div>                
                {!! Form::close() !!}
            </div>
        </div>        
    </div>
</div>
@endsection
@section('page-script')

    $('.deptusers,#deptID,#ccID').multiselect({
        maxHeight: 200,    
        includeSelectAllOption: true,
        enableCaseInsensitiveFiltering: true,
        enableFiltering: true,
        buttonWidth: '100%',
        buttonClass: 'form-control',
    });  

    $('#date_range .input-daterange').datepicker({
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true
    });

    $('.sbalert').click(function () {

        var $nonempty = $('.deptusers').filter(function() {
            return this.value != ''
          });

        if ($nonempty.length > 0) {

            if($('#urdet').val() != "" && $('#ursub').val() != ""){
                swal({
                    title: "Good job!",
                    text: "I'm now going to submit your request!",
                    type: "success"
                });            
                $('#urbtn').click();
            }else{

                $('#urbtn').click();
            }                    
        }else{            
            swal({
                title: "Warning",
                text: "Ooops! I'm Not Intellegient Enough To Know Whom Do You Want To Recieve This Request!",
                type: "warning"
            });        
        }        
    });

    $('#attach_file').filer({

        showThumbs:true,
        addMore:true
    });

    $('#deptID').on("change", function(){

        var dept_id = $('#deptID').val();                           

        $.ajax({
            type:"POST",
            dataType: 'json',
            data: {dept_id: dept_id},
            url: "../../user_request/filterUserDept",
            success: function(data){            

                $('select#userID').empty();
                
                $.each(data, function(i, text) {
                    $('<option />',{value: i, text: text}).appendTo($('select#userID'));
                });          
                                        
                $('select#userID').multiselect('rebuild');
                $('select#ccID').empty();
                $('select#ccID').multiselect('rebuild');
            }
        });      
        
    });    

    $('#userID').on("change", function(){

        var users_id = $('#userID').val(); 
        $.ajax({
            type:"POST",
            dataType: 'json',
            data: {users_id: users_id},
            url: "../../user_request/UserCC",
            success: function(data){            

                $('select#ccID').empty();
                
                $.each(data, function(i, text) {
                    $('<option />',{value: i, text: text}).appendTo($('select#ccID'));
                });          
                                        
                $('select#ccID').multiselect('rebuild');
            }
        });      
    });    
@endsection