@extends('layouts.master')
@section('main-body')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2><i class="fa fa-university"></i> JOB ORDER LIST</h2>       
    </div>
</div>        
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-lg-12 animated flash">
		    <?php if (session('is_success')): ?>
		        <div class="alert alert-success alert-dismissible fade in" role="alert">
		            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
		        </button>
		            <center><h4>Module was successfully added!<i class="fa fa-check"></i></h4></center>                
		        </div>
		    <?php endif;?>
		    <?php if (session('is_update')): ?>
		        <div class="alert alert-success alert-dismissible fade in" role="alert">
		            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
		        </button>
		            <center><h4>Module was successfully updated!<i class="fa fa-check"></i></h4></center>                
		        </div>
		    <?php endif;?>
		</div>
	</div>
	 <div class="row">
        <div class="col-lg-12">
            <div class="tabs-container">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#tab-myjob">MY JOB ORDER</a></li>                  
                    <li class=""><a data-toggle="tab" href="#tab-assigned">ASSIGNED JOB ORDER</a></li>                      
                </ul>
                <div class="tab-content">
                    <div id="tab-myjob" class="tab-pane active">
                        <div class="panel-body">
                            <div class="tabs-container">
                            	<div class="col-lg-12">
							        <div class="ibox float-e-margins">
							            <div class="ibox-title" style="background-color:#009688">
							                <h5 style="color:white"><i class="fa fa-university"></i> Job Orders</h5>	                
							            </div>
							            <div class="ibox-content">
							            	<div class="table-responsive">
						                       	<table class="table table-striped table-hover dataTables-example">
										            <thead>
											            <tr>
											                <th>#</th>		
											                <th>Subject</th>		                				                
											                <th>Assigned By</th>
											                <th>Date Created</th>				                
											                <th>Date Return</th>
											                <th>Status</th>
											                <th>Rating</th>				                
											            </tr>
										            </thead>
										            <tbody>
											           @forelse($jobs as $job)
											           		@if($date_now > $job->end_date AND $job->status == 1)
							                                    <tr style="background-color: #fab1a0">                  
							                                @elseif($job->status > 1 AND $job->return_date > $job->end_date)
							                                    <tr style="background-color: #fab1a0">                  
							                                @else
							                                    <tr>
							                                @endif             				           		
											           			<td>{!! Html::decode(link_to_route('job_orders.overview', $job->id,$job->encryptname, array())) !!}</td>	
											           			<td>{!! $job->subject !!}</td>
											           			<td>
											           				{!! strtoupper($job->user->first_name.' '.$job->user->last_name) !!}
											           			</td>
											           			<td>{!! $job->created_at !!}</td>
											           			<td>
											           				@if(!empty($job->return_date))
											           					{!! $job->return_date !!}
											           				@else
											           					N/A
											           				@endif
											           			</td>
											           			<td>
											           				@if($job->status === 1)
												                        <span class="label label-info"><i class="fa fa-clock-o"></i> Ongoing</span>
												                    @elseif($job->status === 2)
												                        <span class="label label-success"><i class="fa fa-check"></i> Returned</span>
												                    @elseif($job->status === 3)
												                        <span class="label label-primary"> <i class="fa fa-user"></i>Closed</span>
												                    @endif
											           			</td>				           			
											           			<td>
											           				@if($job->status == 3)
							                                            @for($i=0; $i < $job->rating; $i++)
							                                                <i class="fa fa-star"></i>
							                                            @endfor
							                                            @for($j=$i; $j < 10; $j++)
							                                            	<i class="fa fa-star-o"></i>
							                                            @endfor
							                                        @else
							                                        N/A
							                                        @endif
											           			</td>											           		
											           		</tr>
											           	@empty
											           	@endforelse
										            </tbody>			            
								            	</table>
								            </div>
							            </div>
							        </div>
							    </div>
                            </div>
                        </div>
                    </div>
                    <div id="tab-assigned" class="tab-pane">
                        <div class="panel-body">
                            <div class="tabs-container">
                            	<div class="col-lg-12">
							        <div class="ibox float-e-margins">
							            <div class="ibox-title" style="background-color:#009688">
							                <h5 style="color:white"><i class="fa fa-university"></i> Job Orders</h5>	                
							            </div>
							            <div class="ibox-content">
							            	<div class="table-responsive">
						                       	<table class="table table-striped table-hover dataTables-example">
										            <thead>
											            <tr>
											                <th>#</th>				                				                
											                <th>Subject</th>
											                <th>Date Created</th>				                
											                <th>Date Return</th>
											                <th>Status</th>
											                <th>Rating</th>				                
											            </tr>
										            </thead>
										            <tbody>
											           @forelse($assigned as $job)
											           		@if($date_now > $job->end_date AND $job->status == 1)
							                                    <tr style="background-color: #fab1a0">                  
							                                @elseif($job->status > 1 AND $job->return_date > $job->end_date)
							                                    <tr style="background-color: #fab1a0">                  
							                                @else
							                                    <tr>
							                                @endif             				           		
											           			<td>{!! Html::decode(link_to_route('job_orders.overview', $job->id,$job->encryptname, array())) !!}</td>	
											           			<td>
											           				{!! $job->subject !!}
											           			</td>
											           			<td>{!! $job->created_at !!}</td>
											           			<td>
											           				@if(!empty($job->return_date))
											           					{!! $job->return_date !!}
											           				@else
											           					N/A
											           				@endif
											           			</td>
											           			<td>
											           				@if($job->status === 1)
												                        <span class="label label-info"><i class="fa fa-clock-o"></i> Ongoing</span>
												                    @elseif($job->status === 2)
												                        <span class="label label-success"><i class="fa fa-check"></i> Returned</span>
												                    @elseif($job->status === 3)
												                        <span class="label label-primary"> <i class="fa fa-user"></i>Closed</span>
												                    @endif
											           			</td>				           			
											           			<td>
											           				@if($job->status == 3)
							                                            @for($i=0; $i < $job->rating; $i++)
							                                                <i class="fa fa-star"></i>
							                                            @endfor
							                                            @for($j=$i; $j < 10; $j++)
							                                            	<i class="fa fa-star-o"></i>
							                                            @endfor
							                                        @elseif($job->status == 2)
							                                        	<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#ratingtModal" id="{{$job->id}}}" value="{{$job->id}}}" onclick="RatingFunction(this)">
					                                                        <i class="fa fa-star"></i>
					                                                    </button>    
							                                        @else
							                                        N/A
							                                        @endif
											           			</td>											           			
											           		</tr>
											           	@empty
											           	@endforelse
										            </tbody>			            
								            	</table>
								            </div>
							            </div>
							        </div>
							    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>   
</div>	
<div class="modal inmodal" id="ratingtModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-users modal-icon"></i>
                <h4 class="modal-title">RATE THIS JOB ORDER BEFORE CLOSING IT</h4>                    
            </div>              
            {!! Form::open(array('route'=>'job_orders.rating','method'=>'POST','files'=>true)) !!}                                                                               
            <div class="modal-body">                                                 
                <div class="row" style="padding-top:10px">                    
                    <div class="col-lg-4">
                        {!! Form::label('rating','Rate')!!}                        
                        {!! Form::select('rating',$ratings,'',['class'=>'form-control']) !!}
                    </div>                      
                    <div class="col-lg-12">
                        {!! Form::label('rating_remarks','Remarks')!!}                        
                        {!! Form::textarea('rating_remarks','',['class'=>'form-control','placeholder'=>'Remarks','required'=>'required','id'=>'rateremID']) !!}
                    </div>  
                </div>                                    
            </div>
            <div class="modal-footer">                   
                {!! Form::hidden('job_id','',['id'=>'jobID']) !!}
                {!! Form::hidden('status','3') !!}
                {!! Form::button('Close', array('type' => 'submit', 'class' => 'btn btn-white','data-dismiss'=>'modal')) !!}            
                {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'button', 'class' => 'btn btn-primary sbalert','id'=>'rtbtn')) !!}
                {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'submit', 'class' => 'btn btn-primary hidden', 'id'=>'rtbtn2')) !!}                    
            </div>   
            {!! Form::close() !!}                           
        </div>
    </div>
</div>
@stop
@section('page-script')
$('#rtbtn').click(function () {
	alert('x');
    if($('#rateremID').val() != ""){        
        
        swal({
            title: "That's Great!",
            text: "Please wait....!",
            type: "success"
        });            
        $('#rtbtn2').click();                         
    }else{
        swal({
            title: "Warning",
            text: "Oooopss! You forgot to put a remarks!",
            type: "warning"
        });      
    }                            
});
@endsection
<script type="text/javascript">
    
    function RatingFunction(val){

    var x = val.value;
        if(x != ""){
            $('#jobID').val(x);
        }    
        else{
            alert("Please Reload The Page And Try Again.")
        }
    }

</script>