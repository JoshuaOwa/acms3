@extends('layouts.master')
@section('main-body')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2><i class="fa fa-building-o"></i> <i class="fa fa-eye"></i> JOB ORDER {!! $job->id !!} DETAILS</h2>        
    </div>
</div>        
<div class="row  border-bottom white-bg dashboard-header">
    @if($job->status == 1 AND $date_now > $job->end_date)    
        <div class="ibox-content ibox-heading" style="background-color: #fab1a0">                  
            <h2>OVERDUE</h2>
    @elseif($job->status > 1 AND $job->return_date > $job->end_date)
        <div class="ibox-content ibox-heading" style="background-color: #fab1a0">                  
            <h2>OVERDUE</h2>
    @else
        <div class="ibox-content ibox-heading">                  
    @endif             
        <center>
            <h2><strong>{!! strtoupper($job->id) !!}</strong></h2>
            <small>Job Order No.</small>
        </center>
    </div>   
    <div class="col-md-6">                
        <ul class="list-group clear-list m-t">
            <li class="list-group-item fist-item">
                <span class="pull-right">
                    {!! strtoupper($job->user->first_name.' '.$job->user->last_name) !!}
                </span>
                <span class="label label-info">*</span> Assigned By:
            </li>                            
            <li class="list-group-item">
                <span class="pull-right">
                    {!! $job->start_date !!}
                </span>
                <span class="label label-info">*</span> Start Date:
            </li>                
            <li class="list-group-item">
                <span class="pull-right">
                    {!! $job->end_date !!}
                </span>
                <span class="label label-info">*</span> End Date:
            </li>   
            <li class="list-group-item">
                <span class="pull-right">                    
                    @if($job->status === 1)
                        <span class="label label-info"><i class="fa fa-clock-o"></i> Ongoing</span>
                    @elseif($job->status === 2)
                        <span class="label label-success"><i class="fa fa-check"></i> Returned</span>
                    @elseif($job->status === 3)
                        <span class="label label-primary"> <i class="fa fa-user"></i>Closed</span>
                    @endif
                </span>
                <span class="label label-info">*</span> Job Order Status:
            </li>  
        </ul>
    </div>
    <div class="col-md-6">                
        <ul class="list-group clear-list m-t">                                                                                            
            <li class="list-group-item">
                <span class="pull-right">
                    {!! $job->updated_at->format('M d Y h:i a') !!}
                </span>
                <span class="label label-info">*</span> Last Update:
            </li>   
            <li class="list-group-item">
                <span class="pull-right">
                    {!! number_format($expense_approved,2) !!}
                </span>
                <span class="label label-info">*</span> Total Expense:
            </li>                                                    
            <li class="list-group-item">
                <span class="pull-right">
                    {!! number_format($ot_approved,2) !!}
                </span>
                <span class="label label-info">*</span> Total Overtime:
            </li>            
        </ul>
    </div>
</div>&nbsp;
<div class="wrapper wrapper-content animated fadeInRight">  
    <div class="row">             
        <div class="row">                  
            <div class="col-lg-12 animated flash">
                <?php if (session('is_update_status')): ?>
                    <div class="alert alert-success alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                        <center><h4>Job Order has been returned!<i class="fa fa-check"></i></h4></center>                
                    </div>
                <?php endif;?>            
            </div>        
        </div>        
        <div class="row">
            <div class="col-lg-12 animated flash">
                <?php if (session('is_sent')): ?>
                    <div class="alert alert-success alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                        <center><h4>Message sent! <i class="fa fa-check"></i></h4></center>                
                    </div>
                <?php endif;?>            
            </div>        
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="ibox float-e-margins">
                    <div class="ibox-title" style="background-color:#009688">
                        <div class="pull-right">                                                                            
                            <button type="button" class="btn btn-white btn-xs" data-toggle="modal" data-target="#workgroupAddModal">
                                Add Member
                            </button>                                                                      
                        </div>                          
                    </div>
                    <div class="ibox-content ibox-heading">
                        <center><h3><i class="fa fa-users"> Participants</i></h3></center>
                    </div>
                    <div class="ibox-content">
                        <table class="table table-hover no-margins">
                            <thead>
                            <tr>
                                <th>Name</th>                                                                
                                <th>Rating</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($members as $member)
                                    <tr>
                                        <td>{!! strtoupper($member->user->first_name.' '.$member->user->last_name) !!}</td>                                        
                                        <td>                                                                                        
                                            @if($job->status == 2)
                                                @if($uid == $job->created_by)
                                                    <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#ratingtModal" id="{{$member->reciever_id}}" value="{{$member->reciever_id}}" onclick="RatingFunction(this)">
                                                        <i class="fa fa-star"></i>
                                                    </button>    
                                                @else
                                                    N/A
                                                @endif
                                            @elseif($job->status == 3 AND $member->rating < 1)
                                                @if($uid == $job->created_by)
                                                    <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#ratingtModal" id="{{$member->reciever_id}}" value="{{$member->reciever_id}}" onclick="RatingFunction(this)">
                                                        <i class="fa fa-star"></i>
                                                    </button>    
                                                @else
                                                    N/A
                                                @endif
                                            @elseif($job->status == 1)                                                
                                                N/A                                                
                                            @else
                                                @for($i=0; $i < $member->rating; $i++)
                                                <i class="fa fa-star"></i>
                                                @endfor
                                                @for($j=$i; $j < 10; $j++)
                                                    <i class="fa fa-star-o"></i>
                                                @endfor
                                            @endif                                                                                            
                                        </td>
                                    </tr>                   
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div> 
            <div class="col-lg-6">
                <div class="ibox float-e-margins">
                    <div class="ibox-title" style="background-color:#009688">
                        <div class="pull-right">                                                                            
                            <button type="button" class="btn btn-white btn-xs" data-toggle="modal" data-target="#workgroupAddCCModal">
                                Add CC
                            </button>       
                            <button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#workgroupRemoveCCModal">
                                Remove CC
                            </button>                                                                      
                        </div>                          
                    </div>
                    <div class="ibox-content ibox-heading">
                        <center><h3><i class="fa fa-users"> CC</i></h3></center>
                    </div>
                    <div class="ibox-content">
                        <table class="table table-hover no-margins">
                            <thead>
                            <tr>
                                <th>Name</th>                                
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($ccs as $cc)
                                    <tr>
                                        <td>{!! strtoupper($cc->full_name) !!}</td>
                                    </tr>                   
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div> 
        </div>
        <div class="row">
            <div class="col-lg-12">
                <h3>CLIENT: 
                    @if($job->contact_id > 0)                    
                        {!! strtoupper($job->client->company) !!}
                    @else
                        N/A
                    @endif
                </h3>
                <h3>PROJECT: 
                    @if($job->project_id)
                        {!! strtoupper($job->proj->project_name) !!}
                    @else
                        N/A
                    @endif
                </h3>
                <h3>Subject: 
                    @if(!empty($job->subject))
                        {!! strtoupper($job->subject) !!}
                    @else
                        N/A
                    @endif
                </h3>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8">
                <div class="ibox float-e-margins">
                    <div class="ibox-title" style="background-color:#009688">                                                                
                        <div class="pull-right">
                            @if($job->status < 2)
                                <button type="button" class="btn btn-success btn-xs" id="returnjo"><i class="fa fa-reply"> Return JO</i></button>                                                                                                                                                 
                            @endif
                        </div>
                    </div>
                    <div class="ibox-content ibox-heading">
                        <center><h3><i class="fa fa-comment"> Message Threads!</i></h3></center>
                    </div>
                    <div class="ibox-content">
                        <div class="feed-activity-list">
                            <div class="feed-element box" id="flowTimeProj">
                                @forelse($threads as $thread)
                                @if(!empty($thread->user->image))                                           
                                    {!! Html::image('/uploads/users/'.$thread->user->employee_number.'/'.$thread->user->image,'',['class'=>'img-circle pull-left','style'=>'width:50px; height:50px;']) !!}
                                @else                                           
                                    <img src="/uploads/users/default.jpg" class="img-circle pull-left" style="width:50px; height:50px;"></img>
                                @endif                                       
                                <div class="media-body" style="padding-right:10px;">                                        
                                    <div style="padding-top:10px;"></div>
                                    <small class="text-muted"></small>
                                    @if($thread->status == 1 AND $date_now > $thread->end_date)
                                        <div class="well" style="background-color: #fab1a0">                  
                                    @elseif($thread->status > 1 AND $thread->return_date > $thread->end_date)
                                        <div class="well" style="background-color: #fab1a0">                  
                                    @else
                                        <div class="well">                  
                                    @endif                                                                                      
                                        <strong>Status:  </strong> 
                                            @if($thread->status === 1)
                                                <span class="label label-info"><i class="fa fa-clock-o"></i> Ongoing</span>
                                            @elseif($thread->status === 2)
                                                <span class="label label-success"><i class="fa fa-check"></i> Returned</span>
                                            @elseif($thread->status === 3)
                                                <span class="label label-primary"> <i class="fa fa-user"></i>Closed</span>                                                                               
                                            @endif                                              
                                        @if($thread->status > 1)                                                                                              
                                            <br/>
                                            <strong>Return Date: </strong>{!! $thread->return_date !!}
                                        @endif                                                                                                                     
                                        <br/><br />                                                                                                                                         
                                        <strong>From: </strong>
                                            {!! strtoupper($thread->user->first_name.' '.$thread->user->last_name) !!} <br/>                                            
                                                                                                                    
                                        <br />                                                                                                                                                        
                                        <strong>Job Order #: </strong> {!! $thread->id !!} <br/>
                                        <strong>Scope: </strong>
                                        @if($thread->type < 1)
                                            Internal
                                        @else
                                            External
                                        @endif
                                        <br />                                        
                                        <strong>Date: </strong> {!! strtoupper($thread->start_date.' - '.$thread->end_date) !!} <br/>                                                                                                                            
                                        <br />
                                        <strong>Details: </strong><br/>
                                        {!! $thread->details!!}                                                                                                                                                         
                                        @if(count($thread->files) > 0)
                                            <div style="padding-top:30px;">
                                                <strong>Attachment: </strong><br/>                                                    
                                                @forelse($thread->files as $thread_file)                                                                                                                                                                 
                                                    {!! Form::open(array('route'=>'job_orders.download_files','method'=>'POST')) !!}
                                                    {!! Form::hidden('encname',$thread_file->encrpytname) !!}
                                                    {!! Form::submit($thread_file->filename, array('type' => 'submit', 'class' => 'btn btn-primary btn-xs')) !!}                                                        
                                                    {!! Form::close() !!}                                                           
                                                @empty
                                                @endforelse                                                    
                                            </div>      
                                        @else                                                                                       
                                        @endif               
                                        @if(!empty($thread->emails))
                                            <div style="padding-top: 20px;"></div>
                                            <strong>*Sent to: </strong>
                                            <p>{!! $thread->emails !!}</p>
                                        @endif
                                        <br /><br />                                                        
                                        <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#addJobOrderCommentModal" id="{{$thread->mainthreadid}}" value="{{$thread->mainthreadid}}" onclick="ThreadJobCommentFunction(this)">
                                            <i class="fa fa-comment"></i>
                                        </button>                                                                                 
                                        @if($thread->status < 2)
                                            {!! Form::open(array('route'=>array('job_orders.update_status'),'method'=>'POST'))!!}
                                                {!! Form::hidden('job_id',$thread->id) !!}                                                                                                                                    
                                                {!! Form::button('<i class="fa fa-save"></i> Return', array('value'=>'2','type' => 'submit', 'class' => 'btn btn-primary hidden', 'id'=>'returnJObtn','name'=>'status')) !!}                            
                                            {!! Form::close() !!}
                                        @endif                  
                                        @if(!empty($thread->comments))
                                            @foreach($thread->comments as $comment)                                                    
                                                <div style="padding-top: 10px;">                              
                                                    @if(!empty($comment->user->image))                                           
                                                        {!! Html::image('/uploads/users/'.$comment->user->employee_number.'/'.$comment->user->image,'',['class'=>'img-circle pull-left','style'=>'width:50px; height:50px;']) !!}
                                                    @else                                           
                                                        <img src="/uploads/users/default.jpg" class="img-circle pull-left" style="width:50px; height:50px;"></img>
                                                    @endif    
                                                    <div class="media-body" style="padding-right:10px;">                                        
                                                        <div style="padding-top:10px;"></div>
                                                        <small class="text-muted"></small>
                                                        <div class="well" style="border-color: #009688;">
                                                            <small class="pull-right">
                                                                {!! $comment->created_at !!}
                                                            </small>                                                                                                                                                        
                                                            <strong>From: </strong>                                                             
                                                            {!! strtoupper($comment->user->first_name.' '.$comment->user->last_name) !!}                                                              
                                                            <br /><br />
                                                            <strong>Message: </strong><br/>
                                                                {!! $comment->message !!}                                                                                                                                                                                       
                                                            <br />
                                                            @if(count($comment->files) > 0)
                                                                <div style="padding-top:20px;">
                                                                    <strong>Attachment: </strong><br/>                                                                      
                                                                    @forelse($comment->files as $comment_file)                                                                                                                                                                   
                                                                        {!! Form::open(array('route'=>'job_order_thread_comment_files.download','method'=>'POST')) !!}
                                                                        {!! Form::hidden('encname',$comment_file->encrpytname) !!}
                                                                        {!! Form::submit($comment_file->filename, array('type' => 'submit', 'class' => 'btn btn-primary btn-xs')) !!}                                                       
                                                                        {!! Form::close() !!}                                                           
                                                                    @empty
                                                                    @endforelse                                                 
                                                                </div>      
                                                            @else                                                                                       
                                                            @endif 
                                                            @if(!empty($thread->emails))
                                                                <div style="padding-top: 20px;"></div>
                                                                <strong>*Sent to: </strong>
                                                                <p>{!! $thread->emails !!}</p>
                                                            @endif                                                         
                                                        </div>                                                            
                                                    </div>                                                          
                                                </div>
                                            @endforeach
                                            @else
                                        @endif                                                         
                                    </div>                                                                      
                                  <br />                                                            
                                </div>
                                @empty
                                @endforelse
                            </div>                                     
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="ibox float-e-margins">
                    <div class="ibox-title"  style="background-color:#009688">                        
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>                                
                        </div>
                    </div>
                    <div class="ibox-content ibox-heading">
                        <center><h3><i class="fa fa-book"> Job Order logs!</i></h3></center>
                    </div>
                    <div class="ibox-content inspinia-timeline" id="flowTimeProj">
                        @foreach($logs as $log)                           
                            <div class="timeline-item">
                                <div class="row">
                                    <div class="col-xs-3 date">
                                        <i class="fa fa-book"></i>
                                        {!! $log->created_at->diffForHumans() !!}
                                        <br/>
                                        <small class="text-navy">21 hour ago</small>
                                    </div>
                                    <div class="col-xs-7 content">
                                        <p class="m-b-xs"><strong>Details</strong></p>
                                        <p>
                                            {!! $log->logs !!}
                                        </p>
                                    </div>
                                </div>
                            </div>  
                        @endforeach                            
                    </div>
                </div>
            </div>
        </div>
        <!-- <div class="row">
            <div class="col-lg-12 animated flash">
                <?php if (session('is_expense')): ?>
                    <div class="alert alert-success alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                        <center><h4>Expense has been added!<i class="fa fa-check"></i></h4></center>                
                    </div>
                <?php endif;?> 
                <?php if (session('ot_is_success')): ?>
                    <div class="alert alert-success alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                        <center><h4>Overtime has been filed!<i class="fa fa-check"></i></h4></center>                
                    </div>
                <?php endif;?>            
            </div>        
        </div> -->
        <!-- <div class="row"> -->
            <!-- <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title" style="background-color:#009688">
                        <div class="pull-right">                                                                            
                            <button type="button" class="btn btn-white btn-xs" data-toggle="modal" data-target="#expenseAddModal">
                                Add Expense
                            </button>                                                                      
                        </div>                          
                    </div>
                    <div class="ibox-content ibox-heading">
                        <center><h3><i class="fa fa-money"> Expenses!</i></h3></center>
                        <div class="pull-right">
                            <h3 style="color: green;">Total Approved Expense: {!! number_format($expense_approved,2) !!}</h4>
                            <h3 style="color: red;">Total Expense For-Approval: {!! number_format($expense_approval,2) !!}</h4>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Date</th>
                                    <th>Particular</th>                      
                                    <th>Type</th>
                                    <th>Qty</th>
                                    <th>Amount</th>                                    
                                    <th>Total Amount</th>
                                    <th>Status</th>                                
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($expenses as $expense)
                                        <tr>
                                            <td>{!! strtoupper($expense->user->first_name.' '.$expense->user->last_name) !!}</td>
                                            <td>{!! $expense->date !!}</td>
                                            <td>{!! $expense->particular !!}</td>
                                            <td>{!! $expense->type->type !!}</td>       
                                            <td>{!! $expense->qty !!}</td>
                                            <td>{!! $expense->amount !!}</td>
                                            <td>{!! $expense->amount*$expense->qty !!}</td>
                                            <td>
                                                @if($expense->status === 1)
                                                    <span class="label label-info"><i class="fa fa-clock-o"></i> For Approval</span>
                                                @elseif($expense->status === 2)
                                                    <span class="label label-success"><i class="fa fa-check"></i> Approved</span>
                                                @elseif($expense->status === 3)
                                                    <span class="label label-primary"> <i class="fa fa-user"></i>Denied</span>
                                                @endif
                                            </td>                                            
                                        </tr>                   
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div> -->
            <!-- <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title" style="background-color:#009688">
                        <div class="pull-right">
                            @if(!empty($my_supers))                                                                
                            <button type="button" class="btn btn-white btn-xs" data-toggle="modal" data-target="#overtimeModal">
                                File Over Time
                            </button>
                            @endif                                                             
                        </div>                          
                    </div>
                    <div class="ibox-content ibox-heading">
                        <center><h3><i class="fa fa-clock-o"> Overtime!</i></h3></center>
                    </div>
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>                      
                                    <th>Start Time</th>
                                    <th>End Time</th>                                   
                                    <th>Status</th>                                
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($ots as $ot)
                                        <tr>
                                            <td>{!! strtoupper($ot->user->first_name.' '.$ot->user->last_name) !!}</td>
                                            <td>{!! date('Y-m-d',strtotime($ot->start_date)) !!}</td>                  
                                            <td>{!! date('Y-m-d',strtotime($ot->end_date)) !!}</td>
                                            <td>{!! date('H:i',strtotime($ot->start_time)) !!}</td>
                                            <td>{!! date('H:i',strtotime($ot->end_time)) !!}</td>
                                            <td>
                                                @if($ot->status < 2)
                                                    <span class="label label-info"><i class="fa fa-clock-o"></i> For Approval</span>
                                                @elseif($ot->status == 2)
                                                    <span class="label label-success"><i class="fa fa-check"></i> Approved</span>
                                                @elseif($ot->status == 3)
                                                    <span class="label label-primary"> <i class="fa fa-user"></i>Denied</span>
                                                @endif
                                            </td>                           
                                        </tr>                   
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div> -->
        <!-- </div>     -->
    </div>
</div>
<div class="modal inmodal" id="overtimeModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-clock-o modal-icon"></i>
                <h4 class="modal-title">FILE OVERTIME</h4>                    
            </div>              
            {!! Form::open(array('route'=>'overtimes.store','method'=>'POST','files'=>true)) !!}                                                                               
            <div class="modal-body">                                                 
                <div class="row" style="padding-top:10px">
                    <div class="col-lg-6">                    
                        {!!Form::label('date','Start Date')!!}
                        <input type="text" class="input-md form-control" name="start_date" id="date_from"required="">
                    </div>
                    <div class="col-lg-6">                    
                        {!!Form::label('date','End Date')!!}
                        <input type="text" class="input-md form-control" name="end_date" id="date_to" required="">
                    </div>
                    <div class="col-lg-6">
                        {!! Form::label('starttime','Start Time')!!}
                        <div class="input-group clockpicker" data-placement="left" data-align="top" data-autoclose="true">
                            <input type="text" class="form-control" name="start_time">
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-time"></span>
                            </span>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        {!! Form::label('endtime','End Time')!!}                        
                        <div class="input-group clockpicker" data-placement="left" data-align="top" data-autoclose="true">
                            <input type="text" class="form-control" name="end_time">
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-time"></span>
                            </span>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <br>
                        <center>
                            <table>
                                <tr>
                                    <td><input type="radio" name="categ" value="1"><label> OFFSET</label></td>
                                    <td><input type="radio" name="categ" value="2"><label> PAID</label></td>
                                </tr>
                                <tr>
                                    <td><input type="radio" name="type" value="1"><label> INTERNAL</label></td>
                                    <td><input type="radio" name="type" value="2"><label> EXTERNAL</label></td>
                                </tr>
                            </table>
                        </center>
                    </div>
                    <div class="col-lg-12">
                        {!! Form::label('rating_remarks','Details')!!}                        
                        {!! Form::textarea('remarks','',['class'=>'form-control','placeholder'=>'Details','required'=>'required','id'=>'rateremID']) !!}
                    </div>  
                </div>                                    
            </div>
            <div class="modal-footer">                   
                {!! Form::hidden('job_order_id',$job->id) !!}
                {!! Form::hidden('project_id',$job->project_id) !!}             
                {!! Form::button('Close', array('type' => 'submit', 'class' => 'btn btn-white','data-dismiss'=>'modal')) !!}            
                {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'button', 'class' => 'btn btn-primary sbalert','id'=>'otbtn')) !!}
                {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'submit', 'class' => 'btn btn-primary hidden', 'id'=>'otbtn2')) !!}                    
            </div>   
            {!! Form::close() !!}                           
        </div>
    </div>
</div>
<div class="modal inmodal" id="ratingtModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-users modal-icon"></i>
                <h4 class="modal-title">RATE THIS JOB ORDER BEFORE CLOSING IT</h4>                    
            </div>              
            {!! Form::open(array('route'=>'job_orders.rating','method'=>'POST','files'=>true)) !!}                                                                               
            <div class="modal-body">                                                 
                <div class="row" style="padding-top:10px">                    
                    <div class="col-lg-4">
                        {!! Form::label('rating','Rate')!!}                        
                        {!! Form::select('rating',$ratings,'',['class'=>'form-control']) !!}
                    </div>                      
                    <div class="col-lg-12">
                        {!! Form::label('rating_remarks','Remarks')!!}                        
                        {!! Form::textarea('rating_remarks','',['class'=>'form-control','placeholder'=>'Remarks','required'=>'required','id'=>'rateremIDModal']) !!}
                    </div>  
                </div>                                    
            </div>
            <div class="modal-footer">                   
                {!! Form::hidden('job_order_id',$job->id) !!}
                {!! Form::hidden('reciever_id','',['id'=>'jobreciever']) !!}                
                {!! Form::button('Close', array('type' => 'submit', 'class' => 'btn btn-white','data-dismiss'=>'modal')) !!}            
                {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'button', 'class' => 'btn btn-primary sbalert','id'=>'rtbtn')) !!}
                {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'submit', 'class' => 'btn btn-primary hidden', 'id'=>'rtbtn2')) !!}                    
            </div>   
            {!! Form::close() !!}                           
        </div>
    </div>
</div>
<div class="modal inmodal" id="addJobOrderCommentModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content animated fadeIn">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <i class="fa fa-comment modal-icon"></i>
                    <h4 class="modal-title">Comment</h4>                    
                </div>              
                {!! Form::open(array('route'=>'job_order_threads.comment','method'=>'POST','files'=>true,'enctype'=>'multipart/form-data')) !!}
                    <div class="modal-body">                           
                        <div class="row">
                            <div class="col-lg-4">
                                {!! Form::label('email','Option') !!}
                                {!! Form::select('option',$wemail,'',['class'=>'form-control','id'=>'comoptionID']) !!}
                            </div>                                                
                        </div>     
                        <div class="row">
                            <div class="col-lg-6 hidden" id="comEMAIL">
                                {!! Form::label('email','Email') !!}                    
                                {!! Form::label('email','*For multiple email, please use ";" at the end of each email address',['style'=>'color:red']) !!}                    
                                {!! Form::text('email','',['class'=>'form-control','placeholder'=>'Enter email. Use ; for multiple email','required'=>'required','id'=>'comemailID']) !!}    
                            </div>                        
                        </div>
                        <div class="row">
                            <div class="col-lg-6 hidden" id="comSUBJ">
                                {!! Form::label('subject','Subject') !!}                    
                                {!! Form::text('subject','',['class'=>'form-control','placeholder'=>'Enter Subject.','required'=>'required','id'=>'comsubjID']) !!}    
                            </div>                        
                        </div>      
                        <div class="row">                  
                            {!! Form::label('message','Comment') !!}                    
                            {!! Form::textarea('message','',['class'=>'form-control','placeholder'=>'Enter it here....','required'=>'required','id'=>'commentmessageid']) !!}
                        </div>                                                                              
                        <div class="row" style="padding-top:5px;">
                            <div class="col-lg-12">                  
                                {!! Form::label('attached','Attached File')!!}
                                {!! Form::file('attached[]', array('id' => 'comment_inputs', 'class' => 'photo_files', 'accept' => 'pdf|docx')) !!}                   
                            </div>
                        </div>       
                    </div>
                    <div class="modal-footer">                                              
                        {!! Form::hidden('thread_id','',['id'=>'joborderthread_id']) !!}
                        {!! Form::button('Close', array('type' => 'submit', 'class' => 'btn btn-white','data-dismiss'=>'modal')) !!}              
                        {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'button', 'class' => 'btn btn-primary sbalert','id'=>'commentbtn')) !!}                              
                        <button class="btn btn-primary hidden" id="commentbtn2" value="Submit" onclick="this.disabled=true; this.form.submit();">Submit</button>                    
                    </div>                  
                {!! Form::close() !!}
        </div>

    </div>
</div>
<div class="modal inmodal" id="workgroupAddModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-users modal-icon"></i>
                <h4 class="modal-title">Add Participant</h4>                    
            </div>              
            {!! Form::open(array('route'=>'job_orders.add_participant','method'=>'POST','files'=>true)) !!}                                                                               
            <div class="modal-body">                                                 
                <div class="row" style="padding-top:10px">                    
                    <div class="col-lg-6">
                        {!! Form::label('reciever','Name')!!}
                        {!! Form::select('reciever[]',$users,'',['class'=>'form-control','id'=>'user_id','multiple'=>'multiple']) !!}                     
                    </div>  
                </div>                                    
            </div>
            <div class="modal-footer">                   
                {!! Form::hidden('job_order_id',$job->id) !!}
                {!! Form::button('Close', array('type' => 'submit', 'class' => 'btn btn-white','data-dismiss'=>'modal')) !!}            
                {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'button', 'class' => 'btn btn-primary hidden sbalert','id'=>'urbtn')) !!}
                {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'submit', 'class' => 'btn btn-primary hidden', 'id'=>'urbtn2')) !!}                    
            </div>   
            {!! Form::close() !!}                           
        </div>
    </div>
</div>
<div class="modal inmodal" id="workgroupAddCCModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-users modal-icon"></i>
                <h4 class="modal-title">Add CC</h4>                    
            </div>              
            {!! Form::open(array('route'=>'job_orders.add_member_cc','method'=>'POST','files'=>true)) !!}                                                                               
            <div class="modal-body">                                                 
                <div class="row" style="padding-top:10px">                    
                    <div class="col-lg-6">
                        {!! Form::label('reciever','Name')!!}
                        {!! Form::select('reciever[]',$notInCC,'',['class'=>'form-control','id'=>'user_idcc','multiple'=>'multiple']) !!}                     
                    </div>  
                </div>                                    
            </div>
            <div class="modal-footer">                   
                {!! Form::hidden('job_order_id',$job->id) !!}
                {!! Form::button('Close', array('type' => 'submit', 'class' => 'btn btn-white','data-dismiss'=>'modal')) !!}            
                {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'button', 'class' => 'btn btn-primary hidden','id'=>'sbalert211')) !!}
                {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'submit', 'class' => 'btn btn-primary hidden', 'id'=>'urbtn22')) !!}                    
            </div>   
            {!! Form::close() !!}                           
        </div>
    </div>
</div>
<div class="modal inmodal" id="workgroupRemoveCCModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-users modal-icon"></i>
                <h4 class="modal-title">Remove CC</h4>                    
            </div>              
            {!! Form::open(array('route'=>'job_orders.remove_member_cc','method'=>'POST','files'=>true)) !!}                                                                               
            <div class="modal-body">                                                 
                <div class="row" style="padding-top:10px">                    
                    <div class="col-lg-6">
                        {!! Form::label('reciever','Name')!!}
                        {!! Form::select('reciever[]',$inCC,'',['class'=>'form-control','id'=>'user_idrecc','multiple'=>'multiple']) !!}                     
                    </div>  
                </div>                                    
            </div>
            <div class="modal-footer">                   
                {!! Form::hidden('job_order_id',$job->id) !!}
                {!! Form::button('Close', array('type' => 'submit', 'class' => 'btn btn-white','data-dismiss'=>'modal')) !!}            
                {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'button', 'class' => 'btn btn-primary hidden','id'=>'sbalert22')) !!}
                {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'submit', 'class' => 'btn btn-primary hidden', 'id'=>'urbtn222')) !!}                    
            </div>   
            {!! Form::close() !!}                           
        </div>
    </div>
</div>
<div class="modal inmodal" id="expenseAddModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-money modal-icon"></i>
                <h4 class="modal-title">ADD EXPENSE</h4>                    
            </div>              
            {!! Form::open(array('route'=>'expenses.store','method'=>'POST','files'=>true)) !!}                                                                               
            <div class="modal-body">                                                 
                <div class="row" style="padding-top:10px">
                    <div class="col-lg-6">
                        {!! Form::label('date','Date')!!}
                        <span>{!! Form::text('date', '', array('class' => 'form-control date_input', 'id' => 'dateid', 'data-date-format' => 'yyyy-mm-dd', 'placeholder' => 'mm/dd/yyyy','required'=>'required')) !!}</span>
                        @if ($errors->has('date')) <p class="help-block" style="color:red;">{{ $errors->first('date') }}</p> @endif                        
                    </div>                          
                </div>          
                <div class="row" style="padding-top: 10px">
                    <div class="col-lg-6">
                        {!! Form::label('particular','Particular') !!}
                        {!! Form::text('particular','',['class'=>'form-control','placeholder'=>'Enter Particular','required'=>'required','id'=>'particular_id']) !!}
                    </div>                    
                </div>    
                <div class="row" style="padding-top: 10px">
                    <div class="col-lg-6">
                        {!! Form::label('particular_type_id','Particular Type')!!}
                        {!! Form::select('particular_type_id',$types,'',['class'=>'form-control','id'=>'type_id','required'=>'required','id'=>'partType_id']) !!}                     
                    </div>                  
                </div>    
                <div class="row" style="padding-top: 10px">
                    <div class="col-lg-6">
                        {!! Form::label('amount','Amount') !!}
                        {!! Form::text('amount','',['class'=>'form-control','placeholder'=>'Enter Amount','required'=>'required','id'=>'amountID']) !!}
                    </div>                    
                </div>    
                <div class="row" style="padding-top: 10px">
                    <div class="col-lg-6">
                        {!! Form::label('qty','Qty') !!}
                        {!! Form::number('qty','',['class'=>'form-control','placeholder'=>'Enter Qty','required'=>'required','min'=>'0','id'=>'qtyID']) !!}
                    </div>                    
                </div>    
                <div class="row" style="padding-top:10px;">
                    <div class="col-lg-12">                            
                        {!!Form::label('attached','Attached File')!!}                             
                        {!! Form::file('attached[]', array('id' => 'expfileid', 'class' => 'photo_files', 'accept' => 'pdf|docx')) !!}                   
                    </div>
                </div>         
            </div>
            <div class="modal-footer">                   
                {!! Form::hidden('contact_id',$job->contact_id) !!}
                {!! Form::hidden('project_id',$job->project_id) !!}
                {!! Form::hidden('timetable_id',$job->timetable_id) !!}
                {!! Form::hidden('task_id',$job->task_id) !!}
                {!! Form::hidden('job_order_id',$job->id) !!}
                {!! Form::button('Close', array('type' => 'submit', 'class' => 'btn btn-white','data-dismiss'=>'modal')) !!}            
                {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'button', 'class' => 'btn btn-primary','id'=>'expbtn')) !!}
                {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'submit', 'class' => 'btn btn-primary hidden', 'id'=>'expbtn2')) !!}                    
            </div>   
            {!! Form::close() !!}                           
        </div>
    </div>
</div>
@stop
@section('page-script')
$('.clockpicker').clockpicker();

$('#start_time').datetimepicker({
    use24hours: true,
    pickDate: false
});

$('#otbtn').click(function () {            
    swal({
        title: "Are you sure?",
        text: "Your Overtime will be filed",
        type: "info",
        showCancelButton: true,
        confirmButtonColor: "#1abc9c",
        confirmButtonText: "Yes, return it!",
        closeOnConfirm: false
    }, function () {
        swal("Got it!", "Please wait...", "success");
        $('#otbtn2').click();
    });      
});

$('#deptid,#user_id,#ex_user_id,#jouserid,#user_idcc,#user_idrecc').multiselect({
    maxHeight: 200,    
    includeSelectAllOption: true,
    enableCaseInsensitiveFiltering: true,
    enableFiltering: true,
    buttonWidth: '100%',
    buttonClass: 'form-control'        
});

$('#expfileid,#comment_inputs,#thread_inputs').filer({

    showThumbs:true,
    addMore:true
});

$('#deptid').on("change", function(){

    var dept_id = $('#deptid').val();                           
    var proj_id = $('#proj_id').val();
    $.ajax({
        type:"POST",
        dataType: 'json',
        data: {dept_id: dept_id, proj_id: proj_id},
        url: "../../projects/getusers",
        success: function(data){            

            $('select#user_id').empty();
            
            $.each(data, function(i, text) {
                $('<option />',{value: i, text: text}).appendTo($('select#user_id'));
            });          
            var user = $('#user_id').val();       
            
            if(user != ""){                
                $('#urbtn').removeClass('hidden');
            }else{
                $('#urbtn').addClass('hidden');
            }        

            $('select#user_id').multiselect('rebuild');
        }
    });      
});    
$('#user_id').on("change", function(){

   var user2 = $('#user_id').val();       
            
    if(user2 != ""){                
        $('#urbtn').removeClass('hidden');
    }else{
        $('#urbtn').addClass('hidden');
    }        
});    


$('#expbtn').click(function () {

    if($('#dateid').val() != ""){        
        if($('#particular_id').val() !=""){
            if($('#partType_id').val() !=""){

                if($('#amountID').val() !=""){

                    if($('#qtyID').val() !=""){
                    swal({
                        title: "Allright!",
                        text: "Please wait....!",
                        type: "success"
                        });            
                        $('#expbtn2').click();     
                    }else{
                        $('#expbtn2').click();
                    }         
                }else{
                    $('#expbtn2').click();
                }
            }else{
                $('#expbtn2').click();
            }   
        }else{
            $('#expbtn2').click();
        }             
    }else{
        swal({
            title: "Warning",
            text: "Oh Come on! Select a date!",
            type: "warning"
        });      
    }                            
});
$('#rtbtn').click(function () {

    var x = $("#ionrange_2").html();
    $('#rangeID').val(x);
    var remarks = $('#rateremIDModal').val();
    
    if(remarks != ""){
        swal({
            title: "That's Great!",
            text: "Please wait....!",
            type: "success"
        });            
        $('#rtbtn2').click();                         
    }else{
        swal({
            title: "Warning",
            text: "Oooopss! You forgot to put a remarks!",
            type: "warning"
        });      
    }                            
});

$('#sbalert2').click(function () {            
    swal({
        title: "Are you sure?",
        text: "You will about to return the job order",
        type: "info",
        showCancelButton: true,
        confirmButtonColor: "#1abc9c",
        confirmButtonText: "Yes, return it!",
        closeOnConfirm: false
    }, function () {
        swal("Got it!", "Please wait...", "success");
        $('#appbtn').click();
    });      
});
$("#dateid").datepicker({
    dateFormat: "yy-mm-dd",        
});
$("#date_from").datepicker({
    keyboardNavigation: false,
    forceParse: false,
    autoclose: true
});
$("#date_to").datepicker({
    keyboardNavigation: false,
    forceParse: false,
    autoclose: true
});

$('#threadbtn').click(function () {

    if($('#threadmessageid').val() != ""){        
        
        swal({
            title: "That's Great!",
            text: "Please wait....!",
            type: "success"
        });            
        $('#threadbtn2').click();                         
    }else{
        swal({
            title: "Warning",
            text: "Oooopss! You forgot to put a message!",
            type: "warning"
        });      
    }                            
});
$('#commentbtn').click(function () {

    if($('#commentmessageid').val() != ""){        
        
        swal({
            title: "That's Great!",
            text: "Please wait....!",
            type: "success"
        });            
        $('#commentbtn2').click();                         
    }else{
        swal({
            title: "Warning",
            text: "Oooopss! You forgot to put a message!",
            type: "warning"
        });      
    }                            
});

$('#optionID').on("change", function(){

    var x = $('#optionID').val();    
    
    if(x > 0 ){
        
        $('#subjID').val('');            
        $('#emailID').val('');            
        $('#subjID').addClass('required');            
        $('#emailID').addClass('required');     
        $('#colEMAIL').removeClass('hidden');            
        $('#colSUBJ').removeClass('hidden');           
    }else{
        $('#subjID').val(''); 
        $('#emailID').val(''); 
        $('#subjID').removeClass('required');            
        $('#emailID').removeClass('required');    
        $('#colEMAIL').addClass('hidden');        
        $('#colSUBJ').addClass('hidden');      

    }
});

$('#comoptionID').on("change", function(){

    var x = $('#comoptionID').val();    
    
    if(x > 0 ){
        
        $('#comsubjID').val('');            
        $('#comemailID').val('');            
        $('#comsubjID').addClass('required');            
        $('#comemailID').addClass('required');            
        $('#comEMAIL').removeClass('hidden');            
        $('#comSUBJ').removeClass('hidden');           
    }else{
        $('#comsubjID').val(''); 
        $('#comemailID').val(''); 
        $('#comsubjID').removeClass('required');            
        $('#comemailID').removeClass('required');    
        $('#comEMAIL').addClass('hidden');        
        $('#comSUBJ').addClass('hidden');                       
    }
});
$('#returnjo').click(function () {                
    swal({
        title: "Are you sure?",
        text: "You will about to return the job order",
        type: "info",
        showCancelButton: true,
        confirmButtonColor: "#1abc9c",
        confirmButtonText: "Yes, return it!",
        closeOnConfirm: false
    }, function () {
        swal("Got it!", "Please wait...", "success");
        $('#returnJObtn').click();
    });      
});

$('#urbtn').click(function () {

    if($('#user_id').val() != ""){
        
        swal({
            title: "Allright!",
            text: "I'm now going to add this to workgroup!",
            type: "success"
            });            
        $('#urbtn2').click();            
    }else{

        swal({
            title: "Warning",
            text: "Oh Come on! Select at least one member before you submit!",
            type: "warning"
        });      
    }                            
});

$('#user_idcc').on("change", function(){

   var user2 = $('#user_idcc').val();       

    if(user2 != ""){                
        $('#sbalert211').removeClass('hidden');
    }else{
        $('#sbalert211').addClass('hidden');
    }        
});    
$('#sbalert211').click(function () {

    if($('#user_idcc').val() != ""){
        
        swal({
            title: "Allright!",
            text: "I'm now going to add this to CC!",
            type: "success"
            });            
        $('#urbtn22').click();            
    }else{

        swal({
            title: "Warning",
            text: "Oh Come on! Select at least one member before you submit!",
            type: "warning"
        });      
    }                            
});

$('#user_idrecc').on("change", function(){

   var user2 = $('#user_idrecc').val();       
            
    if(user2 != ""){                
        $('#sbalert22').removeClass('hidden');
    }else{
        $('#sbalert22').addClass('hidden');
    }        
});    
$('#sbalert22').click(function () {

    if($('#user_idccc').val() != ""){
        
        swal({
            title: "Allright!",
            text: "I'm now going to remove this to CC!",
            type: "success"
            });            
        $('#urbtn222').click();            
    }else{

        swal({
            title: "Warning",
            text: "Oh Come on! Select at least one member before you submit!",
            type: "warning"
        });      
    }                            
});

@endsection
<script type="text/javascript">
    
   function ThreadJobCommentFunction(val){

    var x = val.value;
        if(x != ""){
            $('#joborderthread_id').val(x);
        }    
        else{
            alert("Please Reload The Page And Try Again.")
        }
    }
</script>
<script type="text/javascript">
    
    function RatingFunction(val){

    var x = val.value;    
        if(x != ""){            
            $('#jobreciever').val(x);
        }    
        else{
            alert("Please Reload The Page And Try Again.")
        }
    }

</script>