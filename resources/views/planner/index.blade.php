@extends('layouts.master')
@section('main-body')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2>Planner</h2>        
    </div>
</div>
<div class="wrapper wrapper-content">
    <center><h4>Legends:</h4> <h3><span class="label label-primary">Closed</span> <span class="label label-success">On Going</span> <span class="label label-warning">Reminder From Other</span><!--  <span class="label label-info">Job Order #</span>  --></h3></center>
    <div class="row animated fadeInDown">        
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Planner</h5>                    
                    <button type="button" class="btn btn-info btn-xs pull-right" data-toggle="modal" data-target="#reminderModal"><i class="fa fa-plus"></i> New Reminder</button>               
                </div>
                <div class="ibox-content">
                    {!! $calendar->calendar() !!}
                    <br />
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal" id="reminderModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-refresh fa-spin modal-icon"></i>
                <h4 class="modal-title">Reminder</h4>                    
            </div>  
            {!! Form::open(array('route'=>'planner.store','method'=>'POST','files'=>'true')) !!}                            
                <div class="modal-body">                  
                    <!-- <div class="row" style="padding-top:10px;">                                                 
                        <div class="col-lg-6">                            
                            {!! Form::label('type','Settings') !!}
                            {!! Form::select('type',$types,$types,['class'=>'form-control','id'=>'type']) !!}
                        </div>
                    </div>           -->       
                    <div class="row" style="padding-top:10px;">                                                 
                        <div class="col-lg-6">                            
                            {!! Form::label('to_user_id','Share To') !!}
                            {!! Form::select('to_user_id[]',$users,null,['class'=>'form-control','id'=>'users_id','multiple'=>true]) !!}
                        </div>
                    </div>
                    <div class="row" style="padding-top:10px;" id="start_end_dates"> 
                        <div class="col-lg-6">
                            {!!Form::label('start_date','Start Date')!!}       
                            <div class="input-group date form_date col-lg-12" data-date-format="yyyy-mm-dd" data-link-field="dtp_input11" id="start_date">
                                <input class="form-control" size="16" type="text" value="" readonly name="start_date" id="st_date">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
                            </div>
                            <input type="hidden" id="dtp_input11" value="" /><br/>       
                        </div>                               
                        <div class="col-lg-6">
                            {!!Form::label('end_date','End Date')!!}       
                            <div class="input-group date form_date col-lg-12" data-date-format="yyyy-mm-dd" data-link-field="dtp_input12" id="end_date">
                                <input class="form-control" size="16" type="text" value="" readonly name="end_date" id="ed_date">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
                            </div>
                            <input type="hidden" id="dtp_input12" value="" /><br/>       
                        </div>       
                    </div>
                   <!--  <div class="row" style="padding-top:10px;" id="reminds_time"> 
                        <div class="col-lg-6">
                            {!!Form::label('time','Remind Time')!!}       
                            <div class="input-group date form_time col-lg-12" data-date-format="hh:ii" data-link-field="dtp_input13" id="remind_time">
                                <input class="form-control" size="16" type="text" value="" readonly name="remind_time" id="r_time">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
                            </div>
                            <input type="hidden" id="dtp_input13" value="" /><br/>       
                        </div>                        
                    </div> -->
                    <div class="row hidden" style="padding-top:10px;" id="every_month_date"> 
                        <div class="col-lg-6">
                            {!!Form::label('start_date','Set Start Date')!!}       
                            <div class="input-group date form_time col-lg-12" data-date-format="hh:ii" data-link-field="dtp_input14" id="every_month_dates">
                                <input class="form-control" size="16" type="text" value="" readonly name="e_month" id="e_month">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
                            </div>
                            <input type="hidden" id="dtp_input14" value="" /><br/>       
                        </div>                        
                    </div>
                    <div class="row" style="padding-top:10px;">
                        <div class="col-lg-12">
                            {!! Form::label('title','Title') !!}
                        </div>
                        <div class="col-lg-6">
                            {!! Form::text('title','',['class'=>'form-control'])!!}
                        </div>                           
                    </div>                                                                          
                    <div class="row" style="padding-top:10px;">
                        <div class="col-lg-12">
                            {!! Form::label('details','Details') !!}
                        </div>
                        <div class="col-lg-12">
                            {!! Form::textarea('details','',['class'=>'form-control'])!!}
                        </div>                           
                    </div>                     
                    <div class="row" style="padding-top:10px;">
                        <div class="col-lg-12">                              
                            {!! Form::label('attached','Attached File')!!}
                        </div>
                        <div class="col-lg-6">                            
                            {!! Form::file('attached[]', array('id' => 'attach_file', 'class' => 'photo_files')) !!}                                                                           
                        </div>
                    </div>        
                </div>                  
                <div class="modal-footer">                         
                    {!! Form::button('Close', array('type' => 'submit', 'class' => 'btn btn-white','data-dismiss'=>'modal')) !!}
                    {!! Form::submit('Submit', array('type' => 'submit', 'class' => 'btn btn-primary')) !!}
                </div>   
            {!! Form::close() !!}                           
            </div>
        </div>
    </div>
</div>
@stop
@section('page-calendar')
{!! $calendar->script() !!}

@stop
@section('page-script') 

    $('#attach_file').filer({

        showThumbs:true,
        addMore:true
    });

    $('#reminderModal').on('shown.bs.modal', function() {                        
          $('#start_date').datetimepicker({   
            format: "yyyy-mm-dd",         
            weekStart: 1,
            todayBtn:  1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0             
        });       
                
        $('#end_date').datetimepicker({   
            format: "yyyy-mm-dd",         
             weekStart: 1,
            todayBtn:  1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0            
        });       

        $('#every_month_dates').datetimepicker({   
            format: "yyyy-mm-dd",         
            weekStart: 1,
            todayBtn:  1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0            
        });       

        $('#remind_time').datetimepicker({   
            format: "hh:ii",                     
            autoclose: 1,                        
            minView: 0,
            maxView: 1,
            forceParse: 0
        });       
    });


    $('#users_id').multiselect({
        maxHeight: 200,
        includeSelectAllOption: true,
        enableCaseInsensitiveFiltering: true,
        enableFiltering: true,
        buttonWidth: '100%',
        buttonClass: 'form-control',
    });  

    $('#type').on("change", function(){

        var x = $('#type').val();        
        if(x < 1 ){
            $('#start_end_dates').removeClass('hidden');
            $('#reminds_time').removeClass('hidden');
            $('#every_month_date').addClass('hidden');
        }
        else{
            $('#start_end_dates').addClass('hidden');
            $('#reminds_time').addClass('hidden');
            $('#every_month_date').removeClass('hidden');
        }
    });       

@stop