@extends('layouts.master')
@section('main-body')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2><i class="fa fa-futbol-o"></i> REMINDER DETAILS</h2>        
    </div>
</div>    
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-6 animated flash">
            <?php if (session('is_closed')): ?>
                <div class="alert alert-success alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                    <center><h4>Reminder was successfully closed!<i class="fa fa-check"></i></h4></center>                
                </div>
            <?php endif;?>          
        </div>
    </div>
    <div class="row animated fadeInRight">        
        <div class="col-lg-12">
            <div class="ibox-content">                
                <div class="col-lg-8">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title" style="background-color:#009688">    
                            <div class="pull-right">
                                {!! Form::open(array('route'=>'planners.stop','method'=>'POST')) !!}
                                    {!! Form::button('<i class="fa fa-close"></i> Stop This Reminder', array('type' => 'submit', 'class' => 'btn btn-warning btn-xs')) !!}
                                    {!! Form::hidden('reminder_id',$reminder->id) !!}
                                {!! Form::close() !!}
                            </div>
                        </div>
                        <div class="ibox-content ibox-heading">
                            <center>
                                <h3><strong>{!! $reminder->title !!}</strong></h3>
                                <small>Title</small>
                            </center>
                        </div>
                        <div class="ibox-content">
                            <div class="feed-activity-list">
                                <div class="feed-element box" id="flowTimeProj">
                                    
                                    @if(!empty($reminder->sender->image))                                           
                                        {!! Html::image('/uploads/users/'.$reminder->sender->employee_number.'/'.$reminder->sender->image,'',['class'=>'img-circle pull-left','style'=>'width:50px; height:50px;']) !!}
                                    @else                                           
                                        <img src="/uploads/users/default.jpg" class="img-circle pull-left" style="width:50px; height:50px;"></img>
                                    @endif                                       
                                    <div class="media-body" style="padding-right:10px;">                                        
                                        <div style="padding-top:10px;"></div>
                                        <small class="text-muted"></small>                                        
                                            <div class="well">                                                                                                                                                                                                                                                     
                                            <strong>From: </strong>
                                                {!! strtoupper($reminder->sender->first_name.' '.$reminder->sender->last_name) !!} <br/>                                            
                                            <br />                                                                                                                                                        
                                            <strong>Title: </strong> {!! $reminder->title !!} <br/>                                                                    
                                            <strong>Start Date: </strong> {!! $reminder->start_date !!} <br/>                                                                                                                            
                                            <strong>End Date: </strong> {!! $reminder->end_date !!} <br/>                                                                                                                            
                                            <br />
                                            <strong>Details: </strong><br/>
                                            {!! $reminder->details!!}                                                                                                                                                         
                                            @if(count($reminder->files) > 0)
                                                <div style="padding-top:30px;">
                                                    <strong>Attachment: </strong><br/>                                                    
                                                    @forelse($reminder->files as $thread_file)                                                                                                                                                                 
                                                        {!! Form::open(array('route'=>'planners.download','method'=>'POST')) !!}
                                                        {!! Form::hidden('encname',$thread_file->encryptname) !!}
                                                        {!! Form::submit($thread_file->filename, array('type' => 'submit', 'class' => 'btn btn-primary btn-xs')) !!}                                                        
                                                        {!! Form::close() !!}                                                           
                                                    @empty
                                                    @endforelse                                                    
                                                </div>      
                                            @else                                                                                       
                                            @endif                                                          
                                            <br /><br />                                                        
                                            <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#addCommentModal" id="{{$reminder->planner_id}}" value="{{$reminder->planner_id}}" onclick="ThreadJobCommentFunction(this)">
                                                <i class="fa fa-comment"></i>
                                            </button>                                                                                                                                     
                                            @if(!empty($reminder->comments))
                                                @foreach($reminder->comments as $comment)                                                    
                                                    <div style="padding-top: 10px;">                              
                                                        @if(!empty($comment->user->image))                                           
                                                            {!! Html::image('/uploads/users/'.$comment->user->employee_number.'/'.$comment->user->image,'',['class'=>'img-circle pull-left','style'=>'width:50px; height:50px;']) !!}
                                                        @else                                           
                                                            <img src="/uploads/users/default.jpg" class="img-circle pull-left" style="width:50px; height:50px;"></img>
                                                        @endif    
                                                        <div class="media-body" style="padding-right:10px;">                                        
                                                            <div style="padding-top:10px;"></div>
                                                            <small class="text-muted"></small>
                                                            <div class="well" style="border-color: #009688;">
                                                                <small class="pull-right">
                                                                    {!! $comment->created_at !!}
                                                                </small>                                                                                                                                                        
                                                                <strong>From: </strong>                                                             
                                                                {!! strtoupper($comment->user->first_name.' '.$comment->user->last_name) !!}                                                              
                                                                <br /><br />
                                                                <strong>Message: </strong><br/>
                                                                    {!! $comment->details !!}                                                                                                                                                                                       
                                                                <br />
                                                                @if(count($comment->files) > 0)
                                                                    <div style="padding-top:20px;">
                                                                        <strong>Attachment: </strong><br/>                                                                      
                                                                        @forelse($comment->files as $comment_file)                                                                                                                                                                   
                                                                            {!! Form::open(array('route'=>'planners.comment_download','method'=>'POST')) !!}
                                                                            {!! Form::hidden('encname',$comment_file->encryptname) !!}
                                                                            {!! Form::submit($comment_file->filename, array('type' => 'submit', 'class' => 'btn btn-primary btn-xs')) !!}                                                       
                                                                            {!! Form::close() !!}                                                           
                                                                        @empty
                                                                        @endforelse                                                 
                                                                    </div>      
                                                                @else                                                                                       
                                                                @endif                                                                                                             
                                                            </div>                                                            
                                                        </div>                                                          
                                                    </div>
                                                @endforeach
                                                @else
                                            @endif                                                       
                                        </div>                                                                      
                                      <br />                                                            
                                    </div>                                  
                                </div>                                     
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal" id="addCommentModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content animated fadeIn">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <i class="fa fa-comment modal-icon"></i>
                    <h4 class="modal-title">Comment</h4>                    
                </div>              
                {!! Form::open(array('route'=>'planners.comment','method'=>'POST','files'=>true,'enctype'=>'multipart/form-data')) !!}
                    <div class="modal-body">                                                   
                        <div class="row">                  
                            {!! Form::label('details','Comment') !!}                    
                            {!! Form::textarea('details','',['class'=>'form-control','placeholder'=>'Enter it here....','required'=>'required','id'=>'commentmessageid']) !!}
                        </div>                                                                              
                        <div class="row" style="padding-top:5px;">
                            <div class="col-lg-12">                  
                                {!! Form::label('attached','Attached File')!!}
                                {!! Form::file('attached[]', array('id' => 'comment_inputs', 'class' => 'photo_files', 'accept' => 'pdf|docx')) !!}                   
                            </div>
                        </div>       
                    </div>
                    <div class="modal-footer">                                              
                        {!! Form::hidden('planner_id',$reminder->id) !!}
                        {!! Form::button('Close', array('type' => 'submit', 'class' => 'btn btn-white','data-dismiss'=>'modal')) !!}              
                        {!! Form::button('<i class="fa fa-save"></i> Submit', array('type' => 'button', 'class' => 'btn btn-primary sbalert','id'=>'commentbtn')) !!}                              
                        <button class="btn btn-primary hidden" id="commentbtn2" value="Submit" onclick="this.disabled=true; this.form.submit();">Submit</button>                    
                    </div>                  
                {!! Form::close() !!}
        </div>
    </div>
</div>
@stop
@section('page-script')    

    $('#expfileid,#comment_inputs').filer({

        showThumbs:true,
        addMore:true
    });

    $('#commentbtn').click(function () {

        if($('#commentmessageid').val() != ""){        
            
            swal({
                title: "That's Great!",
                text: "Please wait....!",
                type: "success"
            });            
            $('#commentbtn2').click();                         
        }else{
            swal({
                title: "Warning",
                text: "Oooopss! You forgot to put a message!",
                type: "warning"
            });      
        }                            
    });
@stop