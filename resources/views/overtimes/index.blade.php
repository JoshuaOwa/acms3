@extends('layouts.master')
@section('main-body')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2><i class="fa fa-clock-o"></i> OVERTIMES FOR APPROVAL</h2>        
    </div>
</div>        
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12 animated flash">
            <?php if (session('is_approved')): ?>
                <div class="alert alert-success alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                    <center><h4>Overtime was approved!<i class="fa fa-check"></i></h4></center>                
                </div>
            <?php endif;?>
            <?php if (session('is_denied')): ?>
                <div class="alert alert-success alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                    <center><h4>Overtime was denied!<i class="fa fa-check"></i></h4></center>                
                </div>
            <?php endif;?>
            <?php if (session('is_assigned')): ?>
                <div class="alert alert-success alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                    <center><h4>Overtime was successfully assigned!<i class="fa fa-check"></i></h4></center>                
                </div>
            <?php endif;?>
            <?php if (session('not_assigned')): ?>
                <div class="alert alert-danger alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                    <center><h4>Please Try To Assigned The Again!<i class="fa fa-close"></i></h4></center>                
                </div>
            <?php endif;?>
            <?php if (session('is_error_not_found')): ?>
                <div class="alert alert-danger alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                    <center><h4>Error in Opening Overtime! Please report this to the admin<i class="fa fa-close"></i></h4></center>                
                </div>
            <?php endif;?>
            <?php if (session('is_transfer_proj')): ?>
                <div class="alert alert-success alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                    <center><h4>Overtime was successfully transfered! <i class="fa fa-check"></i></h4></center>                
                </div>
            <?php endif;?>     
            <?php if (session('error_transfer_proj')): ?>
                <div class="alert alert-danger alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                    <center><h4>Overtime Approval Failed! <i class="fa fa-wrong"></i></h4></center>                
                </div>
            <?php endif;?>     
        </div>
        <div class="ibox-content">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover dataTables-example">
                    <thead>
                    <tr>
                        <th>Project Name</th>
                        <th>JO#</th>
                        <th>Employee Name</th>
                        <th>Date</th>
                        <th>Category</th>                      
                        <th>Type</th>                                  
                        <th>Hours</th>
                        <th>Status</th>
                        <th>Action</th>                               
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($ots as $ot)
                            <tr>
                                <td>{!! $ot->project->project_name !!}</td>
                                <td>JO#{!! $ot->job_order_id !!}</td>
                                <td>{!! strtoupper($ot->user->first_name.' '.$ot->user->last_name) !!}</td>
                                <td>{!! date('Y-m-d',strtotime($ot->start_date)).' to '.date('Y-m-d',strtotime($ot->end_date)) !!}</td>
                                <td>
                                    @if($ot->category_id == 1)
                                        OFFSET
                                    @else
                                        PAID
                                    @endif
                                </td>
                                <td>
                                    @if($ot->type_id == 1)
                                        INTERNAL
                                    @else
                                        EXTERNAL
                                    @endif
                                </td>
                                <td>
                                    <?php $date1 = new DateTime($ot->start_date.' '.$ot->start_time); ?>
                                    <?php $date2 = new DateTime($ot->end_date.' '.$ot->end_time); ?>
                                    <?php $hour = $date1->diff($date2)->format('%h'); ?>
                                    <?php $minute = $date1->diff($date2)->format('%i'); ?>
                                    <?php $total = number_format($hour + $minute/60,2); ?>
                                    {!! $total !!}
                                </td>
                                <td>
                                    @if($ot->status < 3)
                                        <span class="label label-info"><i class="fa fa-clock-o"></i> For Approval</span>
                                    @elseif($ot->status == 3)
                                        <span class="label label-success"><i class="fa fa-check"></i> Approved</span>
                                    @elseif($ot->status == 4)
                                        <span class="label label-primary"> <i class="fa fa-user"></i>Denied</span>
                                    @endif
                                </td>
                                <td>
                                    {!! Html::decode(link_to_route('overtimes.overview', '<i class="fa fa-eye"></i> Details',$ot->encrypt_name, array('class' => 'btn btn-xs btn-info'))) !!}
                                </td>                                        
                            </tr>                   
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>   
    </div>                 
</div>	
@stop
@section('page-script')        

    $('#project_id').multiselect({
    maxHeight: 200,
    includeSelectAllOption: true,
    enableCaseInsensitiveFiltering: true,
    enableFiltering: true,
    buttonWidth: '100%',
    buttonClass: 'form-control',
    });       
    
   
@stop