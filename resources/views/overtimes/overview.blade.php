@extends('layouts.master')
@section('main-body')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12 animated flash">
        <?php if (session('error_in_action')): ?>
            <div class="alert alert-success alert-danger fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
            </button>
                <center><h4>Error in approving/denying expense!<i class="fa fa-check"></i></h4></center>                
            </div>
        <?php endif;?>
        <?php if (session('is_not_found')): ?>
            <div class="alert alert-success alert-danger fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
            </button>
                <center><h4>Error in download file!<i class="fa fa-check"></i></h4></center>                
            </div>
        <?php endif;?>
    </div>
    <div class="col-lg-12">
        <h2><i class="fa fa-clock-o"></i> <i class="fa fa-eye"></i> OVERTIME DETAILS</h2>        
    </div>
</div>
&nbsp;
<div class="row">
    <div class="col-lg-12">      
        <div class="panel-body">
            <div class="tabs-container">
                <div class="row  border-bottom white-bg dashboard-header">
                    <div class="ibox-content ibox-heading">
                        <center>
                            <h2><strong>{!! strtoupper($project->project_name) !!}</strong></h2>
                            <small>Project Name</small>
                        </center>
                    </div>   
                    <div class="col-md-6">                
                        <ul class="list-group clear-list m-t">
                            <li class="list-group-item fist-item">
                                <span class="pull-right">
                                    @if($project->status === 0)
                                        <span class="label label-info"><i class="fa fa-clock-o"></i> For Approval</span>
                                    @elseif($project->status === 1)
                                        <span class="label label-success"><i class="fa fa-check"></i> Approved</span>
                                    @elseif($project->status === 2)
                                        <span class="label label-primary"> <i class="fa fa-user"></i>Assigned</span>                                       
                                    @elseif($project->status === 3)
                                        <span class="label label-danger"> <i class="fa fa-angellist"></i>Denied</span>                                       
                                    @elseif($project->status === 4)
                                        <span class="label label-warning"> <i class="fa fa-refresh"></i> For Re-Approve</span>                                       
                                    @endif
                                </span>
                                <span class="label label-info">*</span> <strong>Project Status:</strong>
                            </li>  
                            <li class="list-group-item ">
                                <span class="pull-right">
                                    {!! $project->tag !!}
                                </span>
                                <span class="label label-info">*</span> <strong>Tag:</strong>
                            </li>      
                            <li class="list-group-item">
                                <span class="pull-right">
                                    {!! strtoupper($project->approver->first_name.' '.$project->approver->last_name) !!}
                                </span>
                                <span class="label label-info">*</span> <strong>Project Approver:</strong>
                            </li>
                            <li class="list-group-item">
                                <span class="pull-right">
                                    {!! $project->project_address !!}
                                </span>
                                <span class="label label-info">*</span> <strong>Project Address:</strong>
                            </li>      
                            <li class="list-group-item">
                                <span class="pull-right">
                                    {!! $project->branch !!}
                                </span>
                                <span class="label label-info">*</span> <strong>Branch:</strong>
                            </li>                                                                                                  
                        </ul>                                    
                    </div>
                    <div class="col-md-6">                
                        <ul class="list-group clear-list m-t">
                            <li class="list-group-item fist-item">
                                <span class="pull-right">
                                    {!! strtoupper($project->user->first_name.' '.$project->user->last_name) !!}
                                </span>
                                <span class="label label-info">*</span> <strong>Created By:</strong>
                            </li>                        
                            
                            <li class="list-group-item fist-item">
                                <span class="pull-right">
                                    {!! $project->start_date !!}
                                </span>
                                <span class="label label-info">*</span> <strong>Start Date:</strong>
                            </li>                
                            <li class="list-group-item">
                                <span class="pull-right">
                                    {!! $project->end_date !!}
                                </span>
                                <span class="label label-info">*</span> <strong>End Date:</strong>
                            </li>
                            <li class="list-group-item">
                                <span class="pull-right">
                                    {!! $project->allowed_ot.' HOURS' !!}
                                </span>
                                <span class="label label-info">*</span> <strong>Allowed OT:</strong>
                            </li>                
                            <li class="list-group-item">
                                <span class="pull-right">
                                    {!! $project->project_cost !!}
                                </span>
                                <span class="label label-info">*</span> <strong>Project Cost:</strong>
                            </li>                                                  
                        </ul>
                    </div>
                </div>&nbsp;
                <div class="row  border-bottom white-bg dashboard-header">
                    <div class="ibox-content ibox-heading">
                        <center>
                            <h2><strong>Overtime Details</strong></h2>                        
                        </center>        
                    </div>
                    <div class="col-md-6">                
                        <ul class="list-group clear-list m-t">
                            <li class="list-group-item fist-item">
                                <span class="pull-right">
                                    {!! strtoupper($ot->user->first_name.' '.$ot->user->last_name) !!}
                                </span>
                                <span class="label label-info">*</span> <strong>Filed By:</strong>
                            </li> 
                            <li class="list-group-item fist-item">
                                <span class="pull-right">
                                    JOB ORDER #{!! $ot->job_order_id !!}
                                </span>
                                <span class="label label-info">*</span> <strong>Job Order #:</strong>
                            </li>
                            <li class="list-group-item fist-item">
                                <span class="pull-right">
                                    @if($ot->category_id == 1)
                                        OFFSET
                                    @else
                                        PAID
                                    @endif
                                </span>
                                <span class="label label-info">*</span> <strong>Category:</strong>
                            </li>                                                                        
                        </ul>
                    </div>
                    <div class="col-md-6">                
                        <ul class="list-group clear-list m-t">
                            <li class="list-group-item fist-item">
                                <span class="pull-right">
                                    @if($ot->type_id == 1)
                                        INTERNAL
                                    @else
                                        EXTERNAL
                                    @endif
                                </span>
                                <span class="label label-info">*</span> <strong>Type:</strong>
                            </li>
                            <li class="list-group-item fist-item">
                                <span class="pull-right">
                                    {!! $ot->remarks !!}
                                </span>
                                <span class="label label-info">*</span> <strong>Remarks:</strong>
                            </li>                                                                        
                        </ul>
                    </div>&nbsp;
                    <hr>
                    <div class="ibox-content ibox-heading">
                        <center>
                            <h2><strong>Approval</strong></h2>                        
                        </center>        
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            {!! Form::open(array('route'=>'overtimes.approval','method'=>'POST')) !!}
                            <div class="col-md-10">
                                <div class="form-group">
                                    {!! Form::label('con_remarks','Remarks') !!}
                                    {!! Form::textarea('confirmed_remarks','',['id'=>'confirm_id','placeholder'=>'Remarks','required','class'=>'form-control']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::hidden('id',$ot->id) !!}
                                    <button class="btn btn-md btn-success" id="btnaprove" type="button">Approve</button>
                                    <button class="btn hidden" id="approved" name="type" value="1" type="submit"></button>
                                    <button class="btn btn-md btn-danger" id="btndeny">Deny</button>
                                    <button class="btn hidden" id="denied" name="type" value="2" type="submit"></button>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>                                                        
            </div>
        </div>
    </div>
</div>
@stop
@section('page-script')
$('#btnaprove').click(function () {
    var content = $('#confirm_id').val();
    if(content == ''||content == null || content == ' '){
        swal('Please enter some remarks');
    }else{
        swal({
            title: "Are you sure?",
            text: "This Expense will be approved!",
            icon: "warning",
            dangerMode: true,
            showCancelButton: true,
            confirmButtonText: "Yes, i am sure!"
        },function(){
            $('#approved').click();
        });
    }
});
$('#btndeny').click(function () {
    var content = $('#confirm_id').val();
    if(content == ''||content == null || content == ' '){
        swal('Please enter some remarks');
    }else{
        swal({
            title: "Are you sure?",
            text: "This Expense will be denied!",
            icon: "warning",
            dangerMode: true,
            showCancelButton: true,
            confirmButtonText: "Yes, i am sure!"
        },function(){
            $('#denied').click();
        });
    }
});
@endsection
