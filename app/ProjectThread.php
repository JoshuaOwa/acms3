<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectThread extends Model
{
    protected $table = 'project_threads';

    protected $fillable = [

    	'contact_id',
    	'project_id',    	
    	'message',
    	'user_id',
    	'main',
    	'comment_to'
    ];

    public function user(){

        return $this->hasOne('App\User','id','user_id');
    }

    public static function savePayload($payload){

        $thread = static::query()->create($payload);

        return $thread->id;
    }

    public static function byProject($proj_id){

        $threads = self::where('project_id',$proj_id)->where('main',1)->orderBy('updated_at','DESC')->get();

        if(!empty($threads)){

            foreach ($threads as $key => $thread) {
                                
                $threads[$key]->files = ProjectThreadFile::byThread($thread->id);
                $threads[$key]->comments = ProjectThread::byMainThread($thread->id);                
            }
        }

        return $threads;
    }

    public static function byMainThread($thread_id){

        $comments = self::where('main',0)->where('comment_to',$thread_id)->orderBy('updated_at','DESC')->get();

        foreach ($comments as $key => $comment) {
            
            $comments[$key]->files = ProjectThreadFile::byThread($comment->id);
        }

        return $comments;
    }
}
