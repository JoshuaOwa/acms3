<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Announcement extends Model
{
    protected $table = 'announcements';

    protected $fillble = [

    	'subject',
    	'message'
    ];

    public static function getLatest($id){

    	$anns = self::where('id',$id)->first();

    	$files = AnnouncementFile::where('ann_id',$anns->id)->get();

    	if(!empty($files)){
    		$anns->files = $files;
    	}else{
    		$anns->files = "";
    	}

    	return $anns;
    }
}
