<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{       
    protected $table = 'contacts';

    protected $fillable = [   	
    	'created_by',
        'contact_type_id',
    	'approver_id',        
		'address',		    	
    	'status',    	        
        'industry',        
        'company',
        'comp_address',
        'encryptname'
    ];    

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'created_by');    
    }

    public function approver(){

        return $this->hasOne('App\User','id','approver_id');
    }

    public function contacttype()
    {
        return $this->hasOne('App\ContactType','id','contact_type_id');
    }

    public static function createdByMe($id){

        return self::where('created_by',$id)->get();
    }

    public static function savePayload($payload){

        $contact = static::query()->create($payload);

        return $contact->id;
    }
    
    public static function byEncrypt($encrypt){

        $contact = self::where('encryptname',$encrypt)->first();

        $contact['person'] = ContactPerson::where('contact_id',$contact->id)->first();

        return $contact;
    }

    public static function byEncrypt2($encrypt){

        $contact = self::where('encryptname',$encrypt)->first();

        $contact['person'] = ContactPerson::where('contact_id',$contact->id)->get();

        return $contact;
    }

    public static function updatePayload($payload,$id){

        $contact = Contact::findOrFail($id);
        $contact->fill($payload)->save();

        return $contact->id;
    }

    public static function filter($id,$status){

        return self::where('status',$status)->get();
        // return self::where('created_by',$id)->where('status',$status)->get();
    }

    public static function forApproval(){

        $contacts =  self::where('status','<',1)->orWhere('status',3)->get();

        foreach ($contacts as $key => $value) {
            
            $contacts[$key]->person = ContactPerson::where('contact_id',$value->id)->first();
        }

        return $contacts;
    }

    public static function createdAndApproved($id){

        return self::where('status','!=',2)->where('status','!=',0)->pluck('company','id');
        // return self::where('created_by',$id)->where('status','!=',2)->where('status','!=',0)->pluck('contact_person','id');
    }

    public static function filterBy($status){

        if($status < 1){

            return self::where('status',0)->orWhere('status',3)->get();
        }else{

            return self::where('status',$status)->get();
        }        
        // return self::where('created_by',$id)->where('status',$status)->get();
    }

    public static function getAll(){

        $contacts = self::all();

        foreach ($contacts as $key => $value) {
            
            $contacts[$key]->person = ContactPerson::where('contact_id',$value->id)->get();
        }

        return $contacts;
    }

    public static function getList(){

        return self::where('status','!=',2)->where('status','!=',0)->pluck('company','id');
    }
}
