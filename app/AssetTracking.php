<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssetTracking extends Model
{
    protected $table = 'asset_trackings';

    protected $fillable = [

    	'barcode',
    	'item_name',
    	'io',
    	'remarks',
    	'holder',
        'must_date',
        'returned_date'
    

    ];

    public function user(){

    	return $this->hasOne('App\User','id','holder');
    }

    public static function byBarcode($barcode){

    	return self::where('barcode',$barcode)->first();
    }

    public static function byCreated($id){

        return self::where('user_id',$id)->get();
    }

    public static function getList(){

        return AssetTracking::all()->pluck('item_name','item_name');
    }

    public static function savePayload($payload){

        $asset = static::query()->create($payload);

        return $asset;
    }
}
