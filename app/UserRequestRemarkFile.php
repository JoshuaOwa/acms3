<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRequestRemarkFile extends Model
{
    protected $table = 'user_request_remark_files';

    protected $fillable = [

    	'request_id',
		'filename',
		'encryptname',
		'download',
		'uploaded_by'
    ];

    public static function byRequest($id){

    	return self::where('request_id',$id)->get();
    }

    public static function ByEncryptName($enc){

        return self::where('encryptname',$enc)->first();
    }

    public static function byRemark($id){

        return self::where('remark_id',$id)->get();
    }

}
