<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
    protected $table = 'expenses';

    protected $fillable = [

    	'contact_id',
    	'project_id',
    	'timetable_id',
    	'task_id',
    	'job_order_id',
    	'user_id',
    	'particular',
    	'particular_type_id',
    	'date',
    	'amount',
    	'qty',
        'total_amount',
    	'status',
    	'approved_by',
        'encryptname'
    ];

    public function user(){

        return $this->hasOne('App\User','id','user_id');
    }
    public function type(){

        return $this->hasOne('App\ParticularType','id','particular_type_id');
    }
    public function project(){

        return $this->hasOne('App\Project','id','project_id');
    }


    public static function savePayload($payload){

        $expense = static::query()->create($payload);

        return $expense;
    }

    public static function byJobOrder($job_id){

        return self::where('job_order_id',$job_id)->get();
    }

    public static function byJobOrderForApproval($job_id){

        return self::where('job_order_id',$job_id)->where('status',1)->sum('total_amount');
    }

    public static function byJobOrderApproved($job_id){

        return self::where('job_order_id',$job_id)->where('status',2)->sum('total_amount');
    }

    public static function findAllForApproval(){

        return self::where('status','<',2)->orderBy('created_at','DESC')->get();
    }

    public static function byEncrypted($encrypt){

        return self::where('encryptname',$encrypt)->first();
    }
}
