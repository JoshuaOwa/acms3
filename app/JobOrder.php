<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Department;
use App\UserIndustry;
class JobOrder extends Model
{
    protected $table = 'job_orders';

    protected $fillable = [

    	'contact_id',
    	'contact_person',
    	'project_id',
    	'project_name',    	
    	'created_by',
    	'assigned_to',
    	'assigned_name',
    	'jo_workgroup',
    	'start_date',
    	'end_date',
    	'details',
    	'status',
        'rating',
        'rating_remarks',    	
        'encryptname',
        'timetable_id',
        'task_id',
        'onetime',
        'return_date',
        'type',
        'recuring',
        'subject',                
    ];

    public function client(){

        return $this->hasOne('App\Contact','id','contact_id');
    }

    public function proj(){

        return $this->hasOne('App\Project','id','project_id');
    }

    public function user(){

        return $this->hasOne('App\User','id','created_by');
    }

    public function assigned(){

        return $this->hasOne('App\User','id','assigned_to');
    }

    public static function savePayload($payload){

        $job_order = static::query()->create($payload);

        return $job_order->id;
    }

    public static function updatePayload($payload,$id){

        $job_order = JobOrder::findOrFail($id);
        $job_order->fill($payload)->save();

        return $job_order->id;
    }

    public static function byRequest($reqid){

        return self::where('main_request_id',$reqid)->orderBy('id','DESC')->get();
    }

    public static function byProject($id){

        return self::where('project_id',$id)->get();
    }

    public static function byProjectAndUser($project_id, $user_id){

        return self::where('project_id',$project_id)->where('assigned_to',$user_id)->get();
    }

    public static function byEncrypt($encryptname){

        return self::where('encryptname',$encryptname)->first();
    }

    public static function byJO($id){

        return self::where('id',$id)->first();
    }

    public static function byUser($user){

        $jobID = [];
        $job_main =  self::where('assigned_to',$user->id)->get();

        foreach($job_main as $jobm){

            $jobIDp[] = $jobm->id;
        }

        $jobD = JobOrderDetail::where('reciever_id',$user->id)->get();

        foreach ($jobD as $job) {
            $jobID[] = $job->job_order_id;
        }

        return self::whereIn('id',$jobID)->groupBy('id')->get();

        
    }

    public static function byMonitor($dateToday){

        $job = self::where('status',1)->where('type',1)->where('start_date','<=',$dateToday)->where('end_date','>=',$dateToday)->orderBy('id','desc')->get();

        $jobid = [];
        foreach ($job as $key => $value) {
            $jobid[] = $value->id;
        }

        $jobdet = JobOrderDetail::whereIn('job_order_id',$jobid)->orderBy('job_order_id','DESC')->get();

        foreach ($jobdet as $key => $value) {
            
            $user = User::find($value->reciever_id);
            $jobs = JobOrder::find($value->job_order_id);

            $jobdet[$key]->job_id = $value->job_order_id;
            $jobdet[$key]->assigned_name = strtoupper($user->first_name.' '.$user->last_name);
            $jobdet[$key]->subject = $jobs->subject;
            $jobdet[$key]->start_date = $jobs->start_date;
            $jobdet[$key]->end_date = $jobs->end_date;

        }

        return $jobdet;

    }

    public static function byAssigned($uid){

        return self::where('created_by',$uid)->orderBy('status','DESC')->get();
    }

    public static function byDept($dept_id){        
        // $users = User::where('dept_id',$dept_id)->get();
        $users = UserIndustry::where('industry_id',$dept_id)->get();
                
        $uid = [];

        foreach($users as $user){

            $uid[] = $user->user_id;
        }
        $uids = array_unique($uid);        
        return self::whereIn('assigned_to',$uids)->get();
    }

    public static function allEmployee(){

        $users = User::select(DB::raw("CONCAT(first_name,' ',last_name) as full_name ,id"))->where('active',1)->get();
        $date_now = date('Y-m-d');

        foreach ($users as $key => $value) {
            
            $ongoings = 0;
            $returned = 0;
            $closed = 0;
            $overdue = 0;

            $ind = UserIndustry::select('industry_id')->where('user_id',$value->id)->first();
            
            $department = Department::select('department')->where('id',$ind['industry_id'])->first();
            
            $users[$key]->department = $department['department'];

            $mainJO = JobOrder::where('assigned_to',$value->id)->get();
            
            $subJo = JobOrderDetail::where('reciever_id',$value->id)->groupBy('job_order_id')->get();
            $subJoCnt = count($subJo);
            
            $jobInId = [];

            foreach($mainJO as $job){

                $jobInId[] = $job->id;
            }

            foreach($subJo as $sjob){

                $jobInId[] = $sjob->job_order_id;
            }
            
            $jobsID = array_unique($jobInId);

            $mainJOCnt = count($jobsID);

            $jobOrders = JobOrder::whereIn('id',$jobsID)->get();          

            $ongoings = JobOrder::whereIn('id',$jobsID)->where('status',1)->count();
            $returned = JobOrder::whereIn('id',$jobsID)->where('status',2)->count();
            $closed = JobOrder::whereIn('id',$jobsID)->where('status',3)->count();

            foreach($jobOrders as $job_order){

                if($date_now > $job_order->end_date AND $job_order->status == 1){
                    $overdue++;
                }                                                                
                elseif($job_order->status > 1 AND $job_order->return_date > $job_order->end_date){
                    $overdue++;
                }else{

                }                                                             

                // if($job_order->status ==1){
                //     $ongoings++;
                // }elseif($job_order->status ==2){
                //     $returned++;
                // }else{
                //     $closed++;
                // }
            }

            $users[$key]->totalJO = $mainJOCnt;            
            $users[$key]->ongoings = $ongoings;
            $users[$key]->returned = $returned;
            $users[$key]->closed = $closed;
            $users[$key]->overdue = $overdue;

        }
        
        return $users;
    }

    public static function perEmployee($uids){

        $users = User::select(DB::raw("CONCAT(first_name,' ',last_name) as full_name ,id"))->whereIn('id',$uids)->where('active',1)->get();
        $date_now = date('Y-m-d');

        foreach ($users as $key => $value) {
            
            $ongoings = 0;
            $returned = 0;
            $closed = 0;
            $overdue = 0;

            $mainJO = JobOrder::where('assigned_to',$value->id)->get();
            $mainJOCnt = count($mainJO);
            $subJo = JobOrderDetail::where('reciever_id',$value->id)->groupBy('job_order_id')->get();
            $subJoCnt = count($subJo);
            
            $jobInId = [];

            foreach($mainJO as $job){

                $jobInId[] = $job->id;
            }

            foreach($subJo as $sjob){

                $jobInId[] = $sjob->job_order_id;
            }
            
            $jobsID = array_unique($jobInId);

            $jobOrders = JobOrder::whereIn('id',$jobsID)->get();            

            $ongoings = JobOrder::whereIn('id',$jobsID)->where('status',1)->count();
            $returned = JobOrder::whereIn('id',$jobsID)->where('status',2)->count();
            $closed = JobOrder::whereIn('id',$jobsID)->where('status',3)->count();

            foreach($jobOrders as $job_order){

                if($date_now > $job_order->end_date AND $job_order->status == 1){
                    $overdue++;
                }                                                                
                elseif($job_order->status > 1 AND $job_order->return_date > $job_order->end_date){
                    $overdue++;
                }else{

                }                                                             
            }



            $users[$key]->totalJO = $mainJOCnt;
            $users[$key]->totalsubJO = $subJoCnt;
            $users[$key]->ongoings = $ongoings;
            $users[$key]->returned = $returned;
            $users[$key]->closed = $closed;
            $users[$key]->overdue = $overdue;

        }
        
        return $users;
    }

    public static function detailedByUser($uid){

        $user = User::find($uid);
        $allJOBM = JobOrder::where('assigned_to',$uid)->get();
        $allJobDets = JobOrderDetail::where('reciever_id',$uid)->get();
        $ongoingJobID = [];
        $returnJobID = [];
        $closedJobID = [];

        $alljbids = [];

        foreach($allJobDets as $d){
            $alljbids[] = $d->job_order_id;
        }
        
        $allJOBS = JobOrder::whereIn('id',$alljbids)->get();


        foreach($allJOBM as $alljbm){

            if($alljbm->status == 1){

                $ongoingJobID[] = $alljbm->id;

            }elseif($alljbm->status == 2){

                $returnJobID[] = $alljbm->id;
            }else{

                $closedJobID[] = $alljbm->id;
            }
        }

        foreach($allJOBS as $alljbs){

            if($alljbs->status == 1){

                $ongoingJobID[] = $alljbs->id;

            }elseif($alljbs->status == 2){

                $returnJobID[] = $alljbs->id;
            }else{

                $closedJobID[] = $alljbs->id;
            }
        }
        
        $uniqueOnJob = array_unique($ongoingJobID);

        $user->jobOngoings = JobOrder::whereIn('id',$uniqueOnJob)->get();
      
        $uniqueRetJob = array_unique($returnJobID);

        $user->jobReturns = JobOrder::whereIn('id',$uniqueRetJob)->get();
    
        $uniqueCloJob = array_unique($closedJobID);

        $user->jobClosed = JobOrder::whereIn('id',$uniqueCloJob)->get();

        return $user;
    }

    public static function getUnderProject($project_id){

        $job_orders =  self::where('project_id',$project_id)->get();

        foreach ($job_orders as $key => $value) {
            
            $usersID = [] ;

            if($value->assigned_to > 0){
                $usersID[] = $value->assigned_to;
            }            

            $details = JobOrderDetail::where('job_order_id',$value->id)->get();

            foreach ($details as $detail) {
                
                $usersID[] = $detail->reciever_id;
            }

            $uniqueUser = array_unique($usersID);

            $users = User::whereIn('id',$uniqueUser)->get();

            $userName = [];
            foreach($users as $user){

                $userName[] = strtoupper($user->first_name.' '.$user->last_name);
            }

            $job_orders[$key]->userName = implode(' - ', $userName);
        }

        return $job_orders;
    }

    public static function byContact($contact_id){

        $job_orders =  self::join('contacts','contacts.id','=','job_orders.contact_id')->whereIn('contact_id',$contact_id)->get();

        foreach ($job_orders as $key => $value) {
            
            $usersID = [] ;

            if($value->assigned_to > 0){
                $usersID[] = $value->assigned_to;
            }            

            $details = JobOrderDetail::where('job_order_id',$value->id)->get();

            foreach ($details as $detail) {
                
                $usersID[] = $detail->reciever_id;
            }

            $uniqueUser = array_unique($usersID);

            $users = User::whereIn('id',$uniqueUser)->get();

            $userName = [];
            foreach($users as $user){

                $userName[] = strtoupper($user->first_name.' '.$user->last_name);
            }

            $job_orders[$key]->userName = implode(' - ', $userName);
        }

        return $job_orders;
    }

    public static function byProjects($project_id){

        $job_orders =  self::join('projects','projects.id','job_orders.project_id')->whereIn('project_id',$project_id)->get();

        foreach ($job_orders as $key => $value) {
            
            $usersID = [] ;

            if($value->assigned_to > 0){
                $usersID[] = $value->assigned_to;
            }            

            $details = JobOrderDetail::where('job_order_id',$value->id)->get();

            foreach ($details as $detail) {
                
                $usersID[] = $detail->reciever_id;
            }

            $uniqueUser = array_unique($usersID);

            $users = User::whereIn('id',$uniqueUser)->get();

            $userName = [];
            foreach($users as $user){

                $userName[] = strtoupper($user->first_name.' '.$user->last_name);
            }

            $job_orders[$key]->userName = implode(' - ', $userName);
        }

        return $job_orders;
    }

    public static function byUsers($users){

        $jobID = [];
        $job_main =  self::whereIn('assigned_to',$users)->get();

        foreach($job_main as $jobm){

            $jobIDp[] = $jobm->id;
        }

        $jobD = JobOrderDetail::whereIn('reciever_id',$users)->get();

        foreach ($jobD as $job) {
            $jobID[] = $job->job_order_id;
        }

        $job_orders =  self::whereIn('id',$jobID)->groupBy('id')->get();

        foreach ($job_orders as $key => $value) {
            
            $usersID = [] ;

            if($value->assigned_to > 0){
                $usersID[] = $value->assigned_to;
            }            

            $details = JobOrderDetail::where('job_order_id',$value->id)->get();

            foreach ($details as $detail) {
                
                $usersID[] = $detail->reciever_id;
            }

            $uniqueUser = array_unique($usersID);

            $users = User::whereIn('id',$uniqueUser)->get();

            $userName = [];
            foreach($users as $user){

                $userName[] = strtoupper($user->first_name.' '.$user->last_name);
            }

            $job_orders[$key]->userName = implode(' - ', $userName);
        }

        return $job_orders;

        
    }

    // public static function allActive(){

    //     $users = User::where('active',1)->get();

    //     $uids = [];

    //     foreach ($users as $key => $value) {
            
    //         $uids[] = $value->id;
    //     }

    //     $allJOBM = JobOrder::where('assigned_to',$uids)->get();
    //     $allJobDets = JobOrderDetail::where('reciever_id',$uids)->get();        

        
        
    // }
    
}
