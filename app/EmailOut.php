<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailOut extends Model
{
    protected $table = 'email_outs';

    protected $fillable = [

    	'contact_id',
    	'project_id',
    	'to',
    	'cc',
    	'subject',
    	'message',    	
    ];

    public static function savePayload($payload){

        $email = static::query()->create($payload);

        return $email;
    }

    public static function byProject($proj_id){

        $emails = self::where('project_id',$proj_id)->orderBy('id','DESC')->get();

        return $emails;

    }

}
