<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectThreadFile extends Model
{
    protected $table = 'project_thread_files';

    protected $fillable = [

    	'contact_id',
    	'project_id',    	
    	'thread_id',
		'filename',
		'encrpytname',
		'download',
		'uploaded_by'
    ];

    public static function byThread($thread_id){

        return self::where('thread_id',$thread_id)->get();
    }

    public static function ByEncryptName($encrpytname){

        return self::where('encrpytname',$encrpytname)->first();
    }
}
