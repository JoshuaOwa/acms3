<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OverTimeSupervisor extends Model
{
    protected $table = 'over_time_supervisors';

    protected $fillable = [
    	'user_id','superior_id'
    ];

    public static function getMyUnderlings($id)
    {
    	return self::where('superior_id',$id)->pluck('user_id');
    }

    public static function findMine($id)
    {
        return self::where('user_id',$id)->get();
    }

    public static function findMySupervisors($id)
    {
    	return self::where('user_id',$id)->pluck('superior_id')->toArray();
    }
}
