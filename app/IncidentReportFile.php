<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IncidentReportFile extends Model
{
    protected $table = 'incident_report_files';

    protected $fillable = [

    	'contact_id',
    	'project_id',
    	'ir_id',
    	'filename',
    	'uploaded_by',
    	'encryptname'
    ];

    public static function ByEncryptName($encryptname){

        return self::where('encryptname',$encryptname)->first();
    }
}
