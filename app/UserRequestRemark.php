<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRequestRemark extends Model
{
    protected $table = 'user_request_remarks';

    protected $fillable = [

    	'request_id',
        'remark_id',
    	'user_id',
    	'remarks'
    ];

    public function user(){

    	return $this->hasOne('App\User','id','user_id');
    }

    public static function byRequest($id){

    	$remarks =  self::where('request_id',$id)->orderBy('id','DESC')->get();

        foreach ($remarks as $key => $value) {
            
            $remarks[$key]->files = UserRequestRemarkFile::byRemark($value->id);
        }        
        return $remarks;
    }

    public static function savePayload($payload){

        $remark = static::query()->create($payload);

        return $remark->id;
    }
}
