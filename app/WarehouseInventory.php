<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WarehouseInventory extends Model
{
    protected $table = 'warehouse_inventories';

    protected $fillable = [

    	'barcode',
    	'item',
    	'description',
    	'qty',
    	'user_id'
    ];

    public static function getAll(){

    	return self::all();
    }

    public static function savePayload($payload){

        $item = static::query()->create($payload);

        return $item->id;
    }

    public static function updatePayload($payload,$id){

        $item = WarehouseInventory::findOrFail($id);
        $item->fill($payload)->save();

        return $item->id;
    }

    public static function checkExist($bcode,$item){

        return self::where('barcode',$bcode)->where('item',$item)->first();
    }

    public static function forRelease($itemID,$for_release){
        
        $item = WarehouseInventory::findOrFail($itemID);

        if($item->qty >= $for_release){

            $item->qty = $item->qty-$for_release;
            $item->update();

            return 1;

        }else{

            return 2;
        }        
    }    

    public static function byBarcode($barcode){

        return self::where('barcode',$barcode)->first();
    }
}
