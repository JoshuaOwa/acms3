<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExpenseFile extends Model
{
    protected $table = 'expense_files';

    protected $fillable = [
    	'id','contact_id','project_id','timetable_id','task_id','job_order_id','expense_id','filename','encrpytname','download','uploaded_by'
    ];

    public function user()
    {
    	return $this->hasOne('App\User','id','uploaded_by');
    }

    public static function findByExpenseId($id)
    {
    	return self::where('expense_id',$id)->get();
    }
}
