<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectContact extends Model
{
    protected $table = 'project_contacts';

    protected $fillable = [

    	'contact_id',
    	'project_id',
    	'contact_person',
    	'function',
    	'email',
    	'tel_no',
    	'fax_number',
    	'mobile_number'    	

    ];

    public static function savePayload($payload){

        $project = static::query()->create($payload);

        return $project->id;
    }

    public static function updatePayload($payload,$id){

        $project = ProjectContact::where('contact_id',$id)->first();
        $project->fill($payload)->save();

        return $project->id;
    }
}
