<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Timetable extends Model
{
    protected $table = 'timetables';

    protected $fillable =[

    	'timetable'
    ];


    public static function savePayload($payload){

        $timetable = static::query()->create($payload);

        return $timetable;
    }

    public static function getAll(){

    	$times = self::all();

    	foreach ($times as $key => $value) {
    		
    		$times[$key]->taskcount = TimetableTask::where('timetable_id',$value->id)->count();
    	}

    	return $times;
    }    

    public static function getAllDetail($project){

        $timetables = self::all();  
        $cnt = 0;

        foreach ($timetables as $key => $value) {
            
            $initial1 = str_replace('.', '', $value->timetable);            
            $initial = str_replace(' ', '', $initial1);
            $timetables[$key]->tab = strtolower(preg_replace('/[^a-zA-Z0-9-_\.]/','-', $initial)); 
            $timetables[$key]->tasks = TimetableTask::where('timetable_id',$value->id)->get();
                    

            
        }
        
        foreach ($timetables as $key => $timetable) {
            foreach ($timetable->tasks as $key => $value) {
                
                $initial1 = str_replace('.', '', $value->task);
                $initial = str_replace(' ', '', $initial1);                         
                $timetable->tasks[$key]->tab = strtolower(preg_replace('/[^a-zA-Z0-9-_\.]/','-', $initial));

                $timetable->tasks[$key]->threads = JobOrder::where('contact_id',$project->contact_id)
                                                    ->where('project_id',$project->id)
                                                    ->where('timetable_id',$timetable->id)
                                                    ->where('task_id',$value->id)
                                                    ->get();                
            }            
        }

        foreach ($timetables as $key => $timetable) {
            foreach ($timetable->tasks as $key => $tasks) {
                foreach ($tasks->threads as $key => $value) {
                    
                    $tasks->threads[$key]->files = JobOrderFile::where('job_order_id',$value->id)->get();

                    $Jobthreads = JobOrderThread::where('job_order_id',$value->id)->where('main',1)->first();
                    $jobDet = JobOrderDetail::where('job_order_id',$value->id)->get();

                    $jobUid = [];
                    $uname = [];
                    if(!empty($jobDet)){
                        foreach($jobDet as $jdet){

                            $jobUid[] = $jdet->reciever_id;
                        }

                        $in_users = User::whereIn('id',$jobUid)->get();

                        foreach($in_users as $i_user){

                            $uname[] = strtoupper($i_user->first_name.' '.$i_user->last_name);
                        }
                    }
                    $jobUdet = implode(',', $uname);
                    if(!empty($Jobthreads->id)){
                        $tasks->threads[$key]->mainthreadid = $Jobthreads->id;
                        $tasks->threads[$key]->comments = JobOrderThread::byMainThread($Jobthreads->id);
                        $tasks->threads[$key]->assigned = $jobUdet;
                    }else{
                        $tasks->threads[$key]->mainthreadid = '';
                        $tasks->threads[$key]->comments = [];
                        $tasks->threads[$key]->assigned = $jobUdet;
                    }
                }
            }            
        }
        return $timetables;
    }
}
