<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
    protected $table = 'positions';
    protected $fillable = [

    	'position'
    ];

    public static function getList(){

    	return self::pluck('position','id');
    }
}
