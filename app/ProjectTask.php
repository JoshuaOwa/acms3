<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectTask extends Model
{
    protected $table = 'project_tasks';

    protected $fillable = [

    	'contact_id',
    	'project_id',
    	'timetable_id',
    	'task_id',
    	'encryptname'
    ];

    public function task(){

    	return $this->hasOne('App\TimetableTask','id','task_id');
    }

    public function projtimetable(){

        return $this->hasOne('App\ProjectTimetable','timetable_id','timetable_id');
    }

    public static function savePayload($payload){

        $task = static::query()->create($payload);

        return $task;
    }

    public static function deleteRecord($proj_id){

        self::where('project_id',$proj_id)->delete();
    }

    public static function byTimetable($timeid){

        return self::where('timetable_id',$timeid)->get();
    }

    public static function byEncryptWithDetails($encryptname){

        $task = self::where('encryptname',$encryptname)->first();

        $jobs = JobOrder::where('contact_id',$task->contact_id)
                    ->where('project_id',$task->project_id)
                    ->where('timetable_id',$task->timetable_id)
                    ->where('task_id',$task->task_id)
                    ->get();

        return $jobs;
    }

    public static function byEncrypt($encryptname){

        return self::where('encryptname',$encryptname)->first();        
    }
}
