<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRequestFile extends Model
{
    protected $table = 'user_request_files';

    protected $fillable = [

    	'request_id',
		'filename',
		'encrpytname',
		'download',
		'uploaded_by'
    ];

    public static function byRequest($id){

    	return self::where('request_id',$id)->get();
    }

    public static function ByEncryptName($enc){

        return self::where('encrpytname',$enc)->first();
    }
}
