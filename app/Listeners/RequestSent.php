<?php

namespace App\Listeners;

use App\Events\SendRequest;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RequestSent
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SendRequest  $event
     * @return void
     */
    public function handle(SendRequest $event)
    {
        //
    }
}
