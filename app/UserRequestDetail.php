<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRequestDetail extends Model
{
    protected $table = 'user_request_details';

    protected $fillable = [

    	'request_id',
    	'reciever_id',    	
    	'dept_id'
    ];

    public function user(){

        return $this->hasOne('App\User','id','reciever_id');
    }

    public static function getReceiver($request_id){

    	$users = self::where('request_id',$request_id)->get();
    	$uids = [];

    	foreach($users as $user){

    		$uids[] = $user->reciever_id;
    	}

    	return $uids;
    }

    public static function byReqID($reqid){

        return self::where('request_id',$reqid)->get();
    }

    public static function byReqUid($reqid,$reciever){

        return self::where('request_id',$reqid)->where('reciever_id',$reciever)->first();
    }
}
