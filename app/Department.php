<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
	protected $table = 'departments';

	protected $fillable = [

		'department',
		'head'
	];

	public function dhead(){

		return $this->hasOne('App\User','id','head');
	}

	public static function getList(){
        //use in projects for workgroup member adding
        //use in user request
    	return self::pluck('department','id');
    }

    public static function getAllId(){

    	$ids = self::get();

    	foreach ($ids as $key => $value) {
    		
    		$ids[$key]->users = User::getOnDepartment($value->id);
    	}

    	return $ids;
    }

    public static function getHead($id){

        return self::select('head')->where('id',$id)->first();
    }
  
}
