<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRequest extends Model
{
    protected $table = 'user_requests';

    protected $fillable = [

    	'request_code',
    	'request_no',
    	'requestor',
    	'contact_id',
    	'project_id',
    	'details',    	
    	'status',
        'start_date',
        'end_date',
        'recuring',
        'reqtype',
        'subject'

    ];    

    public function user(){

        return $this->hasOne('App\User','id','requestor');
    }

    public function con(){

        return $this->hasOne('App\Contact','id','contact_id');
    }

    public function proj(){

        return $this->hasOne('App\Project','id','project_id');
    }

    public static function updatePayload($payload,$id){

        $req = UserRequest::findOrFail($id);
        $req->fill($payload)->save();

        return $req->id;
    }

    public static function checkCode($code){

        return count(self::where('request_code',$code)->get());
    }

    public static function byUser($id){

        $req =  self::where('requestor',$id)->get();

        foreach ($req as $key => $value) {

            $reciever = UserRequestDetail::where('request_id',$value->id)->get();

            if(count($reciever) > 1){

                $names = [];

                foreach($reciever as $rec){

                    $user = User::findOrFail($rec->reciever_id);                    
                    $names[] = strtoupper($user->first_name.' '.$user->last_name);
                }

                $name = implode(',', $names);

            }else{

                foreach($reciever as $rec){

                    $user = User::findOrFail($rec->reciever_id);                                    
                    $name = strtoupper($user->first_name.' '.$user->last_name);
                }                                
            }

            $req[$key]->reciever = $name;

            //contact
            if($value->contact_id > 0){

                $con = Contact::findOrFail($value->contact_id);
                $req[$key]->contact = strtoupper($con->contact_person);
            }else{

                $req[$key]->contact = "N/A";
            }

            //project
            if($value->project_id > 0){

                $proj = Project::findOrFail($value->project_id);
                $req[$key]->project = strtoupper($proj->project_name);
            }else{

                $req[$key]->project = "N/A";
            }
        }

        return $req;
    }

    public static function forMe($id){

        $reqs =  self::join('user_request_details','user_request_details.request_id','=','user_requests.id')
            ->where('reciever_id',$id)
            ->get();

        foreach ($reqs as $key => $value) {
            
            $name = User::findOrFail($value->requestor);

            $reqs[$key]->requestor = strtoupper($name->first_name.' '.$name->last_name);

            //contact
            if($value->contact_id > 0){

                $con = Contact::findOrFail($value->contact_id);
                $reqs[$key]->contact = strtoupper($con->contact_person);
            }else{

                $reqs[$key]->contact = "N/A";
            }

            //project
            if($value->project_id > 0){

                $proj = Project::findOrFail($value->project_id);
                $reqs[$key]->project = strtoupper($proj->project_name);
            }else{

                $reqs[$key]->project = "N/A";
            }
        }

        return $reqs;

    }

    public static function byEncrypt($encrypt){

        $req =  self::where('request_code',$encrypt)->first();        

        if($req->contact_id > 0){

            $contact = Contact::findOrFail($req->contact_id);
            $req->contact = $contact->company;
        }else{

            $req->contact = "N/A";
        }   

        if($req->project_id > 0){

            $project = Project::findOrFail($req->project_id);
            $req->project = $project->project_name;
        }else{

            $req->project = "N/A";
        }

        $userReqDetails = UserRequestDetail::where('request_id',$req->id)->first();

        $industry = Department::find($userReqDetails->dept_id);

        $req->dept = $industry->department;

        return $req;
    }

    public static function byContact($contact){

        return self::where('contact_id',$contact)->get();
    }

    public static function byProject($proj_id){

        return self::where('project_id',$proj_id)->orderBy('created_at','ASC')->get();
    }
}
