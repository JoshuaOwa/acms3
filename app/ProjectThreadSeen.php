<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class ProjectThreadSeen extends Model
{
    protected $table = 'project_thread_seens';

    protected $fillable = [

    	'contact_id',
    	'project_id',    	
    	'thread_id',
    	'from_user',
    	'to_user',
    	'is_seen'    	
    ];

    public static function savePayload($payload){

        $users = ProjectWorkgroup::byProject($payload['project_id']);
        $myId = Auth::id();
        
        foreach ($users as $key => $value) {
            
            $payload['is_seen'] = 0;

            if($myId == $value->user_id){
                $payload['is_seen'] = 1;
            }

            $payload['to_user'] = $value->user_id;        
            static::query()->create($payload);
        }        
        return true;
    }

    public static function sendByProject($project_id,$uid){

        $not_sent = self::where('project_id',$project_id)->where('to_user',$uid)->get();
        
        foreach ($not_sent as $key => $value) {
            
            $postdata['contact_id'] = $value->contact_id;
            $postdata['project_id'] = $value->project_id;            
            $postdata['thread_id'] = $value->thread_id;
            $postdata['from_user'] = $value->from_user;
            $postdata['to_user'] = $uid;
            $postdata['is_seen'] = 0;
            static::query()->create($postdata);
        }

        return true;
    }
}
