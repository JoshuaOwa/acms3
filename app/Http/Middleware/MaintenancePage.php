<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\AccessControl;

class MaintenancePage
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $id = Auth::id();

        $module_id = [1,2,4,5,6,18];

        $access_control = AccessControl::CheckUserModuleMultiple($id, $module_id);

        if($access_control > 0){

            return $next($request);
        }
        else{           

            return redirect('restricted_page');
        }    
    }
}
