<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\AccessControl;

class AccessPage
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $id = Auth::id();

        $module_id = 3;

        $access_control = AccessControl::CheckUserModule($id, $module_id);

        if($access_control){

            return $next($request);
        }
        else{           

            return redirect('restricted_page');
        }    
    }
}
