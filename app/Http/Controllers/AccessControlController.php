<?php

namespace App\Http\Controllers;

use App\AccessControl;
use App\Modules;
use App\User;
use Illuminate\Http\Request;

class AccessControlController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = $request->get('uid');
        $postdata = $request->get('module_id');

        AccessControl::DeleteByUser($id);
        if(!empty($postdata)){

            foreach ($postdata as $postd) {         
                $access = new AccessControl();
                $access->user_id = $id;
                $access->module_id = $postd;
                $access->save();
            }
        }       
        
        return back()->with('is_success', 'Access was successfully saved');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AccessControl  $accessControl
     * @return \Illuminate\Http\Response
     */
    public function show(AccessControl $accessControl)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AccessControl  $accessControl
     * @return \Illuminate\Http\Response
     */
    public function edit(AccessControl $accessControl)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AccessControl  $accessControl
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AccessControl $accessControl)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AccessControl  $accessControl
     * @return \Illuminate\Http\Response
     */
    public function destroy(AccessControl $accessControl)
    {
        //
    }

    public function manage($uid){

        $user = User::find($uid);
        $access_controls = AccessControl::ByUser($uid);
        $modules = Modules::all();
        $access_control = [];        
        if(!empty($access_controls)){

            foreach($access_controls as $ac){

            $access_control[] = $ac->module_id;
            }
        }                
        return view('access_control.manage',compact('user','modules','access_control','uid'));
    }
}
