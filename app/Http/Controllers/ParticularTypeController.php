<?php

namespace App\Http\Controllers;

use App\ParticularType;
use Illuminate\Http\Request;
use Response;

class ParticularTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $types = ParticularType::all();

        return view('particular_types.index',compact('types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $type = new ParticularType($request->all());
        $type->save();

        return back()->with('is_success','Saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ParticularType  $particularType
     * @return \Illuminate\Http\Response
     */
    public function typeDetails(Request $request){

        $type = ParticularType::findOrFail($request->get('typeid'));

        return Response::json($type);
    }

    public function typeUpdates(Request $request){

        $type = ParticularType::findOrFail($request->get('typeid'));
        $type->type = $request->get('type');        
        $type->update();

        return back()->with('is_update','Updated!');
        
    }
}
