<?php

namespace App\Http\Controllers;

use App\RequestType;
use Illuminate\Http\Request;
use Response;

class RequestTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $types = RequestType::orderBy('type','ASC')->get();

        return view('request_type.index',compact('types'));
    }

    public function store(Request $request)
    {
        $type = new RequestType($request->all());
        $type->save();

        return back()->with('is_success','Saved!');
    }

    public function typeDetails(Request $request){

        $type = RequestType::findOrFail($request->get('typeid'));

        return Response::json($type);
    }

    public function typeUpdates(Request $request){

        $type = RequestType::findOrFail($request->get('type_id'));
        $type->type = $request->get('type');        
        $type->update();

        return back()->with('is_update','Updated!');
        
    }
}
