<?php

namespace App\Http\Controllers;

use App\WarehouseInventory;
use App\WarehouseInventoryFile;
use App\StockHistory;
use App\JobOrder;
use App\BillOfMaterial;
use Box\Spout\Reader\ReaderFactory;
use Illuminate\Support\Facades\Input;
use Box\Spout\Common\Type;
use Auth;
use File;
use Response;
use Illuminate\Http\Request;

class WarehouseInventoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = WarehouseInventory::getAll();

        return view('warehouse.inventory',compact('items'));
    }
   
    public function store(Request $request)
    {
        $postdata = $request->all();
        $postdata['user_id'] = Auth::id();

        $item = WarehouseInventory::savePayload($postdata);

        return back()->with('is_success','Save!');        
    }

    public function uploads(Request $request){

        $user = Auth::user();      

        $file = Input::file('attached');        

        $destinationPath = storage_path().'/uploads/warehouse/inventory/files';
        if (!\File::exists($destinationPath))
        {
            mkdir($destinationPath, 0755, true); 
        }  
        
        $filename = $file->getClientOriginalName();

        $file_name = pathinfo($filename, PATHINFO_FILENAME);

        $extension = File::extension($filename);
        $actual_name = $file_name.'.'.$extension;
        $file_path = $destinationPath.$actual_name;

        $count = 1;
        while(File::exists($file_path)) {
            $actual_name = $file_name.'_'.$count.'.'.$extension;
            $file_path = $destinationPath.$actual_name;
            $count++;
        }                    

        $file->move($destinationPath, $actual_name); 
        $filePath = $destinationPath ."/". $actual_name;
        $reader = ReaderFactory::create(Type::XLSX); // for XLSX files        
        $reader->open($filePath);
        foreach ($reader->getSheetIterator() as $sheet) {
            
            foreach ($sheet->getRowIterator() as $rowcount => $col) {
                        
                if($rowcount > 1){       
                    
                    if(empty($col[0])){
                        break;
                    }

                    $checkExist = WarehouseInventory::checkExist($col[0],$col[1]);
                    $postdata = [];
                    $postdata['barcode'] = $col[0];
                    $postdata['item'] = $col[1];
                    $postdata['description'] = $col[2];                    
                    $postdata['user_id'] = $user->id;

                    if(count($checkExist) > 0){

                        $postdata['qty'] = $checkExist->qty + $col[3];
                        $item = WarehouseInventory::updatePayload($postdata,$checkExist->id);
                    }else{                        
                        $postdata['qty'] = $col[3];                    
                        $item = WarehouseInventory::savePayload($postdata);
                    } 
                    $histLogs['item_id'] = $item;
                    $histLogs['logs'] = strtoupper($user->first_name.' '.$user->last_name).' ADDED '.$postdata['qty'].' PC/S';
                    StockHistory::savePayload($histLogs);
                }                               
            }
        }

        $hashname = \Hash::make($actual_name);

        $enc = str_replace("/","", $hashname);

        $item_file = new WarehouseInventoryFile();        
        $item_file->filename = $actual_name;
        $item_file->encrpytname = $enc;
        $item_file->download = 0;          
        $item_file->uploaded_by = $user->id;        
        $item_file->save();  

        return back()->with('is_upload','Save');
    }

    public function itemUpdates(Request $request){

        $user = Auth::user();

        $itemid = $request->itemid;
        $postdata = $request->all();

        $itemDet = WarehouseInventory::findOrFail($itemid);
        $item = WarehouseInventory::updatePayload($postdata,$itemid);

        $histLogs['item_id'] = $itemid;
        $histLogs['logs'] = strtoupper($user->first_name.' '.$user->last_name).' MODIFIED PC/S FROM '.$itemDet->qty.' TO '.$request->qty;

        StockHistory::savePayload($histLogs);
        
        return back()->with('item_update','Updated!');        
    }

    public function itemDetails(Request $request){
        
        $item = WarehouseInventory::findOrFail($request->get('itemid'));

        return Response::json($item);
    }
    
    public function release(Request $request){

        $user = Auth::user();

        $for_release = $request->qty;
        $itemID = $request->itemid;
        $job = JobOrder::byJO($request->job_id);
        $bom = BillOfMaterial::checkInProject($job,$itemID);        

        if(count($bom) > 0){

            $item = WarehouseInventory::forRelease($itemID,$for_release);

            if($item > 1){

                return back()->with('release_exceed','Updated!');     
            }else{

                $histLogs['item_id'] = $itemID;
                $histLogs['logs'] = strtoupper($user->first_name.' '.$user->last_name).' RELEASED '.$request->qty.' PC/S FOR JOB ORDER #: '.$request->job_id;

                StockHistory::savePayload($histLogs);

                BillOfMaterial::deduction($bom,$for_release);

                return back()->with('item_release','Updated!');     
            }
        }else{
            
            return back()->with('item_error','Updated!');  
        }        
    }

    public function history($barcode){

        $item = WarehouseInventory::byBarcode($barcode);

        $histories = StockHistory::byItem($item);

        return view('warehouse.histories',compact('histories','item'));
    }
}
