<?php

namespace App\Http\Controllers;

use App\EmailOut;
use App\Project;
use App\Contact;
use Auth;
use Illuminate\Http\Request;

class EmailOutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $postdata = $request->all();
        $postdata['message'] = nl2br($request->get('message'));

        $email = EmailOut::savePayload($postdata);

        $email_adds = explode(';', $postdata['to']);
        $to_email = [];

        $sender = Auth::user();

        foreach($email_adds as $email){

            if(filter_var($email, FILTER_VALIDATE_EMAIL)) {

                $to_email[] = $email;
            }   
        }

        \Mail::send('emails.emai_out',
            [
                "mess"=>$postdata['message'],
                "subject" => $postdata['subject']
                
            ],function($m) use($sender,$to_email,$postdata){
            $m->from($sender->email,strtoupper($sender->first_name.' '.$sender->last_name));
            $m->to($to_email)->subject($postdata['subject']);                               
        });                        

        return back()->with('email_sent','Sent');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EmailOut  $emailOut
     * @return \Illuminate\Http\Response
     */
    public function show(EmailOut $emailOut)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EmailOut  $emailOut
     * @return \Illuminate\Http\Response
     */
    public function edit(EmailOut $emailOut)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EmailOut  $emailOut
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EmailOut $emailOut)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EmailOut  $emailOut
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmailOut $emailOut)
    {
        //
    }
}
