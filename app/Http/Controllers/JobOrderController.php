<?php

namespace App\Http\Controllers;

use App\JobOrder;
use App\JobOrderFile;
use App\ScheduleJobOrder;
use App\JobOrderWorkgroup;
use App\JobOrderThread;
use App\JobOrderLog;
use App\User;
use App\Contact;
use App\Project;
use App\JobOrderDetail;
use App\JobOrderCc;
use App\Department;
use App\Expense;
use App\ParticularType;
use App\UserNotification;
use App\ReminderUpdate;
use App\UserIndustry;
use App\OverTime;
use App\OverTimeSupervisor;
use Auth;
use Date;
use Response;
use File;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class JobOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $user = Auth::user();        
        $jobs = JobOrder::byUser($user);
        $assigned = JobOrder::byAssigned($user->id);
        $date_now = Date('m/d/Y');
        $ratings = [
            '1'=>'1',
            '2'=>'2',
            '3'=>'3',
            '4'=>'4',
            '5'=>'5',
            '6'=>'6',
            '7'=>'7',
            '8'=>'8',
            '9'=>'9',
            '10'=>'10',
        ];        
        
        return view('job_orders.index',compact('jobs','date_now','assigned','ratings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $scopes = ['0'=>'Internal','1'=>'External'];
        $departments = Department::orderBy('department','ASC')->pluck('department','id');        
        $date_now = Date('m/d/Y');                
        $users = [];

        return view('job_orders.create',compact('scopes','departments','date_now','users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $user = Auth::user();
        $hash = \Hash::make($request->details);
        // $reciever = User::findOrFail($request->assigned_to);
        $project = Project::findOrFail($request->project_id);
        $postdata = $request->all();                
        $postdata['created_by'] = $user->id;        
        $postdata['encryptname'] = str_replace("/","", $hash);
        $postdata['assigned_name'] = "";//strtoupper($reciever->first_name.' '.$reciever->last_name);
        $postdata['start_date'] = Date('Y-m-d',strtotime($request->start_date));
        $postdata['end_date'] = Date('Y-m-d',strtotime($request->end_date));
        $postdata['contact_person'] = $project->contact->contact_person;
        $postdata['project_name'] = $project->project_name;
        $postdata['details'] = nl2br($request->details);
        $type = $request->get('settings');        
        $postdata['type'] = $request->get('type2');
        $counter = 0;        

        $joborder = JobOrder::savePayload($postdata);
        $jobfind = JobOrder::find($joborder);
        $postdata['onetime'] = 1;               
        foreach($postdata['reciever'] as $reciever){

            $user = User::findOrFail($reciever);
            $job_order_details = new JobOrderDetail();
            $job_order_details->job_order_id = $jobfind->id;
            $job_order_details->reciever_id = $reciever;
            $job_order_details->dept_id = $user->dept_id;                
            $job_order_details->save();
                           
            if($jobfind->recuring == 1){
                
                $upd = "DAILY";
                $date1 = date_create($request->start_date);
                $date2 = date_create($request->end_date);
                $counter2=date_diff($date1,$date2);  
                $counter = $counter2->days;

                if($counter < 1){
                    $counter=1;
                }
            }
            elseif($jobfind->recuring == 2){

                $upd = "WEEKLY";                    
                $endDate = strtotime($request->end_date);
                $days=array('1'=>'Monday','2' => 'Tuesday','3' => 'Wednesday','4'=>'Thursday','5' =>'Friday','6' => 'Saturday','7'=>'Sunday');
                
                for($i = strtotime(Date('l',strtotime($request->start_date)), strtotime($request->start_date)); $i <= $endDate; $i = strtotime('+1 week', $i))
                
                $date_array[]=date('Y-m-d',$i);
                $cntdate = count($date_array);
                
                if($cntdate < 1){
                     $counter=1;
                }else{
                     $counter=$cntdate-1;
                }
            }
            else{
                
                $upd = "MONTHLY";
                $start = new \DateTime($request->start_date);
                $end = new \DateTime($request->end_date);
                $counter = $start->diff($end)->m;

                if($counter < 1){
                    $counter=1;
                }
            }                            
            

            $email_adds[] = $user->email.'^'."JOB ORDER #: ".$jobfind->id.'^'.$jobfind->start_date.' - '.$jobfind->end_date.'^'.strtoupper($user->first_name.' '.$user->last_name).'^'.$upd;
            $sender = Auth::user();
            $user_notificaton = new UserNotification();
            $user_notificaton->user_id = $reciever;
            $user_notificaton->details = strtoupper($sender->first_name.' '.$sender->last_name).' TAGGED YOU IN A JOB ORDER';
            $user_notificaton->ref_link = "UR-".$jobfind->id;
            $user_notificaton->save();                
            $post2['info'] = 'JO-'.$jobfind->id;
            $post2['link'] = $jobfind->encryptname;

            $post2['last_update'] = Date('Y-m-d H:i:s',strtotime($jobfind->updated_at));

            $post2['cnt_need'] = $counter;
            $post2['cnt_donw'] = 0;
            $post2['type'] = $jobfind->recuring;
            $post2['user_id'] = $reciever;
            $post2['created_by'] = Auth::id();       
            
            $reminder = ReminderUpdate::savePayload($post2);

            $jobWorkgroup['contact_id'] = $request->contact_id;
            $jobWorkgroup['project_id'] = $request->project_id;
            $jobWorkgroup['job_order_id'] = $joborder;
            $jobWorkgroup['user_id'] =  $reciever;
            $jobWorkgroup['workgroup_name'] = $project->project_name.' - JO:'.$joborder.' WORKGROUP';
            $jobWorkgroup['added_by'] = Auth::id();
            $jobWorkgroup['position'] = $user->position->position;

            JobOrderWorkgroup::savePayload($jobWorkgroup);
        }
        
        $cntFile = 0;
        $destinationPath = "";
        $filesSending = [];                            
        if($request->hasFile('attached')){               
            $files = Input::file('attached');                        
            if(!empty($files)){
                $cntFile = 1;
                $destinationPath = storage_path().'/uploads/joborders/new/'.$joborder;
                if (!\File::exists($destinationPath))
                {
                    mkdir($destinationPath, 0755, true); 
                }  

                foreach ($files as $file) {
                    
                    $filename = $file->getClientOriginalName();                    

                    $file_name = pathinfo($filename, PATHINFO_FILENAME);

                    $extension = File::extension($filename);
                    $actual_name = $file_name.'.'.$extension;
                    $file_path = $destinationPath.$actual_name;

                    $count = 1;
                    while(File::exists($file_path)) {
                        $actual_name = $file_name.'_'.$count.'.'.$extension;
                        $file_path = $destinationPath.$actual_name;
                        $count++;
                    }                    

                    $file->move($destinationPath, $actual_name);   

                    $hashname = \Hash::make($filename);

                    $enc = str_replace("/","", $hashname);

                    $jo_file = new JobOrderFile();
                    $jo_file->contact_id = 0;
                    $jo_file->project_id = 0;
                    $jo_file->timetable_id = 0;             
                    $jo_file->task_id = 0;
                    $jo_file->job_order_id = $joborder;                          
                    $jo_file->filename = $filename;
                    $jo_file->encrpytname = $enc;
                    $jo_file->download = 0;          
                    $jo_file->uploaded_by = Auth::id();        
                    $jo_file->save();            

                    $filesSending[] = $filename;          
                }                      
            }                                 
        }               

        
        $loggedUser = Auth::user();
        $job_logs['contact_id'] = 0;
        $job_logs['project_id'] = 0;
        $job_logs['timetable_id'] = 0;
        $job_logs['task_id'] = 0;
        $job_logs['job_order_id'] = $joborder;
        $job_logs['user_id'] = $loggedUser->id;
        $job_logs['logs'] = strtoupper($loggedUser->first_name.' '.$loggedUser->last_name).' CREATED THE JOB ORDER.';

        JobOrderLog::savePayload($job_logs);

        $job = JobOrder::find($joborder);

        $postJob['contact_id'] = $job->contact_id;
        $postJob['project_id'] = $job->project_id;
        $postJob['timetable_id'] = $job->timetable_id;
        $postJob['task_id'] = $job->task_id;
        $postJob['job_order_id'] = $joborder;       
        $postJob['user_id'] = Auth::id();
        $postJob['message'] = nl2br($request->message);

        $thread = JobOrderThread::savePayload($postJob);

        if(!empty($request->get('cc'))){

                $reqID = $jobfind->id;
                foreach($request->get('cc') as $ccs) {

                    $cc = new JobOrderCc();                    
                    $cc->job_order_id = $reqID;
                    $cc->user_id = $ccs;

                    $cc->save();                    
                }
            }
            if(!empty($email_adds)){                
                foreach($email_adds as $email){

                    $breakdown = explode('^', $email);                    
                    if(filter_var($breakdown[0], FILTER_VALIDATE_EMAIL)) {
                        
                        \Mail::send('emails.user_job',
                            [
                                "data"=>$postdata['details'],
                                "range"=>$breakdown[2],                         
                                "name"=>$breakdown[3],
                                "needupdate" =>$breakdown[4]
                            ],function($m) use($sender,$breakdown,$cntFile,$destinationPath,$filesSending){
                            $m->from($sender->email,strtoupper($sender->first_name.' '.$sender->last_name));
                            $m->to($breakdown[0])->subject($breakdown[1]);           
                            if($cntFile > 0){
                                foreach($filesSending as $fsending){

                                    $m->attach($destinationPath.'/'.$fsending);         
                                }                                
                            }
                        
                        });                        
                    }             
                }
            }
            
        // if($type < 1){            
        //     $postdata['onetime'] = 1;            
        // }                

        // if($type != 3){
        //     $joborder = JobOrder::savePayload($postdata);
        // }

        // if($type == 1) {
            
        //     $date1 = date_create($request->start_date);
        //     $date2 = date_create($request->end_date);

        //     $counter=date_diff($date1,$date2);  

        //     if($counter->days > 0){
        //         $begin = new \DateTime($request->start_date);
        //         $end = clone $begin;

        //         // this will default to a time of 00:00:00    
        //         $end->modify($request->end_date); // 2016-07-08

        //         $end->setTime(0,0,1);

        //         $period = new \DatePeriod(
        //             $begin,
        //             new \DateInterval('P1D'),
        //             $end
                     
        //         );
        //         $today = Date('Y-m-d');
        //         foreach($period as $per){                
                    
        //             $postdata['date'] = $per->format('Y-m-d');
        //             if($today != $postdata['date']){
        //                 $multipleJo = ScheduleJobOrder::savePayload($postdata);
        //             }                 
        //         }
        //     }
        // }elseif ($type == 2) {
            
        //     $start = new \DateTime($request->start_date);
        //     $end = new \DateTime($request->end_date);

        //     $counter = $start->diff($end)->m;
            
        //     $start_month = Date('m',strtotime($request->start_date));
        //     if($counter > 0){
        //         $cnt = 1;
        //         for ($i=0; $i < $counter; $i++) { 
                    
        //             $d = new \DateTime($request->start_date);
        //             $next_month = $d->add(new \DateInterval('P'.$cnt.'M'));
        //             $postdata['date'] = $next_month->format('Y-m-d');
        //             $multipleJo = ScheduleJobOrder::savePayload($postdata);

        //             $cnt++;
        //         }
        //     }
        // }elseif ($type == 3) {
        //     $date_array = [];
        //     $day_number = $request->weeks;
        //     $endDate = strtotime($request->end_date);
        //     $days=array('1'=>'Monday','2' => 'Tuesday','3' => 'Wednesday','4'=>'Thursday','5' =>'Friday','6' => 'Saturday','7'=>'Sunday');
        //     for($i = strtotime($days[$day_number], strtotime($request->start_date)); $i <= $endDate; $i = strtotime('+1 week', $i))
        //     $date_array[]=date('Y-m-d',$i);            
            
        //     $today = Date('Y-m-d');            
        //     if(!empty($date_array)){

        //         foreach($date_array as $dates){

        //             if($today != $dates){

        //                 $postdata['date'] = $dates;
        //                 $multipleJo = ScheduleJobOrder::savePayload($postdata);
        //             }else{
        //                 $joborder = JobOrder::savePayload($postdata);  
        //             }
        //         }    
        //     }else{
        //         $joborder = JobOrder::savePayload($postdata);  
        //     }
        // }            
        

        // if($request->hasFile('attached')){                 
        //     $files = Input::file('attached');                        
        //     if(!empty($files)){
        //         $destinationPath = storage_path().'/uploads/joborders/'. $postdata['timetable_id'].'/'.$postdata['task_id'].'/'.$joborder;
        //         if (!\File::exists($destinationPath))
        //         {
        //             mkdir($destinationPath, 0755, true); 
        //         }  

        //         foreach ($files as $file) {
                    
        //             $filename = $file->getClientOriginalName();                    

        //             $file_name = pathinfo($filename, PATHINFO_FILENAME);

        //             $extension = File::extension($filename);
        //             $actual_name = $file_name.'.'.$extension;
        //             $file_path = $destinationPath.$actual_name;

        //             $count = 1;
        //             while(File::exists($file_path)) {
        //                 $actual_name = $file_name.'_'.$count.'.'.$extension;
        //                 $file_path = $destinationPath.$actual_name;
        //                 $count++;
        //             }                    

        //             $file->move($destinationPath, $actual_name);   

        //             $hashname = \Hash::make($filename);

        //             $enc = str_replace("/","", $hashname);

        //             $jo_file = new JobOrderFile();
        //             $jo_file->contact_id = $postdata['contact_id'];
        //             $jo_file->project_id = $postdata['project_id'];
        //             $jo_file->timetable_id = $postdata['timetable_id'];             
        //             $jo_file->task_id = $postdata['task_id'];
        //             $jo_file->job_order_id = $joborder;                          
        //             $jo_file->filename = $filename;
        //             $jo_file->encrpytname = $enc;
        //             $jo_file->download = 0;          
        //             $jo_file->uploaded_by = Auth::id();        
        //             $jo_file->save();                      
        //         }                      
        //     }                                 
        // }               

        // $jobWorkgroup['contact_id'] = $request->contact_id;
        // $jobWorkgroup['project_id'] = $request->project_id;
        // $jobWorkgroup['job_order_id'] = $joborder;
        // $jobWorkgroup['user_id'] = $reciever->id;
        // $jobWorkgroup['workgroup_name'] = $project->project_name.' - JO:'.$joborder.' WORKGROUP';
        // $jobWorkgroup['added_by'] = $user->id;
        // $jobWorkgroup['position'] = $reciever->position->position;

        // JobOrderWorkgroup::savePayload($jobWorkgroup);

        // $job_logs['contact_id'] = 0;
        // $job_logs['project_id'] = 0;
        // $job_logs['timetable_id'] = 0;
        // $job_logs['task_id'] = 0;
        // $job_logs['job_order_id'] = $joborder;
        // $job_logs['user_id'] = $user->id;
        // $job_logs['logs'] = strtoupper($user->first_name.' '.$user->last_name).' CREATED THE JOB ORDER.';

        // JobOrderLog::savePayload($job_logs);

        // $job = JobOrder::find($joborder);

        // $postJob['contact_id'] = $job->contact_id;
        // $postJob['project_id'] = $job->project_id;
        // $postJob['timetable_id'] = $job->timetable_id;
        // $postJob['task_id'] = $job->task_id;
        // $postJob['job_order_id'] = $joborder;       
        // $postJob['user_id'] = Auth::id();
        // $postJob['message'] = nl2br($request->message);

        // $thread = JobOrderThread::savePayload($postJob);

       return back()->with('is_success_jo','Saved');
    }

    public function new_store(Request $request){
        

        $user = Auth::user();
        $hash = \Hash::make($request->details);
        
        // $project = Project::findOrFail($request->project_id);
        $postdata = $request->all();                
        $postdata['created_by'] = $user->id;        
        $postdata['encryptname'] = str_replace("/","", $hash);
        $postdata['assigned_name'] = "";
        $postdata['start_date'] = Date('Y-m-d',strtotime($request->start_date));
        $postdata['end_date'] = Date('Y-m-d',strtotime($request->end_date));
        $postdata['contact_person'] = "";
        $postdata['project_name'] = "";
        $postdata['contact_id'] = 0;
        $postdata['project_id'] = 0;
        $postdata['timetable_id'] = 0;
        $postdata['task_id'] = 0;
        $postdata['details'] = nl2br($request->details);
        $postdata['subject'] = $request->subject;        
        // $type = $request->get('settings');
        
        $postdata['type'] = $request->get('type2');
        $counter = 0;                
        $joborder = JobOrder::savePayload($postdata);
        $jobfind = JobOrder::find($joborder);
        $postdata['onetime'] = 1;               
        foreach($postdata['reciever'] as $reciever){

            $user = User::findOrFail($reciever);
            $job_order_details = new JobOrderDetail();
            $job_order_details->job_order_id = $jobfind->id;
            $job_order_details->reciever_id = $reciever;
            $job_order_details->dept_id = $request->department_id;                
            $job_order_details->save();
                           
            if($jobfind->recuring == 1){
                
                $upd = "DAILY";
                $date1 = date_create($request->start_date);
                $date2 = date_create($request->end_date);
                $counter2=date_diff($date1,$date2);  
                $counter = $counter2->days;

                if($counter < 1){
                    $counter=1;
                }
            }
            elseif($jobfind->recuring == 2){

                $upd = "WEEKLY";                    
                $endDate = strtotime($request->end_date);
                $days=array('1'=>'Monday','2' => 'Tuesday','3' => 'Wednesday','4'=>'Thursday','5' =>'Friday','6' => 'Saturday','7'=>'Sunday');
                
                for($i = strtotime(Date('l',strtotime($request->start_date)), strtotime($request->start_date)); $i <= $endDate; $i = strtotime('+1 week', $i))
                
                $date_array[]=date('Y-m-d',$i);
                $cntdate = count($date_array);
                
                if($cntdate < 1){
                     $counter=1;
                }else{
                     $counter=$cntdate-1;
                }
            }
            else{
                
                $upd = "MONTHLY";
                $start = new \DateTime($request->start_date);
                $end = new \DateTime($request->end_date);
                $counter = $start->diff($end)->m;

                if($counter < 1){
                    $counter=1;
                }
            }                            
            

            $email_adds[] = $user->email.'^'."JOB ORDER #: ".$jobfind->id.'^'.$jobfind->start_date.' - '.$jobfind->end_date.'^'.strtoupper($user->first_name.' '.$user->last_name).'^'.$upd;
            $sender = Auth::user();
            $user_notificaton = new UserNotification();
            $user_notificaton->user_id = $reciever;
            $user_notificaton->details = strtoupper($sender->first_name.' '.$sender->last_name).' TAGGED YOU IN A JOB ORDER';
            $user_notificaton->ref_link = "UR-".$jobfind->id;
            $user_notificaton->save();                
            $post2['info'] = 'JO-'.$jobfind->id;
            $post2['link'] = $jobfind->encryptname;

            $post2['last_update'] = Date('Y-m-d H:i:s',strtotime($jobfind->updated_at));

            $post2['cnt_need'] = $counter;
            $post2['cnt_donw'] = 0;
            $post2['type'] = $jobfind->recuring;
            $post2['user_id'] = $reciever;
            $post2['created_by'] = Auth::id();       
            
            $reminder = ReminderUpdate::savePayload($post2);
        }
        
        $cntFile = 0;
        $destinationPath = "";
        $filesSending = [];                            
        if($request->hasFile('attached')){               
            $files = Input::file('attached');                        
            if(!empty($files)){
                $cntFile = 1;
                $destinationPath = storage_path().'/uploads/joborders/new/'.$joborder;
                if (!\File::exists($destinationPath))
                {
                    mkdir($destinationPath, 0755, true); 
                }  

                foreach ($files as $file) {
                    
                    $filename = $file->getClientOriginalName();                    

                    $file_name = pathinfo($filename, PATHINFO_FILENAME);

                    $extension = File::extension($filename);
                    $actual_name = $file_name.'.'.$extension;
                    $file_path = $destinationPath.$actual_name;

                    $count = 1;
                    while(File::exists($file_path)) {
                        $actual_name = $file_name.'_'.$count.'.'.$extension;
                        $file_path = $destinationPath.$actual_name;
                        $count++;
                    }                    

                    $file->move($destinationPath, $actual_name);   

                    $hashname = \Hash::make($filename);

                    $enc = str_replace("/","", $hashname);

                    $jo_file = new JobOrderFile();
                    $jo_file->contact_id = 0;
                    $jo_file->project_id = 0;
                    $jo_file->timetable_id = 0;             
                    $jo_file->task_id = 0;
                    $jo_file->job_order_id = $joborder;                          
                    $jo_file->filename = $filename;
                    $jo_file->encrpytname = $enc;
                    $jo_file->download = 0;          
                    $jo_file->uploaded_by = Auth::id();        
                    $jo_file->save();            

                    $filesSending[] = $filename;          
                }                      
            }                                 
        }               

        // $jobWorkgroup['contact_id'] = $request->contact_id;
        // $jobWorkgroup['project_id'] = $request->project_id;
        // $jobWorkgroup['job_order_id'] = $joborder;
        // $jobWorkgroup['user_id'] = $reciever->id;
        // $jobWorkgroup['workgroup_name'] = $project->project_name.' - JO:'.$joborder.' WORKGROUP';
        // $jobWorkgroup['added_by'] = $user->id;
        // $jobWorkgroup['position'] = $reciever->position->position;

        // JobOrderWorkgroup::savePayload($jobWorkgroup);
        $loggedUser = Auth::user();
        $job_logs['contact_id'] = 0;
        $job_logs['project_id'] = 0;
        $job_logs['timetable_id'] = 0;
        $job_logs['task_id'] = 0;
        $job_logs['job_order_id'] = $joborder;
        $job_logs['user_id'] = $loggedUser->id;
        $job_logs['logs'] = strtoupper($loggedUser->first_name.' '.$loggedUser->last_name).' CREATED THE JOB ORDER.';

        JobOrderLog::savePayload($job_logs);

        $job = JobOrder::find($joborder);

        $postJob['contact_id'] = 0;
        $postJob['project_id'] = 0;
        $postJob['timetable_id'] = 0;
        $postJob['task_id'] = 0;
        $postJob['job_order_id'] = $joborder;       
        $postJob['user_id'] = Auth::id();
        $postJob['message'] = nl2br($request->message);

        $thread = JobOrderThread::savePayload($postJob);

        if(!empty($request->get('cc'))){

                $reqID = $jobfind->id;
                foreach($request->get('cc') as $ccs) {

                    $cc = new JobOrderCc();                    
                    $cc->job_order_id = $reqID;
                    $cc->user_id = $ccs;

                    $cc->save();                    
                }
            }
            if(!empty($email_adds)){                
                foreach($email_adds as $email){

                    $breakdown = explode('^', $email);                    
                    if(filter_var($breakdown[0], FILTER_VALIDATE_EMAIL)) {
                        
                        \Mail::send('emails.user_job',
                            [
                                "data"=>$postdata['details'],
                                "range"=>$breakdown[2],                         
                                "name"=>$breakdown[3],
                                "needupdate" =>$breakdown[4]
                            ],function($m) use($sender,$breakdown,$cntFile,$destinationPath,$filesSending){
                            $m->from($sender->email,strtoupper($sender->first_name.' '.$sender->last_name));
                            $m->to($breakdown[0])->subject($breakdown[1]);           
                            if($cntFile > 0){
                                foreach($filesSending as $fsending){

                                    $m->attach($destinationPath.'/'.$fsending);         
                                }                                
                            }
                        
                        });                        
                    }             
                }
            }

        return back()->with('is_success_jo','Saved');
    }

    public function downloadFile(Request $request){

        $encryptname = $request->get('encname');
        $file = JobOrderFile::ByEncryptName($encryptname);        
        $job = JobOrder::findOrFail($file->job_order_id);
        $file_down = storage_path().'/uploads/joborders/'. $job->timetable_id.'/'.$job->task_id.'/'.$job->id.'/'.$file->filename;
        if (!is_readable($file_down)){
            $file_down = storage_path().'/uploads/joborders/new/'.$job->id.'/'.$file->filename;
            return Response::download($file_down);
        }else{
            return Response::download($file_down);
        }  
        
    }

    public function overview($encryptname){
        
        $job = JobOrder::byEncrypt($encryptname);
        // $members = JobOrderWorkgroup::byJobOrder($job->id);
        // $users = User::byJobOrderNotInWorgroup($job->id);
        $departments = Department::getList();
        $files = JobOrderFile::byJobOrder($job->id);        
        $expenses = Expense::byJobOrder($job->id);
        $types = ParticularType::pluck('type','id');
        $expense_approval = Expense::byJobOrderForApproval($job->id);
        $expense_approved = Expense::byJobOrderApproved($job->id);
        $threads = JobOrderThread::byJobOrder($job->id);
        $logs = JobOrderLog::byJobOrder($job->id);
        $wemail = ['0'=>'Without Email','1'=>'With Email'];
        $ots = OverTime::findByJobIdAndUserId($job->id,$job->project_id,Auth::user()->id);
        $ot_approved = 0;
        $total = 0;
        $all_ots = OverTime::findByJobId($job->id);

        $my_supers = OverTimeSupervisor::findMySupervisors(Auth::user()->id);
        foreach ($all_ots as $aot) {
            $date1 = new \DateTime($aot->start_date.' '.$aot->start_time);
            $date2 = new \DateTime($aot->end_date.' '.$aot->end_time);
            $hour = $date1->diff($date2)->format('%h');
            $minute = $date1->diff($date2)->format('%i');
            $equi = number_format($hour + $minute/60,2);
            $total = $total + $equi;
        }
        $ot_approved = $total;

        $uid = Auth::id();
        $memIDs = JobOrderWorkgroup::membersID($job->id);

        $settings = ['0'=>'One Time Only','1'=>'Everyday','2'=>'Every month','3'=>'Every week'];
        $date_now = Date('Y-m-d');

        $ratings = [
            '1'=>'1',
            '2'=>'2',
            '3'=>'3',
            '4'=>'4',
            '5'=>'5',
            '6'=>'6',
            '7'=>'7',
            '8'=>'8',
            '9'=>'9',
            '10'=>'10',
        ];        

        $users = User::byJobOrderNotMember($job->id);        
        $ccs = JobOrderCc::getList($job->id);                

        $members = JobOrderDetail::byJobID($job->id);
        
        $notInCC = User::getJobNotInCC($job->id);
        $inCC = User::getJobInCC($job->id);                

        // if(in_array($uid, $memIDs) || $uid == $job->created_by || $uid == $job->assigned_to){
            return view('job_orders.overview',compact('job','members','departments','users','files','types','expenses','expense_approval','expense_approved','threads','logs','wemail','uid','settings','ratings','date_now','ccs','notInCC','inCC','ots','ot_approved','my_supers'));
        // }else{
        //     return view('errors.102');
        // }        
    }

    public function update_status(Request $request){

        $postdata = $request->all();

        if($postdata['status'] == 2){

            $postdata['return_date'] = date('Y-m-d');
        }
        
        $job = JobOrder::updatePayload($postdata,$request->job_id);        
        return back()->with('is_update_status','Update');
    }

    public function rating(Request $request){

        $postdata = $request->all();        
        $postdata['rating_remarks'] = nl2br($request->rating_remarks);
        $member = JobOrderDetail::where('job_order_id',$request->job_order_id)->where('reciever_id',$request->reciever_id)->first();        
        $member->rating = $request->rating;
        $member->rating_remarks = nl2br($request->rating_remarks);
        $member->update();
        $stat = [];
        $stat['status'] = 3;
        $job = JobOrder::updatePayload($stat,$request->job_order_id);

        ReminderUpdate::removeJob($request->job_order_id,$request->reciever_id);

        return back()->with('is_rated','Rated!');
    }

    public function jobOrderDetails(Request $request){

        $job = JobOrder::byJO($request->jobid);
        if(!empty($job)){
            return Response::json($job);
        }else{
            $job=[];
            return Response::json($job);
        }
    }

    public function add_participant(Request $request){

        $sender = Auth::user();

        $job = JobOrder::findOrFail($request->job_order_id);

        foreach($request->reciever as $reciever){

            $checkUser = JobOrderDetail::byJobUid($job->id,$reciever);

            if(empty($checkUser)){

                $user = User::findOrFail($reciever);
                $job_order_details = new JobOrderDetail();
                $job_order_details->job_order_id = $job->id;
                $job_order_details->reciever_id = $reciever;
                $job_order_details->dept_id = $user->dept_id;                
                $job_order_details->rating = 0;
                $job_order_details->save();
                            

                if($job->recuring == 1){
                    
                    $upd = "DAILY";
                    $date1 = date_create($request->start_date);
                    $date2 = date_create($request->end_date);
                    $counter2=date_diff($date1,$date2);  
                    $counter = $counter2->days;

                    if($counter < 1){
                        $counter=1;
                    }
                }
                elseif($job->recuring == 2){

                    $upd = "WEEKLY";                    
                    $endDate = strtotime($request->end_date);
                    $days=array('1'=>'Monday','2' => 'Tuesday','3' => 'Wednesday','4'=>'Thursday','5' =>'Friday','6' => 'Saturday','7'=>'Sunday');
                    
                    for($i = strtotime(Date('l',strtotime($request->start_date)), strtotime($request->start_date)); $i <= $endDate; $i = strtotime('+1 week', $i))
                    
                    $date_array[]=date('Y-m-d',$i);
                    $cntdate = count($date_array);
                    
                    if($cntdate < 1){
                         $counter=1;
                    }else{
                         $counter=$cntdate-1;
                    }
                }
                else{
                    
                    $upd = "MONTHLY";
                    $start = new \DateTime($request->start_date);
                    $end = new \DateTime($request->end_date);
                    $counter = $start->diff($end)->m;

                    if($counter < 1){
                        $counter=1;
                    }
                }                            
                

                $email_adds[] = $user->email.'^'."JO #: ".$job->id.'^'.$job->start_date.' - '.$job->end_date.'^'.strtoupper($user->first_name.' '.$user->last_name).'^'.$upd;

                $user_notificaton = new UserNotification();
                $user_notificaton->user_id = $reciever;
                $user_notificaton->details = strtoupper($sender->first_name.' '.$sender->last_name).' TAGGED YOU IN A JOB ORDER';
                $user_notificaton->ref_link = "JO-".$job->id;
                $user_notificaton->save();                
                $post2['info'] = "JO-".$job->id;
                $post2['link'] = $job->encryptname;

                $post2['last_update'] = Date('Y-m-d H:i:s',strtotime($job->updated_at));

                $post2['cnt_need'] = $counter;
                $post2['cnt_donw'] = 0;
                $post2['type'] = $job->recuring;
                $post2['user_id'] = $reciever;
                $post2['created_by'] = Auth::id();       
                
                $reminder = ReminderUpdate::savePayload($post2);

                // if(!empty($email_adds)){                
                //     $sender = Auth::user();
                //     foreach($email_adds as $email){

                //         $breakdown = explode('^', $email);                    
                //         if(filter_var($breakdown[0], FILTER_VALIDATE_EMAIL)) {
                            
                //             \Mail::send('emails.user_job',
                //                 [
                //                     "data"=>$postdata['details'],
                //                     "range"=>$breakdown[2],                         
                //                     "name"=>$breakdown[3],
                //                     "needupdate" =>$breakdown[4]
                                    
                //                 ],function($m) use($sender,$breakdown){
                //                 $m->from($sender->email,strtoupper($sender->first_name.' '.$sender->last_name));
                //                 $m->to($breakdown[0])->subject($breakdown[1]);                               
                //             });                        
                //         }             
                //     }
                // }
            }            
        }
        return back()->with('is_added','Added');
    }    

    public function addCC(Request $request){

        $ccs = $request->get('reciever');

        if(!empty($ccs)){

            foreach($ccs as $cc){

                $copy = new JobOrderCc();
                $copy->job_order_id = $request->get('job_order_id');
                $copy->user_id = $cc;
                $copy->save();
            }
        }

        return back();
    }

    public function removeCC(Request $request){

        $ccs = $request->get('reciever');

        if(!empty($ccs)){

            foreach($ccs as $cc){

                JobOrderCc::where('job_order_id',$request->get('job_order_id'))->where('user_id',$cc)->delete();
            }
        }

        return back();
    }

    public function jobOrderReport(){

        $jobs = JobOrder::all();
        $date_now = date('Y-m-d');
        $departments = Department::orderBy('department','ASC')->pluck('department','id');        
        $user_filt = [];

        $filter_by = [

            0 => 'CONTACTS',
            1 => 'PROJECT',
            2 => 'EMPLOYEE'
        ];

        $filter_by_selected = [];
        $filtered = [];

        return view('reports.all_jo',compact('jobs','date_now','departments','user_filt','filter_by','filter_by_selected'));

    }

    public function JoPerEmployee(){

        $users = JobOrder::allEmployee();

        $departments = Department::orderBy('department','ASC')->pluck('department','id');        
        $user_filt = [];
        $date_now = date('Y-m-d');
        return view('reports.empjo_summary',compact('users','departments','user_filt','date_now'));
    }

    public function job_emp_detailed($uid){

        $jobs = JobOrder::detailedByUser($uid);
        $date_now = date('Y-m-d');

        return view('reports.detailed_jo_emp',compact('jobs','date_now'));
    }

    public function filterUserDept(Request $request){

        $users = UserIndustry::bydept2($request->get('dept_id'));

        return Response::json($users);
    }

    public function JoPerEmployeeFilter(Request $request){

        $type = $request->get('type');        
        $users = JobOrder::perEmployee($request->get('userfilt'));

        $departments = Department::orderBy('department','ASC')->pluck('department','id');        
        $user_filt = [];
        $date_now = date('Y-m-d');
        return view('reports.empjo_summary',compact('users','departments','user_filt','date_now'));
    }

    public function JoPerEmployeeFilte2r(Request $request){

        $users = JobOrder::perEmployee($request->get('userfilt'));

        $departments = Department::orderBy('department','ASC')->pluck('department','id');        
        $user_filt = [];
        return view('reports.empjo_summary',compact('users','departments','user_filt'));
    }

    public function exportInProjects(Request $request){

        $project = Project::find($request->project_id);

        $jobs = JobOrder::getUnderProject($project->id);

        Excel::create('Job Orders In '.$project->project_name, function($excel) use($jobs) {


            $excel->sheet('JOB ORDER REPORTS', function($sheet) use($jobs) {
                
                 $sheet->cell('A1:I1', function($cell) {                    
                    $cell->setAlignment('center');
                    $cell->setBackground('#3498db');
            
                 });          
          
                $sheet->row(1,array("JOB ORDER #","SUBJECT","ASSIGNED BY","ASSIGNED TO","START DATE","END DATE","DATE RETURN","STATUS","RATING"));

                if(!empty($jobs)){

                    $cnt = 2;                           
                    foreach ($jobs as $job) {                                                                    
                        if(!empty($job)){
                            
                            $rate = "N/A";

                            if(!empty($job->return_date)){
                                $rDate = $job->return_date;
                            }else{
                                $rDate = "N/A";
                            }

                            if($job->status === 1){
                                $stat = "Ongoing";
                            }else if($job->status === 2){
                                $stat = "Returned";                                
                            }else if($job->status === 3){
                                $stat = "Closed";

                                $rate = $job->rating;
                            }

                            $sheet->row($cnt,array(
                                    
                                "JO#: ".$job->id,
                                strtoupper($job->subject),                                
                                strtoupper($job->user->first_name.' '.$job->user->last_name),
                                $job->userName,
                                $job->start_date,
                                $job->end_date,                                
                                $rDate,
                                $stat,
                                $rate

                            ));
                            $date_now = Date('Y-m-d');
                            if($date_now > $job->end_date AND $job->status == 1){
                                
                                $sheet->cell('A'.$cnt.':I'.$cnt, function($cell) {                                
                                    $cell->setBackground('#fab1a0');
                                });        
                            }else if($job->status > 1 AND $job->return_date > $job->end_date){

                                $sheet->cell('A'.$cnt.':I'.$cnt, function($cell) {                                
                                    $cell->setBackground('#fab1a0');
                                });        
                            }else{

                            }
                            $cnt++;
                        }            
                        
                    }    
                }
            });           
        })->export('xlsx');   
    }  

    public function filterOption(Request $request){

        $filter = $request->filter_by;

        if($filter < 1){

           $filt = Contact::getList();

        }else if($filter == 1){

            $filt = Project::getList();
        }else{

            $filt = User::getList();
        }

        return Response::json($filt);
    }

    public function extraction(Request $request){

        $type = $request->type;

        $filter_by_selected = $request->filter_by;
        $filtered = $request->filtered;

        if($filter_by_selected < 1){

           $jobs = JobOrder::byContact($filtered);

        }else if($filter_by_selected == 1){

            $jobs = JobOrder::ByProjects($filtered);
        }else{

            $jobs = JobOrder::byUsers($filtered);
        }


        if($type < 2){

            $filter_by = [

                0 => 'CONTACTS',
                1 => 'PROJECT',
                2 => 'EMPLOYEE'
            ];

            $date_now = date('Y-m-d');

            return view('reports.all_jo',compact('jobs','filter_by_selected','filter_by','date_now','filtered'));

        }else{

            if($filter_by_selected < 1){

                Excel::create('Job Order Report', function($excel) use($jobs) {


                    $excel->sheet('JOB ORDER REPORTS', function($sheet) use($jobs) {
                    
                        $sheet->cell('A1:J1', function($cell) {                    
                            $cell->setAlignment('center');
                            $cell->setBackground('#3498db');
                    
                         });          
                  
                        $sheet->row(1,array("COMPANY","JOB ORDER #","SUBJECT","ASSIGNED BY","ASSIGNED TO","START DATE","END DATE","DATE RETURN","STATUS","RATING"));

                        if(!empty($jobs)){

                            $cnt = 2;                           
                            foreach ($jobs as $job) {                                                                    
                                if(!empty($job)){
                                    
                                    $rate = "N/A";

                                    if(!empty($job->return_date)){
                                        $rDate = $job->return_date;
                                    }else{
                                        $rDate = "N/A";
                                    }

                                    if($job->status === 1){
                                        $stat = "Ongoing";
                                    }else if($job->status === 2){
                                        $stat = "Returned";                                
                                    }else if($job->status === 3){
                                        $stat = "Closed";

                                        $rate = $job->rating;
                                    }

                                    $sheet->row($cnt,array(
                                        
                                        strtoupper($job->company),
                                        "JO#: ".$job->id,
                                        strtoupper($job->subject),                                
                                        strtoupper($job->user->first_name.' '.$job->user->last_name),
                                        $job->userName,
                                        $job->start_date,
                                        $job->end_date,                                
                                        $rDate,
                                        $stat,
                                        $rate

                                    ));
                                    $date_now = Date('Y-m-d');
                                    if($date_now > $job->end_date AND $job->status == 1){
                                        
                                        $sheet->cell('A'.$cnt.':J'.$cnt, function($cell) {                                
                                            $cell->setBackground('#fab1a0');
                                        });        
                                    }else if($job->status > 1 AND $job->return_date > $job->end_date){

                                        $sheet->cell('A'.$cnt.':J'.$cnt, function($cell) {                                
                                            $cell->setBackground('#fab1a0');
                                        });        
                                    }else{

                                    }
                                    $cnt++;
                                }            
                                
                            }    
                        }
                    });                
                })->export('xlsx'); 

            }if($filter_by_selected == 1){

                Excel::create('Job Order Report', function($excel) use($jobs) {


                    $excel->sheet('JOB ORDER REPORTS', function($sheet) use($jobs) {
                    
                        $sheet->cell('A1:J1', function($cell) {                    
                            $cell->setAlignment('center');
                            $cell->setBackground('#3498db');
                    
                         });          
                  
                        $sheet->row(1,array("PROJECT","JOB ORDER #","SUBJECT","ASSIGNED BY","ASSIGNED TO","START DATE","END DATE","DATE RETURN","STATUS","RATING"));

                        if(!empty($jobs)){

                            $cnt = 2;                           
                            foreach ($jobs as $job) {                                                                    
                                if(!empty($job)){
                                    
                                    $rate = "N/A";

                                    if(!empty($job->return_date)){
                                        $rDate = $job->return_date;
                                    }else{
                                        $rDate = "N/A";
                                    }

                                    if($job->status === 1){
                                        $stat = "Ongoing";
                                    }else if($job->status === 2){
                                        $stat = "Returned";                                
                                    }else if($job->status === 3){
                                        $stat = "Closed";

                                        $rate = $job->rating;
                                    }

                                    $sheet->row($cnt,array(
                                        
                                        strtoupper($job->project_name),
                                        "JO#: ".$job->id,
                                        strtoupper($job->subject),                                
                                        strtoupper($job->user->first_name.' '.$job->user->last_name),
                                        $job->userName,
                                        $job->start_date,
                                        $job->end_date,                                
                                        $rDate,
                                        $stat,
                                        $rate

                                    ));
                                    $date_now = Date('Y-m-d');
                                    if($date_now > $job->end_date AND $job->status == 1){
                                        
                                        $sheet->cell('A'.$cnt.':J'.$cnt, function($cell) {                                
                                            $cell->setBackground('#fab1a0');
                                        });        
                                    }else if($job->status > 1 AND $job->return_date > $job->end_date){

                                        $sheet->cell('A'.$cnt.':J'.$cnt, function($cell) {                                
                                            $cell->setBackground('#fab1a0');
                                        });        
                                    }else{

                                    }
                                    $cnt++;
                                }            
                                
                            }    
                        }
                    });                
                })->export('xlsx'); 

            }else{
                Excel::create('Job Order Report', function($excel) use($jobs) {


                    $excel->sheet('JOB ORDER REPORTS', function($sheet) use($jobs) {
                    
                        $sheet->cell('A1:I1', function($cell) {                    
                            $cell->setAlignment('center');
                            $cell->setBackground('#3498db');
                    
                         });          
                  
                        $sheet->row(1,array("JOB ORDER #","SUBJECT","ASSIGNED BY","ASSIGNED TO","START DATE","END DATE","DATE RETURN","STATUS","RATING"));

                        if(!empty($jobs)){

                            $cnt = 2;                           
                            foreach ($jobs as $job) {                                                                    
                                if(!empty($job)){
                                    
                                    $rate = "N/A";

                                    if(!empty($job->return_date)){
                                        $rDate = $job->return_date;
                                    }else{
                                        $rDate = "N/A";
                                    }

                                    if($job->status === 1){
                                        $stat = "Ongoing";
                                    }else if($job->status === 2){
                                        $stat = "Returned";                                
                                    }else if($job->status === 3){
                                        $stat = "Closed";

                                        $rate = $job->rating;
                                    }

                                    $sheet->row($cnt,array(
                                            
                                        "JO#: ".$job->id,
                                        strtoupper($job->subject),                                
                                        strtoupper($job->user->first_name.' '.$job->user->last_name),
                                        $job->userName,
                                        $job->start_date,
                                        $job->end_date,                                
                                        $rDate,
                                        $stat,
                                        $rate

                                    ));
                                    $date_now = Date('Y-m-d');
                                    if($date_now > $job->end_date AND $job->status == 1){
                                        
                                        $sheet->cell('A'.$cnt.':I'.$cnt, function($cell) {                                
                                            $cell->setBackground('#fab1a0');
                                        });        
                                    }else if($job->status > 1 AND $job->return_date > $job->end_date){

                                        $sheet->cell('A'.$cnt.':I'.$cnt, function($cell) {                                
                                            $cell->setBackground('#fab1a0');
                                        });        
                                    }else{

                                    }
                                    $cnt++;
                                }            
                                
                            }    
                        }
                    });                
                })->export('xlsx'); 
            }
                          
        }

    }

    //old- use inside the request details
    // public function store(Request $request)
    // {
        
    //     $user = Auth::user();
       
    //     $postdata = $request->all();
    //     $request_detail = UserRequest::byEncrypt($postdata['request_enc']);

    //     $postdata['main_request_id'] = $request_detail->id;
    //     $recievers = $request->get('assigned_to');
    //     $postdata['created_by'] = $user->id;               
    //     $type = $request->get('settings');
    //     $counter = 0;

    //     foreach($recievers as $reciever){

    //         $empname = User::findOrFail($reciever);

    //         if($type < 1){

    //             $postdata['main'] = 1;
    //             $postdata['onetime'] = 1;            
    //         }
    //         $postdata['start_date'] = Date('Y-m-d',strtotime($request->start_date));
    //         $postdata['end_date'] = Date('Y-m-d',strtotime($request->end_date));
    //         $hash = \Hash::make($request->details);
    //         $postdata['encryptname'] = str_replace("/","", $hash);
    //         $postdata['assigned_to'] = $reciever;
    //         $postdata['assigned_name'] = strtoupper($empname->first_name.' '.$empname->last_name);

    //         if($type != 3){
    //             $joborder = JobOrder::savePayload($postdata);
    //         }

    //         if($type == 1) {
                
    //             $date1 = date_create($request->start_date);
    //             $date2 = date_create($request->end_date);

    //             $counter=date_diff($date1,$date2);  

    //             if($counter->days > 0){

    //                 $period = new \DatePeriod(
    //                      new \DateTime($request->start_date),
    //                      new \DateInterval('P1D'),
    //                      new \DateTime($request->end_date)
    //                 );

    //                 foreach($period as $per){                
                        
    //                     $postdata['date'] = $per->format('Y-m-d');
    //                     $multipleJo = ScheduleJobOrder::savePayload($postdata);
    //                 }
    //             }
    //         }elseif ($type == 2) {
                
    //             $start = new \DateTime($request->start_date);
    //             $end = new \DateTime($request->end_date);

    //             $counter = $start->diff($end)->m;
                
    //             $start_month = Date('m',strtotime($request->start_date));
    //             if($counter > 0){
    //                 $cnt = 1;
    //                 for ($i=0; $i < $counter; $i++) { 
                        
    //                     $d = new \DateTime($request->start_date);
    //                     $next_month = $d->add(new \DateInterval('P'.$cnt.'M'));
    //                     $postdata['date'] = $next_month->format('Y-m-d');
    //                     $multipleJo = ScheduleJobOrder::savePayload($postdata);

    //                     $cnt++;
    //                 }
    //             }
    //         }elseif ($type == 3) {
                
    //             $day_number = $request->weeks;
    //             $endDate = strtotime($request->end_date);
    //             $days=array('1'=>'Monday','2' => 'Tuesday','3' => 'Wednesday','4'=>'Thursday','5' =>'Friday','6' => 'Saturday','7'=>'Sunday');
    //             for($i = strtotime($days[$day_number], strtotime($request->start_date)); $i <= $endDate; $i = strtotime('+1 week', $i))
    //             $date_array[]=date('Y-m-d',$i);
    //             $today = Date('Y-m-d');
    //             foreach($date_array as $dates){

    //                 if($today != $dates){

    //                     $postdata['date'] = $dates;
    //                     $multipleJo = ScheduleJobOrder::savePayload($postdata);
    //                 }else{
    //                     $joborder = JobOrder::savePayload($postdata);  
    //                 }
    //             }                
    //         }            

    //         if($request->hasFile('attached')){                 
    //             $files = Input::file('attached');                        
    //             if(!empty($files)){
    //                 $destinationPath = storage_path().'/uploads/joborders/'. $joborder;
    //                 if (!\File::exists($destinationPath))
    //                 {
    //                     mkdir($destinationPath, 0755, true); 
    //                 }  

    //                 foreach ($files as $file) {
                        
    //                     $filename = $file->getClientOriginalName();                    

    //                     $file_name = pathinfo($filename, PATHINFO_FILENAME);

    //                     $extension = File::extension($filename);
    //                     $actual_name = $file_name.'.'.$extension;
    //                     $file_path = $destinationPath.$actual_name;

    //                     $count = 1;
    //                     while(File::exists($file_path)) {
    //                         $actual_name = $file_name.'_'.$count.'.'.$extension;
    //                         $file_path = $destinationPath.$actual_name;
    //                         $count++;
    //                     }                    

    //                     $file->move($destinationPath, $actual_name);   

    //                     $hashname = \Hash::make($filename);

    //                     $enc = str_replace("/","", $hashname);

    //                     $user_request_file = new JobOrderFile();
    //                     $user_request_file->job_order_id = $joborder;                        
    //                     $user_request_file->filename = $filename;
    //                     $user_request_file->encrpytname = $enc;
    //                     $user_request_file->download = 0;          
    //                     $user_request_file->uploaded_by = Auth::id();        
    //                     $user_request_file->save();                      
    //                 }                      
    //             }                                 
    //         }
    //    }

    //    return back()->with('is_success_jo','Saved');
    // }
}
