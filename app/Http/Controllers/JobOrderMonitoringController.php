<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\JobOrder;

class JobOrderMonitoringController extends Controller
{
    public function index(){

    	$date = Date('Y-m-d');
    	$jobs = JobOrder::byMonitor($date);

    	return view('monitoring.index',compact('jobs'));
    }
}
