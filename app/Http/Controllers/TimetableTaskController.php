<?php

namespace App\Http\Controllers;
use App\TimetableTask;
use Illuminate\Http\Request;
use Response;

class TimetableTaskController extends Controller
{
    public function store(Request $request)
    {
        $timetable = TimetableTask::savePayload($request->all(),$request->taskid);        

        return back()->with('is_success_task','Saved!');
    }
    
    public function taskUpdates(Request $request){

         $task = TimetableTask::updatePayload($request->all(),$request->get('taskid'));       

        return back()->with('is_update_task','Updated!');
    }

    public function taskDetails(Request $request){

        $task = TimetableTask::findOrFail($request->get('taskid'));

        return Response::json($task);
    }

    public function getList(Request $request){

        $task = TimetableTask::byTimetableAPI($request->get('topicID'));

        return Response::json($task);
    }
}
