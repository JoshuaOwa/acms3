<?php

namespace App\Http\Controllers;

use App\JobOrderWorkgroup;
use App\JobOrderThreadSeen;
use App\JobOrderLog;
use App\Project;
use App\User;
use Auth;
use Illuminate\Http\Request;

class JobOrderWorkgroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $uid = Auth::id();
        $user = Auth::user();
        $project = Project::findOrFail($request->project_id);
        $postdata = $request->all();        

        $postdata['added_by'] = $uid;
        $postdata['workgroup_name'] = $project->project_name.' - JO:'.$request->job_order_id.' WORKGROUP';        

        $userIds = $request->user_id;

        foreach($userIds as $uid){

            $check = JobOrderWorkgroup::checkExist($request->job_order_id,$uid);
            if($check <  1){

                $mem = User::findOrFail($uid);
                $postdata['user_id'] = $uid;
                $postdata['position'] = $mem->position->position;
                
                $job_workgroup = JobOrderWorkgroup::savePayload($postdata);            
                
                $send = ProjectThreadSeen::sendByProject($project->id,$uid);

                $job_logs['contact_id'] = $project->contact_id;
                $job_logs['project_id'] = $project->id;
                $job_logs['timetable_id'] = $request->timetable_id;
                $job_logs['task_id'] = $request->task_id;
                $job_logs['job_order_id'] = $request->job_order_id;
                $job_logs['user_id'] = $user->id;
                $job_logs['logs'] = strtoupper($user->first_name.' '.$user->last_name).' ADDED '.strtoupper($mem->first_name.' '.$mem->last_name).' TO THE WORKGROUP.';

                JobOrderLog::savePayload($job_logs);

            }                        
        }        

        return back()->with('is_added','Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\JobOrderWorkgroup  $jobOrderWorkgroup
     * @return \Illuminate\Http\Response
     */
    public function show(JobOrderWorkgroup $jobOrderWorkgroup)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\JobOrderWorkgroup  $jobOrderWorkgroup
     * @return \Illuminate\Http\Response
     */
    public function edit(JobOrderWorkgroup $jobOrderWorkgroup)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\JobOrderWorkgroup  $jobOrderWorkgroup
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, JobOrderWorkgroup $jobOrderWorkgroup)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\JobOrderWorkgroup  $jobOrderWorkgroup
     * @return \Illuminate\Http\Response
     */
    public function destroy(JobOrderWorkgroup $jobOrderWorkgroup)
    {
        //
    }

    public function remove_member(Request $request){

        
    }
}
