<?php

namespace App\Http\Controllers;
use App\BillOfMaterial;
use App\BillOfMaterialFile;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;
use Auth;
use Illuminate\Support\Facades\Input;
use File;
use Response;
use Illuminate\Http\Request;

class BillOfMaterialsController extends Controller
{
    public function index(){

    }

    public function store(Request $request){

		$user = Auth::user();
		$contact_id = $request->contact_id;
		$project_id = $request->project_id;

		$file = Input::file('attached'); 		

    	$destinationPath = storage_path().'/uploads/contacts/'.$contact_id.'/'.$project_id.'/boms';
        if (!\File::exists($destinationPath))
        {
            mkdir($destinationPath, 0755, true); 
        }  

        $filename = $file->getClientOriginalName();                    

        $file_name = pathinfo($filename, PATHINFO_FILENAME);

        $extension = File::extension($filename);
        $actual_name = $file_name.'.'.$extension;
        $file_path = $destinationPath.$actual_name;

        $count = 1;
        while(File::exists($file_path)) {
            $actual_name = $file_name.'_'.$count.'.'.$extension;
            $file_path = $destinationPath.$actual_name;
            $count++;
        }                    

        $file->move($destinationPath, $actual_name); 
        $filePath = $destinationPath ."/". $actual_name;
		$reader = ReaderFactory::create(Type::XLSX); // for XLSX files        
        $reader->open($filePath);
        foreach ($reader->getSheetIterator() as $sheet) {
            
            foreach ($sheet->getRowIterator() as $rowcount => $col) {
                        
            	if($rowcount > 1){       
            		if(empty($col[0])){
            			break;
            		}
            		$postdata = [];
	            	$postdata['contact_id'] = $contact_id;
	            	$postdata['project_id'] = $project_id;
	            	$postdata['category'] = $col[0];
	            	$postdata['item'] = $col[1];
	            	$postdata['description'] = $col[2];
	            	$postdata['temp_qty'] = 0;
	            	$postdata['rem_qty'] = $col[3];
	            	$postdata['qty'] = $col[3];
	            	$postdata['unit'] = $col[4];
	            	$postdata['specs'] = $col[5];
	            	$postdata['uploaded_by'] = $user->id;
	            	
	            	$bomsFile = BillOfMaterial::savePayload($postdata);
            	}            	            	
            }
        }

        $hashname = \Hash::make($actual_name);

        $enc = str_replace("/","", $hashname);

        $bom_file = new BillOfMaterialFile();
        $bom_file->contact_id = $contact_id;
        $bom_file->project_id = $project_id;
        $bom_file->filename = $actual_name;
        $bom_file->encrpytname = $enc;
        $bom_file->download = 0;          
        $bom_file->uploaded_by = $user->id;        
        $bom_file->save();  

        return back()->with('is_bom','Save');
    }    

    public function bomDetails(Request $request){

        $bom = BillOfMaterial::findOrFail($request->bomid);

        return Response::json($bom);
    }

    public function bomUpdates(Request $request){

        $bomid = $request->bomid;
        $postdata = $request->all();

        $bom = BillOfMaterial::updatePayload($postdata,$bomid);

        if($bom < 2){
            return back()->with('bom_update','Updated!');        
        }else{
            return back()->with('bom_qty','Updated!');        
        }
    }
}
