<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;
use App\User;
use App\Dtr;
use DB;

class DtrController extends Controller
{
    public function upload(Request $request)
    {
		$fileName = $request->file('file')->getClientOriginalName();

		$destinationPath = storage_path().'/uploads/DTRs/';

		if(!\File::exists($destinationPath))
		{
			mkdir($destinationPath, 0755, true);
		}

		$request->file('file')->move($destinationPath,$fileName);

		$filePath = $destinationPath.'/'.$fileName;

		DB::beginTransaction();
        try {
            $reader = ReaderFactory::create(Type::CSV); // for XLSX files
            $reader->setFieldDelimiter('|');
            $reader->open($filePath);

            foreach ($reader->getSheetIterator() as $sheet) {
            	foreach ($sheet->getRowIterator() as $row) {
            		$data['emp_id'] = '00000'.$row[0];
            		$data['date'] = date('Y-m-d',strtotime($row[1]));
            		$data['in_1'] = $row[2];
            		$data['out_1'] = $row[3];
            		$data['in_2'] = $row[4];
            		$data['out_2'] = $row[5];

            		$dtr = Dtr::findDtr($data);
            		if(!empty($dtr)){
            			$dtr->out_1 = $data['out_1'];
            			$dtr->in_2 = $data['in_2'];
            			$dtr->out_2 = $data['out_2'];
            			$dtr->save();
            			
            		}else{
            			$new = Dtr::firstOrCreate([
            				'emp_id' => $data['emp_id'],
            				'date' => date('Y-m-d',strtotime($data['date'])),
            				'in_1' => $data['in_1'],
            				'out_1' => $data['out_1'],
            				'in_2' => $data['in_2'],
            				'out_2' => $data['out_2']
            			]);
            		}
            	}
            }
            DB::commit();
            return response()->json(array('msg' => 'file uploaded', 'status' => 0));
        }catch(\Exception $e){

        }
    }
}
