<?php

namespace App\Http\Controllers;

use App\Project;
use App\ProjectThread;
use App\ProjectThreadFile;
use App\ProjectThreadSeen;
use App\ProjectLog;
use Response;
use File;
use Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;

class ProjectThreadController extends Controller
{
    public function store(Request $request){

        $postdata = $request->all();
        $user = Auth::user();

        $project = Project::findOrFail($request->proj_id);        

        $postdata['contact_id'] = $project->contact_id;
        $postdata['project_id'] = $project->id;            
        $postdata['user_id'] = Auth::id();
        $postdata['message'] = nl2br($request->message);

        $thread = ProjectThread::savePayload($postdata);

        $proj_logs['log'] = strtoupper($user->first_name.' '.$user->last_name).' CREATED NEW THREAD.';

        if($request->hasFile('attached')){                 
            $files = Input::file('attached');                        
            if(!empty($files)){
                $destinationPath = storage_path().'/uploads/projects/'. $project->id.'/threads/'.$thread;
                if (!\File::exists($destinationPath))
                {
                    mkdir($destinationPath, 0755, true); 
                }  

                foreach ($files as $file) {
                    
                    $filename = $file->getClientOriginalName();                    

                    $file_name = pathinfo($filename, PATHINFO_FILENAME);

                    $extension = File::extension($filename);
                    $actual_name = $file_name.'.'.$extension;
                    $file_path = $destinationPath.$actual_name;

                    $count = 1;
                    while(File::exists($file_path)) {
                        $actual_name = $file_name.'_'.$count.'.'.$extension;
                        $file_path = $destinationPath.$actual_name;
                        $count++;
                    }                    

                    $file->move($destinationPath, $actual_name);   

                    $hashname = \Hash::make($filename);

                    $enc = str_replace("/","", $hashname);

                    $thread_file = new ProjectThreadFile();
                    $thread_file->contact_id = $project->contact_id;
                    $thread_file->project_id = $project->id;                    
                    $thread_file->thread_id = $thread;
                    $thread_file->filename = $actual_name;
                    $thread_file->encrpytname = $enc;
                    $thread_file->download = 0;          
                    $thread_file->uploaded_by = Auth::id();        
                    $thread_file->save();                      
                }                  

                $proj_logs['log'] = strtoupper($user->first_name.' '.$user->last_name).' CREATED NEW THREAD WITH ATTACHED FILE.';    
            }                                 
        }
        $postdata['thread_id'] = $thread;
        $postdata['from_user'] = Auth::id();
        $send = ProjectThreadSeen::savePayload($postdata);

        $proj_logs['contact_id'] = $project->contact_id;
        $proj_logs['project_id'] = $project->id;
        $proj_logs['created_by'] = $user->id;        
        $proj_logs['remarks'] = "";

        ProjectLog::savePayload($proj_logs);

        return back()->with('is_sent','Sent!');
    }

    public function comment(Request $request){

        $postdata = $request->all();

        $user = Auth::user();

        $mainthread = ProjectThread::findOrFail($request->thread_id);
        $mainthread->updated_at = date('Y-m-d H:i:s');
        $mainthread->update();

        $postdata['contact_id'] = $mainthread->contact_id;
        $postdata['project_id'] = $mainthread->project_id;        
        $postdata['comment_to'] = $mainthread->id;
        $postdata['user_id'] = Auth::id();
        $postdata['main'] = 0;
        $postdata['message'] = nl2br($request->message);

        $thread = ProjectThread::savePayload($postdata);

        $proj_logs['log'] = strtoupper($user->first_name.' '.$user->last_name).' COMMENTED TO THE THREAD CREATED ON THIS DATE '.$mainthread->created_at->format('M-d-Y H:i:s A').'.';    

        if($request->hasFile('attached')){                 
            $files = Input::file('attached');                        
            if(!empty($files)){
                $destinationPath = storage_path().'/uploads/projects/'. $mainthread->project_id.'/threads/'.$thread;
                if (!\File::exists($destinationPath))
                {
                    mkdir($destinationPath, 0755, true); 
                }  

                foreach ($files as $file) {
                    
                    $filename = $file->getClientOriginalName();                    

                    $file_name = pathinfo($filename, PATHINFO_FILENAME);

                    $extension = File::extension($filename);
                    $actual_name = $file_name.'.'.$extension;
                    $file_path = $destinationPath.$actual_name;

                    $count = 1;
                    while(File::exists($file_path)) {
                        $actual_name = $file_name.'_'.$count.'.'.$extension;
                        $file_path = $destinationPath.$actual_name;
                        $count++;
                    }                    

                    $file->move($destinationPath, $actual_name);   

                    $hashname = \Hash::make($filename);

                    $enc = str_replace("/","", $hashname);

                    $thread_file = new ProjectThreadFile();
                    $thread_file->contact_id = $mainthread->contact_id;
                    $thread_file->project_id = $mainthread->project_id;                    
                    $thread_file->thread_id = $thread;
                    $thread_file->filename = $actual_name;
                    $thread_file->encrpytname = $enc;
                    $thread_file->download = 0;          
                    $thread_file->uploaded_by = Auth::id();        
                    $thread_file->save();                      
                }           
                $proj_logs['log'] = strtoupper($user->first_name.' '.$user->last_name).' COMMENTED WITH ATTACHED FILE TO THE THREAD CREATED ON THIS DATE '.$mainthread->created_at->format('M-d-Y H:i:s A').'.';             
            }                                 
        }
        $postdata['thread_id'] = $thread;
        $postdata['from_user'] = Auth::id();
        $send = ProjectThreadSeen::savePayload($postdata);

        $proj_logs['contact_id'] = $mainthread->contact_id;
        $proj_logs['project_id'] = $mainthread->project_id;        
        $proj_logs['created_by'] = $user->id;        
        $proj_logs['remarks'] = "";

        ProjectLog::savePayload($proj_logs);

        return back()->with('is_sent','Sent!');
    }

    public function download(Request $request){

        $encryptname = $request->get('encname');
        $file = ProjectThreadFile::ByEncryptName($encryptname);        
        $file_down= storage_path().'/uploads/projects/'. $file->project_id.'/threads/'.$file->thread_id.'/'.$file->filename;          
        $file->download = $file->download+1;
        $file->update();
        return Response::download($file_down);
    }

    public function comment_download(Request $request){

        echo "<pre>";
        print_r($request->all()); die;
        $encryptname = $request->get('encname');
        $file = ProjectThreadFile::ByEncryptName($encryptname);        
        $file_down= storage_path().'/uploads/projects/'. $file->project_id.'/threads/'.$file->thread_id.'/'.$file->filename;
        $file->download = $file->download+1;
        $file->update();
        return Response::download($file_down);
    }
}
