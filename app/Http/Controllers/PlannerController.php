<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use MaddHatter\LaravelFullcalendar\Calendar;
use App\Planner;
use App\PlannerReciever;
use App\PlannerFile;
use App\PlannerThread;
use App\PlannerThreadFile;
use Illuminate\Support\Facades\Input;
use App\User;
use Auth;
use File;
use Response;


class PlannerController extends Controller
{
    public function index(Calendar $calendar){

    	$user = Auth::id();

    	$reminders = Planner::byUser($user);
    	  
        $events = [];        
        $events2 = [];
        $events3  = [];
        $events4 = [];

        foreach ( $reminders as $reminder ) {                

            if($reminder->stop == 1){

                $events[] = $calendar->event(
                    //
                    $reminder->title, //event title
                    true, //full day event?
                    $reminder->start_date, //start time (you can also use Carbon instead of DateTime)
                    $reminder->end_date, //end time (you can also use Carbon instead of DateTime)
                    $reminder->planner_id//optionally, you can specify an event ID
                );
            }    
            elseif($reminder->stop < 1){

                $events2[] = $calendar->event(
                    //
                    $reminder->title, //event title
                    true, //full day event?
                    $reminder->start_date, //start time (you can also use Carbon instead of DateTime)

                    $reminder->end_date, //end time (you can also use Carbon instead of DateTime)
                    $reminder->planner_id//optionally, you can specify an event ID
                );
            }      

            elseif($reminder->stop == 2){
                 $events3[] = $calendar->event(
                    //
                    $reminder->title, //event title
                    true, //full day event?
                    $reminder->start_date, //start time (you can also use Carbon instead of DateTime)
                    $reminder->end_date, //end time (you can also use Carbon instead of DateTime)
                    $reminder->planner_id//optionally, you can specify an event ID
                );
            }                        
        }
        // echo "<pre>";
        // print_r($events2); die;
        $calendar = $calendar->addEvents($events)->setOptions([ //set fullcalendar options
            'firstDay' => 1,
            'eventTextColor' => '#ffffff',          
        ])->setCallbacks([ //set fullcalendar callback options (will not be JSON encoded)
            
            'eventClick'=>'function(event, jsEvent, view) {
   
                var caseNumber = Math.floor((Math.abs(jsEvent.offsetX + jsEvent.currentTarget.offsetLeft) / $(this).parent().parent().width() * 100) / (100 / 7));

                var table = $(this).parent().parent().parent().parent().children();
			    $(table).each(function(){
			      
			        if($(this).is("thead")){
			            var tds = $(this).children().children();
			            var dateClicked = $(tds[caseNumber]).attr("data-date");
			            var id          = $("#id").val();
			          event.url  = "../planner/"+event.id+"/"+dateClicked;
			           window.open(event.url, "_blank");			            
			        }
			    });
			}'            
        ]);

        $calendar = $calendar->addEvents($events2,['color'=>'blue'])->setOptions([ //set fullcalendar options
            'firstDay' => 1,
            
            'eventTextColor' => '#ffffff',          
        ])->setCallbacks([ //set fullcalendar callback options (will not be JSON encoded)
            
            'eventClick'=>'function(event, jsEvent, view) {
   
                var caseNumber = Math.floor((Math.abs(jsEvent.offsetX + jsEvent.currentTarget.offsetLeft) / $(this).parent().parent().width() * 100) / (100 / 7));

                var table = $(this).parent().parent().parent().parent().children();
                $(table).each(function(){
                  
                    if($(this).is("thead")){
                        var tds = $(this).children().children();
                        var dateClicked = $(tds[caseNumber]).attr("data-date");
                        var id          = $("#id").val();
                       event.url  = "../planner/"+event.id+"/reminder";
                       window.open(event.url, "_blank");                        
                    }
                });
            }'            
        ]);        

        $calendar = $calendar->addEvents($events3,['color'=>'orange'])->setOptions([ //set fullcalendar options
            'firstDay' => 1,
            'eventTextColor' => '#ffffff',          
        ])->setCallbacks([ //set fullcalendar callback options (will not be JSON encoded)
            
            'eventClick'=>'function(event, jsEvent, view) {
   
                var caseNumber = Math.floor((Math.abs(jsEvent.offsetX + jsEvent.currentTarget.offsetLeft) / $(this).parent().parent().width() * 100) / (100 / 7));

                var table = $(this).parent().parent().parent().parent().children();
                $(table).each(function(){
                  
                    if($(this).is("thead")){
                        var tds = $(this).children().children();
                        var dateClicked = $(tds[caseNumber]).attr("data-date");
                        var id          = $("#id").val();
                       event.url  = "../planner/"+event.id+"/reminder";
                       window.open(event.url, "_blank");                        
                    }
                });
            }'            
        ]);        


        
        $users = User::nameExcludedme(Auth::id());
        $types = [
            0 => 'DEFAULT',
            1 => 'EVERY MONTH'
        ];

    	return view('planner.index',compact('reminders','calendar','users','types'));
    }

    public function store(Request $request){
    	
        // $type = $request->get('type');
        $type = 0;
        $postdata = $request->all();

        if($type < 1){
            
            $postdata['created_by'] = Auth::id();            

            $planner = Planner::savePayload($postdata);

            $post2['user_id'] = Auth::id();
            $post2['planner_id'] = $planner->id;
            $post2['stop'] = 0;

            PlannerReciever::savePayload($post2);


            if(!empty($postdata['to_user_id'])){

                foreach ($postdata['to_user_id'] as $share_to) {            

                    $remind_them = new PlannerReciever();                                   
                    $remind_them->stop = 2;
                    $remind_them->user_id = $share_to;
                    $remind_them->planner_id = $planner->id;
                    $remind_them->save();
                }
            }
        }

        if($request->hasFile('attached')){                 
            $files = Input::file('attached');                        
            if(!empty($files)){
                $destinationPath = storage_path().'/uploads/planner/'.$planner->id;
                if (!\File::exists($destinationPath))
                {
                    mkdir($destinationPath, 0755, true); 
                }  

                foreach ($files as $file) {
                    $filename = $file->getClientOriginalName();                    

                    $file_name = pathinfo($filename, PATHINFO_FILENAME);

                    $extension = File::extension($filename);
                    $actual_name = $file_name.'.'.$extension;
                    $file_path = $destinationPath.$actual_name;

                    $count = 1;
                    while(File::exists($file_path)) {
                        $actual_name = $file_name.'_'.$count.'.'.$extension;
                        $file_path = $destinationPath.$actual_name;
                        $count++;
                    }                    

                    $file->move($destinationPath, $actual_name);      

                    $hashname = \Hash::make($actual_name);

                    $enc = str_replace("/","", $hashname);

                    $incident_file = new PlannerFile();
                    $incident_file->planner_id = $planner->id;                    
                    $incident_file->filename = $actual_name;
                    $incident_file->encryptname = $enc;                    
                    $incident_file->uploaded_by = Auth::id();
                    $incident_file->save();                      
                }                      
            }                                 
        }
        // else{

        //     $start = $request->get('e_month');
        //     $get_year = date('Y',strtotime($start));
        //     $get_month = date('m',strtotime($start));
        //     $get_date = date('d',strtotime($start));

        //     for ($i=$get_month; $i<=12 ; $i++) {           

        //         if(strlen($i) < 2){
        //             $months[] = "0".$i;
        //         }                
        //         else{
        //             $months[] = $i;
        //         }
        //     }

        //     foreach ($months as $month){

        //         $new_date = $get_year.'-'.$month.'-'.$get_date;

        //         $my_reminder = new Planner();
        //         $my_reminder->title = $postdata['title'];
        //         $my_reminder->details = $postdata['details'];
        //         $my_reminder->start_date = $new_date;
        //         $my_reminder->end_date = $new_date;                        
        //         $my_reminder->stop = 0;
        //         $my_reminder->share_to = Auth::id();
        //         $my_reminder->created_by = Auth::id();
        //         $my_reminder->save();

        //         if(!empty($postdata['to_user_id'])){

        //             foreach ($postdata['to_user_id'] as $share_to) {            

        //                 $remind_them = new Planner();                
        //                 $remind_them->title = $postdata['title'];
        //                 $remind_them->details = $postdata['details'];
        //                 $remind_them->start_date = $new_date;
        //                 $remind_them->end_date = $new_date;                        
        //                 $remind_them->stop = 2;
        //                 $remind_them->share_to = $share_to;
        //                 $remind_them->created_by = Auth::id();
        //                 $remind_them->save();
        //             }
        //         }
        //     }
            
        // }

        return redirect('planner');
    }

    public function details($id){

        $reminder = Planner::getDetails($id);        
        return view('planner.details',compact('reminder'));
    }

    public function stop(Request $request){

        $reminder = PlannerReciever::stopReminder($request->get('reminder_id'),Auth::id());
        $reminder->stop = 1;
        $reminder->update();
        
        return back()->with('is_closed','Closed');
    }

    public function download(Request $request){

        $encryptname = $request->get('encname');
        $file = PlannerFile::ByEncryptName($encryptname);        

        
        $file_down= storage_path().'/uploads/planner/'.$file->planner_id.'/'.$file->filename;

        return Response::download($file_down);
    }

    public function comment(Request $request){


        $planner = Planner::find($request->planner_id);

        $postdata = $request->all();

        $postdata['user_id'] = Auth::id();        
        $postdata['planner_id'] = $planner->id;

        $thread = PlannerThread::savePayload($postdata);

        

        if($request->hasFile('attached')){                 
            $files = Input::file('attached');                        
            if(!empty($files)){
                $destinationPath = storage_path().'/uploads/planner/'.$planner->id.'/'.$thread->id;
                if (!\File::exists($destinationPath))
                {
                    mkdir($destinationPath, 0755, true); 
                }  

                foreach ($files as $file) {
                    $filename = $file->getClientOriginalName();                    

                    $file_name = pathinfo($filename, PATHINFO_FILENAME);

                    $extension = File::extension($filename);
                    $actual_name = $file_name.'.'.$extension;
                    $file_path = $destinationPath.$actual_name;

                    $count = 1;
                    while(File::exists($file_path)) {
                        $actual_name = $file_name.'_'.$count.'.'.$extension;
                        $file_path = $destinationPath.$actual_name;
                        $count++;
                    }                    

                    $file->move($destinationPath, $actual_name);      

                    $hashname = \Hash::make($actual_name);

                    $enc = str_replace("/","", $hashname);

                    $thread_file = new PlannerThreadFile();
                    $thread_file->planner_id = $planner->id;
                    $thread_file->planner_thread_id = $thread->id;
                    $thread_file->filename = $actual_name;
                    $thread_file->encryptname = $enc;                    
                    $thread_file->uploaded_by = Auth::id();
                    $thread_file->save();                      
                }                      
            }                                 
        }

        return back();
    }

    public function comment_download(Request $request){

        $encryptname = $request->get('encname');
        $file = PlannerThreadFile::ByEncryptName($encryptname);        
        
        $file_down= storage_path().'/uploads/planner/'.$file->planner_id.'/'.$file->planner_thread_id.'/'.$file->filename;

        return Response::download($file_down);
    }
}
