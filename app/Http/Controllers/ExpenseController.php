<?php

namespace App\Http\Controllers;

use App\Expense;
use App\ExpenseFile;
use App\JobOrder;
use App\Project;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use File;
use Response;

class ExpenseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $expenses = Expense::findAllForApproval();

        return view('expenses.index',compact('expenses'));
    }

    public function overview($encrypt)
    {
        $expense = Expense::byEncrypted($encrypt);
        if(!empty($expense)){
            $job = JobOrder::findOrFail($expense->job_order_id);
            $project = Project::findOrFail($expense->project_id);
            $files = ExpenseFile::findByExpenseId($expense->id);
            return view('expenses.overview',compact('expense','job','project','files'));
        }
        return redirect()->back()->with('is_error_not_found','Error');
    }

    public function approval(Request $request)
    {
        $type = $request->type;
        $expense = Expense::findOrFail($request->id);
        if(!empty($expense)){
            if($type == 1){
                // approved
                $expense->status = 2;
                $expense->approved_by = Auth::id();
                $expense->save();

                return redirect('expenses')->with('is_approved','Success');
            }
            elseif($type == 2){
                // denied
                $expense->status = 3;
                $expense->approved_by = Auth::id();
                $expense->save();
                
                return redirect('expenses')->with('is_denied','Deny');
            }
        }

        return redirect()->back()->with('error_in_action','Error');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {           

        $postdata = $request->all();
        $postdata['user_id'] = Auth::id();
        $hash = \Hash::make($request->particular);
        $postdata['encryptname'] = str_replace("/","", $hash);
        if($request->qty < 1){
            $postdata['qty'] = 1;
        }
        $postdata['total_amount'] = $request->amount*$postdata['qty'] = 1;        
        $expense = Expense::savePayload($postdata);

        if($request->hasFile('attached')){                 
            $files = Input::file('attached');                        
            if(!empty($files)){
                $destinationPath = storage_path().'/uploads/joborders/'. $expense->timetable_id.'/'.$expense->task_id.'/'.$expense->job_order_id.'/'.$expense->id;
                if (!\File::exists($destinationPath))
                {
                    mkdir($destinationPath, 0755, true); 
                }  

                foreach ($files as $file) {
                    
                    $filename = $file->getClientOriginalName();                    

                    $file_name = pathinfo($filename, PATHINFO_FILENAME);

                    $extension = File::extension($filename);
                    $actual_name = $file_name.'.'.$extension;
                    $file_path = $destinationPath.$actual_name;

                    $count = 1;
                    while(File::exists($file_path)) {
                        $actual_name = $file_name.'_'.$count.'.'.$extension;
                        $file_path = $destinationPath.$actual_name;
                        $count++;
                    }                    

                    $file->move($destinationPath, $actual_name);   

                    $hashname = \Hash::make($filename);

                    $enc = str_replace("/","", $hashname);

                    $exp_file = new ExpenseFile();
                    $exp_file->contact_id = $expense->contact_id;
                    $exp_file->project_id = $expense->project_id;
                    $exp_file->timetable_id = $expense->timetable_id; 
                    $exp_file->task_id = $expense->task_id;
                    $exp_file->job_order_id = $expense->job_order_id;     
                    $exp_file->expense_id = $expense->id;
                    $exp_file->filename = $filename;
                    $exp_file->encrpytname = $enc;
                    $exp_file->download = 0;          
                    $exp_file->uploaded_by = Auth::id();        
                    $exp_file->save();                      
                }                      
            }                                 
        }        

        return back()->with('is_expense','Save');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Expense  $expense
     * @return \Illuminate\Http\Response
     */
    public function show(Expense $expense)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Expense  $expense
     * @return \Illuminate\Http\Response
     */
    public function edit(Expense $expense)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Expense  $expense
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Expense $expense)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Expense  $expense
     * @return \Illuminate\Http\Response
     */
    public function destroy(Expense $expense)
    {
        //
    }

    public function download(Request $request)
    {
        $id = $request->id;
        $file = ExpenseFile::findOrFail($id);
        if(!empty($file)){
            $file_down = storage_path().'/uploads/joborders/'. $file->timetable_id.'/'.$file->task_id.'/'.$file->job_order_id.'/'.$file->expense_id.'/'.$file->filename;
            if(!is_readable($file_down)){
                return Response::download($file_down);
            }else{
                return Response::download($file_down);
            }
        }else{
            return redirect()->back()->with('is_not_found','File not found');
        }
    }
}
