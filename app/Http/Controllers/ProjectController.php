<?php

namespace App\Http\Controllers;

use App\Project;
use App\ProjectLog;
use App\ProjectFile;
use App\Contact;
use App\JobOrder;
use App\User;
use App\Department;
use App\ProjectWorkgroup;
use App\ProjectTimetable;
use App\Timetable;
use App\BillOfMaterial;
use App\ProjectThread;
use App\ProjectThreadFile;
use App\ProjectThreadSeen;
use App\TimetableTask;
use App\UserRequest;
use App\EmailOut;
use App\IncidentReport;
use App\IncidentReportFile;
use App\IncidentReportThread;
use App\OverTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;
use File;
use Response;
use Excel;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $uid = Auth::id();
        $projects = Project::createdByMe($uid);
        $project_list = $projects->pluck('project_name','id');
        $employees = User::nameExcludedMe($uid);        

        return view('projects.mylist',compact('projects','project_list','employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $uid = Auth::id();
        $contacts = Contact::where('status',1)->orWhere('status',4)->pluck('company','id');
        $date_now = Date('m/d/Y');
        $tagging = ['CLOSED'=>'CLOSED','POTENTIAL'=>'POTENTIAL'];

        return view('projects.create',compact('contacts','date_now','tagging'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $postdata = $request->all();
        $postdata['created_by'] = Auth::id();
        $hash = \Hash::make($request->project_name);
        $postdata['encryptname'] = str_replace("/","", $hash);
        $postdata['start_date'] = Date('Y-m-d',strtotime($request->get('start_date')));
        $postdata['end_date'] = Date('Y-m-d',strtotime($request->get('end_date')));
        $postdata['project_details'] = nl2br($request->project_details);        
        if(empty($request->project_address)){

            $postdata['project_address'] = "";
        }
        $project = Project::savePayload($postdata);

        $user = Auth::user();

        if($request->hasFile('attached')){                 
            $files = Input::file('attached');                        
            if(!empty($files)){
                $destinationPath = storage_path().'/uploads/contacts/'.$project->contact_id.'/'.$project->id;
                if (!\File::exists($destinationPath))
                {
                    mkdir($destinationPath, 0755, true); 
                }  

                foreach ($files as $file) {
                    $filename = $file->getClientOriginalName();                    

                    $file_name = pathinfo($filename, PATHINFO_FILENAME);

                    $extension = File::extension($filename);
                    $actual_name = $file_name.'.'.$extension;
                    $file_path = $destinationPath.$actual_name;

                    $count = 1;
                    while(File::exists($file_path)) {
                        $actual_name = $file_name.'_'.$count.'.'.$extension;
                        $file_path = $destinationPath.$actual_name;
                        $count++;
                    }                    

                    $file->move($destinationPath, $actual_name);      

                    $hashname = \Hash::make($actual_name);

                    $enc = str_replace("/","", $hashname);

                    $contact_file = new ProjectFile();
                    $contact_file->project_id = $project->id;
                    $contact_file->filename = $actual_name;
                    $contact_file->encrpytname = $enc;
                    $contact_file->download = 0;          
                    $contact_file->uploaded_by = $user->id;        
                    $contact_file->save();                      
                }                      
            }                                 
        }

        $log = [
            'log' => strtoupper($user->first_name.' '.$user->last_name).' '."CREATE".' '.strtoupper($request->get('project_name')).' '."IN PROJECT RECORD.",
            'project_id' => $project->id,
            'created_by' => $user->id,
            'remarks' => nl2br($request->get('remarks'))
        ];
        

        $logged = ProjectLog::savePayload($log);

        return redirect('projects')->with('is_success', 'Project was successfully saved');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show($encryptname)
    {
        $project = Project::byEncrypt($encryptname);
        $joborders = JobOrder::byProject($project->id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit($encryptname)
    {   
        $project = Project::byEncrypt($encryptname);
        $uid = Auth::id();
        $contacts = Contact::all()->pluck('company','id');
        $date_now = Date('m/d/Y');
        $tagging = ['CLOSED'=>'CLOSED','POTENTIAL'=>'POTENTIAL'];

        return view('projects.edit',compact('project','contacts','date_now','tagging'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project)
    {
        $postdata = $request->all();
        if($project->status < 1){
            $postdata['status'] = 0;
        }else{
            $postdata['status'] = 4;
        }        
        $postdata['created_by'] = Auth::id();
        if(empty($request->project_address)){

            $postdata['project_address'] = "";
        }
        $project = Project::updatePayload($postdata,$project->id);

        $user = Auth::user();

        if($request->hasFile('attached')){                 
            $files = Input::file('attached');                        
            if(!empty($files)){
                $destinationPath = storage_path().'/uploads/contacts/'.$project->contact_id.'/'.$project->id;
                if (!\File::exists($destinationPath))
                {
                    mkdir($destinationPath, 0755, true); 
                }  

                foreach ($files as $file) {
                    $filename = $file->getClientOriginalName();                    

                    $file_name = pathinfo($filename, PATHINFO_FILENAME);

                    $extension = File::extension($filename);
                    $actual_name = $file_name.'.'.$extension;
                    $file_path = $destinationPath.$actual_name;

                    $count = 1;
                    while(File::exists($file_path)) {
                        $actual_name = $file_name.'_'.$count.'.'.$extension;
                        $file_path = $destinationPath.$actual_name;
                        $count++;
                    }                    

                    $file->move($destinationPath, $actual_name);      

                    $hashname = \Hash::make($actual_name);

                    $enc = str_replace("/","", $hashname);

                    $contact_file = new ProjectFile();
                    $contact_file->project_id = $project->id;
                    $contact_file->filename = $actual_name;
                    $contact_file->encrpytname = $enc;
                    $contact_file->download = 0;          
                    $contact_file->uploaded_by = $user->id;        
                    $contact_file->save();                      
                }                      
            }                                 
        }
        

        $log = [
            'log' =>  strtoupper($user->first_name.' '.$user->last_name).' '."MODIFIED".' '.strtoupper($request->get('project_name')).' '."IN PROJECT RECORD",
            'project_id' => $project->id,
            'created_by' => $user->id,
            'remarks' => nl2br($request->get('remarks'))
        ];
        
        $logged = ProjectLog::savePayload($log);    
        return back()->with('is_update', 'Contact was successfully saved');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        //
    }

    public function transfer(Request $request){

    }

    public function overview($encryptname){

        $project = Project::byEncrypt2($encryptname);
        $joborders = JobOrder::byProject($project->id);
        $members = ProjectWorkgroup::byProject($project->id);
        $departments = Department::getList();
        $users = User::byProjectNotInWorgroup($project->id);
        $logs = ProjectLog::byProject($project->id);        
        // $timetables = ProjectTimetable::getListByProject($project->id);
        $timetables = Timetable::getAllDetail($project);
        $for_leads = User::getList();
        $boms = BillOfMaterial::byProject($project->id);
        $threads = ProjectThread::byProject($project->id);
        $reqs = UserRequest::byProject($project->id);

        $uid = Auth::id();
        $memIDs = ProjectWorkgroup::membersID($project->id);

        $topics = Timetable::orderBy('timetable','ASC')->pluck('timetable','id');        
        $taskList = [];
        $users_jo = User::getList();
        $everyweek = ['1'=>'Monday','2'=>'Tuesday','3'=>'Wednesday','4'=>'Thursday','5'=>'Friday','6'=>'Saturday','7'=>'Sunday'];
        $settings = ['0'=>'One Time Only','1'=>'Everyday','2'=>'Every month','3'=>'Every week'];
        $date_now = Date('m/d/Y');

        $ot_approved = 0;
        $total = 0;
        $all_ots = OverTime::findByProjectId($project->id);
        foreach ($all_ots as $aot) {
            $date1 = new \DateTime($aot->start_date.' '.$aot->start_time);
            $date2 = new \DateTime($aot->end_date.' '.$aot->end_time);
            $hour = $date1->diff($date2)->format('%h');
            $minute = $date1->diff($date2)->format('%i');
            $equi = number_format($hour + $minute/60,2);
            $total = $total + $equi;
        }
        $ot_approved = $total;

        $ratings = [
            '1'=>'1',
            '2'=>'2',
            '3'=>'3',
            '4'=>'4',
            '5'=>'5',
            '6'=>'6',
            '7'=>'7',
            '8'=>'8',
            '9'=>'9',
            '10'=>'10',
        ];

        $wemail = ['0'=>'Without Email','1'=>'With Email'];

        $reqs = UserRequest::byProject($project->id);
        $govern = Auth::user();
        if($govern->position_id == 4){

            $canJo = 0;
        }else{
            $canJo = 1;
        }        

        $type2 = ['0'=>'Internal','1'=>'External'];
        $inID = Auth::id();

        $email_outs = EmailOut::byProject($project->id);

        $jobs = JobOrder::getUnderProject($project->id);

        $incidents = IncidentReport::byProject($project->id);

        $user = Auth::user();

        if($user->position_id > 4 || $user->position_id === 1){
            
            $allow_close = 1;
        }else{

            $allow_close = 0;
        }
        
        if(in_array($uid, $memIDs) || $uid == $project->created_by || $govern->position_id == 6 || $inID == 1){

            return view('projects.overview',compact('project','joborders','members','departments','users','logs','timetables','for_leads','boms','threads','reqs','taskList','users_jo','everyweek','settings','date_now','topics','uid','ratings','wemail','canJo','type2','email_outs','jobs','incidents','allow_close','ot_approved'));
        }else{
            return view('errors.101');
        }        
    }

    public function getUsersApi(Request $request){

        $users = User::filterByProjDept($request->proj_id,$request->dept_id);       

        return Response::json($users);
    }

    public function timetable($encrpytname){

        $project = Project::byEncrypt($encrpytname);
        $timetables = Timetable::getAll();
        $proj_timetable = ProjectTimetable::byProject($project->id);

        return view('projects.timetable',compact('project','timetables','proj_timetable'));
    }

    public function getLeadUsersApi(Request $request){

        $users = User::getOnDepartment($request->dept_id);

        return Response::json($users);
    }

    public function downloadFile(Request $request){

        $encryptname = $request->get('encname');
        $file = ProjectFile::ByEncryptName($encryptname);        
        $project = Project::findOrFail($file->project_id);            
        $file_down = storage_path().'/uploads/contacts/'.$project->contact_id.'/'.$project->id.'/'.$file->filename;
        
        return Response::download($file_down);
    }

    public function report_project(){

        $projects = Project::activeOnly();

        return view('reports.all_project',compact('projects'));
    }

    public function report_export(Request $request){


        $projects = Project::activeOnly();

        Excel::create('Active Projects Report', function($excel) use($projects) {


            $excel->sheet('PROJECT REPORTS', function($sheet) use($projects) {
                
                 $sheet->cell('A1:L1', function($cell) {                    
                    $cell->setAlignment('center');
                    $cell->setBackground('#3498db');
            
                 });          
          
                $sheet->row(1,array("PROJECT #","CLIENT","PROJECT NAME","START DATE","END DATE","CREATED BY","APPROVED BY","TAG","PROJECT COST","PENDING JO","RETURNED JO","CLOSED JO"));

                if(!empty($projects)){
                    $cnt = 2;
                           
                    foreach ($projects as $project) {                                                                    
                        if(!empty($project)){
                           
                            $sheet->row($cnt,array(
                                    
                                $project->id,
                                strtoupper($project->contact->company),
                                $project->project_name,
                                $project->start_date,
                                $project->end_date,
                                strtoupper($project->user->first_name.' '.$project->user->last_name),
                                strtoupper($project->approver->first_name.' '.$project->approver->last_name),                                
                                $project->tag,
                                $project->project_cost,
                                $project->jo_pending,
                                $project->jo_returned,
                                $project->jo_closed,

                            ));

                            $cnt++;
                        }            
                        
                    }    
                }
            });           
        })->export('xlsx');   
    }
}
