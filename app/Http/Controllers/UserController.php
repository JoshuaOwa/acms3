<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\User;
use App\Position;
use App\Department;
use App\Workgroup;
use App\UserIndustry;
use App\OverTimeSupervisor;
use Auth;
use Response;
use Redirect;

class UserController extends Controller
{
    public function index(){

    	$users = User::getAll();        

    	return view('users.index',compact('users'));
    }

    public function create(){

    	$positions = Position::getList();
    	$departments = Department::getList();
        $users = User::all(); 	

    	return view('users.create',compact('positions','departments','users'));
    }

    public function store(Request $request){

    	$postdata = $request->all();
    	$postdata['password'] = \Hash::make($request->password);
    	$postdata['active'] = 1;    	
    	$postdata['cellno'] = "";
    	$postdata['localno'] = "";
    	$postdata['online'] = 0;
    	$postdata['dept_id'] = 0;
        
    	$user = new User($postdata);
    	

    	if($request->hasFile('image'));
        { 
            $file = Input::file('image');                        
            if(!empty($file)){
                $destinationPath = public_path().'/uploads/users/'. $request->get('employee_number');
                $filename = $file->getClientOriginalName();
                $file->move($destinationPath, $filename);                

                $user['image'] = $filename;
                
            }else{
                $user['image'] = "";
            }
        }	        
        $user->save();

        if(!empty($request->supers)){
            foreach ($request->supers as $super) {
                $find = User::findOrFail($super);
                if(!empty($find)){
                    $spr = new OverTimeSupervisor();
                    $spr->user_id = $user->id;
                    $spr->superior_id = $find->id;
                    $spr->save();
                }
            }
        }

        $indus = $request->get('dept_id');        
        if(!empty($indus)){

            foreach ($indus as $ind) {
                $post2['user_id'] = $user->id;
                $post2['industry_id'] = $ind;
                UserIndustry::savePayload($post2);    
            }
        }
        return redirect('users')->with('is_success', 'User was successfully saved');

    }

    public function edit($id){

    	$user = User::findOrFail($id);

    	$positions = Position::getList();
    	$departments = Department::getList();
        $myIndus = UserIndustry::byUser($id);
        $user_supers = OverTimeSupervisor::findMySupervisors($id);
        $users = User::all();

    	return view('users.edit',compact('user','positions','departments','myIndus','user_supers','users'));
    }

    public function update(Request $request, $id){

    	$user = User::findOrFail($id);
    	$user->first_name = $request->first_name;
    	$user->last_name = $request->last_name;
    	$user->empid = $request->empid;
    	$user->email = $request->email;
    	$user->position_id = $request->position_id;    	
    	// $user->dept_id = $request->dept_id;
    	$user->username = $request->username;

    	if($request->hasFile('image'));
        {   
        	if(!empty($file)){
	            $old_files = public_path().'/uploads/users/'. $request->get('employee_number').'/'.$user->image;// get all file names                                
	            if(is_file($old_files)){
	                unlink($old_files); // delete file
	            }                 
            }   
            $file = Input::file('image');                        
            if(!empty($file)){
                $destinationPath = public_path().'/uploads/users/'. $request->get('employee_number');
                $filename = $file->getClientOriginalName();
                $file->move($destinationPath, $filename);
                $user->image = $filename;
            }                                    
        }                       
        $user->update();

        $supervisors = OverTimeSupervisor::findMine($user->id);
        if(!empty($supervisors)){
            foreach ($supervisors as $super) {
                $super->delete();
            }
        }

        if(!empty($request->supers)){
            foreach ($request->supers as $super) {
                $find = User::findOrFail($super);
                if(!empty($find)){
                    $spr = new OverTimeSupervisor();
                    $spr->user_id = $user->id;
                    $spr->superior_id = $find->id;
                    $spr->save();
                }
            }
        }

        $indus = $request->get('dept_id');        
        UserIndustry::reset($user->id);
        if(!empty($indus)){
            foreach ($indus as $ind) {
                $post2['user_id'] = $user->id;
                $post2['industry_id'] = $ind;
                UserIndustry::savePayload($post2);    
            }
        }
        return redirect('users')->with('is_update', 'User was successfully saved');
    }

    public function activate($id){

    	$user = User::findOrFail($id);

        if($user->active > 0){

            $user->active = 0;
        }else{

            $user->active = 1;
        }

        $user->update();
        return redirect('users');
    }

    public function getUserDetails(Request $request){

        $user = User::findOrFail($request->userid);

        return Response::json($user);
    }

    public function user_reset_pass($payload){

        return view('users.reset_password_user');
    }

    public function reset_password_update_user(Request $request){

        $this->validate($request, [            
            'password' => 'required|min:6',                              
        ]);

        $user = User::find(Auth::id());
        $user->password = \Hash::make($request->password);
        $user->update();        

        return redirect('user/reset/password.php')->with('is_reset', 'User was successfully saved');
    }

   
}
