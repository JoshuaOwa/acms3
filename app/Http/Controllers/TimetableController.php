<?php

namespace App\Http\Controllers;

use App\Timetable;
use App\TimetableTask;
use Illuminate\Http\Request;
use Response;

class TimetableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $timetables = Timetable::getAll();
        $timetable_list = $timetables->pluck('timetable','id');
        $tasks = TimetableTask::all();

        return view('timetable.index',compact('timetables','tasks','timetable_list'));
    }

    public function store(Request $request)
    {
        $timetable = Timetable::savePayload($request->all());        

        return back()->with('is_success','Saved!');
    }

  
    public function timetableUpdates(Request $request){

        $timetable = Timetable::findOrFail($request->get('timeid'));
        $timetable->timetable = $request->get('timetable');        
        $timetable->update();

        return back()->with('is_update','Updated!');
    }

    public function timetableDetails(Request $request){

        $timetable = Timetable::findOrFail($request->get('timeid'));

        return Response::json($timetable);
    }

    
}
