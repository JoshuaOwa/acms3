<?php

namespace App\Http\Controllers;

use App\Workgroup;
use App\User;
use Illuminate\Http\Request;
use Response;

class WorkgroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $workgroups = Workgroup::all();
        $users = User::getList();

        return view('workgroup.index',compact('workgroups','users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $workgroup = new Workgroup($request->all());
        $workgroup->save();

        return back()->with('is_success','Saved!');
    }

    public function workgroupDetails(Request $request){

        $workgroup = Workgroup::findOrFail($request->get('workgroupid'));

        return Response::json($workgroup);
    }

    public function workgroupUpdates(Request $request){

        $workgroup = Workgroup::findOrFail($request->get('workgroup_id'));
        $workgroup->workgroup = $request->get('workgroup_name');        
        $workgroup->update();

        return back()->with('is_update','Updated!');
        
    }
}
