<?php

namespace App\Http\Controllers;

use App\JobOrder;
use App\JobOrderThread;
use App\JobOrderThreadFile;
use App\JobOrderThreadSeen;
use App\JobOrderLog;
use Response;
use File;
use Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;

class JobOrderThreadController extends Controller
{

    public function store(Request $request){

    	$postdata = $request->all();
        $emails = explode(';', preg_replace('/\s+/', '', $postdata['email']));
        $subject = $request->subject;
        $user = Auth::user();

    	$job = JobOrder::findOrFail($request->job_id);        

    	$postdata['contact_id'] = $job->contact_id;
    	$postdata['project_id'] = $job->project_id;
    	$postdata['timetable_id'] = $job->timetable_id;
    	$postdata['task_id'] = $job->task_id;
    	$postdata['job_order_id'] = $job->id;    	
    	$postdata['user_id'] = Auth::id();
        $postdata['message'] = nl2br($request->message);

    	$thread = JobOrderThread::savePayload($postdata);

        $job_logs['logs'] = strtoupper($user->first_name.' '.$user->last_name).' CREATED NEW THREAD.';
        $cntFile = 0;
    	if($request->hasFile('attached')){                 
            $files = Input::file('attached');                        
            if(!empty($files)){
                $destinationPath = storage_path().'/uploads/joborders/'. $job->timetable_id.'/'.$job->task_id.'/'.$job->id.'/threads/'.$thread;
                if (!\File::exists($destinationPath))
                {
                    mkdir($destinationPath, 0755, true); 
                }  
                $cntFile = 1;
                foreach ($files as $file) {
                    
                    $filename = $file->getClientOriginalName();                    

                    $file_name = pathinfo($filename, PATHINFO_FILENAME);

                    $extension = File::extension($filename);
                    $actual_name = $file_name.'.'.$extension;
                    $file_path = $destinationPath.$actual_name;

                    $count = 1;
                    while(File::exists($file_path)) {
                        $actual_name = $file_name.'_'.$count.'.'.$extension;
                        $file_path = $destinationPath.$actual_name;
                        $count++;
                    }                    

                    $file->move($destinationPath, $actual_name);   

                    $hashname = \Hash::make($filename);

                    $enc = str_replace("/","", $hashname);

                    $thread_file = new JobOrderThreadFile();
                    $thread_file->contact_id = $job->contact_id;
                    $thread_file->project_id = $job->project_id;
                    $thread_file->timetable_id = $job->timetable_id;
                    $thread_file->task_id = $job->task_id;
                    $thread_file->job_order_id = $job->id;
                    $thread_file->thread_id = $thread;
                    $thread_file->filename = $actual_name;
                    $thread_file->encrpytname = $enc;
                    $thread_file->download = 0;          
                    $thread_file->uploaded_by = Auth::id();        
                    $thread_file->save();

                    $filesSending[] = $actual_name;                 
                }                  

                $job_logs['logs'] = strtoupper($user->first_name.' '.$user->last_name).' CREATED NEW THREAD WITH ATTACHED FILE.';    
            }                                 
        }
        $postdata['thread_id'] = $thread;
        $postdata['from_user'] = Auth::id();
        $send = JobOrderThreadSeen::savePayload($postdata);

        $job_logs['contact_id'] = $job->contact_id;
        $job_logs['project_id'] = $job->project_id;
        $job_logs['timetable_id'] = $job->timetable_id;
        $job_logs['task_id'] = $job->task_id;
        $job_logs['job_order_id'] = $job->id;
        $job_logs['user_id'] = $user->id;        

        JobOrderLog::savePayload($job_logs);
        $emailTo = [];
        if(!empty($emails)){

            if(!empty($user->email)){

                foreach($emails as $email){

                    if(filter_var($email, FILTER_VALIDATE_EMAIL)) {
                        
                        \Mail::send('emails.thread',["data"=>$postdata['message']],function($m) use($user,$subject,$email,$cntFile,$job,$thread,$filesSending){
                            $m->from($user->email,strtoupper($user->first_name.' '.$user->last_name));
                            $m->to($email)->subject($subject);           
                            if($cntFile > 0){
                                foreach($filesSending as $fsending){

                                    $m->attach(storage_path('/uploads/joborders/'. $job->timetable_id.'/'.$job->task_id.'/'.$job->id.'/threads/'.$thread.'/'.$fsending));         
                                }                                
                            }
                        
                        });  

                        $emailTo[] = $email;
                    }                
                }

                $upData['emails'] = implode(',', $emailTo);
                JobOrderThread::updatePayload($upData,$thread);
            }           
        }        

        return back()->with('is_sent','Sent!');
    }

    public function comment(Request $request){

        
        $postdata = $request->all();
        $emails = explode(';', preg_replace('/\s+/', '', $postdata['email']));
        $user = Auth::user();
        $subject = $request->subject;

        $mainthread = JobOrderThread::findOrFail($request->thread_id);
        $mainthread->updated_at = date('Y-m-d H:i:s');
        $mainthread->update();

        $postdata['contact_id'] = $mainthread->contact_id;
        $postdata['project_id'] = $mainthread->project_id;
        $postdata['timetable_id'] = $mainthread->timetable_id;
        $postdata['task_id'] = $mainthread->task_id;
        $postdata['job_order_id'] = $mainthread->job_order_id;       
        $postdata['comment_to'] = $mainthread->id;
        $postdata['user_id'] = Auth::id();
        $postdata['main'] = 0;
        $postdata['message'] = nl2br($request->message);

        $thread = JobOrderThread::savePayload($postdata);

        $job_logs['logs'] = strtoupper($user->first_name.' '.$user->last_name).' COMMENTED TO THE THREAD CREATED ON THIS DATE '.$mainthread->created_at->format('M-d-Y H:i:s A').'.';    
        $cntFile = 0;
        $filesSending = [];
        if($request->hasFile('attached')){                 
            $files = Input::file('attached');                        
            if(!empty($files)){
                $destinationPath = storage_path().'/uploads/joborders/'. $mainthread->timetable_id.'/'.$mainthread->task_id.'/'.$mainthread->job_order_id.'/threads/'.$thread;
                if (!\File::exists($destinationPath))
                {
                    mkdir($destinationPath, 0755, true); 
                }  
                $cntFile = 1;
                foreach ($files as $file) {
                    
                    $filename = $file->getClientOriginalName();                    

                    $file_name = pathinfo($filename, PATHINFO_FILENAME);

                    $extension = File::extension($filename);
                    $actual_name = $file_name.'.'.$extension;
                    $file_path = $destinationPath.$actual_name;

                    $count = 1;
                    while(File::exists($file_path)) {
                        $actual_name = $file_name.'_'.$count.'.'.$extension;
                        $file_path = $destinationPath.$actual_name;
                        $count++;
                    }                    

                    $file->move($destinationPath, $actual_name);   

                    $hashname = \Hash::make($filename);

                    $enc = str_replace("/","", $hashname);

                    $thread_file = new JobOrderThreadFile();
                    $thread_file->contact_id = $mainthread->contact_id;
                    $thread_file->project_id = $mainthread->project_id;
                    $thread_file->timetable_id = $mainthread->timetable_id;
                    $thread_file->task_id = $mainthread->task_id;
                    $thread_file->job_order_id = $mainthread->job_order_id;
                    $thread_file->thread_id = $thread;
                    $thread_file->filename = $actual_name;
                    $thread_file->encrpytname = $enc;
                    $thread_file->download = 0;          
                    $thread_file->uploaded_by = Auth::id();        
                    $thread_file->save();                      

                    $filesSending[] = $actual_name;  
                }           
                $job_logs['logs'] = strtoupper($user->first_name.' '.$user->last_name).' COMMENTED WITH ATTACHED FILE TO THE THREAD CREATED ON THIS DATE '.$mainthread->created_at->format('M-d-Y H:i:s A').'.';             
            }                                 
        }
        $postdata['thread_id'] = $thread;
        $postdata['from_user'] = Auth::id();
        $send = JobOrderThreadSeen::savePayload($postdata);

        $job_logs['contact_id'] = $mainthread->contact_id;
        $job_logs['project_id'] = $mainthread->project_id;
        $job_logs['timetable_id'] = $mainthread->timetable_id;
        $job_logs['task_id'] = $mainthread->task_id;
        $job_logs['job_order_id'] = $mainthread->job_order_id;
        $job_logs['user_id'] = $user->id;        

        JobOrderLog::savePayload($job_logs);

        $emailTo = [];
        if(!empty($emails)){

            if(!empty($user->email)){

                foreach($emails as $email){

                    if(filter_var($email, FILTER_VALIDATE_EMAIL)) {
                        
                        \Mail::send('emails.thread',["data"=>$postdata['message']],function($m) use($user,$subject,$email,$cntFile,$mainthread,$thread,$filesSending){
                            $m->from($user->email,strtoupper($user->first_name.' '.$user->last_name));
                            $m->to($email)->subject($subject);         
                            if($cntFile > 0){
                                foreach($filesSending as $fsending){

                                    $m->attach(storage_path('/uploads/joborders/'. $mainthread->timetable_id.'/'.$mainthread->task_id.'/'.$mainthread->job_order_id.'/threads/'.$thread.'/'.$fsending));         
                                }                                
                            }           
                        });  

                        $emailTo[] = $email;
                    }                
                }

                $upData['emails'] = implode(',', $emailTo);
                JobOrderThread::updatePayload($upData,$thread);
            }           
        }        

        return back()->with('is_sent','Sent!');
    }

    public function download(Request $request){

        $encryptname = $request->get('encname');
        $file = JobOrderThreadFile::ByEncryptName($encryptname);                
        $file_down= storage_path().'/uploads/joborders/'. $file->timetable_id.'/'.$file->task_id.'/'.$file->job_order_id.'/threads/'.$file->thread_id.'/'.$file->filename;  
        $file->download = $file->download+1;
        $file->update();
        return Response::download($file_down);
    }

    public function comment_download(Request $request){

    	$encryptname = $request->get('encname');
        $file = JobOrderThreadFile::ByEncryptName($encryptname);        
        $file_down= storage_path().'/uploads/joborders/'. $file->timetable_id.'/'.$file->task_id.'/'.$file->job_order_id.'/threads/'.$file->thread_id.'/'.$file->filename;  
        $file->download = $file->download+1;
        $file->update();
        return Response::download($file_down);
    }
}
