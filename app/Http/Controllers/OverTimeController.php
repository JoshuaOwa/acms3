<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\JobOrder;
use App\User;
use App\OverTime;
use App\OverTimeSupervisor;
use App\Project;

class OverTimeController extends Controller
{
	public function index()
	{
		$user = Auth::user();
		$mines = OverTimeSupervisor::getMyUnderlings($user->id);
		$ots = OverTime::findPendingByUserId($mines);

		return view('overtimes.index',compact('ots'));
	}

    public function store(Request $request)
    {
    	$ot = new OverTime();
    	$ot->user_id = Auth::user()->id;
    	$ot->job_order_id = $request->job_order_id;
    	$ot->project_id = $request->project_id;
    	$ot->company = 0;
    	$ot->branch_id = 0;
    	$ot->start_date = date('Y-m-d',strtotime($request->start_date));
    	$ot->end_date = date('Y-m-d',strtotime($request->end_date));
    	$ot->start_time = date('H:i',strtotime($request->start_time));
    	$ot->end_time = date('H:i',strtotime($request->end_time));
    	$ot->category_id = $request->categ;
    	$ot->type_id = $request->type;
    	$ot->remarks = nl2br($request->remarks);
    	$ot->encrypt_name = 0;
    	$ot->save();

    	$hash = \Hash::make($ot->remarks.''.$ot->id);
    	$ot->encrypt_name = str_replace("/","", $hash);
    	$ot->update();

    	return redirect()->back()->with('ot_is_success','Success');
    }

    public function overview($encrypt)
    {
    	$ot = OverTime::findByEncryptName($encrypt);
    	if(!empty($ot)){
    		$jo = JobOrder::findOrFail($ot->job_order_id);
    		$project = Project::findOrFail($ot->project_id);

    		return view('overtimes.overview',compact('ot','jo','project'));
    	}
    }

    public function approval(Request $request)
    {
    	$user = Auth::user();
    	$sub = $request->type;
    	$ot = OverTime::findOrFail($request->id);
    	if(!empty($ot)){
    		if($sub == 1){
	    		$ot->update_remarks = nl2br($request->confirmed_remarks);
	    		$ot->approved_by = $user->id;
	    		$ot->status = 2;
	    		$ot->save();

	    		return redirect('overtimes')->with('is_success','Approved');
	    	}
	    	if($sub == 2){
	    		$ot->update_remarks = nl2br($request->confirmed_remarks);
	    		$ot->approved_by = $user->id;
	    		$ot->status = 3;
	    		$ot->save();

	    		return redirect('overtimes')->with('is_denied','Denied');
	    	}
    	}
    	return redirect()->back()->with('is_error_not_found','Error');
    }
}
