<?php

namespace App\Http\Controllers;

use App\IncidentReport;
use App\IncidentReportThread;
use App\IncidentReportFile;
use App\IncidentReportSolutionFile;
use App\IncidentReportThreadFile;
use App\Project;
use App\User;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Input;
use Response;
use File;
use Excel;

class IncidentReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $project = Project::find($request->project_id);
        $postdata = $request->all();

        $postdata['created_by'] = Auth::id();
        $postdata['main'] = 1;
        $postdata['main_id'] = 0;
        $postdata['status'] = 1;
        $postdata['details'] = nl2br($request->details);

        $incident = IncidentReport::savePayload($postdata);

        if($request->hasFile('attached')){                 
            $files = Input::file('attached');                        
            if(!empty($files)){
                $destinationPath = storage_path().'/uploads/contacts/'.$project->contact_id.'/'.$project->id.'/'.$incident->id;
                if (!\File::exists($destinationPath))
                {
                    mkdir($destinationPath, 0755, true); 
                }  

                foreach ($files as $file) {
                    $filename = $file->getClientOriginalName();                    

                    $file_name = pathinfo($filename, PATHINFO_FILENAME);

                    $extension = File::extension($filename);
                    $actual_name = $file_name.'.'.$extension;
                    $file_path = $destinationPath.$actual_name;

                    $count = 1;
                    while(File::exists($file_path)) {
                        $actual_name = $file_name.'_'.$count.'.'.$extension;
                        $file_path = $destinationPath.$actual_name;
                        $count++;
                    }                    

                    $file->move($destinationPath, $actual_name);      

                    $hashname = \Hash::make($actual_name);

                    $enc = str_replace("/","", $hashname);

                    $incident_file = new IncidentReportFile();
                    $incident_file->contact_id = $project->contact_id;
                    $incident_file->project_id = $project->id;
                    $incident_file->ir_id = $incident->id;
                    $incident_file->filename = $actual_name;
                    $incident_file->encryptname = $enc;                    
                    $incident_file->uploaded_by = Auth::id();
                    $incident_file->save();                      
                }                      
            }                                 
        }

        return back();
    }

    public function downloadFile(Request $request){

        $encryptname = $request->get('encname');
        $file = IncidentReportFile::ByEncryptName($encryptname);        

        $file_down= storage_path().'/uploads/contacts/'.$file->contact_id.'/'.$file->project_id.'/'.$file->ir_id.'/'.$file->filename;

        return Response::download($file_down);
    }

    public function comment_store(Request $request){

        $project = Project::find($request->project_id);
        $postdata = $request->all();

        $postdata['created_by'] = Auth::id();
        $postdata['ir_id']  = $request->ir_id;
        $postdata['details'] = nl2br($request->details);
        $postdata['user_id'] = Auth::id();

        $incident = IncidentReportThread::savePayload($postdata);

        if($request->hasFile('attached')){                 
            $files = Input::file('attached');                        
            if(!empty($files)){
                $destinationPath = storage_path().'/uploads/contacts/'.$project->contact_id.'/'.$project->id.'/'.$request->ir_id.'/'.$incident->id;
                if (!\File::exists($destinationPath))
                {
                    mkdir($destinationPath, 0755, true); 
                }  

                foreach ($files as $file) {
                    $filename = $file->getClientOriginalName();                    

                    $file_name = pathinfo($filename, PATHINFO_FILENAME);

                    $extension = File::extension($filename);
                    $actual_name = $file_name.'.'.$extension;
                    $file_path = $destinationPath.$actual_name;

                    $count = 1;
                    while(File::exists($file_path)) {
                        $actual_name = $file_name.'_'.$count.'.'.$extension;
                        $file_path = $destinationPath.$actual_name;
                        $count++;
                    }                    

                    $file->move($destinationPath, $actual_name);      

                    $hashname = \Hash::make($actual_name);

                    $enc = str_replace("/","", $hashname);

                    $incident_file = new IncidentReportThreadFile();
                    $incident_file->contact_id = $project->contact_id;
                    $incident_file->project_id = $project->id;
                    $incident_file->ir_thread_id = $incident->id;
                    $incident_file->filename = $actual_name;
                    $incident_file->encryptname = $enc;                    
                    $incident_file->uploaded_by = Auth::id();
                    $incident_file->save();                      
                }                      
            }                                 
        }

        return back();
        
                
    }

    public function solution_store(Request $request){

        $project = Project::find($request->project_id);
        $postdata = $request->all();
            
        $postdata['solution'] = nl2br($request->solution);
        $postdata['resolved_by'] = Auth::id();
        $postdata['resolved_date'] = date('Y-m-d');
        $postdata['status'] = 3;

        $incident = IncidentReport::updatePayload($postdata,$request->ir_id);
        $cntFile = 0;
        $destinationPath = "";
        $filesSending = [];  
        if($request->hasFile('attached')){                 
            $files = Input::file('attached');                        
            if(!empty($files)){
                $destinationPath = storage_path().'/uploads/contacts/'.$project->contact_id.'/'.$project->id.'/'.$request->ir_id.'/solution/';
                if (!\File::exists($destinationPath))
                {
                    mkdir($destinationPath, 0755, true); 
                }  

                foreach ($files as $file) {
                    $filename = $file->getClientOriginalName();                    

                    $file_name = pathinfo($filename, PATHINFO_FILENAME);

                    $extension = File::extension($filename);
                    $actual_name = $file_name.'.'.$extension;
                    $file_path = $destinationPath.$actual_name;

                    $count = 1;
                    while(File::exists($file_path)) {
                        $actual_name = $file_name.'_'.$count.'.'.$extension;
                        $file_path = $destinationPath.$actual_name;
                        $count++;
                    }                    

                    $file->move($destinationPath, $actual_name);      

                    $hashname = \Hash::make($actual_name);

                    $enc = str_replace("/","", $hashname);

                    $incident_file = new IncidentReportSolutionFile();
                    $incident_file->contact_id = $project->contact_id;
                    $incident_file->project_id = $project->id;
                    $incident_file->ir_id = $incident->id;
                    $incident_file->filename = $actual_name;
                    $incident_file->encryptname = $enc;                    
                    $incident_file->uploaded_by = Auth::id();
                    $incident_file->save();     

                    $cntFile++;                 
                    $filesSending[] = $actual_name;
                }                      
            }                                 
        }
        $email_adds = [];
        $users = [];
        $users[] = $project->created_by;
        $users[] = $project->approver_id;

        $email_adds = User::getEmail($users);
        $sender = Auth::user();

        if(!empty($email_adds)){                
            foreach($email_adds as $email){
                
                if(filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    
                    \Mail::send('emails.incident_solution',
                        [
                            "data"=>$incident->solution,
                            "ir" => $incident->id,
                        ],function($m) use($sender,$email,$cntFile,$destinationPath,$filesSending,$incident){
                        $m->from($sender->email,strtoupper($sender->first_name.' '.$sender->last_name));
                        $m->to($email)->subject("IR#: ".$incident->id.' Final Solution');           
                        if($cntFile > 0){
                            foreach($filesSending as $fsending){
                                $m->attach($destinationPath.'/'.$fsending);         
                            }                                
                        }                    
                    });                        
                }             
            }
        }
            

        return back();
        
                
    }

    public function closing_store(Request $request){

        $project = Project::find($request->project_id);
        $postdata = $request->all();
            
        $postdata['final_remarks'] = nl2br($request->final_remarks);
        $postdata['closed_by'] = Auth::id();        
        $postdata['status'] = 4;

        $incident = IncidentReport::updatePayload($postdata,$request->ir_id);   
        $user_r = User::find($incident->resolved_by);
        $email = $user_r->email;

        $sender = Auth::user();

        if(!empty($email)){                            
                
            if(filter_var($email, FILTER_VALIDATE_EMAIL)) {
                
                \Mail::send('emails.incident_closed',
                    [
                        "rating"=>$incident->ratings,
                        "remarks" => $incident->final_remarks,
                        "ir" => $incident->id,
                    ],function($m) use($sender,$email,$incident){
                    $m->from($sender->email,strtoupper($sender->first_name.' '.$sender->last_name));
                    $m->to($email)->subject("IR#: ".$incident->id.' Closed');                                   
                });                        
            }                         
        }
            

        return back();
        
                
    }

    public function downloadThreadFile(Request $request){

        $encryptname = $request->get('encname');
        $file = IncidentReportThreadFile::ByEncryptName($encryptname);        

        
        $file_down= storage_path().'/uploads/contacts/'.$file->contact_id.'/'.$file->project_id.'/'.$request->ir_id.'/'.$file->ir_thread_id.'/'.$file->filename;

        return Response::download($file_down);
    }

    public function downloadSolutionFile(Request $request){

        $encryptname = $request->get('encname');
        $file = IncidentReportSolutionFile::ByEncryptName($encryptname);                
        $file_down= storage_path().'/uploads/contacts/'.$file->contact_id.'/'.$file->id.'/'.$request->ir_id.'/solution/'.$file->filename;

        return Response::download($file_down);
    }

    public function export(Request $request){
        

        $project = Project::find($request->project_id);
        $incidents = IncidentReport::byProject($project->id);

        Excel::create('Incident Reports In '.$project->project_name, function($excel) use($incidents) {


            $excel->sheet('INCIDENT REPORTS', function($sheet) use($incidents) {
                
                 $sheet->cell('A1:N1', function($cell) {                    
                    $cell->setAlignment('center');
                    $cell->setBackground('#3498db');            
                 });          
          
                $sheet->row(1,array("IR #","FILED BY","CLIENT COORDINATOR","SUBJECT","DETAILS","ATTACHMENTS","STATUS","SOLUTION","SOLUTION ATTACHMENTS","RESOLVED BY","RESOLVED DATE","CLOSED BY","FINAL REMARKS","RATING"));

                if(!empty($incidents)){

                    $cnt = 2;                           
                    foreach ($incidents as $incident) {                                                                    
                        if(!empty($incident)){
                            
                            $rate = "N/A";
                            $sol = "N/A";
                            $cName = "N/A";
                            $fRemark = "N/A";

                            if(!empty($incident->resolved_date)){
                                $rDate = $incident->resolved_date;
                                $rname = strtoupper($incident->resolved->first_name.' '.$incident->resolved->last_name);
                            }else{
                                $rDate = "N/A";
                                $rname = "N/A";
                            }

                            if($incident->status === 1){
                                $stat = "New";
                            }else if($incident->status === 2){
                                $stat = "Pending";                                
                            }else if($incident->status === 3){
                                $stat = "Returned";             
                                $sol = strip_tags(preg_replace( "/\r|\n/", " ", $incident->solution));                   
                            }else if($incident->status === 4){
                                $stat = "Closed";
                                $sol = strip_tags(preg_replace( "/\r|\n/", " ", $incident->solution)); 
                                $cName = strtoupper($incident->closed->first_name.' '.$incident->closed->last_name);
                                $fRemark = strip_tags(preg_replace( "/\r|\n/", " ", $incident->final_remarks ));
                                $rate = $incident->ratings;
                            }

                            $sheet->row($cnt,array(
                                    
                                "IR#: ".$incident->id,
                                strtoupper($incident->user->first_name.' '.$incident->user->last_name),
                                $incident->client_coor,
                                $incident->subject,
                                strip_tags(preg_replace( "/\r|\n/", " ", $incident->details )),
                                count($incident->files),
                                $stat,
                                $sol,
                                count($incident->solution_file),
                                $rname,
                                $rDate,
                                $cName,
                                $fRemark,
                                $rate

                            ));                           
                            $cnt++;
                        }            
                        
                    }    
                }
            });           
        })->export('xlsx');           
    }
}
