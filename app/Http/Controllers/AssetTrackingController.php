<?php

namespace App\Http\Controllers;

use App\AssetTracking;
use App\AssetRouting;
use App\User;
use Illuminate\Http\Request;
use Auth;

class AssetTrackingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $assets = AssetTracking::orderBy('item_name','ASC')->get();

        return view('assets.lists',compact('assets'));


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('assets.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $postdata = $request->all();
        $postdata['io'] = 1;
        $postdata['holder'] = Auth::id();
        $postdata['must_date'] = date('Y-m-d',strtotime('0000-00-00'));
        $postdata['returned_date'] = date('Y-m-d',strtotime('0000-00-00'));
        $asset = AssetTracking::savePayload($postdata);

        return redirect('asset_trackings')->with('is_success','Saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AssetTracking  $assetTracking
     * @return \Illuminate\Http\Response
     */
    public function show(AssetTracking $assetTracking)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AssetTracking  $assetTracking
     * @return \Illuminate\Http\Response
     */
    public function edit(AssetTracking $assetTracking)
    {
        $asset = $assetTracking;

        return view('assets.edit',compact('asset'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AssetTracking  $assetTracking
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AssetTracking $assetTracking)
    {
        $assetTracking->barcode = $request->barcode;
        $assetTracking->item_name = $request->item_name;
        $assetTracking->remarks = $request->remarks;
        $assetTracking->update();

        return redirect('asset_trackings')->with('is_updated','Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AssetTracking  $assetTracking
     * @return \Illuminate\Http\Response
     */
    public function destroy(AssetTracking $assetTracking)
    {
        //
    }

    public function route_history($id){

        $asset = AssetTracking::findOrFail($id);        
        $routings = AssetRouting::byAsset($id);
        $users = User::FullDetails();
        return view('assets.route_history',compact('routings','asset','users'));
    }

    public function route_history_update(Request $request){

        $type = $request->get('type');
        $asset = AssetTracking::byBarcode($request->get('barcode'));
        $user = User::findOrFail($request->get('user_id'));
        if($type > 0){
            
            $pass = $request->get('password');

            if(\Hash::check($pass, $user->password)){
                if(!empty($asset)){

                    $routings = $request->all();
                    $routings['holder'] = $request->get('user_id');
                    $routings['remarks'] = nl2br($request->get('remarks'));
                    $routings['remarks2'] = "Asset Has Been Released To ".' '.strtoupper($user->first_name.' '.$user->last_name);
                    $routings['returned_date'] = date('Y-m-d',strtotime('0000-00-00'));
                    $routings['must_date'] = date('Y-m-d',strtotime($request->date));
                    $routings['return_status'] = 0;
                    $routings['asset_id'] = $asset->id;

                    $routs = AssetRouting::savePayload($routings);

                    $asset->io = 0;
                    $asset->holder = $user->id;                    
                    $asset->must_date = date('Y-m-d',strtotime($request->date));
                    $asset->returned_date = date('Y-m-d',strtotime('0000-00-00'));
                    $asset->update();
                }
            }else{

                return redirect()->action('AssetTrackingController@route_history',['id'=>$asset->id])->with('is_pass', 'Saved');
            }

        }else{
                        
            $pass = $request->get('password');

            if(\Hash::check($pass, $user->password)){
                if(!empty($asset)){

                    $routings = $request->all();
                    $routings['holder'] = Auth::id();
                    $routings['remarks'] = nl2br($request->get('remarks'));
                    $routings['remarks2'] = "Asset Has Been Returned To ".' '.strtoupper($user->first_name.' '.$user->last_name);
                    $routings['returned_date'] = date('Y-m-d');
                    $routings['must_date'] = date('Y-m-d',strtotime('0000-00-00'));
                    $routings['return_status'] = 0;
                    $routings['asset_id'] = $asset->id;

                    $routs = AssetRouting::savePayload($routings);
                    
                    $asset->io = 1;
                    $asset->holder = $user->id;             
                    $asset->returned_date = date('Y-m-d');                                        
                    $asset->update();

                    
                    if($routs->returned_date > $asset->must_date){
                        $routs->return_status = 2;
                    }else{
                        $routs->return_status = 1;
                    }
                }   $routs->update();
            }else{
                return redirect()->action('AssetTrackingController@route_history',['id'=>$asset->id])->with('is_pass', 'Saved');
            }            
        }

        
        return redirect()->action('AssetTrackingController@route_history',['id'=>$asset->id]);
    }
}
