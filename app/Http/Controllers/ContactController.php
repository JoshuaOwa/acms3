<?php

namespace App\Http\Controllers;

use App\Contact;
use App\ContactType;
use App\ContactLog;
use App\ContactFile;
use App\ContactPerson;
use App\Project;
use App\ProjectContact;
use App\User;
use App\UserNotification;
use App\UserRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;
use File;

class ContactController extends Controller
{

    protected $contact;
    public function __construct(Contact $contact){

        $this->contact = $contact;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = Auth::id();
        $contacts = Contact::getAll();
        // $logs = ContactLog::byMe($id);
        $employees = User::nameExcludedMe($id);
        $contact_list = $contacts->pluck('contact_person','id');

        return view('contacts.mylist',compact('contacts','employees','contact_list','id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $types = ContactType::getList();
        $user_id = Auth::id();
        return view('contacts.create',compact('types','user_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $postdata = $request->all();
        $postdata['user_id'] = Auth::id();
        $postdata['approver_id'] = 0;
        $postdata['status'] = 0;
        $hash = \Hash::make($request->contact_person);
        $postdata['encryptname'] = str_replace("/","", $hash);                
        $contact = Contact::savePayload($postdata);        
        $postdata['contact_id'] = $contact;
        $contact_person = ContactPerson::savePayload($postdata);
        $user = Auth::user();

        if($request->hasFile('attached')){                 
            $files = Input::file('attached');                        
            if(!empty($files)){
                $destinationPath = storage_path().'/uploads/contacts/'. $contact;
                if (!\File::exists($destinationPath))
                {
                    mkdir($destinationPath, 0755, true); 
                }  

                foreach ($files as $file) {
                    $filename = $file->getClientOriginalName();                    

                    $file_name = pathinfo($filename, PATHINFO_FILENAME);

                    $extension = File::extension($filename);
                    $actual_name = $file_name.'.'.$extension;
                    $file_path = $destinationPath.$actual_name;

                    $count = 1;
                    while(File::exists($file_path)) {
                        $actual_name = $file_name.'_'.$count.'.'.$extension;
                        $file_path = $destinationPath.$actual_name;
                        $count++;
                    }                    

                    $file->move($destinationPath, $actual_name);      

                    $hashname = \Hash::make($actual_name);

                    $enc = str_replace("/","", $hashname);

                    $contact_file = new ContactFile();
                    $contact_file->contact_id = $contact;
                    $contact_file->filename = $actual_name;
                    $contact_file->encrpytname = $enc;
                    $contact_file->download = 0;          
                    $contact_file->uploaded_by = $user->id;        
                    $contact_file->save();                      
                }                      
            }                                 
        }
        

        $log = [
            'log' => strtoupper($user->first_name.' '.$user->last_name).' '."CREATE".' '.strtoupper($request->get('contact_person')).' '."IN CONTACT RECORD",
            'contact_id' => $contact,
            'created_by' => $user->id,
            'remarks' => nl2br($request->get('remarks'))
        ];
        

        $logged = ContactLog::savePayload($log);    
       

        return redirect('contacts')->with('is_success', 'Contact was successfully saved');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function show($encryptname)
    {
        $contact = Contact::byEncrypt($encryptname);
        $logs = ContactLog::byContact($contact->id);
        return view('contacts.details',compact('contact','logs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function edit($encrypt)
    {   
        $contact = Contact::byEncrypt($encrypt);
        $types = ContactType::getList();

        return view('contacts.edit',compact('types','contact'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contact $contact)
    {
        $postdata = $request->all();
        if($contact->status < 1){
            $postdata['status'] = 0;
        }else{
            $postdata['status'] = 3;
        }      
        $postdata['created_by'] = Auth::id();
        $contact = Contact::updatePayload($postdata,$contact->id);
        $contact_person = ContactPerson::updatePayload($postdata,$contact);

        $user = Auth::user();

        if($request->hasFile('attached')){                 
            $files = Input::file('attached');                        
            if(!empty($files)){
                $destinationPath = storage_path().'/uploads/contacts/'. $contact.'/';
                if (!\File::exists($destinationPath))
                {
                    mkdir($destinationPath, 0755, true); 
                }  

                foreach ($files as $file) {
                    $filename = $file->getClientOriginalName();                    

                    $file_name = pathinfo($filename, PATHINFO_FILENAME);

                    $extension = File::extension($filename);
                    $actual_name = $file_name.'.'.$extension;
                    $file_path = $destinationPath.$actual_name;

                    $count = 1;
                    while(File::exists($file_path)) {
                        $actual_name = $file_name.'_'.$count.'.'.$extension;
                        $file_path = $destinationPath.$actual_name;
                        $count++;
                    }                                        
                    $file->move($destinationPath, $actual_name);      

                    $hashname = \Hash::make($actual_name);

                    $enc = str_replace("/","", $hashname);

                    $contact_file = new ContactFile();
                    $contact_file->contact_id = $contact;
                    $contact_file->filename = $actual_name;
                    $contact_file->encrpytname = $enc;
                    $contact_file->download = 0;          
                    $contact_file->uploaded_by = $user->id;        
                    $contact_file->save();                      
                }                      
            }                                 
        }

        $log = [
            'log' => strtoupper($user->first_name.' '.$user->last_name).' '."MODIFIED THE CONTACT INFORMATION.",
            'contact_id' => $contact,
            'created_by' => $user->id,
            'remarks' => nl2br($request->get('remarks'))
        ];
        

        $logged = ContactLog::savePayload($log);    

        return redirect('contacts')->with('is_update', 'Contact was successfully saved');

    }

    public function filter(Request $request){
        
        $id = Auth::id();
        $contacts = Contact::filter($id,$request->get('filter'));
        $logs = ContactLog::byMe($id);
        $employees = User::nameExcludedMe($id);
        $contact_list = $contacts->pluck('contact_person','id');
        return view('contacts.mylist',compact('contacts','logs','employees','contact_list','id'));
    }

    public function transfer(Request $request){
        
        $user = Auth::user();
        $contacts = $request->get('contact_id');
        $postdata['created_by'] = $request->get('created_by');
        if(!empty($contacts)){

            foreach ($contacts as $contact) {

                $emp = User::findOrFail($request->created_by);
                $contact_person = Contact::findOrFail($contact);       
                $contact_person->updatePayload($postdata,$contact_person->id);

                $log = [
                    'log' => strtoupper($user->first_name.' '.$user->last_name).' '."TRANSFERRED".' '.strtoupper($contact_person->contact_person).' '."TO ".strtoupper($emp->first_name.' '.$emp->last_name),
                    'contact_id' => $contact,
                    'created_by' => $user->id,
                    'remarks' => ""
                ];
                

                $logged = ContactLog::savePayload($log);    
            }

            return back()->with('is_transferred','save');
        }else{

            return back()->with('not_transferred','save');
        }        
    }

    public function overview($encrypt){

        $contact = Contact::byEncrypt2($encrypt);
        $projects = Project::byContact($contact->id);   
        // $float_request = UserRequest::byContact($contact->id);
        $logs = ContactLog::byContact($contact->id);
        $reqs = UserRequest::byContact($contact->id);
        
        return view('contacts.overview',compact('contact','projects','logs','reqs'));   
    }

    public function head_overide(Request $request, $id)
    {
        $contact = Contact::findOrFail($id);
        $postdata = $request->all();
               
        $contact = Contact::updatePayload($postdata,$contact->id);
        $contact_person = ContactPerson::updatePayload($postdata,$contact);

        $user = Auth::user();

        if($request->hasFile('attached')){                 
            $files = Input::file('attached');                        
            if(!empty($files)){
                $destinationPath = storage_path().'/uploads/contacts/'. $contact.'/';
                if (!\File::exists($destinationPath))
                {
                    mkdir($destinationPath, 0755, true); 
                }  

                foreach ($files as $file) {
                    $filename = $file->getClientOriginalName();                    

                    $file_name = pathinfo($filename, PATHINFO_FILENAME);

                    $extension = File::extension($filename);
                    $actual_name = $file_name.'.'.$extension;
                    $file_path = $destinationPath.$actual_name;

                    $count = 1;
                    while(File::exists($file_path)) {
                        $actual_name = $file_name.'_'.$count.'.'.$extension;
                        $file_path = $destinationPath.$actual_name;
                        $count++;
                    }                                        
                    $file->move($destinationPath, $actual_name);      

                    $hashname = \Hash::make($actual_name);

                    $enc = str_replace("/","", $hashname);

                    $contact_file = new ContactFile();
                    $contact_file->contact_id = $contact;
                    $contact_file->filename = $actual_name;
                    $contact_file->encrpytname = $enc;
                    $contact_file->download = 0;          
                    $contact_file->uploaded_by = $user->id;        
                    $contact_file->save();                      
                }                      
            }                                 
        }

        $log = [
            'log' => strtoupper($user->first_name.' '.$user->last_name).' '."OVERRIDE THE CONTACT INFORMATION.",
            'contact_id' => $contact,
            'created_by' => $user->id,
            'remarks' => nl2br($request->get('remarks'))
        ];
        

        $logged = ContactLog::savePayload($log);    

        return back()->with('is_update_com', 'Contact was successfully saved');

    }

    public function add_person(Request $request){

        $postdata = $request->all();

        $contact_person = ContactPerson::savePayload($postdata);
        $project_contact = ProjectContact::savePayload($postdata);

        return back();
    }

}
