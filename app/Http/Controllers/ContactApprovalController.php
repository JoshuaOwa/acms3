<?php

namespace App\Http\Controllers;

use App\Contact;
use App\ContactType;
use App\ContactFile;
use App\ContactLog;
use Auth;
use Illuminate\Http\Request;

class ContactApprovalController extends Controller
{
	
	public function index(){

		$contacts = Contact::forApproval();

		return view('contacts.forapproval',compact('contacts'));
	}

	public function show($encrypt){

		$contact = Contact::byEncrypt($encrypt);
		$logs = ContactLog::byContact($contact->id);
		$types = ContactType::getList();

		return view('contacts.approval_details',compact('contact','logs','types'));
	}

	public function update_status(Request $request){

		$user = Auth::user();
		$contact = Contact::findOrFail($request->get('contact_id'));
		$postdata['status'] = $request->get('status');
		$postdata['approver_id'] = $user->id;		

		Contact::updatePayload($postdata,$contact->id);
		if($request->get('status') < 2){
			$status_label = "APPROVED";
		}else{
			$status_label = "DENIED";
		}		
		
		$log = [
			'log' => strtoupper($user->first_name.' '.$user->last_name).' '.$status_label.' '."THE CONTACT.",
			'contact_id' => $contact->id,
			'created_by' => $user->id,
			'remarks' => nl2br($request->get('remarks'))
		];
        
        $logged = ContactLog::savePayload($log);    

		return back()->with('is_update_com','updated!');
	}

	public function head_overide(Request $request, $id){

		
	}

	public function filter(Request $request){
               
        $contacts = Contact::filterBy($request->get('filter'));        
        return view('contacts.forapproval',compact('contacts'));
    }
}
