<?php

namespace App\Http\Controllers;

use App\Announcement;
use App\AnnouncementFile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;
use Response;


class AnnouncementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('announcement.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   

        set_time_limit(0);
        ini_set('memory_limit', '-1');      
        
        $ann = new Announcement();
        $ann->subject = $request->get('subject');
        $ann->message = nl2br($request->get('message'));
        $ann->save();


        if($request->hasFile('attached')){
                
            $files = Input::file('attached');                    
                   
                $destinationPath = public_path().'/uploads/Announcement/'.$ann->id.'/';  

                   if (!\File::exists($destinationPath))
                {
                    mkdir($destinationPath, 0755, true); 
                }  
            foreach ($files as $file) {
               

                $filename = $file->getClientOriginalName();    
             

                $namefile = Date('YmdHis').$filename;
                // move_uploaded_file($destinationPath, $namefile);
                $file->move($destinationPath, $namefile);

                
                $hashname = \Hash::make($namefile);

                $enc = str_replace("/","", $hashname);

                $message_file = new AnnouncementFile();
                $message_file->ann_id = $ann->id;
                $message_file->filename = $namefile;
                $message_file->encryptname = $enc;
                $message_file->uploaded_by = Auth::id();                
                $message_file->save();
            }
        }        
        return back()->with('is_success','Saved!');
    }

    public function getDetails(Request $request){

        $memo = Announcement::getlatest($request->memoid);

        return Response::json($memo);
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    public function show(Announcement $announcement)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    public function edit(Announcement $announcement)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Announcement $announcement)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    public function destroy(Announcement $announcement)
    {
        //
    }

    public function memo_details($id){

     $memo = Announcement::getlatest($id);


     return view('announcement.details',compact('memo'));
    }

    public function download_file(Request $request){

        $encryptname = $request->get('encname');
        $file = AnnouncementFile::where('encryptname',$encryptname)->first();
        
        

        $file_down= public_path().'/uploads/Announcement/'.$file->ann_id.'/'.$file->filename;               
        

        return Response::download($file_down);
    }
}
