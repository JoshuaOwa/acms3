<?php

namespace App\Http\Controllers;

use App\Position;
use Illuminate\Http\Request;
use Response;

class PositionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $positions = Position::all();

        return view('position.index',compact('positions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $position = new Position($request->all());
        $position->save();

        return back()->with('is_success','Saved!');
    }

    public function positionDetails(Request $request){

        $position = Position::findOrFail($request->get('positionid'));

        return Response::json($position);
    }

    public function positionUpdates(Request $request){

        $position = Position::findOrFail($request->get('position_id'));
        $position->position = $request->get('position');        
        $position->update();

        return back()->with('is_update','Updated!');
        
    }
}
