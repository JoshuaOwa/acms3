<?php

namespace App\Http\Controllers;

use App\UserRequest;
use App\UserRequestDetail;
use App\UserRequestRemark;
use App\UserRequestFile;
use App\UserRequestRemarkFile;
use App\UserRequestCc;
use App\User;
use App\JobOrder;
use App\ScheduleJobOrder;
use App\Contact;
use App\Project;
use App\ReminderUpdate;
use App\UserNotification;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Response;
use File;

class MyRequestController extends Controller
{
    public function index(){

    	$user = Auth::id();
    	$my_requests = UserRequest::byUser($user);        
    	$requests_for_me = UserRequest::forMe($user);
       $contacts = Contact::createdAndApproved($user);
       $projects = [];
       $types = ['0'=>'Company','1'=>'Project'];
        // return view('my_request.list',compact('my_requests','requests_for_me'));        

    	return view('my_request.list',compact('my_requests','requests_for_me','contacts','projects','types'));
    }

    public function show($encrypt){

        $uid = Auth::id();
    	$req = UserRequest::byEncrypt($encrypt);
    	$remarks = UserRequestRemark::byRequest($req->id);
    	$files = UserRequestFile::byRequest($req->id);        
        $users = User::byRequestNotMember($req->id);
        $ccs = UserRequestCc::getList($req->id);        
        // $users = User::nameExcludedme($uid);
        // $settings = ['0'=>'One Time Only','1'=>'Everyday','2'=>'Every month','3'=>'Every week'];
        // $everyweek = ['1'=>'Monday','2'=>'Tuesday','3'=>'Wednesday','4'=>'Thursday','5'=>'Friday','6'=>'Saturday','7'=>'Sunday'];
        // $months = ['1'=>'January','2'=>'February','3'=>'March','4'=>'April','5'=>'May','6'=>'June','7'=>'July','8'=>'August','9'=>'September','10'=>'October','11'=>'November','12'=>'December'];
        // $days = ['1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5','6'=>'6','7'=>'7','8'=>'8','9'=>'9','10'=>'10','11'=>'11','12'=>'12','13'=>'13','14'=>'14','15'=>'15','16'=>'16','17'=>'17','18'=>'18','19'=>'19','20'=>'20','21'=>'21','22'=>'22','23'=>'23','24'=>'24','25'=>'25','26'=>'26','27'=>'27','28'=>'28','29'=>'29','30'=>'30','31'=>'31'];
        $date_now = Date('m/d/Y');

        $members = UserRequestDetail::byReqID($req->id);
        // $joborders = JobOrder::byRequest($req->id);  

        $notInCC = User::getNotInCC($req->id);
        $inCC = User::getInCC($req->id);

    	return view('my_request.details',compact('req','remarks','files','uid','members','users','ccs','notInCC','inCC'));

        // return view('my_request.details',compact('req','remarks','files','users','settings','everyweek','date_now','encrypt','joborders'));
    }

    public function add_remarks(Request $request){

    	$type = $request->get('sub');
    	$postdata = $request->all();
        $postdata['remarks'] = nl2br($request->remarks);
    	$postdata['user_id'] = Auth::id();        
        $userreq = UserRequest::findOrFail($postdata['request_id']);
    	if($type == 2){    		
    		$userreq->status = 2;
    		$userreq->update();
    	}
        if($type == 3){            
            $userreq->status = 1;
            $userreq->update();
        }
        if($type == 4){            
            $userreq->status = 3;
            $userreq->update();

            ReminderUpdate::remove($userreq->id);
        }
    	$reqsave = UserRequestRemark::savePayload($postdata);

        if($request->hasFile('attached')){                 
            $files = Input::file('attached');                        
            if(!empty($files)){
                $destinationPath = storage_path().'/uploads/UR/'. $postdata['request_id'].'/remarks';
                if (!\File::exists($destinationPath))
                {
                    mkdir($destinationPath, 0755, true); 
                }  

                foreach ($files as $file) {
                    
                    $filename = $file->getClientOriginalName();                    

                    $file_name = pathinfo($filename, PATHINFO_FILENAME);

                    $extension = File::extension($filename);
                    $actual_name = $file_name.'.'.$extension;
                    $file_path = $destinationPath.$actual_name;

                    $count = 1;
                    while(File::exists($file_path)) {
                        $actual_name = $file_name.'_'.$count.'.'.$extension;
                        $file_path = $destinationPath.$actual_name;
                        $count++;
                    }                    

                    $file->move($destinationPath, $actual_name);   

                    $hashname = \Hash::make($filename);

                    $enc = str_replace("/","", $hashname);

                    $user_request_file = new UserRequestRemarkFile();
                    $user_request_file->request_id = $postdata['request_id'];
                    $user_request_file->remark_id = $reqsave;
                    $user_request_file->filename = $filename;
                    $user_request_file->encryptname = $enc;
                    $user_request_file->download = 0;          
                    $user_request_file->uploaded_by = Auth::id();        
                    $user_request_file->save();                      
                }                      
            }                                 
        }

        $req_users = UserRequestDetail::getReceiver($userreq->id);

        if(in_array(Auth::id(), $req_users)){

            $reminder = ReminderUpdate::byUserRequest(Auth::id(),$userreq->request_no);
            $today = Date('Y-m-d');
            if(!empty($reminder)){

                if($reminder->type == 1){

                    if($today != $reminder->last_update){

                        if($reminder->cnt_need < 1){

                            $reminder->cnt_need = 0;                        
                        }else{

                            $reminder->cnt_need = $reminder->cnt_need - 1;                                                    
                        }   

                        $reminder->cnt_donw = $reminder->cnt_donw + 1;                            
                        $reminder->last_update = $today;
                        $reminder->update();                        
                    }  
                }
                elseif($reminder->type == 2){

                    $next_week = date("Y-m-d",strtotime("+1 week", strtotime($reminder->last_update)));

                    if($today == $next_week){

                        if($reminder->cnt_need < 1){

                            $reminder->cnt_need = 0;                        
                        }else{

                            $reminder->cnt_need = $reminder->cnt_need - 1;                                                    
                        }   
                                     
                        $reminder->cnt_donw = $reminder->cnt_donw + 1;                            
                        $reminder->last_update = $today;
                        $reminder->update();      
                    }
                }else{                

                    if(Date('m') != Date('m',strtotime($reminder->last_update))){

                        if($reminder->cnt_need < 1){

                            $reminder->cnt_need = 0;                        
                        }else{

                            $reminder->cnt_need = $reminder->cnt_need - 1;                                                    
                        }   
                                     
                        $reminder->cnt_donw = $reminder->cnt_donw + 1;                            
                        $reminder->last_update = $today;
                        $reminder->update();      
                    }
                }
            }
                    
        }

    	return back()->with('is_success','Saved');
    }

    public function downloadFile(Request $request){

        $encryptname = $request->get('encname');
        $file = UserRequestRemarkFile::ByEncryptName($encryptname);        
        $file_down= storage_path().'/uploads/UR/'.$file->request_id.'/remarks/'.$file->filename;

        return Response::download($file_down);
    }

    public function getProjectDetailsApi(Request $request){    

        $projects = Project::byContactApi($request->con);

        return Response::json($projects);
    }

    public function linking(Request $request){


        $req = UserRequest::byEncrypt($request->request_code);

        $type = $request->type;

        if($type < 1){
            
            $con_person = Contact::findOrFail($request->contact_person);

            $postdata = [
                'contact_id' => $request->contact_person,
                'contact_person' => $con_person->company
            ];            
            
            UserRequest::updatePayload($postdata,$req->id);
            
            // $joborders = JobOrder::byRequest($req->id);            

            // if(!empty($joborders)){

            //     foreach($joborders as $jobs){

            //         JobOrder::updatePayload($postdata,$jobs->id);                    
            //     }
            // }

            // $schedJob = ScheduleJobOrder::byRequest($req->id);

            // if(!empty($schedJob)){

            //     foreach($schedJob as $sched){

            //         ScheduleJobOrder::updatePayload($postdata,$sched->id);
            //     }
            // }

            return back()->with('is_linked','Linked!');
        }
        if($type == 1){

            $con_person = Contact::findOrFail($request->contact_person);
            $proj_name = Project::findOrFail($request->project_id);

            $postdata = [
                'contact_id' => $request->contact_person,     
                'contact_person' => $con_person->company,
                'project_id' => $request->project_id,
                'project_name' => $proj_name->project_name

            ];            

            UserRequest::updatePayload($postdata,$req->id);

            // $joborders = JobOrder::byRequest($req->id);            
            // if(!empty($joborders)){

            //     foreach($joborders as $jobs){

            //         JobOrder::updatePayload($postdata,$jobs->id);                    
            //     }
            // }

            // $schedJob = ScheduleJobOrder::byRequest($req->id);

            // if(!empty($schedJob)){

            //     foreach($schedJob as $sched){                    

            //         ScheduleJobOrder::updatePayload($postdata,$sched->id);
            //     }
            // }

            return back()->with('is_linked','Linked!');
        }

        return back()->with('is_linked_failed','not Linked!');

    }

    public function add_rating(Request $request){

        $remark = UserRequestRemark::find($request->request_id);
        $remark->rating = $request->rating;
        $remark->update();

        return back();
    }

    public function download(Request $request){

        $encryptname = $request->get('encname');
        $file = UserRequestFile::ByEncryptName($encryptname);        
        $file_down= storage_path().'/uploads/UR/'.$file->request_id.'/'.$file->filename;                
        return Response::download($file_down);
    }

    public function add_participant(Request $request){

        $sender = Auth::user();

        $user_request = UserRequest::findOrFail($request->request_id);

        foreach($request->reciever as $reciever){

            $checkUser = UserRequestDetail::byReqUid($user_request->id,$reciever);

            if(empty($checkUser)){

                $user = User::findOrFail($reciever);
            $user_request_details = new UserRequestDetail();
            $user_request_details->request_id = $user_request->id;
            $user_request_details->reciever_id = $reciever;
            $user_request_details->dept_id = $user->dept_id;                
            $user_request_details->save();
                        

            if($user_request->recuring == 1){
                
                $upd = "DAILY";
                $date1 = date_create($request->start_date);
                $date2 = date_create($request->end_date);
                $counter2=date_diff($date1,$date2);  
                $counter = $counter2->days;

                if($counter < 1){
                    $counter=1;
                }
            }
            elseif($user_request->recuring == 2){

                $upd = "WEEKLY";                    
                $endDate = strtotime($request->end_date);
                $days=array('1'=>'Monday','2' => 'Tuesday','3' => 'Wednesday','4'=>'Thursday','5' =>'Friday','6' => 'Saturday','7'=>'Sunday');
                
                for($i = strtotime(Date('l',strtotime($request->start_date)), strtotime($request->start_date)); $i <= $endDate; $i = strtotime('+1 week', $i))
                
                $date_array[]=date('Y-m-d',$i);
                $cntdate = count($date_array);
                
                if($cntdate < 1){
                     $counter=1;
                }else{
                     $counter=$cntdate-1;
                }
            }
            else{
                
                $upd = "MONTHLY";
                $start = new \DateTime($request->start_date);
                $end = new \DateTime($request->end_date);
                $counter = $start->diff($end)->m;

                if($counter < 1){
                    $counter=1;
                }
            }                            
            

            $email_adds[] = $user->email.'^'."REQUEST #: ".$user_request->request_no.'^'.$user_request->start_date.' - '.$user_request->end_date.'^'.strtoupper($user->first_name.' '.$user->last_name).'^'.$upd;

            $user_notificaton = new UserNotification();
            $user_notificaton->user_id = $reciever;
            $user_notificaton->details = strtoupper($sender->first_name.' '.$sender->last_name).' TAGGED YOU IN A REQUEST';
            $user_notificaton->ref_link = "UR-".$user_request->request_code;
            $user_notificaton->save();                
            $post2['info'] = $user_request->request_no;
            $post2['link'] = $user_request->request_code;

            $post2['last_update'] = Date('Y-m-d H:i:s',strtotime($user_request->updated_at));

            $post2['cnt_need'] = $counter;
            $post2['cnt_donw'] = 0;
            $post2['type'] = $user_request->recuring;
            $post2['user_id'] = $reciever;
            $post2['created_by'] = Auth::id();       
            
            $reminder = ReminderUpdate::savePayload($post2);
            }            
        }
        return back()->with('is_added','Added');
    }    

    public function addCC(Request $request){

        $ccs = $request->get('reciever');

        if(!empty($ccs)){

            foreach($ccs as $cc){

                $copy = new UserRequestCc();
                $copy->request_id = $request->get('request_id');
                $copy->user_id = $cc;
                $copy->save();
            }
        }

        return back();
    }

    public function removeCC(Request $request){

        $ccs = $request->get('reciever');

        if(!empty($ccs)){

            foreach($ccs as $cc){

                UserRequestCc::where('request_id',$request->get('request_id'))->where('user_id',$cc)->delete();
            }
        }

        return back();
    }
}
