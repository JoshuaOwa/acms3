<?php

namespace App\Http\Controllers;

use App\Department;
use App\DepartmentSub;
use App\User;
use Illuminate\Http\Request;
use Response;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $users = User::getList();
        $departments = Department::all();

        return view('department.index',compact('departments','users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $department = new Department($request->all());
        $department->save();

        $ccs = $request->get('cc');
        if(!empty($ccs)){
            foreach($ccs as $cc){

                $dept_cc = new DepartmentSub();
                $dept_cc->dept_id = $department->id;
                $dept_cc->cc = $cc;
                $dept_cc->save();
            }            
        }        
        return back()->with('is_success','Saved!');
    }

    public function departmentDetails(Request $request){

        $department = Department::findOrFail($request->get('departmentid'));

        return Response::json($department);
    }

    public function departmentUpdates(Request $request){

        $department = Department::findOrFail($request->get('department_id'));
        $department->department = $request->get('department');        
        $department->update();

        $ccs = $request->get('cc');
        DepartmentSub::deleteCC($department->id);
        if(!empty($ccs)){
            foreach($ccs as $cc){

                $dept_cc = new DepartmentSub();
                $dept_cc->dept_id = $department->id;
                $dept_cc->cc = $cc;
                $dept_cc->save();
            }            
        }        

        return back()->with('is_update','Updated!');
        
    }

    public function subDepartmentDetails(Request $request){

        $cc = DepartmentSub::getCC($request->get('dept'));

        $ccs = [];
        if(!empty($cc)){

            foreach ($cc as $key => $value) {
               $ccs[] = $value->cc;
           }       
        }
        return Response::json($ccs);
    }
}
