<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use App\ProjectFile;
use App\ProjectLog;
use App\Contact;
use Auth;

class ProjectApprovalController extends Controller
{
    public function index(){

		$projects = Project::forApproval();

		return view('projects.forapproval',compact('projects'));
	}

	public function show($encrypt){

		$project = Project::byEncrypt($encrypt);
		$logs = ProjectLog::byProject($project->id);
		$uid = Auth::id();
        $contacts = Contact::createdByMe($uid)->pluck('contact_person','id');
        $files = ProjectFile::byProject($project->id);

		return view('projects.approval_details',compact('contact','logs','project','files'));
	}

	public function update_status(Request $request){

		$user = Auth::user();
		$project = Project::findOrFail($request->get('project_id'));
		
		if(!empty($request->allowed_ot)){
			$postdata['allowed_ot'] = $request->allowed_ot;
		}
		if(!empty($request->project_cost)){

			$postdata['project_cost'] = $request->project_cost;
		}
		$postdata['status'] = $request->get('status');
		$postdata['approver_id'] = $user->id;				

		Project::updatePayload($postdata,$project->id);

		if($request->get('status') < 2){
			$status_label = "APPROVED";
		}else{
			$status_label = "DENIED";
		}		
		
		$log = [
			'log' => strtoupper($user->first_name.' '.$user->last_name).' '.$status_label.' '."THE PROJECT.",
			'project_id' => $project->id,
			'created_by' => $user->id,
			'remarks' => nl2br($request->get('remarks'))
		];
        
        $logged = ProjectLog::savePayload($log);    

		return back()->with('is_update','updated!');		
	}

	public function head_overide(Request $request, $id){


	}

	public function filter(Request $request){
               
        $projects = Project::filterBy($request->get('filter'));        
        return view('projects.forapproval',compact('projects'));
    }
}
