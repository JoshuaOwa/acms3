<?php

namespace App\Http\Controllers;

use App\ProjectTimetable;
use App\Project;
use App\TimetableTask;
use App\ProjectTask;
use Illuminate\Http\Request;
use App\User;

class ProjectTimetableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $project_id = $request->project_id;
        
        $project = Project::findOrFail($project_id);
        
        $postdata['contact_id'] = $project->contact_id;
        $postdata['project_id'] = $project->id;        
        $hash = \Hash::make("owaowaowa");
        $postdata['encryptname'] = str_replace("/","", $hash);

        $timetables = $request->timetable_id;

        ProjectTimetable::deleteRecord($project->id);
        ProjectTask::deleteRecord($project->id);

        foreach ($timetables as $key => $value) {
            
            $postdata['timetable_id'] = $value;
            $hash = \Hash::make("owaowaowa");
            $postdata['encryptname'] = str_replace("/","", $hash);

            ProjectTimetable::savePayload($postdata);                    
            
            $tasks = TimetableTask::byTimetable($value);

            if(!empty($tasks)){

                foreach ($tasks as $task) {
                    
                    $hash = \Hash::make("owaowaowa");
                    $postdata['encryptname'] = str_replace("/","", $hash);
                    $postdata['task_id'] = $task->id;

                    ProjectTask::savePayload($postdata);
                }
            }        
        }

        return back()->with('is_success','save');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProjectTimetable  $projectTimetable
     * @return \Illuminate\Http\Response
     */
    public function show(ProjectTimetable $projectTimetable)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProjectTimetable  $projectTimetable
     * @return \Illuminate\Http\Response
     */
    public function edit(ProjectTimetable $projectTimetable)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProjectTimetable  $projectTimetable
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProjectTimetable $projectTimetable)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProjectTimetable  $projectTimetable
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProjectTimetable $projectTimetable)
    {
        //
    }

    public function overview($encrypt){

        $timetable = ProjectTimetable::byEncryptWithDetails($encrypt);        
        $time = ProjectTimetable::byEncrypt($encrypt);        
        $taskList = TimetableTask::byTimetable($time->timetable_id)->pluck('task','id');        
        $users_jo = User::getList();
        $everyweek = ['1'=>'Monday','2'=>'Tuesday','3'=>'Wednesday','4'=>'Thursday','5'=>'Friday','6'=>'Saturday','7'=>'Sunday'];
        $settings = ['0'=>'One Time Only','1'=>'Everyday','2'=>'Every month','3'=>'Every week'];
        $date_now = Date('m/d/Y');
        $project = Project::findOrFail($time->project_id);
        return view('projects.timetable_details',compact('time','taskList','users_jo','timetable','everyweek','settings','date_now','project'));
    }

    public function add_leads(Request $request){

        $postdata = $request->all();

        $proj_timetable = ProjectTimetable::updatePayload($postdata,$request->timetable_id);

        return back()->with('is_lead','Added');
    }
}
