<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Notes;
use Auth;
use App\ReminderUpdate;
use App\Announcement;
use MaddHatter\LaravelFullcalendar\Calendar;
use App\JobOrder;
use App\Department;
use DateTime;
use App\UserRequestCc;
use App\JobOrderCc;

class DashboardController extends Controller
{
    public function index(Calendar $calendar){

    	$user = Auth::user();
    	$notes = Notes::byUser($user->id);

    	$daily = ReminderUpdate::daily($user->id);
    	$weekly = ReminderUpdate::weekly($user->id);
    	$monthly= ReminderUpdate::monthly($user->id);

    	$announcements = Announcement::orderBy('created_at','Desc')->get();		
        
		$ann_cntr = count($announcements);        

        $copieds = UserRequestCc::getCC($user->id);
        $copieds2 = JobOrderCC::getCC($user->id);

        $joborders = JobOrder::all();
        $date_now = Date('Y-m-d');
        $events = [];        
        $events2 = [];
        $events3  = [];
        $events4 = [];            

        foreach ( $daily as $reminder ) {                
            

            $events[] = $calendar->event(
                //
                $reminder->info, //event title
                true, //full day event?
                $reminder->start_date, //start time (you can also use Carbon instead of DateTime)
                $reminder->end_date, //end time (you can also use Carbon instead of DateTime)
                $reminder->link,//optionally, you can specify an event ID
                [
                    'url' => "../my_requests/".$reminder->link,
                ]
            );            
        }
        foreach ( $weekly as $reminder ) {                
            

            $events[] = $calendar->event(
                //
                $reminder->info, //event title
                true, //full day event?
                $reminder->start_date, //start time (you can also use Carbon instead of DateTime)
                $reminder->end_date, //end time (you can also use Carbon instead of DateTime)
                $reminder->link,//optionally, you can specify an event ID
                [
                    'url' => "../my_requests/".$reminder->link,
                ]
            );            
        }
        foreach ( $monthly as $reminder ) {                
            

            $events[] = $calendar->event(
                //
                $reminder->info, //event title
                true, //full day event?
                $reminder->start_date, //start time (you can also use Carbon instead of DateTime)
                $reminder->end_date, //end time (you can also use Carbon instead of DateTime)
                $reminder->link,//optionally, you can specify an event ID
                [
                    'url' => "../my_requests/".$reminder->link,
                ]
            );            
        }
        

        foreach ($joborders as $job) {
            if($date_now > $job->end_date AND $job->status == 1){
                $jobcolor = "#fab1a0";
            }elseif($job->status > 1 AND $job->return_date > $job->end_date){
                 $jobcolor = "#fab1a0";
            }else{                
                $jobcolor = "#1ab394";
            }          

            if($job->status == 1){
                $stat = "PENDING";
            }elseif($job->status == 2){
                $stat = "RETURNED";
            }elseif($job->status == 3){
                $stat = "CLOSED";
            }else{
                $stat = "";
            }            
            $stop_date = date('Y-m-d', strtotime($job->end_date . ' +1 day'));
            $events2[] = $calendar->event(
                $job->assigned_name." - JO#:".$job->id.' - '.$stat,
                true,
                $job->start_date,
                $stop_date,                
                $job->encryptname,
                [
                    'url' => "../job_orders/".$job->encryptname."/overview",
                    'color' => $jobcolor
                ]
            );
        }

        // $calendar = $calendar->addEvents($events)->setOptions([ //set fullcalendar options
        //     'firstDay' => 1,
        //     'eventTextColor' => '#ffffff',          
        // ])->setCallbacks([ //set fullcalendar callback options (will not be JSON encoded)
            
            
        // ]);

        $calendar = $calendar->addEvents($events2)->setOptions([ //set fullcalendar options
            'firstDay' => 1,
            'eventTextColor' => '#ffffff',          
        ])->setCallbacks([ //set fullcalendar callback options (will not be JSON encoded)
                        
        ]);

        $departments = Department::orderBy('department','ASC')->pluck('department','id');
        
    	return view('dashboard.index',compact('notes','daily','weekly','monthly','announcements', 'ann_cntr','calendar','calendar2','departments','copieds','copieds2'));
    }

    public function calendarFilter(Request $request,Calendar $calendar){

        $user = Auth::user();
        $notes = Notes::byUser($user->id);

        $daily = ReminderUpdate::daily($user->id);
        $weekly = ReminderUpdate::weekly($user->id);
        $monthly= ReminderUpdate::monthly($user->id);

        $announcements = Announcement::orderBy('created_at','Desc')->get();     
        
        $ann_cntr = count($announcements);        
        $copieds = UserRequestCc::getCC($user->id);
        $copieds2 = JobOrderCc::getCC($user->id);

        $joborders = JobOrder::byDept($request->get('department_id'));
        $date_now = Date('Y-m-d');
        $events = [];        
        $events2 = [];
        $events3  = [];
        $events4 = [];

        foreach ( $daily as $reminder ) {                
            

            $events[] = $calendar->event(
                //
                $reminder->info, //event title
                true, //full day event?
                $reminder->start_date, //start time (you can also use Carbon instead of DateTime)
                $reminder->end_date, //end time (you can also use Carbon instead of DateTime)
                $reminder->link,//optionally, you can specify an event ID
                [
                    'url' => "../my_requests/".$reminder->link,
                ]
            );            
        }
        foreach ( $weekly as $reminder ) {                
            

            $events[] = $calendar->event(
                //
                $reminder->info, //event title
                true, //full day event?
                $reminder->start_date, //start time (you can also use Carbon instead of DateTime)
                $reminder->end_date, //end time (you can also use Carbon instead of DateTime)
                $reminder->link,//optionally, you can specify an event ID
                [
                    'url' => "../my_requests/".$reminder->link,
                ]
            );            
        }
        foreach ( $monthly as $reminder ) {                
            

            $events[] = $calendar->event(
                //
                $reminder->info, //event title
                true, //full day event?
                $reminder->start_date, //start time (you can also use Carbon instead of DateTime)
                $reminder->end_date, //end time (you can also use Carbon instead of DateTime)
                $reminder->link,//optionally, you can specify an event ID
                [
                    'url' => "../my_requests/".$reminder->link,
                ]
            );            
        }

        foreach ($joborders as $job) {
            if($date_now > $job->end_date AND $job->status == 1){
                $jobcolor = "#fab1a0";
            }elseif($job->status > 1 AND $job->return_date > $job->end_date){
                 $jobcolor = "#fab1a0";
            }else{                
                $jobcolor = "#1ab394";
            }          

            if($job->status == 1){
                $stat = "PENDING";
            }elseif($job->status == 2){
                $stat = "RETURNED";
            }elseif($job->status == 3){
                $stat = "CLOSED";
            }else{
                $stat = "";
            }            
            $stop_date = date('Y-m-d', strtotime($job->end_date . ' +1 day'));
            $events2[] = $calendar->event(
                $job->assigned_name." - JO#:".$job->id.' - '.$stat,
                true,
                $job->start_date,
                $stop_date,
                $job->encryptname,
                [
                    'url' => "../../job_orders/".$job->encryptname."/overview",
                    'color' => $jobcolor
                ]
            );
        }
        

        // $calendar = $calendar->addEvents($events)->setOptions([ //set fullcalendar options
        //     'firstDay' => 1,
        //     'eventTextColor' => '#ffffff',          
        // ])->setCallbacks([ //set fullcalendar callback options (will not be JSON encoded)
            
            
        // ]);

        $calendar = $calendar->addEvents($events2)->setOptions([ //set fullcalendar options
            'firstDay' => 1,
            'eventTextColor' => '#ffffff',          
        ])->setCallbacks([ //set fullcalendar callback options (will not be JSON encoded)
                        
        ]);

        $departments = Department::orderBy('department','ASC')->pluck('department','id');


        return view('dashboard.index',compact('notes','daily','weekly','monthly','announcements', 'ann_cntr','calendar','calendar2','departments','copieds','copieds2'));
    }
}
