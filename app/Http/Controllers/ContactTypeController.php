<?php

namespace App\Http\Controllers;

use App\ContactType;
use Illuminate\Http\Request;
use Response;

class ContactTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $types = ContactType::all();

        return view('contact_types.list',compact('types'));
    }

    public function store(Request $request)
    {
        $type = new ContactType($request->all());
        $type->save();

        return back()->with('is_success','Saved!');
    }

  
    public function contactTypeUpdates(Request $request){

        $type = ContactType::findOrFail($request->get('type_id'));
        $type->type = $request->get('type');        
        $type->update();

        return back()->with('is_update','Updated!');
    }

    public function typeDetails(Request $request){

        $type = ContactType::findOrFail($request->get('type_id'));

        return Response::json($type);
    }
}
