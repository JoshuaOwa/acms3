<?php

namespace App\Http\Controllers;

use App\ProjectWorkgroup;
use App\Contact;
use App\Project;
use App\User;
use App\ProjectLog;
use Auth;
use Illuminate\Http\Request;

class ProjectWorkgroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();        

        $projects = ProjectWorkgroup::byUser($user);        

        return view('project_workgroup.index',compact('projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $uid = Auth::id();
        $user = Auth::user();   
        $postdata = $request->all();
        $project = Project::findOrFail($request->project_id);
        $contact = Contact::findOrFail($request->contact_id);

        $postdata['added_by'] = $uid;
        $postdata['workgroup_name'] = strtoupper($project->project_name.' WORKGROUP - '.$contact->contact_person);

        $userIds = $request->user_id;

        foreach($userIds as $uid){

            $check = ProjectWorkgroup::checkExist($request->job_order_id,$uid);
            if($check <  1){

                $mem = User::findOrFail($uid);
                $postdata['user_id'] = $uid;
                $postdata['position'] = $mem->position->position;

                $proj_workgroup = ProjectWorkgroup::savePayload($postdata);

                $proj_logs['contact_id'] = $project->contact_id;
                $proj_logs['project_id'] = $project->id;                
                $proj_logs['created_by'] = $uid;
                $proj_logs['remarks'] = "";
                $proj_logs['log'] = strtoupper($user->first_name.' '.$user->last_name).' ADDED '.strtoupper($mem->first_name.' '.$mem->last_name).' TO THE WORKGROUP.';

                ProjectLog::savePayload($proj_logs);
            }
        }        

        return back()->with('is_added','Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProjectWorkgroup  $projectWorkgroup
     * @return \Illuminate\Http\Response
     */
    public function show(ProjectWorkgroup $projectWorkgroup)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProjectWorkgroup  $projectWorkgroup
     * @return \Illuminate\Http\Response
     */
    public function edit(ProjectWorkgroup $projectWorkgroup)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProjectWorkgroup  $projectWorkgroup
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProjectWorkgroup $projectWorkgroup)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProjectWorkgroup  $projectWorkgroup
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProjectWorkgroup $projectWorkgroup)
    {
        //
    }

    public function remove_member(Request $request){

        
    }
}
