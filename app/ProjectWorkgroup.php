<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectWorkgroup extends Model
{
    protected $table = 'project_workgroups';

    protected $fillable = [

    	'contact_id',
    	'project_id',
    	'user_id',
    	'workgroup_name',
    	'added_by',
        'position'
    ];


    public function user(){

    	return $this->hasOne('App\User','id','user_id');
    }

    public static function savePayload($payload){

        $proj_work = static::query()->create($payload);

        return $proj_work;
    }

    public static function byProject($proj_id){

        $members = self::where('project_id',$proj_id)->get();

        foreach ($members as $key => $value) {

            $jobs = JobOrder::byProjectAndUser($value->project_id,$value->user_id);
            $pend = 0;
            $ret = 0;
            $close = 0;
            if(!empty($jobs)){

                foreach($jobs as $job){

                    if($job->status == 1){
                        $pend++;
                    }
                    elseif($job->status == 2){
                        $ret++;
                    }
                    elseif($job->status == 3){
                        $close++;
                    }                            
                }
            }
            $members[$key]->pendJO = $pend;
            $members[$key]->retJO = $ret;
            $members[$key]->cloJO = $close;            
        }
        return $members;
    }

    public static function checkExist($proj_id,$uid){

        return self::where('project_id',$proj_id)->where('user_id',$uid)->count();
    }

    public static function membersID($project_id){

        $users = self::where('project_id',$project_id)->get();
        $uids = [];

        foreach($users as $user){

            $uids[] = $user->user_id;
        }

        return $uids;
    }    

    public static function byUser($user){

        if($user->id == 1){
            $works = self::groupBy('project_id')->get();        
        }else{
            $works = self::where('user_id',$user->id)->get();        
        }        

        foreach ($works as $key => $value) {
            
            $project = Project::find($value->project_id);
            $works[$key]->project_name = $project->project_name;
            $works[$key]->branch = $project->branch;
            $works[$key]->encryptname = $project->encryptname;
            $works[$key]->status = $project->status;
        }

        return $works;
    }
}
