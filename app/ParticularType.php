<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ParticularType extends Model
{
    protected $table = 'particular_types';

    protected $fillable = [

    	'type'
    ];
}
