<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BillOfMaterialFile extends Model
{
    protected $table = 'bill_of_material_files';

    protected $fillable = [

    	'contact_id',
    	'project_id',
    	'filename',
    	'encrpytname',
    	'download',
    	'uploaded_by'    	
    ];    
}
