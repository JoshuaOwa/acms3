<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class UserRequestCc extends Model
{
    protected $table = 'user_request_ccs';

    protected $fillable = [

    	'request_id',
    	'user_id',    	    
    ];

    public static function getList($reqID){

    	$reqs = self::where('request_id',$reqID)->get();

    	$uid = [];

    	foreach($reqs as $req){

    		$uid[] = $req->user_id;
    	}

    	$usersList = User::select(DB::raw("CONCAT(first_name,' ',last_name) as full_name ,id"))
                ->where('active',"1")
                ->whereIn('id',$uid)
                ->get();

        return $usersList;
    }

    public static function getCC($uids){

        $ccs = self::where('user_id',$uids)->get();
        $reqID = [];
        foreach($ccs as $cc){

            $reqID[] = $cc->request_id;
        }

        $reqs = UserRequest::whereIn('id',$reqID)->get();

        $reqsInfo = [];

        foreach($reqs as $req){

            $reqsInfo[] = $req->request_no;
        }
        if(!empty($reqsInfo)){
            $reminders = ReminderUpdate::whereIn('info',$reqsInfo)->groupBy('info')->get();
        }else{
            $reminders = [];
        }

        return $reminders;
    }
}
