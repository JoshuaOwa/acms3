<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class UserIndustry extends Model
{
    protected $table = 'user_industries';

    protected $fillable = [

    	'user_id',
    	'industry_id'
    ];

    public static function savePayload($payload){    	
        $ind = static::query()->create($payload);

        return $ind->id;
    }

    public static function reset($id){

    	return self::where('user_id',$id)->delete();
    }

    public static function byUser($id){

        $inds = self::where('user_id',$id)->get();
        $indID = [];
        foreach ($inds as $key => $value) {
            $indID[] = $value->industry_id;
        }

        return $indID;
    }

    public static function byIndustry($ind){

        $users = self::where('industry_id',$ind)->get();
        $uid = [];

        foreach($users as $user){

            $uid[] = $user->user_id;
        }

        return $uid;
    }

    public static function bydept($deptIDS){

        $users = self::where('industry_id',$deptIDS)->get();

        $uid = [];

        foreach($users as $user){

            $uid[] = $user->user_id;
        }

        $usersList = User::select(DB::raw("CONCAT(first_name,' ',last_name) as full_name ,id"))
                ->where('active',"1")
                ->whereIn('id',$uid)
                ->pluck('full_name','id');

        return $usersList;
    }

    public static function bydept2($deptIDS){

        $users = self::whereIn('industry_id',$deptIDS)->get();

        $uid = [];

        foreach($users as $user){

            $uid[] = $user->user_id;
        }

        $usersList = User::select(DB::raw("CONCAT(first_name,' ',last_name) as full_name ,id"))
                ->where('active',"1")
                ->whereIn('id',$uid)
                ->pluck('full_name','id');

        return $usersList;   
    }

    public static function notInList($usersID){

        return User::select(DB::raw("CONCAT(first_name,' ',last_name) as full_name ,id"))
                ->where('active',"1")
                ->whereNotIn('id',$usersID)
                ->pluck('full_name','id');
    }
}
