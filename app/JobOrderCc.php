<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class JobOrderCc extends Model
{
    protected $table = 'job_order_ccs';

    protected $fillable = [

    	'job_order_id',
    	'user_id'
    ];

    public static function getList($reqID){

    	$reqs = self::where('job_order_id',$reqID)->get();

    	$uid = [];

    	foreach($reqs as $req){

    		$uid[] = $req->user_id;
    	}

    	$usersList = User::select(DB::raw("CONCAT(first_name,' ',last_name) as full_name ,id"))
                ->where('active',"1")
                ->whereIn('id',$uid)
                ->get();

        return $usersList;
    }

    public static function getCC($uids){

        $ccs = self::where('user_id',$uids)->get();
        $jobID = [];
        foreach($ccs as $cc){

            $jobID[] = $cc->job_order_id;
        }

        $jobs = JobOrder::whereIn('id',$jobID)->get();

        $jobInfo = [];

        foreach($jobs as $job){

            $jobInfo[] = $job->id;
        }                
        if(!empty($jobInfo)){
            
            $reminders = JobOrder::whereIn('id',$jobInfo)->groupBy('id')->get();            

            foreach ($reminders as $key => $value) {
                $reminders[$key]->info = "JO-".$value->id;
            }
        }else{
            $reminders = [];
        }                
        return $reminders;
    }
}
