<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestType extends Model
{
    protected $table = 'request_types';

    protected $fillable = [

    	'type'
    ];

    public static function getList(){

    	return self::orderBy('type','ASC')->pluck('type','type');
    }
}
