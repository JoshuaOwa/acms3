<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactLog extends Model
{
    protected $table = 'contact_logs';

    protected $fillable = [

    	'created_by',
    	'contact_id',
    	'log',
    	'remarks'
    ];

    public static function byMe($id){

    	return self::where('created_by',$id)->get();
    }

    public static function savePayload($payload){

        $contact = static::query()->create($payload);

        return $contact->id;
    }
    
    public static function byContact($contact_id){

        return self::where('contact_id',$contact_id)->orderBy('id','DESC')->get();
    }
}
