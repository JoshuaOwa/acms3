<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobOrderWorkgroup extends Model
{
    protected $table = 'job_order_workgroups';

    protected $fillable = [

    	'contact_id',
    	'project_id',
    	'job_order_id',
    	'user_id',
    	'workgroup_name',
    	'added_by',
        'position'

    ];

    public function user(){

        return $this->hasOne('App\User','id','user_id');
    }
    public static function byJobOrder($job_id){

        return self::where('job_order_id',$job_id)->get();
    }

    public static function savePayload($payload){

        $job_orderWg = static::query()->create($payload);        
    }

    public static function checkExist($job_id,$uid){

        return self::where('job_order_id',$job_id)->where('user_id',$uid)->count();
    }

    public static function byUser($user){

        $works = self::where('user_id',$user->id)->get();        

        foreach ($works as $key => $value) {
            
            $job = JobOrder::find($value->job_order_id);
            $works[$key]->lead = $job->assigned_name;
            $works[$key]->status = $job->status;
            $works[$key]->encryptname = $job->encryptname;
        }

        return $works;
    }

    public static function membersID($job_id){

        $users = self::where('job_order_id',$job_id)->get();
        $uids = [];

        foreach($users as $user){

            $uids[] = $user->user_id;
        }

        return $uids;
    }    
}
