<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectLog extends Model
{
    protected $table = 'project_logs';

    protected $fillable = [

    	'created_by',
    	'project_id',
    	'log',
    	'remarks'
    ];

    public function user(){

        return $this->hasOne('App\User','id','created_by');
    }

    public static function byMe($id){

    	return self::where('created_by',$id)->get();
    }

    public static function savePayload($payload){

        $project = static::query()->create($payload);

        return $project->id;
    }

    public static function byProject($project_id){

        return self::where('project_id',$project_id)->orderBy('id','DESC')->get();
    }
}
