<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectFile extends Model
{
    protected $table = 'project_files';

    protected $fillable = [

    	'project_id',
		'filename',
		'encrpytname',
		'download',
		'uploaded_by'
    ];

    public function project(){

    	return $this->hasOne('App\Project','id','project_id');
    }
    public static function byProject($id){

    	return self::where('project_id',$id)->get();
    }
        
    public static function ByEncryptName($encrpytname){

        return self::where('encrpytname',$encrpytname)->first();
    }
}
