<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlannerReciever extends Model
{
    protected $table = 'planner_recievers';

    protected $fillable = [


    	'planner_id',
    	'user_id',
    	'stop'
    ];

    public static function savePayload($payload){

        $planner = static::query()->create($payload);

        return $planner;
    }

    public static function stopReminder($reminder_id,$uid){

        return self::where('planner_id',$reminder_id)->where('user_id',$uid)->first();
    }
    
}
