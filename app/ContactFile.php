<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactFile extends Model
{
    protected $table = 'contact_files';

    protected $fillable = [

    	'contact_id',
		'filename',
		'encrpytname',
		'download',
		'uploaded_by'
    ];
}
