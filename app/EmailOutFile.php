<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailOutFile extends Model
{
    protected $table = 'email_out_files';

    protected $fillable = [

    	'contact_id',
    	'project_id',
    	'email_out_id',
    	'filename',
    	'encryptname'
    ];
}
