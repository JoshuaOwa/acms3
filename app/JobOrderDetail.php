<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobOrderDetail extends Model
{
    protected $table = 'job_order_details';

    protected $fillable = [

    	'job_order_id',
    	'reciever_id',    	
    	'dept_id',
        'rating',
        'rating_remarks'
    ];

    public function user(){

        return $this->hasOne('App\User','id','reciever_id');
    }

    public static function getReceiver($request_id){

    	$users = self::where('job_order_id',$request_id)->get();
    	$uids = [];

    	foreach($users as $user){

    		$uids[] = $user->reciever_id;
    	}

    	return $uids;
    }

    public static function byJobID($jobid){

        return self::where('job_order_id',$jobid)->get();
    }

    public static function byJobUid($jobid,$reciever){

        return self::where('job_order_id',$jobid)->where('reciever_id',$reciever)->first();
    }
}
