<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobOrderThread extends Model
{
    protected $table = 'job_order_threads';

    protected $fillable = [

    	'contact_id',
    	'project_id',
    	'timetable_id',
    	'task_id',
    	'job_order_id',
    	'message',
    	'user_id',
    	'main',
    	'comment_to',
        'emails'
    ];

    public function user(){

        return $this->hasOne('App\User','id','user_id');
    }

    public static function savePayload($payload){

        $thread = static::query()->create($payload);

        return $thread->id;
    }

    public static function updatePayload($payload,$id){

        $thread = JobOrderThread::findOrFail($id);
        $thread->fill($payload)->save();

        return $thread->id;
    }

    public static function byJobOrder($job_id){

        $threads = JobOrder::where('id',$job_id)->get();

        // $threads = self::where('job_order_id',$job_id)->where('main',1)->orderBy('updated_at','DESC')->get();

        // $tasks->threads[$key]->files = JobOrderFile::where('job_order_id',$value->id)->get();

        // $Jobthreads = JobOrderThread::where('job_order_id',$value->id)->where('main',1)->first();
        // $tasks->threads[$key]->mainthreadid = $Jobthreads->id;                    
        // $tasks->threads[$key]->comments = JobOrderThread::byMainThread($Jobthreads->id);
        
        if(!empty($threads)){


            foreach ($threads as $key => $thread) {
                
                $Jobthreads = JobOrderThread::where('job_order_id',$thread->id)->where('main',1)->first();
                if(!empty($Jobthreads)){
                    $threads[$key]->mainthreadid = $Jobthreads->id;     
                                
                    $threads[$key]->files =  JobOrderFile::where('job_order_id',$thread->id)->get();
                    $threads[$key]->comments = JobOrderThread::byMainThread($Jobthreads->id);
                }
            }
        }

        // $joborder->files = 

        return $threads;
    }

    public static function byMainThread($thread_id){

        $comments = self::where('main',0)->where('comment_to',$thread_id)->orderBy('updated_at','DESC')->get();

        foreach ($comments as $key => $comment) {
            
            $comments[$key]->files = JobOrderThreadFile::byThread($comment->id);
        }

        return $comments;
    }
}
