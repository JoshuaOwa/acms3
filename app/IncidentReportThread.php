<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IncidentReportThread extends Model
{
    protected $table = 'incident_report_threads';

    protected $fillable = [

    	'ir_id',
    	'user_id',
    	'details',
        'solution'
    ];


    public function user(){

    	return $this->hasOne('App\User','id','user_id');
    }

	public static function savePayload($payload){

        $incident = static::query()->create($payload);

        return $incident;
    }
}
