<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IncidentReport extends Model
{
    protected $table = 'incident_reports';

    protected $fillable = [

    	'contact_id',
    	'project_id',
    	'created_by',
        'client_coor',
    	'subject',
    	'details',
    	'main',
    	'main_id',
    	'status',
        'solution',
        'resolved_by',
        'resolved_date',
        'closed_by',
        'final_remarks',
        'ratings'
    ];


    public function contact(){

        return $this->hasOne('App\Contact','id','contact_id');
    }

    public function project(){

        return $this->hasOne('App\Project','id','project_id');
    }

    public function user(){

        return $this->hasOne('App\User','id','created_by');
    }

    public function resolved(){

        return $this->hasOne('App\User','id','resolved_by');
    }

    public function closed(){

        return $this->hasOne('App\User','id','closed_by');
    }

    public static function savePayload($payload){

        $incident = static::query()->create($payload);

        return $incident;
    }

    public static function updatePayload($payload,$id){

        $incident = IncidentReport::findOrFail($id);
        $incident->fill($payload)->save();

        return $incident;
    }


    public static function byProject($proj_id){

    	$incidents = self::where('project_id',$proj_id)->where('main',1)->orderBy('id','DESC')->get();

    	foreach ($incidents as $key => $value) {
    		    		
            if($value->status < 2){

                $date_now =  new \DateTime('now');             
                
                $diff = date_diff($value->created_at,$date_now);                
                
                if($diff->days > 2){

                    $value->status = 2;
                    $value->update();
                }
            }

            $incidents[$key]->files = IncidentReportFile::where('ir_id',$value->id)->get();         

            $incidents[$key]->threads = IncidentReportThread::where('ir_id',$value->id)->orderBy('created_at','DESC')->get();

            $incidents[$key]->solution_file = IncidentReportSolutionFile::where('ir_id',$value->id)->get();

    	}

        foreach ($incidents as $key => $value) {
            
            foreach ($value->threads as $key => $thread) {                                

                $value->threads[$key]->thread_file = IncidentReportThreadFile::where('ir_thread_id',$thread->id)->get();

            }
        }

    	return $incidents;
    }
}
