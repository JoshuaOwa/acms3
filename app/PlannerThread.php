<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlannerThread extends Model
{
    protected $table = 'planner_threads';

    protected $fillable = [

    	'planner_id',        
    	'user_id',
    	'details',        
    ];


    public function user(){

    	return $this->hasOne('App\User','id','user_id');
    }

	public static function savePayload($payload){

        $planner_thread = static::query()->create($payload);

        return $planner_thread;
    }
}
