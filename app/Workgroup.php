<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Workgroup extends Model
{
    protected $table = 'workgroups';

    protected $fillable = [

    	'head',
    	'workgroup_name',
        'client_id',
        'project_id',
        'user_id'        
    ];

    public function mngr(){

    	return $this->hasOne('App\User','id','head');
    }

    public static function getList(){

    	return self::pluck('workgroup_name','id');
    }

}
