<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WarehouseInventoryFile extends Model
{
    protected $table = 'warehouse_inventory_files';

    protected $fillable = [
    	
    	'filename',
    	'encrpytname',
    	'download',
    	'uploaded_by'    	
    ];    
}
