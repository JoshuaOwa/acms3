<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IncidentReportThreadFile extends Model
{
    protected $table = 'incident_report_thread_files';

    protected $fillable = [

    	'contact_id',
    	'project_id',
    	'ir_thread_id',
    	'filename',
    	'uploaded_by',
    	'encryptname'
    ];

    public static function ByEncryptName($encryptname){

        return self::where('encryptname',$encryptname)->first();
    }
}
