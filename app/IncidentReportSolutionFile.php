<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IncidentReportSolutionFile extends Model
{
    protected $table = 'incident_report_solution_files';

    protected $fillable = [

    	'contact_id',
    	'project_id',
    	'ir_id',
    	'filename',
    	'uploaded_by',
    	'encryptname'
    ];

    public static function ByEncryptName($encryptname){

        return self::where('encryptname',$encryptname)->first();
    }
}
