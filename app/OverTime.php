<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OverTime extends Model
{
    protected $table = 'over_times';

    protected $fillable = [
    	'id','user_id','job_order_id','project_id','company','branch_id','start_date','end_date','start_time','end_time','category_id','type_id','status','level','remarks','approved_by','confirmed_by','update_remarks','confirmed_remarks','downloaded','encrypt_name'
    ];

    public function user()
    {
    	return $this->hasOne('App\User','id','user_id');
    }

    public function project()
    {
    	return $this->hasOne('App\Project','id','project_id');
    }

    public static function findByJobIdAndUserId($jo_id,$proj_id,$user_id)
    {
    	return self::where('project_id',$proj_id)->where('job_order_id',$jo_id)->where('user_id',$user_id)->get();
    }

    public static function findPendingByUserId($ids)
    {
    	return self::where('status','<',2)->whereIn('user_id',$ids)->get();
    }

    public static function findByEncryptName($encrypt)
    {
    	return self::where('encrypt_name',$encrypt)->first();
    }

    public static function findByJobId($jo_id)
    {
        return self::where('job_order_id',$jo_id)->where('status',2)->get();
    }

    public static function findByProjectId($proj_id)
    {
        return self::where('project_id',$proj_id)->where('status',2)->get();
    }
}
