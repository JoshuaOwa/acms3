<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlannerThreadFile extends Model
{
    protected $table = 'planner_thread_files';

    protected $fillable = [

    	'planner_id',
    	'planner_thread_id',    	
    	'filename',
    	'uploaded_by',
    	'encryptname'
    ];

    public static function ByEncryptName($encryptname){

        return self::where('encryptname',$encryptname)->first();
    }
}
