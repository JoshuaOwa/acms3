<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DepartmentSub extends Model
{
    protected $table = 'department_subs';

    protected $fillable = [

    	'dept_id',
    	'cc'
    ];

    public static function getCC($dept_id){

    	return self::select('cc')->where('dept_id',$dept_id)->get();
    }

    public static function deleteCC($dept_id){

    	self::where('dept_id',$dept_id)->delete();
    	
    	return true;
    }
}
