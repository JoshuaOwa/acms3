<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TimetableTask extends Model
{
    protected $table = 'timetable_tasks';

    protected $fillable = [

    	'timetable_id',
    	'task',    	
    ];

    public function timetable(){

        return $this->hasOne('App\Timetable','id','timetable_id');
    }

    public static function savePayload($payload){

        $task = static::query()->create($payload);

        return $task;
    }

    public static function updatePayload($payload,$id){

        $task = TimetableTask::findOrFail($id);
        $task->fill($payload)->save();

        return $task;
    }

    public static function getCount($timeid){

        return self::where('timetable_id',$timeid)->count();
    }

    public static function byTimetable($timeid){

        return self::where('timetable_id',$timeid)->get();
    }

    public static function byTimetableAPI($timeid){

        return self::where('timetable_id',$timeid)->orderBy('task','ASC')->pluck('task','id');
    }
}
