<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlannerFile extends Model
{
    protected $table = 'planner_files';

    protected $fillable = [
    	'planner_id',
    	'filename',
    	'uploaded_by',
    	'encryptname'
    ];

    public static function ByEncryptName($encryptname){

        return self::where('encryptname',$encryptname)->first();
    }
}
