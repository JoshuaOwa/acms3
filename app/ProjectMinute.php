<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectMinute extends Model
{
    protected $table = 'project_minutes';

    protected $fillable = [

    	'contact_id',
    	'project_id',
    	'message',
    	'user_id',
    	'main',
    	'comment_to'
    ];    
}
