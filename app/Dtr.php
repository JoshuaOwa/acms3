<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dtr extends Model
{
    protected $table = 'dtrs';

    protected $fillable = [
    	'emp_id','date','in_1','out_1','in_2','out_2'
    ];

    public static function findDtr($data)
    {
    	return self::where('date',$data['date'])->where('emp_id',$data['emp_id'])->first();
    }
}
