<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Planner extends Model
{

	protected $table = 'planners';    

	protected $fillable = [

		'created_by',
		'title',
		'details',
		'start_date',
		'end_date',
		'remind_time',		
	];
	
	public function sender(){

        return $this->hasOne('App\User','id','created_by');
    }
    
	public static function ByUser($user){


		return self::join('planner_recievers','planner_recievers.planner_id','=','planners.id')->where('user_id',$user)->get();
	}

	public static function savePayload($payload){

        $planner = static::query()->create($payload);

        return $planner;
    }

    public static function getDetails($id){
    	
    	$planners = self::where('id',$id)->first();

    	$files = PlannerFile::where('planner_id',$id)->get();

    	if(count($files) > 0 ){
    		$planners->files = $files;
    	}    	

    	$comments = PlannerThread::where('planner_id',$id)->orderBy('created_at','DESC')->get();

    	if(!empty($comments)){

    		foreach ($comments as $key => $value) {
    			
    			$comments[$key]->files = PlannerThreadFile::where('planner_thread_id',$value->id)->get();
    		}

    		$planners->comments = $comments;
    	}
    	return $planners;
    }
}
