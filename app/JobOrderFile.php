<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobOrderFile extends Model
{
    protected $table = 'job_order_files';

    protected $fillable = [

    	'job_order_id',
    	'timetable_id',
    	'task_id',
    	'project_id',
    	'contact_id',
		'filename',
		'encrpytname',
		'download',
		'uploaded_by'
    ];

    public static function byJobOrder($job_id){

        return self::where('job_order_id',$job_id)->get();
    }

    public static function ByEncryptName($encrpytname){

        return self::where('encrpytname',$encrpytname)->first();
    }
}
