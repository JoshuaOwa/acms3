<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectTimetable extends Model
{
    protected $table = 'project_timetables';

    protected $fillable = [

    	'contact_id',
    	'project_id',
    	'timetable_id',
        'handle_by',
        'encryptname'        
    ];

    public function timetable(){

        return $this->hasOne('App\Timetable','id','timetable_id');
    }

    public function handle(){

        return $this->hasOne('App\User','id','handle_by');
    }    

    public static function byProject($id){

    	$projects =  self::where('project_id',$id)->get();
    	$timetable_id = [];
    	
    	foreach ($projects as $key => $value) {
    		$timetable_id[] = $value->timetable_id;
    	}

    	return $timetable_id;
    }

    public static function getListByProject($proj_id){

        $timetable =  self::where('project_id',$proj_id)->get();

        foreach ($timetable as $key => $value) {
            
            $job = JobOrder::where('project_id',$proj_id)->where('timetable_id',$value->timetable_id)->count();
            $sub = TimetableTask::getCount($value->timetable_id);

            $timetable[$key]->jobcount = $job;
            $timetable[$key]->subcount = $sub;
        }

        return $timetable;
    }

    public static function savePayload($payload){

        $time = static::query()->create($payload);

        return $time;
    }

    public static function updatePayload($payload,$time_id){

        $proj_time = self::where('project_id',$payload['project_id'])->where('timetable_id',$time_id)->first();
        $proj_time->fill($payload)->save();

        return $proj_time;
    }

    public static function deleteRecord($proj_id){

        
        self::where('project_id',$proj_id)->delete();
    }

    public static function byEncryptWithDetails($encryptname){

        $proj_time = self::where('encryptname',$encryptname)->first();

        $times = ProjectTask::byTimetable($proj_time->timetable_id);

        foreach ($times as $key => $value) {
            
            $jobcount = JobOrder::where('contact_id',$proj_time->contact_id)
                            ->where('project_id',$proj_time->project_id)
                            ->where('timetable_id',$proj_time->timetable_id)
                            ->where('task_id',$value->task_id)
                            ->count();
            $ongoing = JobOrder::where('contact_id',$proj_time->contact_id)
                            ->where('project_id',$proj_time->project_id)
                            ->where('timetable_id',$proj_time->timetable_id)
                            ->where('task_id',$value->task_id)
                            ->where('status',1)
                            ->count();
            $returned = JobOrder::where('contact_id',$proj_time->contact_id)
                            ->where('project_id',$proj_time->project_id)
                            ->where('timetable_id',$proj_time->timetable_id)
                            ->where('task_id',$value->task_id)
                            ->where('status',2)
                            ->count();
            $closed = JobOrder::where('contact_id',$proj_time->contact_id)
                            ->where('project_id',$proj_time->project_id)
                            ->where('timetable_id',$proj_time->timetable_id)
                            ->where('task_id',$value->task_id)
                            ->where('status',3)
                            ->count();

            $times[$key]->jobcount = $jobcount;
            $times[$key]->ongoing = $ongoing;
            $times[$key]->returned = $returned;
            $times[$key]->closed = $closed;
            $times[$key]->task = $value->task->task;

        }

        return $times;
    }

    public static function byEncrypt($encryptname){

        return self::where('encryptname',$encryptname)->first();
    }
}
