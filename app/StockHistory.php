<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StockHistory extends Model
{
    protected $table = 'stock_histories';

    protected $fillable = [

    	'item_id',    	
    	'logs'
    ];

    public static function savePayload($payload){

        $item = static::query()->create($payload);

        return $item->id;
    }

    public static function byItem($item){    	

        return self::where('item_id',$item->id)->get();
    }
}
