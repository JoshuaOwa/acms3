<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReminderUpdate extends Model
{
    protected $table = 'reminder_updates';

    protected $fillable = [

    	'info',
    	'link',
    	'last_update',
    	'cnt_need',
    	'cnt_donw',
    	'type',
        'user_id',
        'created_by'
    ];

    public static function daily($uid){

    	$sends =  self::where('type',1)->where('user_id',$uid)->where('cnt_need','>',0)->orderBy('updated_at','DESC')->orderBy('last_update','DESC')->get();
        foreach ($sends as $key => $value) {
            
            $req = UserRequest::where('request_no',$value->info)->first();
            if(empty($req)){
                $det = explode('-', $value->info);
                $req = JobOrder::where('id',$det[1])->first();
                $sends[$key]->type = 1;
            }else{
                $sends[$key]->type = 0;
            }
            if(!empty($req)){
                if($req->status == 1){
                    $stats = "PENDING";
                }elseif($req->status == 2){
                    $stats = "RETURNED";
                }else{
                    $stats = "CLOSED";
                }            
                $sends[$key]->status = $stats;           
            }
             
        }

        return $sends;
    }

    public static function weekly($uid){

    	return self::where('type',2)->where('user_id',$uid)->where('cnt_need','>',0)->orderBy('last_update','DESC')->get();
    }

    public static function monthly($uid){

    	return self::where('type',3)->where('user_id',$uid)->where('cnt_need','>',0)->orderBy('last_update','DESC')->get();
    }

    public static function savePayload($payload){

        $reminder = static::query()->create($payload);

        return $reminder->id;
    }

    public static function updatePayload($payload,$id){

        $reminder = JobOrder::findOrFail($id);
        $reminder->fill($payload)->save();

        return $reminder->id;
    }

    public static function byUserRequest($uid, $reqNO){

        return self::where('user_id',$uid)->where('info',$reqNO)->first();
    }

    public static function remove($reqID){

        $req = UserRequest::where('id',$reqID)->first();

        self::where('info',$req->request_no)->delete();
    }

    public static function removeJob($jobId,$recId){

        $info = "JO-".$jobId;

        self::where('user_id',$recId)->where('info',$info)->delete();

        return 1;
    }
}
