<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactPerson extends Model
{
    protected $table = 'contact_people';

    protected $fillable = [

    	'contact_id',
    	'contact_person',
    	'function',
    	'email',
    	'tel_no',
    	'fax_number',
    	'mobile_number'    	

    ];

    public static function savePayload($payload){

        $contact = static::query()->create($payload);

        return $contact->id;
    }

    public static function updatePayload($payload,$id){

        $contact = ContactPerson::where('contact_id',$id)->first();
        $contact->fill($payload)->save();

        return $contact->id;
    }
}
