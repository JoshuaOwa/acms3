<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $table = 'projects';

    protected $fillable = [

    	'created_by',    	
    	'contact_id',            	
		'project_name',
		'project_details', 
		'project_address',       
		'date_reported',        
		'start_date',
		'end_date',                
		'status',
		'approver_id',
		'project_cost',
		'allowed_ot',
		'encryptname',
        'tag',
        'branch',
        'so_number'
    ];

    public function user(){

        return $this->hasOne('App\User','id','created_by');
    }
    public function contact(){

    	return $this->hasOne('App\Contact','id','contact_id');
    }

    public function approver(){

        return $this->hasOne('App\User','id','approver_id');
    }

    public static function createdByMe($uid){

    	$projects =  self::where('created_by',$uid)->get();

        foreach ($projects as $key => $value) {
            
            $projects[$key]->person = ProjectContact::where('contact_id',$value->contact_id)->where('project_id',$value->id)->get();
        }

        return $projects;
    }

    public static function savePayload($payload){

        $project = static::query()->create($payload);

        return $project;
    }

    public static function byEncrypt($encrypt){

        return self::where('encryptname',$encrypt)->first();
    }
    
    public static function byEncrypt2($encrypt){

        $project = self::where('encryptname',$encrypt)->first();

        $project['person'] = ProjectContact::where('contact_id',$project->contact_id)->where('project_id',$project->id)->get();

        $project['expense'] = Expense::where('project_id',$project->id)->where('status',2)->sum('total_amount');

        return $project;
    }

    public static function updatePayload($payload,$id){

        $project = Project::findOrFail($id);
        $project->fill($payload)->save();

        return $project;
    }

    public static function forApproval(){

        $projects = self::where('status','<',1)->orWhere('status',4)->get();

        foreach ($projects as $key => $value) {
            
            $projects[$key]->person = ProjectContact::where('contact_id',$value->contact_id)->where('project_id',$value->id)->get();
        }

        return $projects;
    }

    public static function byContact($id){

        $projects =  self::where('contact_id',$id)->where('status','!=',0)->where('status','!=',3)->get();

        foreach ($projects as $key => $value) {            

            // $projects[$key]->jo_count = Project::where('id',$value->id)->count();
            $projects[$key]->req_count = UserRequest::where('project_id',$value->id)->count();
            $projects[$key]->jo_count = JobOrder::where('project_id',$value->id)->count();
        }

        return $projects;
    }

    public static function byContactApi($id){

        return self::where('contact_id',$id)->where('status','!=',0)->where('status','!=',3)->pluck('project_name','id');        
    }

    public static function filterBy($status){

        if($status < 1){

            return self::where('status',0)->orWhere('status',4)->get();
        }else{

            return self::where('status',$status)->get();
        }        
        // return self::where('created_by',$id)->where('status',$status)->get();
    }

    public static function activeOnly(){        

        $projects = self::where('status','!=',0)->where('status','!=',3)->get();

        foreach ($projects as $key => $value) {
            
            $projects[$key]->jo_pending = JobOrder::where('status',1)->where('project_id',$value->id)->count();
            $projects[$key]->jo_returned = JobOrder::where('status',2)->where('project_id',$value->id)->count();
            $projects[$key]->jo_closed = JobOrder::where('status',3)->where('project_id',$value->id)->count();
        }

        return $projects;
    }

    public static function getList(){

        return self::where('status','!=',0)->where('status','!=',3)->pluck('project_name','id');
    }
}
