<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScheduleJobOrder extends Model
{
    protected $table = 'schedule_job_orders';

    protected $fillable = [

    	'contact_id',
    	'contact_person',
    	'project_id',
    	'project_name',    	
    	'created_by',
    	'assigned_to',
    	'assigned_name',
    	'toworkgroup',    	
    	'date',
    	'details',
    	'status',
    	'main_jo_id',    
        'timetable_id',
        'task_id'
    ];

    public static function savePayload($payload){

        $schedule = static::query()->create($payload);

        return $schedule->id;
    }

    public static function updatePayload($payload,$id){

        $schedule = ScheduleJobOrder::findOrFail($id);
        $schedule->fill($payload)->save();

        return $schedule->id;
    }

    public static function byRequest($id){

        return self::where('main_request_id',$id)->get();
    }

    public static function byDate($today){

        return self::where('date',$today)->get();
    }
}
