<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BillOfMaterial extends Model
{
    protected $table = 'bill_of_materials';

    protected $fillable = [

    	'contact_id',
    	'project_id',
    	'category',    	
    	'item',
    	'description',
        'rem_qty',
    	'temp_qty',
        'qty',
        'unit',
        'specs',
        'uploaded_by'        
    ];

    public static function savePayload($payload){

        $bom = static::query()->create($payload);

        return $bom->id;
    }

    public static function updatePayload($payload,$id){

        $bom = BillOfMaterial::findOrFail($id);
        
        $oldqty = $bom->qty;
        $oldrem = $bom->rem_qty;
        $oldtemp = $bom->temp_qty;

        $newqty = $payload['qty'];

        if($newqty > $oldqty){

            $excess = $newqty - $oldqty;

            $payload['rem_qty'] = $oldrem+$excess;

        }elseif($newqty < $oldqty){

            $excess = $oldqty - $newqty;
            $checkRem = $oldrem - $excess;

            if($checkRem >= 0){

                $payload['rem_qty'] = $checkRem;
            }else{

                $stat = 2;
                return $stat;
            }
        }

        $bom->fill($payload)->save();
        $stat = 1;
        return $stat;
    }

    public static function byProject($proj_id){

        return self::where('project_id',$proj_id)->get();
    }

    public static function checkInProject($job,$itemID){

        $item = WarehouseInventory::findOrFail($itemID);

        $bom =  self::where('project_id',$job->project_id)->where('item',$item->item)->where('description',$item->description)->first();

        if(!empty($bom)){
            
            if($bom->rem_qty <= 0){

                return $bom = [];
            }else{

                return $bom;
            }
        }else{

            $bom = [];
        }    
    }

    public static function deduction($bom,$for_release){

        $bom->rem_qty = $bom->rem_qty - $for_release;
        $bom->update();

        return 1;
    }
}
