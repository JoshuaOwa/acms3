<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobOrderLog extends Model
{
    protected $table = 'job_order_logs';

    protected $fillable = [

    	'contact_id',
    	'project_id',
    	'timetable_id',
    	'task_id',
    	'job_order_id',
    	'user_id',
    	'logs'
    ];

    public function user(){

    	return $this->hasOne('App\User','id','user_id');
    }

    public static function savePayload($payload){

        $log = static::query()->create($payload);

        return $log->id;
    }

    public static function byJobOrder($job_id){

        return self::where('job_order_id',$job_id)->orderBy('created_at','DESC')->get();
    }
}
